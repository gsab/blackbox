#include <VibrationSample.h>


VibrationSample::VibrationSample()
{
}

VibrationSample::VibrationSample(double xAxle, double yAxle, double zAxle)
{
	this->xAxle = xAxle;
	this->yAxle = yAxle;
	this->zAxle = zAxle;
}

String VibrationSample::Serialize()
{
	
	return String(xAxle) +";" + String(yAxle) + ";" + String(zAxle);
}

