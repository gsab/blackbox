/*
 Name:		Library.h
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <stdlib.h>
#include <WString.h>
#include <Arduino.h>
#include <ArduinoJson.h>

class AccDataTrigger {

public:
	AccDataTrigger();
	AccDataTrigger(int inputID);
	AccDataTrigger(int inputID, double threshold, int onThresholdCount, int offThresholdCount, int multiplier, bool relativeLastValue, bool absolute, bool xAxle, bool yAxle, bool zAxle);
	int inputID;
	double threshold = 0;
	int onThresholdCount = 0;
	int offThresholdCount = 0;
	int multiplier = 0;
	bool currentState = false;
	bool relativeLastValue = false;
	bool absolute = false;
	bool addReading(double xAxleValue, double yAxleValue, double zAxleValue);
	bool xAxle = false;
	bool yAxle = false;
	bool zAxle = false;
	double lastValue = 0;
	String serialize();
private:
	void initialize(int inputID, double threshold, int onThresholdCount, int offThresholdCount, int multiplier, bool relativeLastValue, bool absolute, bool xAxle, bool yAxle, bool zAxle);
	int currentStateCount = 0;
	double getAxleValue(double value, bool addValue);
};
