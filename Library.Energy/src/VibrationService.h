/*
 Name:		Library.h
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <RTCZero.h>
#include <MemoryService.h>
#include <MQTTService.h>
#include <DebugService.h>
#include <AccDataTrigger.h>
#include <WString.h>
#include <ArduinoJson.h>
#include <VibrationSample.h>
#include <ArduinoQueue.h>
#include <SerialFlash.h>

class VibrationService {

public:
	void initialize(RTCZero* rtcTimer, int inputNumberOffset, MemoryService* memoryService, MQTTService* mqttService, ConnectionService* connectionService);
	void addRecording(double xAxleValue, double yAxleValue, double zAxleValue);
	void loop();
	void createEvent(bool flank, int inputID);
	void handleMessage(char* topic, byte* payload, unsigned int length);
	void handleLearnMessage(ArduinoJson::StaticJsonDocument<256U>& doc);
	void getAccTriggerSetting(byte* payload, unsigned int length);
private:
	AccDataTrigger accDataTriggers[4];
	RTCZero* rtcTimer;
	MemoryService* memoryService;
	MQTTService* mqttService;
	ConnectionService* connectionService;
	void doRecording(int samples);
	bool recordingActive = false;
	int samplesToGo;
	SerialFlashFile* currentSampleFile;
	bool useBuffer1 = true;
	int buffer1Amount = 0;
	int buffer2Amount = 0;
	double xAxleBuffer1[300];
	double xAxleBuffer2[300];
	double yAxleBuffer1[300];
	double yAxleBuffer2[300];
	double zAxleBuffer1[300];
	double zAxleBuffer2[300];
	void setInputSettings(AccDataTrigger dataTriggerSettings);
	void saveInputSettings(AccDataTrigger dataTriggerSettings);
	void sendVibrationBuffer(double* xAxle, double* yAxle, double* zAxle, int amount);
	void loadInputSettings();
	String fileName = "AccDataInput_";
	String fileType = ".json";
	int inputNumberOffset = 0;
	int currentPackageID = 0;
};