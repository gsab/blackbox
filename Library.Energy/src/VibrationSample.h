/*
 Name:		Library.h
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>

class VibrationSample {

public:
	VibrationSample();
	VibrationSample(double xAxle, double yAxle, double zAxle);
	double xAxle = 0;
	double yAxle = 0;
	double zAxle = 0;
	String Serialize();
private:
	
};