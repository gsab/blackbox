#include "VibrationService.h"


void VibrationService::initialize(RTCZero* rtcTimer, int inputNumberOffset, MemoryService* memoryService, MQTTService* mqttService, ConnectionService* connectionService)
{
	this->rtcTimer = rtcTimer;
	this->memoryService = memoryService;
	this->mqttService = mqttService;
	this->connectionService = connectionService;
	this->inputNumberOffset = inputNumberOffset;

	this->loadInputSettings();

}

void VibrationService::addRecording(double xAxleValue, double yAxleValue, double zAxleValue)
{
	for (int i = 0; i < 4;i++)
	{
		
		if (this->accDataTriggers[i].addReading(xAxleValue, yAxleValue, zAxleValue))
		{
			createEvent(this->accDataTriggers[i].currentState, this->accDataTriggers[i].inputID);
		}
	}

	if (recordingActive && samplesToGo > 0)
	{
		if(useBuffer1)
		{
			if (buffer1Amount < 300) {
				xAxleBuffer1[buffer1Amount] = xAxleValue * 100;
				yAxleBuffer1[buffer1Amount] = yAxleValue * 100;
				zAxleBuffer1[buffer1Amount] = zAxleValue * 100;
				buffer1Amount++;
			}
			else
			{
				DebugService::Warning("VibrationService::addRecording", "Vibration buffer full!");
			}
		}
		else
		{
			if (buffer2Amount < 300)
			{
				xAxleBuffer2[buffer2Amount] = xAxleValue * 100;
				yAxleBuffer2[buffer2Amount] = yAxleValue * 100;
				zAxleBuffer2[buffer2Amount] = zAxleValue * 100;
				buffer2Amount++;
			}
			else
			{
				DebugService::Warning("VibrationService::addRecording", "Vibration buffer full!");
			}
		}

		this->samplesToGo--;
	}
}

void VibrationService::sendVibrationBuffer(double* xAxle, double* yAxle, double* zAxle, int amount)
{
	DebugService::Information("VibrationService::loop", "Saving " + String(amount) + " samples");

	String messageData = "";
	bool firstLine = true;
	for (int i = 0; i < amount;i++)
	{
		if (!firstLine)
		{
			messageData += "|";
		}
		messageData += String(xAxle[i]) + ";" + String(yAxle[i]) + ";" + String(zAxle[i]);
		firstLine = false;

		if ((i != 0 && i % 10 == 0) || i == amount - 1)
		{
			this->mqttService->sendAccData(("{\"packageId\":" + String(currentPackageID) + " ,\"values\": \"" + messageData + "\"}").c_str());
			currentPackageID++;
			messageData = "";
			firstLine = true;
		}
	}
}

void VibrationService::loop()
{
	while (this->recordingActive && this->mqttService->connect())
	{
		if (useBuffer1 && buffer1Amount > 0)
		{
			useBuffer1 = false;
			sendVibrationBuffer(xAxleBuffer1, yAxleBuffer1, zAxleBuffer1, buffer1Amount);
			buffer1Amount = 0;
		}
		else if (!useBuffer1 && buffer2Amount > 0)
		{
			useBuffer1 = true;
			sendVibrationBuffer(xAxleBuffer2, yAxleBuffer2, zAxleBuffer2, buffer2Amount);
			buffer2Amount = 0;
		}
		delay(300);
		if (this->recordingActive && samplesToGo == 0 && buffer1Amount == 0 && buffer2Amount == 0)
		{
			this->recordingActive = false;
			DebugService::Information("VibrationService::loop", "Recording done.");
		}
	}
}

void VibrationService::createEvent(bool flank, int inputID)
{
	InputEvent newEvent = InputEvent();
	newEvent.inputID = inputID;
	newEvent.flank = flank;
	newEvent.rtcEpoch = this->rtcTimer->getEpoch();
	this->mqttService->addEvent(newEvent);
	DebugService::Information("VibrationService::createEvent", String(flank) + " event detected on input " + String(inputID));
}

void VibrationService::handleMessage(char* topic, byte* payload, unsigned int length)
{
	DebugService::Information("VibrationService::handleMessage","callback topic " + String(topic) + " payload " + String(*payload));
	if (String(topic).endsWith("learn"))
	{
		StaticJsonDocument<256> doc;
		deserializeJson(doc, payload, length);
		if (doc.containsKey("samples"))
		{
			handleLearnMessage(doc);
		}
	}
	if (String(topic).endsWith("accDataTrigger"))
	{
		getAccTriggerSetting(payload, length);
	}
	if (String(topic).endsWith("getcurrentsettings"))
	{
		//sendAccTriggerSetting(payload, length);
		for (int i = 0; i < 4;i++)
		{
			mqttService->sendAccDataSetting(accDataTriggers[i].serialize().c_str());
		}
	}
}

void VibrationService::handleLearnMessage(ArduinoJson::StaticJsonDocument<256U>& doc)
{
	DebugService::Information("VibrationService::handleMessage", "Learn message deserialized");
	int samplesRequested = doc["samples"];
	this->doRecording(samplesRequested);
}

void VibrationService::getAccTriggerSetting(byte* payload, unsigned int length)
{
	StaticJsonDocument<256> doc;
	deserializeJson(doc, payload, length);
	if (doc.containsKey("inputNumber"))
	{
		DebugService::Information("VibrationService::handleMessage", "AccDataTrigger message deserialized");
		int inputID = doc["inputNumber"];

		int offsetInputID = inputID - inputNumberOffset;

		if (offsetInputID >= 0 && offsetInputID < 4)
		{
			DebugService::Information("VibrationService::getAccTriggerSetting", "Getting settings data");
			accDataTriggers[offsetInputID].threshold = doc["threshold"];
			accDataTriggers[offsetInputID].onThresholdCount = doc["onThresholdCount"];
			accDataTriggers[offsetInputID].offThresholdCount = doc["offThresholdCount"];
			accDataTriggers[offsetInputID].multiplier = doc["multiplier"];
			accDataTriggers[offsetInputID].absolute = doc["absolute"];
			accDataTriggers[offsetInputID].relativeLastValue = doc["relativeLastValue"];
			accDataTriggers[offsetInputID].xAxle = doc["xAxle"];
			accDataTriggers[offsetInputID].yAxle = doc["yAxle"];
			accDataTriggers[offsetInputID].zAxle = doc["zAxle"];

			VibrationService::saveInputSettings(accDataTriggers[offsetInputID]);
			DebugService::Information("VibrationService::getAccTriggerSetting", "Settings data saved");
		}
	}
}

void VibrationService::doRecording(int samples)
{
	DebugService::Information("VibrationService::doRecording", "Start recording of "+ String(samples) + " samples");

	this->samplesToGo = samples;

	this->currentPackageID = 1;

	this->recordingActive = true;
}

void VibrationService::setInputSettings(AccDataTrigger dataTriggerSettings)
{
}

void VibrationService::saveInputSettings(AccDataTrigger dataTrigger)
{
	DebugService::Information("VibrationService::saveInputSettings", "Saving AccDataTrigger " + String(dataTrigger.inputID));
	

	String currentFileName = this->fileName + String(dataTrigger.inputID) + this->fileType;

	this->memoryService->saveFile(currentFileName, dataTrigger.serialize());
}

void VibrationService::loadInputSettings()
{
	for (int i = 0; i < 4;i++)
	{
		String currentFileName = this->fileName + String(i + (this->inputNumberOffset)) + this->fileType;

		String fileContent = this->memoryService->loadFile(currentFileName);

		DebugService::Information("VibrationService::loadInputSettings", "Loaded file content for input + " + String(i + (this->inputNumberOffset)) + " content: " + fileContent);

		if (fileContent == "")
		{
			//No setting file found, create trigger with standard values
			DebugService::Information("VibrationService::loadInputSettings", "No file found, initializing with standard parameters");
			this->accDataTriggers[i] = AccDataTrigger(i + (this->inputNumberOffset));
		}
		else
		{
			const int size = 1024;
			StaticJsonDocument<size> doc;
			deserializeJson(doc, fileContent.c_str(), size);
			if (doc.containsKey("threshold"))
			{
				AccDataTrigger trigger = AccDataTrigger();
				trigger.inputID = i + this->inputNumberOffset;
				trigger.threshold = doc["threshold"];
				trigger.onThresholdCount = doc["onThresholdCount"];
				trigger.offThresholdCount = doc["offThresholdCount"];
				trigger.multiplier = doc["multiplier"];
				trigger.absolute = doc["absolute"];
				trigger.relativeLastValue = doc["relativeLastValue"];
				trigger.xAxle = doc["xAxle"];
				trigger.yAxle = doc["yAxle"];
				trigger.zAxle = doc["zAxle"];
				this->accDataTriggers[i] = trigger;
			}
			else {
				//No setting file found or corrupt file, create trigger with standard values
				DebugService::Information("VibrationService::loadInputSettings", "File could not be deserialized");
				this->accDataTriggers[i] = AccDataTrigger(i + this->inputNumberOffset);
			}
		}
	}
}
