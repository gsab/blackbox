/*
 Name:		Library.cpp
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#include "AccDataTrigger.h"



AccDataTrigger::AccDataTrigger()
{
}

AccDataTrigger::AccDataTrigger(int inputID)
{
	this->initialize(inputID, 0.03, 10, 20, 10, true, true, true, true, true);
}

AccDataTrigger::AccDataTrigger(int inputID, double threshold, int onThresholdCount, int offThresholdCount, int multiplier, bool relativeLastValue, bool absolute, bool xAxle, bool yAxle, bool zAxle)
{
	this->initialize(inputID, threshold, onThresholdCount, offThresholdCount, multiplier, relativeLastValue, absolute, xAxle, yAxle, zAxle);
}

String AccDataTrigger::serialize()
{
	DynamicJsonDocument doc(256);
	doc["inputNumber"] = this->inputID;
	doc["threshold"] = this->threshold;
	doc["onThresholdCount"] = this->onThresholdCount;
	doc["offThresholdCount"] = this->offThresholdCount;
	doc["multiplier"] = this->multiplier;
	doc["absolute"] = this->absolute;
	doc["relativeLastValue"] = this->relativeLastValue;
	doc["xAxle"] = this->xAxle;
	doc["yAxle"] = this->yAxle;
	doc["zAxle"] = this->zAxle;
	char buffer[256];
	serializeJson(doc, buffer);
	return String(buffer);
}

void AccDataTrigger::initialize(int inputID, double threshold, int onThresholdCount, int offThresholdCount, int multiplier, bool relativeLastValue, bool absolute, bool xAxle, bool yAxle, bool zAxle)
{
	this->inputID = inputID;
	this->threshold = threshold;
	this->onThresholdCount = onThresholdCount;
	this->offThresholdCount = offThresholdCount;
	this->relativeLastValue = relativeLastValue;
	this->multiplier = multiplier;
	this->absolute = absolute;
	this->xAxle = xAxle;
	this->yAxle = yAxle;
	this->zAxle = zAxle;
	this->currentStateCount = onThresholdCount;
}

double AccDataTrigger::getAxleValue(double value, bool addValue)
{
	if (addValue)
	{
		if (this->absolute)
		{
			return abs(value);
		}
		else
		{
			return value;
		}
	}
}

bool AccDataTrigger::addReading(double xAxleValue, double yAxleValue, double zAxleValue)
{
	double currentSummaryValue = 0;

	if(this->absolute)
	{
		currentSummaryValue += abs(getAxleValue(xAxleValue, xAxle));
		currentSummaryValue += abs(getAxleValue(yAxleValue, yAxle));
		currentSummaryValue += abs(getAxleValue(zAxleValue, zAxle));
	}
	else
	{
		currentSummaryValue += getAxleValue(xAxleValue, xAxle);
		currentSummaryValue += getAxleValue(yAxleValue, yAxle);
		currentSummaryValue += getAxleValue(zAxleValue, zAxle);
	}
	currentSummaryValue = currentSummaryValue * this->multiplier;

	double difference = currentSummaryValue;
	if (this->relativeLastValue)
	{
		difference = currentSummaryValue - this->lastValue;
	}

	if (this->absolute)
	{
		difference = abs(difference);
	}

	bool newState = false;
	if (this->threshold > 0)
	{
		newState = difference > this->threshold;
	}
	else
	{
		newState = difference < this->threshold;
	}

	this->lastValue = currentSummaryValue;

	if (newState != this->currentState)
	{
		this->currentStateCount -= 1;
		if (this->currentStateCount <= 0)
		{
			this->currentState = newState;

			if (this->currentState)
			{
				this->currentStateCount = this->offThresholdCount;
			}
			else
			{
				this->currentStateCount = this->onThresholdCount;
			}
			return true;
		}
	}
	else
	{
		if (this->currentState)
		{
			this->currentStateCount = this->offThresholdCount;
		}
		else
		{
			this->currentStateCount = this->onThresholdCount;
		}
	}

	return false; // State did not change
}