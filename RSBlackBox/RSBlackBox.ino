#include <SPI.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <ArduinoUniqueID.h>
#include <WiFiNINA.h>
#include <NTPClient.h>
#include <EthernetENC.h>
#include <PubSubClient.h>
#include <Arduino_LSM6DS3.h>
#include <RTCLib.h>
#include <SerialFlash.h>
#include <Adafruit_ZeroTimer.h>

#define TINY_GSM_MODEM_SIM800

#include <TinyGsmClient.h>

#include <ArduinoQueue.h>

#include <DebugService.h>
#include <WString.h>
#include <AccDataTrigger.h>
#include <StringUtils.h>
#include <InputEvent.h>
#include <MemoryService.h>
#include <MQTTService.h>
#include <ConnectionService.h>
#include <VibrationService.h>
#include <WebServerService.h>
#include <VibrationSample.h>
#include <ConfigurationService.h>
#include <WDTZero.h>

WDTZero MyWatchDoggy; // Define WDT  

#define LOCAL_DEBUG       true  //false

#define SerialAT Serial1
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 9600

#include <RTCZero.h>  // include the library for realtime clock
RTCZero rtc;           // realtime clock instance
bool timeIsSet = false; // determines if time has been set since restart. This needs to be done to be able to send any events

// Defining global variables

bool D0_LAST_STATUS = false;
bool D1_LAST_STATUS = false;
bool D2_LAST_STATUS = false;
bool D3_LAST_STATUS = false;
bool D4_LAST_STATUS = false;
bool D5_LAST_STATUS = false;

bool X_LAST_STATUS = false;
bool Y_LAST_STATUS = false;
bool Z_LAST_STATUS = false;

const int8_t ERROR_LED = 9;
const int8_t WIFI_LED = 8;

const int8_t D0_INPUT = 2;
const int8_t D1_INPUT = 3;
const int8_t D2_INPUT = 4;
const int8_t D3_INPUT = 5;
const int8_t D4_INPUT = 6;
const int8_t D5_INPUT = 7;
const int8_t X_INPUT = 8;
const int8_t Y_INPUT = 9;
const int8_t Z_INPUT = 10;

const int8_t LED_OFF = 0;
const int8_t LED_ON = 1;
const int8_t LED_SLOWBLINK = 2;
const int8_t LED_FASTBLINK = 3;

int8_t ERROR_LED_STATUS = 0;
int8_t WIFI_LED_STATUS = 0;

int cpuCycleCounter = 0;
int loopCounter = 0;

bool enableAccDataOutput = false;

bool recordSamples = false;

bool setupStageActive = true;

int samplesRequested = 0;

int samplesRecorded = 0;

float* xSampleArray;
float* ySampleArray;
float* zSampleArray;

long firstStartupDate = 0;

Adafruit_ZeroTimer zt3 = Adafruit_ZeroTimer(3);
Adafruit_ZeroTimer zt4 = Adafruit_ZeroTimer(4);

ConnectionService connectionService = ConnectionService();
MemoryService memoryService = MemoryService();
MQTTService mqttService = MQTTService();
VibrationService vibrationService = VibrationService();
WebServerService webServerService = WebServerService();

void setup()
{
  // Debug console
  
  
  Serial.begin(115200);
  ERROR_LED_STATUS = LED_FASTBLINK;
  WIFI_LED_STATUS = LED_FASTBLINK;

  MyWatchDoggy.attachShutdown(myshutdown);
  MyWatchDoggy.setup(WDT_SOFTCYCLE8M);

  configurePins(); //Configures the pins on the arduino cpu

  startRTC(); //Real time clock chip start

  
  DebugService::Information("Setup", "Initializing memory");
  memoryService.initialize(A2);
  DebugService::Information("Setup", "Initializing done");

  DebugService::Information("Setup", "Load configuration");
  memoryService.loadConfiguration();
  DebugService::Information("Setup", "Load done");

  DebugService::Information("Setup", "Initializing vibration service");
  vibrationService.initialize(&rtc,6, &memoryService, &mqttService, &connectionService);
  DebugService::Information("Setup", "Initializing done");

  startIMU(); //Accelerometer chip start

  activateInterrupts(); //Input reader, accelerometer reader interrupts

  DebugService::Information("Setup", "Initializing connectionservice");
  connectionService.initialize();
  DebugService::Information("Setup", "Initializing done");

  DebugService::Information("Setup", "Initializing mqttservice");
  mqttService.setCallback(mqttMessageHandler);
  mqttService.initialize(&connectionService, &memoryService);
  DebugService::Information("Setup", "Initializing done");

  DebugService::Information("Setup", "Initializing webserverservice");
  webServerService.initialize(&connectionService, &memoryService);
  DebugService::Information("Setup", "Initializing done");

  ERROR_LED_STATUS = LED_OFF;
  WIFI_LED_STATUS = LED_OFF;
  setupStageActive = false;
}

void myshutdown()
{

	Serial.print("\nWe gonna shut down ! ...");

}

void startIMU()
{
  IMU.begin();
  calibrate();
}

void startRTC()
{
  rtc.begin();

  //Todo check if rtc is synced. If it is then set timeIsSet = true;
}

void configurePins()
{

  pinMode(ERROR_LED, OUTPUT);
  pinMode(WIFI_LED, OUTPUT);

  pinMode(D0_INPUT, INPUT_PULLUP);
  pinMode(D1_INPUT, INPUT_PULLUP);
  pinMode(D2_INPUT, INPUT_PULLUP);
  pinMode(D3_INPUT, INPUT_PULLUP);
  pinMode(D4_INPUT, INPUT_PULLUP);
  pinMode(D5_INPUT, INPUT_PULLUP);
}

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

void activateInterrupts()
{
  zt3.configure(TC_CLOCK_PRESCALER_DIV64, // prescaler
                TC_COUNTER_SIZE_16BIT,   // bit width of timer/counter
                TC_WAVE_GENERATION_MATCH_PWM  // frequency || PWM mode
               );

  zt3.setCompare(0, 15000); // 1 match, channel 0
  zt3.setCallback(true, TC_CALLBACK_CC_CHANNEL0, accReadInterrupt_Handler);  // this one sets pin low
  zt3.enable(true);

  zt4.configure(TC_CLOCK_PRESCALER_DIV256, // prescaler
                TC_COUNTER_SIZE_16BIT,   // bit width of timer/counter
                TC_WAVE_GENERATION_MATCH_PWM   // match style
               );

  zt4.setCompare(0, 18250); // 1 match, channel 0
  zt4.setCallback(true, TC_CALLBACK_CC_CHANNEL0, inputReadInterrupt_Handler);  // set DAC in the callback
  zt4.enable(true);
}

int xPosDeviations = 0;
int yPosDeviations = 0;
int zPosDeviations = 0;

int xNegDeviations = 0;
int yNegDeviations = 0;
int zNegDeviations = 0;

void updateCpuCounter()
{
  cpuCycleCounter += 1;

  if (cpuCycleCounter > 100000)
  {
    cpuCycleCounter = 0;
  }
}

void(*resetFunc) (void) = 0; //declare reset function @ address 0


int lastFreeMemory = 0;

void loop()
{
  loopCounter++;

  MyWatchDoggy.clear();  // refresh wdt - before it loops

  int currentFreeMemory = freeMemory();

  if (currentFreeMemory != lastFreeMemory)
  {
	  Serial.println(" Amount of free memory: " + String(currentFreeMemory));
  }

  lastFreeMemory = currentFreeMemory;

  connectionService.loop();

  synchronizeTime();

  mqttService.loop(lastFreeMemory,firstStartupDate);

  webServerService.loop();

  vibrationService.loop();

  

  if (loopCounter == 60)
  {
	  loopCounter = 0;
  }
  if (ConfigurationService::Reset)
  {
	  resetFunc();
  }

  delay(1000);
}



void synchronizeTime()
{
	if (connectionService.connected && (!timeIsSet || loopCounter == 60))
	{
		DebugService::Information("loop", "Synchronize time");
		long epochTime = 0;
		if (connectionService.getEpochTime(&epochTime))
		{
			DebugService::Information("loop", "Time recieved " + String(epochTime));
			long timeDifference = epochTime - rtc.getEpoch();
			DebugService::Information("loop", "Time recieved deviation = " + String(timeDifference));
			rtc.setEpoch(epochTime);
			if (!timeIsSet)
			{
				DebugService::Information("loop", "Adjusting time on existing events");

				mqttService.timeSynchronized(timeDifference);

				firstStartupDate = rtc.getEpoch();

				timeIsSet = true;
			}
		}
	}
}

void mqttMessageHandler(char* topic, byte* payload, unsigned int length)
{
	DebugService::Information("mqttMessageHandler", "Message recieved: " + String(topic) + " payload " + String(*payload));

	vibrationService.handleMessage(topic, payload, length);
	mqttService.handleMessage(topic, payload, length);
	connectionService.handleMessage(topic, payload, length);
}

void TC3_Handler() {
  Adafruit_ZeroTimer::timerHandler(3);
}

void TC4_Handler() {
  Adafruit_ZeroTimer::timerHandler(4);
}

void imu_read(float *ax, float *ay, float *az)
{
  float _ax, _ay, _az;

  IMU.readAcceleration(_ax, _ay, _az);

  *ax = _ax;
  *ay = _ay;
  *az = _az;
}

#define NUM_SAMPLES 30
#define NUM_AXES 3
// sometimes you may get "spikes" in the readings
// set a sensible value to truncate too large values
#define TRUNCATE_AT 20
#define ACCEL_THRESHOLD 0.1
double baseline[NUM_AXES];
double features[NUM_SAMPLES * NUM_AXES];

void calibrate()

{
  float ax, ay, az;

  for (int i = 0; i < 10; i++) {
    imu_read(&ax, &ay, &az);
    delay(100);
  }

  baseline[0] = ax;
  baseline[1] = ay;
  baseline[2] = az;
}

AccDataTrigger allAxles = AccDataTrigger(10, 0.03, 10, 20, 10, true, true, true, true, true);

void accReadInterrupt_Handler()
{
	float ax, ay, az;

	imu_read(&ax, &ay, &az);


	double correctedAx = ax - baseline[0];
	double correctedAz = ay - baseline[1];
	double correctedAy = az - baseline[2];

	vibrationService.addRecording(correctedAx, correctedAy, correctedAz);
}

void inputReadInterrupt_Handler()
{
  //YOUR CODE HERE
  readInputs();
  if(!setupStageActive){
    if (MQTTService::current->eventsInQueueCount() > 0)
    {
      if (MQTTService::current->connected)
      {
        WIFI_LED_STATUS = LED_FASTBLINK;
        ERROR_LED_STATUS = LED_OFF;
      }
      else
      {
        WIFI_LED_STATUS = LED_OFF;
        ERROR_LED_STATUS = LED_FASTBLINK;
      }
    }
    else
    {
      if (MQTTService::current->connected)
      {
        WIFI_LED_STATUS = LED_ON;
        ERROR_LED_STATUS = LED_OFF;
      }
      else
      {
        WIFI_LED_STATUS = LED_OFF;
        ERROR_LED_STATUS = LED_SLOWBLINK;
      }
    }
  }

  //Update && set LEDs
  setLEDOutputs();

  //Update CpuCounter used to schedule functions
  updateCpuCounter();
}

void readInputs()
{
  D0_LAST_STATUS = readInput(D0_INPUT, D0_LAST_STATUS);
  D1_LAST_STATUS = readInput(D1_INPUT, D1_LAST_STATUS);
  D2_LAST_STATUS = readInput(D2_INPUT, D2_LAST_STATUS);
  D3_LAST_STATUS = readInput(D3_INPUT, D3_LAST_STATUS);
  D4_LAST_STATUS = readInput(D4_INPUT, D4_LAST_STATUS);
  D5_LAST_STATUS = readInput(D5_INPUT, D5_LAST_STATUS);
}

bool readInput(int inputID, bool lastStatus)
{
  bool currentStatus = !digitalRead(inputID);

  if (lastStatus != currentStatus)
  {
    createEvent(currentStatus, inputID);
  }
  return currentStatus;
}

void createEvent(bool flank, int inputID)
{
  InputEvent newEvent = InputEvent();
  newEvent.inputID = inputID;
  newEvent.flank = flank;
  newEvent.rtcEpoch = rtc.getEpoch();
  MQTTService::current->addEvent(newEvent);
  DebugService::Information("createEvent", String(flank) + " event detected on input " + String(inputID));
}

void setLEDOutput(int ledID, int ledStatus)
{
	if (ledStatus == LED_OFF && digitalRead(ledID) == HIGH)
	{
		digitalWrite(ledID, LOW);
	}
	else if (ledStatus == LED_ON && digitalRead(ledID) == LOW)
	{
		digitalWrite(ledID, HIGH);
	}
	else if (ledStatus == LED_SLOWBLINK && cpuCycleCounter % 10 == 0)
	{
		if (digitalRead(ledID) == HIGH)
		{
			digitalWrite(ledID, LOW);
		}
		else
		{
			digitalWrite(ledID, HIGH);
		}
	}
	else if (ledStatus == LED_FASTBLINK && cpuCycleCounter % 2 == 0)
	{
		if (digitalRead(ledID) == HIGH)
		{
			digitalWrite(ledID, LOW);
		}
		else
		{
			digitalWrite(ledID, HIGH);
		}
	}
}

void setLEDOutputs()
{
	setLEDOutput(ERROR_LED, ERROR_LED_STATUS);
	setLEDOutput(WIFI_LED, WIFI_LED_STATUS);
}

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}
