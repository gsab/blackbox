/*
 Name:		Library.h
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>
#include <SerialFlash.h>
#include <StringUtils.h>
#include <ConfigurationService.h>

class MemoryService {

public:
	void initialize(int spiPin);
	String loadFile(String fileName);
	bool saveFile(String fileName, String fileData);
	bool removeFile(String fileName);
	SerialFlashFile* createOpenFile(String fileName, int fileSize);
	static void WriteDataToFile(SerialFlashFile file, String fileData);
	void loadConfiguration();
	void saveConfiguration();
};