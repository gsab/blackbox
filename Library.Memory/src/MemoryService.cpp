/*
 Name:		Library.cpp
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "MemoryService.h"

void MemoryService::initialize(int spiPin)
{
	if (SerialFlash.begin(spiPin))
	{
		Serial.println("Memory init succesful");
	}
	else
	{
		Serial.println("Memory init fail");
	}
}

String MemoryService::loadFile(String fileName)
{

	SerialFlashFile file = SerialFlash.open(fileName.c_str());

	if (file == NULL)
	{
		return "";
	}
	Serial.println("File loaded");
	char buffer[256];
	
	String resultString = "";
	int position = 0;
	int length = file.size();
	Serial.println("File size = " + String(length));
	while (position < length)
	{
		file.read(buffer, sizeof(buffer));
		resultString += String(buffer);
		position += 256;
	}
	file.close();
	return resultString;
}

bool MemoryService::saveFile(String fileName, String fileData)
{
	Serial.println("Creating file " + String(fileData.length()));

	const char* fileNameChar = fileName.c_str();

	//Remove file if it exists
	SerialFlash.remove(fileNameChar);

	if (SerialFlash.create(fileNameChar, fileData.length()))
	{
		Serial.println("File created");
		SerialFlashFile file = SerialFlash.open(fileNameChar);

		if (file != NULL)
		{
			Serial.println("File loaded");
			MemoryService::WriteDataToFile(file, fileData);

			file.close();
			return true;
		}
	}

	return false;
}

bool MemoryService::removeFile(String fileName)
{
	SerialFlash.remove(fileName.c_str());
	return true;
}

void MemoryService::WriteDataToFile(SerialFlashFile file, String fileData)
{
	int position = 0;
	const int bufferSize = 256;
	while (position < fileData.length())
	{
		char buffer[bufferSize];
		int endPosition = position + bufferSize;
		if (endPosition > fileData.length())
		{
			endPosition = fileData.length();
		}
		fileData.substring(position, endPosition).toCharArray(buffer, bufferSize);
		file.write(buffer, bufferSize);
		position += bufferSize;
	}
}

void MemoryService::loadConfiguration()
{
	String data = loadFile("configuration.conf");

	if (data == "")
	{
		return;
	}

	ConfigurationService::LoadConfig(data);
}
void MemoryService::saveConfiguration()
{
	String data = ConfigurationService::SerializeConfig();

	saveFile("configuration.conf", data);
}

SerialFlashFile* MemoryService::createOpenFile(String fileName, int fileSize)
{
	Serial.println("Creating file " + fileName + " size " + String(fileSize));

	const char* fileNameChar = fileName.c_str();

	//Remove file if it exists
	SerialFlash.remove(fileNameChar);

	if (SerialFlash.create(fileNameChar, fileSize))
	{
		Serial.println("File created");
		SerialFlashFile file = SerialFlash.open(fileNameChar);

		if (file != NULL)
		{
			
			return &file;
		}
	}

	return NULL;
}
