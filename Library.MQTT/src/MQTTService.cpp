/*
 Name:		Library.cpp
 Created:	11/20/2020 1:53:43 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "MQTTService.h"

MQTTService* MQTTService::current = NULL;

MQTTService::MQTTService()
{
	MQTTService:current = this;
}

void MQTTService::initialize(ConnectionService* connectionService, MemoryService* memoryService)
{
	this->memoryService = memoryService;
	this->connectionService = connectionService;
}

void MQTTService::loop(long currentFreeMemory, long firstStartupDate)
{
	this->connected = connect();
	if (this->connected)
	{
		mqtt->loop();

		if (publishMQTTCounter == 0)
		{
			//Publish a status update to server

			String message = String("{\"memory\" : " + String(currentFreeMemory) + ",\"startupEpoch\" : " + String(firstStartupDate) + "}");
			if (mqtt->publish(MQTT_Status_Topic.c_str(), message.c_str()))
			{

			}
		}
		if (this->timeIsSyncronized)
		{
			while (eventQueue.itemCount() > 0)
			{
				InputEvent nextEvent = eventQueue.getHead();
				long epochTime = nextEvent.rtcEpoch;

				if (epochTime < 1000000000)
				{
					// Event is collected before timesync. Compensate collected time
					DebugService::Information("publishMQTT", "Compensate time on event");
					DebugService::Information("publishMQTT", String(epochTime));
					epochTime += timeCompensation;
				}

				String message = String("{\"inputID\" : " + String(nextEvent.inputID) + ", \"flank\" : " + String(nextEvent.flank) + ", \"epoch\" : " + String(epochTime) + "}");
				DebugService::Information("publishMQTT", MQTT_Input_Event_Topic);
				DebugService::Information("publishMQTT", message.c_str());
				if (mqtt->publish(MQTT_Input_Event_Topic.c_str(), message.c_str()))
				{
					eventQueue.dequeue();
				}
				else
				{
					DebugService::Information("publishMQTT", "Publish of event failed!");
					break;
				}
			}
		}
		else
		{
			DebugService::Warning("publishMQTT", "Time is not yet synchronized");
		}

		publishMQTTCounter++;
		if (publishMQTTCounter == 30)
		{
			publishMQTTCounter = 0;
		}
	}
}
void MQTTService::addEvent(InputEvent inputEvent)
{
	DebugService::Information("MQTTService::addEvent", "Event added");
	this->eventQueue.enqueue(inputEvent);

	DebugService::Information("MQTTService::addEvent", "Number of events: " + String(this->eventsInQueueCount()));
}

unsigned int MQTTService::eventsInQueueCount()
{
	return this->eventQueue.itemCount();
}

void MQTTService::handleMessage(char* topic, byte* payload, unsigned int length)
{
}

void MQTTService::setCallback(MQTTService_CALLBACK_SIGNATURE)
{
	MQTTService::callbackMethod = callbackMethod;
}

bool MQTTService::sendAccData(const char* payload)
{
	if (mqtt && mqtt->connected())
	{
		if (mqtt->publish(MQTT_AccData_Topic.c_str(), payload))
		{
			payload = NULL;
			return true;
		}
	}
	payload = NULL;
	return false;
}

bool MQTTService::sendAccDataSetting(const char* payload)
{
	if (mqtt && mqtt->connected())
	{
		if (mqtt->publish(MQTT_AccDataSetting_Topic.c_str(), payload))
		{
			payload = NULL;
			return true;
		}
	}
	payload = NULL;
	return false;
}

void MQTTService::callback(char* topic, byte* payload, unsigned int length)
{
	
	MQTTService::current->callbackMethod(topic, payload, length);
}

void MQTTService::timeSynchronized(long timeDifference)
{
	this->timeCompensation = timeDifference;
	this->timeIsSyncronized = true;
}

bool MQTTService::connect()
{
	Client* connectedClient = this->connectionService->getClient();

	if (connectedClient != NULL)
	{
		if (!this->mqtt)
		{
			DebugService::Information("MQTTService::connect", "Creating MQTT instance");

			this->mqtt = new PubSubClient;

			this->mqtt->setServer(ConfigurationService::MQTTUrl.c_str(), ConfigurationService::MQTTPort);
			
			this->mqtt->setCallback(callback);

			this->mqtt->setBufferSize(512);

			this->MQTT_Status_Topic = String("rsproduction/blackbox/") + UniqueIDService::getUniqueID() + "/Status";

			this->MQTT_Input_Event_Topic = String("rsproduction/blackbox/") + UniqueIDService::getUniqueID() + "/Input_Event";

			this->MQTT_AccData_Topic = String("rsproduction/blackbox/") + UniqueIDService::getUniqueID() + "/AccData";

			this->MQTT_AccDataSetting_Topic = String("rsproduction/blackbox/") + UniqueIDService::getUniqueID() + "/AccDataSetting";

			this->MQTT_Connect_Topic = String("rsproduction/blackbox/") + UniqueIDService::getUniqueID() + "/Connect";
		}
		
		if (this->activeClient != connectedClient)
		{
			DebugService::Information("MQTTService::connect", "Changing connected client");
			if (mqtt->connected())
			{
				mqtt->disconnect();
			}

			mqtt->setClient(*connectedClient);
			activeClient = connectedClient;

		}

		mqtt->loop();

		if (mqtt->connected())
		{
			return true;
		}

		DebugService::Information("MQTT_connect", "Connecting to " + String(ConfigurationService::MQTTUrl) + " port " + String(ConfigurationService::MQTTPort));

		if (mqtt->connect(UniqueIDService::getUniqueID().c_str()))
		{
			DebugService::Information("MQTT_connect", "Connected");

			DebugService::Information("MQTT_connect", "Subscribing to topics");

			mqtt->subscribe(String("rsproduction/blackbox/" + UniqueIDService::getUniqueID() + "/learn").c_str());

			mqtt->subscribe(String("rsproduction/blackbox/" + UniqueIDService::getUniqueID() + "/accDataTrigger").c_str());

			mqtt->subscribe(String("rsproduction/blackbox/" + UniqueIDService::getUniqueID() + "/getcurrentsettings").c_str());

			failCounter = 0;

			this->sendConnectedMessage();

			return true;
		}
		else
		{
			failCounter++;
			DebugService::Warning("MQTT_connect", "Connection failed");

			if(this->activeClient->connected())
			{
				DebugService::Warning("MQTT_connect", "Client is connected");
				DebugService::Warning("MQTT_connect", "Current writeerror " + String(this->activeClient->getWriteError()));

				this->activeClient->clearWriteError();
				this->activeClient->flush();
			}
			else
			{
				DebugService::Warning("MQTT_connect", "Client is not connected");
				this->activeClient->stop();
			}

			if (failCounter >= 10)
			{
				DebugService::Warning("MQTT_connect", "Connection failed 10 times, resetting network connection");

				this->connectionService->resetConnection();

				failCounter = 0;
			}
			return false;
		}
	}
	else
	{
		DebugService::Information("MQTTService::connect", "No connected client");
		return false;
	}
}

void MQTTService::sendConnectedMessage()
{
	String message = String("{\"reset\" : " + String(firstConnect) + "}");
	
	mqtt->publish(MQTT_Connect_Topic.c_str(), message.c_str());

	this->firstConnect = false;
}


