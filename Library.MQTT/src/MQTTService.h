/*
 Name:		Library.h
 Created:	11/20/2020 1:53:43 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <ConnectionService.h>
#include <MemoryService.h>
#include <PubSubClient.h>
#include <ArduinoQueue.h>
#include <WString.h>
#include <ArduinoUniqueID.h>
#include <InputEvent.h>
#include <stdio.h>
#include <UniqueIDService.h>

#define MQTTService_CALLBACK_SIGNATURE void (*callbackMethod)(char*, uint8_t*, unsigned int)

class MQTTService
{
public:
	MQTTService();
	static MQTTService* current;
	void initialize(ConnectionService* connectionService, MemoryService* memoryService);
	void loop(long currentFreeMemory, long firstStartupDate);
	bool connected;
	void addEvent(InputEvent inputEvent);
	void timeSynchronized(long timeDifference);
	unsigned int eventsInQueueCount();
	void handleMessage(char* topic, byte* payload, unsigned int length);
	void setCallback(MQTTService_CALLBACK_SIGNATURE);
	bool sendAccData(const char* payload);
	bool sendAccDataSetting(const char* payload);
	bool connect();
private:
	PubSubClient* mqtt;
	ConnectionService* connectionService;
	MemoryService* memoryService;
	Client* activeClient;
	String MQTT_Status_Topic;
	String MQTT_Input_Event_Topic;
	String MQTT_AccData_Topic;
	String MQTT_AccDataSetting_Topic;
	String MQTT_Connect_Topic;
	static void callback(char* topic, byte* payload, unsigned int length);
	
	int publishMQTTCounter;
	long timeCompensation;
	bool timeIsSyncronized;
	ArduinoQueue<InputEvent> eventQueue = ArduinoQueue<InputEvent>(1000);
	int failCounter;
	MQTTService_CALLBACK_SIGNATURE;
	bool firstConnect = true;
	void sendConnectedMessage();
};