#pragma once

#include <SerialFlash.h>
#include <SPI.h>
#include "inputEvent.h"
#include "debug.h"

extern bool mqttConnected;

extern ArduinoQueue<InputEvent> eventQueue;

bool useMemoryEventFile = false;
bool memoryHardwareInitiated = false;

void setupMemory()
{
  DebugMessage("setupMemory", "Initializing memory",1);
  if(SerialFlash.begin(A2)) 
  {
    DebugMessage("setupMemory", "Initializing memory succeded",1);
    memoryHardwareInitiated = true;
    useMemoryEventFile = true;
  }
  else
  {
    DebugMessage("setupMemory", "Initializing memory failed",1);
  }
}


void addEventToMemoryFile(InputEvent event)
{
  SerialFlashFile currentEventFile = SerialFlash.open("savedevents.txt");
    
  if(!currentEventFile)
  {
    DebugMessage("checkOrCreateEventFile", "File not found. Creating new file",2);
    if(SerialFlash.createErasable("savedevents.txt", 10000000))
    {
      currentEventFile = SerialFlash.open("savedevents.txt");
    }
    else
    {
      //Could not create file. Add to eventqueue instead
      DebugMessage("checkOrCreateEventFile", "Could not create file. Add to eventqueue instead",2);
      eventQueue.enqueue(event);
      return;
    }
  }
  
  String eventData = event.Serialize();
  DebugMessage("addEventToMemoryFile", "EventContent = " + eventData,2);

  char* buffer;
  eventData.toCharArray(buffer, 16);
  currentEventFile.write(buffer, 16);
  
}

void updateMemoryStatus()
{
  if(memoryHardwareInitiated){
    if(mqttConnected && useMemoryEventFile)
    {
      //Read memoryFile && add data to queue
      DebugMessage("UpdateMemoryStatus", "Read memoryFile && add data to queue", 2);
      useMemoryEventFile = false;
    }
    else if(!mqttConnected && !useMemoryEventFile)
    {
      //Create file && save all pending events
      DebugMessage("UpdateMemoryStatus", "Create file && save all pending events", 2);
  
      //Create file if eventqueue is larger than 0 (Do not use memory if not needed!)
  
      //Go thru all events && serialize them to file
      
      useMemoryEventFile = true;
    }
  }
}

