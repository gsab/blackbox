#include <NTPClient.h>
#include <ArduinoJson.h>

#pragma once
#include <PubSubClient.h>

#include <Arduino_LSM6DS3.h>
#include <ArduinoUniqueID.h>
#include "inputEvent.h"
#include "debug.h"

WiFiClient client;
WiFiUDP wifiUDP;
NTPClient* timeClient;

extern TinyGsm modem;
extern TinyGsmClient gsmClient;

extern EthernetClient eClient;
EthernetUDP ethernetUDP;

bool ntpClientStarted = false;

PubSubClient *mqtt = NULL;

Client* activeClient = NULL;

extern bool ethernetConnected;
extern bool wifiConnected;
extern bool gsmConnected;

extern bool mqttConnected;

extern bool D0_LAST_STATUS;
extern bool D1_LAST_STATUS;
extern bool D2_LAST_STATUS;
extern bool D3_LAST_STATUS;
extern bool D4_LAST_STATUS;
extern bool D5_LAST_STATUS;

extern bool X_LAST_STATUS;
extern bool Y_LAST_STATUS;
extern bool Z_LAST_STATUS;

extern const int8_t ERROR_LED;
extern const int8_t WIFI_LED;

extern const int8_t D0_INPUT;
extern const int8_t D1_INPUT;
extern const int8_t D2_INPUT;
extern const int8_t D3_INPUT;
extern const int8_t D4_INPUT;
extern const int8_t D5_INPUT;

extern const int8_t LED_OFF;
extern const int8_t LED_ON;
extern const int8_t LED_SLOWBLINK;
extern const int8_t LED_FASTBLINK;

extern int8_t ERROR_LED_STATUS;
extern int8_t WIFI_LED_STATUS;

extern bool recordSamples;
extern int samplesRecorded;
extern int samplesRequested;
extern float* xSampleArray;
extern float* ySampleArray;
extern float* zSampleArray;

const byte noMode = 0;
const byte ethernetMode = 1;
const byte wifiMode = 2;
const byte gsmMode = 3;

byte newConnectionMode = noMode;
byte currentConnectionMode = noMode;

#include <RTCZero.h>
extern RTCZero rtc;
extern bool timeIsSet;
long timeCompensation = 0;



extern ArduinoQueue<InputEvent> eventQueue;
#define USE_GLOBAL_TOPIC    true

#if USE_GLOBAL_TOPIC
  String MQTT_Status_Topic;

  String MQTT_Input_Event_Topic;
#endif

String getUniqueID()
{
  String result = "ID";
  for (size_t i = 0; i < UniqueIDsize; i++)
  {                                        
      if (UniqueID[i] < 0x10)
      {
        result += "0";
      }
      result += String(UniqueID[i], HEX);
    }
  return result;
}

void callback(char* topic, byte* payload, unsigned int length)
{
  DebugMessage("callback",topic,2);
  if(topic == "learn")
  {
    StaticJsonDocument<256> doc;
    deserializeJson(doc, payload, length);
    if(doc.containsKey("samples"))
    {
      DebugMessage("callback","Deserialized message",2);
      samplesRecorded = 0;
      samplesRequested = doc["samples"];
      recordSamples = true;
      DebugMessage("callback",String(samplesRequested),2);
    }
  }
  DebugMessage("callback","End of callback",2);
}

void cycleConnectionMode()
{
  if((currentConnectionMode == noMode || currentConnectionMode == gsmMode) && ethernetConnected)
  {
    newConnectionMode = ethernetMode;
  }
  else if((currentConnectionMode == noMode || currentConnectionMode == ethernetMode) && wifiConnected)
  {
    newConnectionMode = wifiMode;
  }
  else if((currentConnectionMode == noMode || currentConnectionMode == wifiMode) && gsmConnected)
  {
    newConnectionMode = gsmMode;
  }
  else if(currentConnectionMode == wifiMode && ethernetConnected)
  {
    newConnectionMode = ethernetMode;
  }
  else if(currentConnectionMode == gsmMode && wifiConnected)
  {
    newConnectionMode = wifiMode;
  }
  else if(currentConnectionMode == ethernetMode && gsmConnected)
  {
    newConnectionMode = gsmMode;
  }
  else
  {
    newConnectionMode = noMode;
  }

  DebugMessage("cycleConnectionMode","Current connection mode: " + String(newConnectionMode),2);
}

void setMQTTClient()
{
  if(newConnectionMode == ethernetMode)
    {
      DebugMessage("createNewInstances","Connecting with ethernet",1);
      mqtt->setClient(eClient);
      timeClient = new NTPClient(ethernetUDP);
      ntpClientStarted = false;
      currentConnectionMode = newConnectionMode;
      activeClient = &eClient;
    }
    else if (newConnectionMode == wifiMode)
    {
      DebugMessage("createNewInstances","Connecting with wifi",1);
      mqtt->setClient(client);
      timeClient = new NTPClient(wifiUDP);
      ntpClientStarted = false;
      currentConnectionMode = newConnectionMode;
      activeClient = &client;
    }
    else if (newConnectionMode == gsmMode)
    {
      DebugMessage("createNewInstances","Connecting with gsm",1);
      mqtt->setClient(gsmClient);
      currentConnectionMode = newConnectionMode;
      activeClient = &gsmClient;
    }
}

void createNewInstances(void)
{
  DebugMessage("createNewInstances","Start",4);
  if(currentConnectionMode == noMode)
  {
    cycleConnectionMode();
  }

  if(!mqtt)
  {
    mqtt = new PubSubClient;

    mqtt->setServer(AIO_SERVER,atoi(AIO_SERVERPORT));
    mqtt->setCallback(callback);
    
    MQTT_Status_Topic = String("rsproduction/blackbox/") + getUniqueID() + "/Status";
    
    MQTT_Input_Event_Topic = String("rsproduction/blackbox/") + getUniqueID() + "/Input_Event";

    if(newConnectionMode != noMode)
    {
      setMQTTClient(); 
    }
  }
  
  else if (!mqtt->connected() && newConnectionMode != noMode && currentConnectionMode != newConnectionMode)
  {
    setMQTTClient();
  }
  DebugMessage("createNewInstances","End",4);
}

int connectionErrorCount = 0;

bool MQTT_connect()
{
  int8_t ret;
  DebugMessage("MQTT_connect","Start",4);
  createNewInstances();
  if(currentConnectionMode != noMode)
  {
    // Return if already connected
    if (mqtt->connected())
    {
      mqttConnected = true;
      updateMemoryStatus();
      DebugMessage("MQTT_connect","End connected",4);
      return true;
    }

    if(activeClient)
    {
      //activeClient->flush();
    }
    DebugMessage("MQTT_connect","MQTT not connected ",1);
    DebugMessage("MQTT_connect","Connecting as " + String(getUniqueID()),2);
    DebugMessage("MQTT_connect","Current state = " +String(mqtt->state()),1);
    
    if(mqtt->connect(getUniqueID().c_str()))
    {
      DebugMessage("MQTT_connect","Connected",1);
       
      DebugMessage("MQTT_connect","Subscribing to learn topic",2);
      
      mqtt->subscribe(String("rsproduction/blackbox/" + getUniqueID() + "/learn").c_str());
      
      mqttConnected = true;
      updateMemoryStatus();
      DebugMessage("MQTT_connect","End reconnected",4);
      return true;
    }
    else
    {
      DebugMessage("MQTT_connect","Connection failed",1);
      connectionErrorCount++;
      if(connectionErrorCount > 10)
      {
        connectionErrorCount = 0;
        DebugMessage("MQTT_connect","Failed more than 10 times. ",1);
        if(activeClient)
        {
          DebugMessage("Connection failed 10", "Client status " + String(client.connected()),2);

          DebugMessage("Connection failed 10", "Client status " + String(client.status()),2);

          DebugMessage("Connection failed 10", "Client status " + String(client.remoteIP()),2);

          DebugMessage("Connection failed 10", "Client status " + String(client.remotePort()),2);

          DebugMessage("Connection failed 10", "Client status " + String(client.available()),2);

          DebugMessage("Connection failed 10", "Client status " + String(WiFi.localIP()),2);
          DebugMessage("Connection failed 10", "Client status " + String(WiFi.status()),2);
          //activeClient->stop();
          //MQTT->unsubscribe(String("rsproduction/blackbox/" + getUniqueID() + "/learn").c_str());
          //mqtt->disconnect();
          //WiFi.disconnect();
        }
      }
      //cycleConnectionMode();
    }
  }
  else
  {
    DebugMessage("MQTT_connect","No mqtt instance",2);
  }
  mqttConnected = false;
  updateMemoryStatus();
  DebugMessage("MQTT_connect","End failed",4);
  return false;
}

int8_t publishMQTTCounter = 0;

String convertArray(float* axleArray,int startIndex, int length)
{

  String returnValue = "";
  for(int i=startIndex;i<startIndex+length;i++)
  {
    returnValue += String(axleArray[i]);
    if(i<startIndex+length-1){
      returnValue += ",";
    }
  }

  return returnValue;
}

void publishMQTT(void)
{
  DebugMessage("publishMQTT","Start",4);

  //if(publishMQTTCounter == 5)
  //{
    DebugMessage("mqtt_loop","Before loop()",4);
    mqtt->loop();
    DebugMessage("mqtt_loop","After loop()",4);
  //}
  
  if(publishMQTTCounter == 0)
  {
    //Publish a status update to server

    String message = String("{\"Connected\" : 1}"); 
    DebugMessage("publishMQTT",MQTT_Status_Topic,2);
    DebugMessage("publishMQTT",message.c_str(),2);
    if(mqtt->publish(MQTT_Status_Topic.c_str(), message.c_str()))
    {
      
    }
  }
  DebugMessage("publishMQTT","Send events",4);
  while(eventQueue.itemCount() > 0 && timeIsSet)
  {
    InputEvent nextEvent = eventQueue.getHead();
    long epochTime = nextEvent.rtcEpoch;

    if(epochTime < 1000000000)
    {
      // Event is collected before timesync. Compensate collected time
      DebugMessage("publishMQTT","Compensate time on event",1);
      DebugMessage("publishMQTT",String(epochTime),1);
      epochTime += timeCompensation;
    }
    
    String message = String("{\"inputID\" : " + String(nextEvent.inputID) + ", \"flank\" : " + String(nextEvent.flank) + ", \"epoch\" : " + String(epochTime) + "}");
    DebugMessage("publishMQTT",MQTT_Input_Event_Topic,2);
    DebugMessage("publishMQTT",message.c_str(),2);
    if(mqtt->publish(MQTT_Input_Event_Topic.c_str(), message.c_str()))
    {
       eventQueue.dequeue();
    }
    else
    {
      DebugMessage("publishMQTT","Publish of event failed!",1);
      break;
    }
  }
  DebugMessage("publishMQTT","End send events",4);
  
  if(!recordSamples && samplesRecorded > 0)
  {
    DebugMessage("publishMQTT","Send samples to server",1);

    int currentIndex = 0;
    int maxSamplesPerMessage = 5;

    while(currentIndex * maxSamplesPerMessage  < samplesRecorded)
    {
      DynamicJsonDocument doc(256);
      doc["packetNumber"] = currentIndex + 1;
      doc["xaxle"] = convertArray(xSampleArray,currentIndex * maxSamplesPerMessage, maxSamplesPerMessage);
      doc["yaxle"] = convertArray(ySampleArray,currentIndex * maxSamplesPerMessage, maxSamplesPerMessage);
      doc["zaxle"] = convertArray(zSampleArray,currentIndex * maxSamplesPerMessage, maxSamplesPerMessage);
      char buffer[256];
      serializeJson(doc, buffer);
      mqtt->publish((String("rsproduction/blackbox/") + getUniqueID() + "/RecordedData").c_str(), buffer);

      currentIndex += 1;
    }
    samplesRecorded = 0;
    xSampleArray = NULL;
    ySampleArray = NULL;
    zSampleArray = NULL;
  }
  
  ERROR_LED_STATUS = LED_OFF;

  publishMQTTCounter++;
  
  if(publishMQTTCounter >= 6)
  {
    publishMQTTCounter = 0;
  }
  DebugMessage("publishMQTT","End",4);
}

bool checkConnectionStatus()
{
  return ethernetConnected || wifiConnected || gsmConnected;
}

int rtcSyncCounter = 0;

void synchronizeRTC()
{
  DebugMessage("synchronizeRTC","Start",4);
  if(!timeIsSet || rtcSyncCounter == 0){
    if(currentConnectionMode == gsmMode)
    {
      DebugMessage("synchronizeRTC","GSM Synchronization",4);
      int year3 = 0;
      int month3 = 0;
      int day3 = 0;
      int hour3 = 0;
      int min3 = 0;
      int sec3 = 0;
      float timezone = 0;
      for (int8_t i = 5; i; i--) {
        DebugMessage("synchronizeRTC", "Requesting current network time",4);
        if (modem.getNetworkTime(&year3, &month3, &day3, &hour3, &min3, &sec3,
                                     &timezone)) {
          DebugMessage("synchronizeRTC", "Year:" + String(year3)+ "\tMonth:"+  String(month3)+ "\tDay:"+  String(day3),4);
          DebugMessage("synchronizeRTC", "Hour:"+  String(hour3)+ "\tMinute:"+  String(min3)+ "\tSecond:"+  String(sec3),4);
          DebugMessage("synchronizeRTC", "Timezone:", timezone);
  
          long oldTime = rtc.getEpoch();
          
          rtc.setDate(day3,month3,year3);
          rtc.setTime(hour3,min3,sec3);
  
          if(!timeIsSet)
          {
            //First run after reset to compensate events collected before timesync
            timeCompensation = rtc.getEpoch() - oldTime;
          }
          timeIsSet = true;
          
        } else {
          DebugMessage("synchronizeRTC", "Couldn't get network time",2);
        }
      }
    }
    else
    {
      DebugMessage("synchronizeRTC","Wifi/Ethernet Synchronization",4);
      
      long timeOffset = 0;
      if(!ntpClientStarted)
      {
        timeClient->begin();
        ntpClientStarted = true;
      }
      timeClient->update();
      
      // MQTT related jobs
      timeOffset = abs(timeClient->getEpochTime() - rtc.getEpoch());
    
      if(abs(timeOffset) > 1)
      {
        DebugMessage("publishMQTT","Synchronizing internal clock",1);
        DebugMessage("publishMQTT",timeClient->getFormattedTime(),1);
        rtc.setEpoch(timeClient->getEpochTime());
    
        if(!timeIsSet)
        {
          //First run after reset to compensate events collected before timesync
          timeCompensation = timeOffset;
        }
        timeIsSet = true;
      }
    }
  }

  rtcSyncCounter++;

  if(rtcSyncCounter == 20)
  {
    rtcSyncCounter = 0;
  }
  DebugMessage("synchronizeRTC","End",4);
}

void mqtt_loop()
{
  if (MQTT_connect())
  {

    
    synchronizeRTC();
    publishMQTT();
    
  }
}
