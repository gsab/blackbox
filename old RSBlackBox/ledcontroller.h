#pragma once

extern const int8_t LED_OFF;
extern const int8_t LED_ON;
extern const int8_t LED_SLOWBLINK;
extern const int8_t LED_FASTBLINK;

extern int8_t ERROR_LED_STATUS;
extern int8_t WIFI_LED_STATUS;

extern int cpuCycleCounter;

void setLEDOutput(int ledID, int ledStatus)
{
  if(ledStatus == LED_OFF && digitalRead(ledID) == HIGH)
  {
    digitalWrite(ledID, LOW);
  }
  else if(ledStatus == LED_ON && digitalRead(ledID) == LOW)
  {
    digitalWrite(ledID, HIGH);
  }
  else if(ledStatus == LED_SLOWBLINK && cpuCycleCounter % 50 == 0)
  {
    if(digitalRead(ledID) == HIGH)
    {
      digitalWrite(ledID, LOW);
    }
    else
    {
      digitalWrite(ledID, HIGH);
    }
  }
  else if(ledStatus == LED_FASTBLINK && cpuCycleCounter % 10 == 0)
  {
    if(digitalRead(ledID) == HIGH)
    {
      digitalWrite(ledID, LOW);
    }
    else
    {
      digitalWrite(ledID, HIGH);
    }
  }
}

void setLEDOutputs()
{
  setLEDOutput(ERROR_LED, ERROR_LED_STATUS);
  setLEDOutput(WIFI_LED, WIFI_LED_STATUS);
}
