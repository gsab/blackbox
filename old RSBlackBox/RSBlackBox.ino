
#include <WiFiNINA.h>
#include <uEEPROMLib.h>
#include <EthernetENC.h>

#include <Arduino_LSM6DS3.h>
#include <RTCLib.h>

#include <Adafruit_ZeroTimer.h>

#define TINY_GSM_MODEM_SIM800

#include <TinyGsmClient.h>

#include <ArduinoQueue.h>

#include <FlashAsEEPROM_SAMD.h>

#include <AccDataTrigger.h>

#include "defines.h"
#include "Credentials.h"
#include "dynamicParams.h"
#include "MQTT.h"
#include "ledcontroller.h"
#include "inputEvent.h"
#include <debug.h>
#include "Memory.h"
#include <MemoryService.h>
#include <ConnectionService.h>


#define LOCAL_DEBUG       true  //false

#define SerialAT Serial1
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200

// Your GPRS credentials, if any
const char apn[] = "services.telenor.se";
const char gprsUser[] = "";
const char gprsPass[] = "";

#include <RTCZero.h>  // include the library for realtime clock
RTCZero rtc;           // realtime clock instance
bool timeIsSet = false; // determines if time has been set since restart. This needs to be done to be able to send any events

// Defining global variables

bool D0_LAST_STATUS = false;
bool D1_LAST_STATUS = false;
bool D2_LAST_STATUS = false;
bool D3_LAST_STATUS = false;
bool D4_LAST_STATUS = false;
bool D5_LAST_STATUS = false;

bool X_LAST_STATUS = false;
bool Y_LAST_STATUS = false;
bool Z_LAST_STATUS = false;

bool mqttConnected = false;

const int8_t ERROR_LED = 9;
const int8_t WIFI_LED = 8;

const int8_t D0_INPUT = 2;
const int8_t D1_INPUT = 3;
const int8_t D2_INPUT = 4;
const int8_t D3_INPUT = 5;
const int8_t D4_INPUT = 6;
const int8_t D5_INPUT = 7;
const int8_t X_INPUT = 8;
const int8_t Y_INPUT = 9;
const int8_t Z_INPUT = 10;

const int8_t LED_OFF = 0;
const int8_t LED_ON = 1;
const int8_t LED_SLOWBLINK = 2;
const int8_t LED_FASTBLINK = 3;

int8_t ERROR_LED_STATUS = 0;
int8_t WIFI_LED_STATUS = 0;

int cpuCycleCounter = 0;

int debugLevel = 5;

bool enableAccDataOutput = false;

bool noWifiConfigured = false;
bool noEthernetHardware = false;
bool noGSMHardware = false;

bool ethernetConnected = false;
bool wifiConnected = false;
bool gsmConnected = false;

bool recordSamples = false;

bool setupStageActive = true;

int samplesRequested = 0;

int samplesRecorded = 0;

WiFiManager_NINA_Lite* WiFiManager_NINA;

TinyGsm modem(SerialAT);

TinyGsmClient gsmClient(modem);

EthernetClient eClient;

ArduinoQueue<InputEvent> eventQueue(1000);

float* xSampleArray;
float* ySampleArray;
float* zSampleArray;

Adafruit_ZeroTimer zt3 = Adafruit_ZeroTimer(3);
Adafruit_ZeroTimer zt4 = Adafruit_ZeroTimer(4);

void setup()
{
  // Debug console
  Serial.begin(115200);

  configurePins(); //Configures the pins on the arduino cpu

  startRTC(); //Real time clock chip start

  startIMU(); //Accelerometer chip start

  activateInterrupts(); //Input reader, accelerometer reader interrupts

  delay(5000);

  ConnectionService service();


  //MemoryService testManager(A2);

  ERROR_LED_STATUS = LED_FASTBLINK;
  WIFI_LED_STATUS = LED_FASTBLINK;

  delay(5000);
  
  noGSMHardware = true;
  //noWifiConfigured = true;
  noEthernetHardware = true;
  //initializeGSM();
  //initializeEthernet();
  initializeWifi();

  ERROR_LED_STATUS = LED_OFF;
  WIFI_LED_STATUS = LED_OFF;
  setupStageActive = false;
}

void startIMU()
{
  IMU.begin();
  calibrate();
}

void startRTC()
{
  rtc.begin();

  //Todo check if rtc is synced. If it is then set timeIsSet = true;
}

void configurePins()
{

  pinMode(ERROR_LED, OUTPUT);
  pinMode(WIFI_LED, OUTPUT);

  pinMode(D0_INPUT, INPUT_PULLUP);
  pinMode(D1_INPUT, INPUT_PULLUP);
  pinMode(D2_INPUT, INPUT_PULLUP);
  pinMode(D3_INPUT, INPUT_PULLUP);
  pinMode(D4_INPUT, INPUT_PULLUP);
  pinMode(D5_INPUT, INPUT_PULLUP);
}

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

void initializeEthernet()
{
  Ethernet.init(10); //10 is the pinnumber for ethernet module
  DebugMessage("initializeEthernet", "Initialize Ethernet with DHCP:", 1);
  delay(1000);
  Ethernet.begin(mac);
  if (Ethernet.linkStatus() == Unknown)
  {
    DebugMessage("initializeEthernet", "No ethernet hardware detected", 1);
    noEthernetHardware = true;
  }
}

void initializeWifi()
{

  WiFiManager_NINA = new WiFiManager_NINA_Lite();
  WiFiManager_NINA->setConfigPortalChannel(1);
  WiFiManager_NINA->setConfigPortal("RSBlackBox_" + getUniqueID(), "");
  WiFiManager_NINA->begin("RSBlackBox");
}

void updateEthernetStatus()
{
  if (!noEthernetHardware)
  {

    //Check if cable has been pulled after last check
    if (ethernetConnected && Ethernet.linkStatus() == LinkOFF)
    {
      ethernetConnected = false;
    }

    if (Ethernet.linkStatus() != LinkOFF)
    {
      int maintainResult = Ethernet.maintain();

      //0: nothing happened
      //1: renew failed
      //2: renew success
      //3: rebind fail
      //4: rebind success

      if (maintainResult == 1 || maintainResult == 3)
      {
        ethernetConnected = false;
      }
      else
      {
        ethernetConnected = true;
      }
    }
    DebugMessage("updateEthernetStatus", "Current status: " + String(ethernetConnected), 3);
  }
}

void updateWifiStatus()
{
  if(!noWifiConfigured){
    WiFiManager_NINA->run();
  
    if (WiFi.status() == WL_CONNECTED)
    {
      wifiConnected = true;
    }
    else
    {
      wifiConnected = false;
    }
  
    DebugMessage("updateWifiStatus", "Current status: " + String(wifiConnected), 3);
  }
}

void connectToGSM()
{
  if (!modem.isGprsConnected())
  {
    DebugMessage("initializeGSM", " recycling connection!", 2);

    if (!modem.isNetworkConnected())
    {
      modem.waitForNetwork();

      if(!modem.isNetworkConnected())
      {
        DebugMessage("initializeGSM", "Could not get network!", 2);
        gsmConnected = false;
        return;
      }
    }
    
    DebugMessage("initializeGSM", "Connecting to " + String(apn), 2);

    if (modem.isGprsConnected())
      {
        DebugMessage("initializeGSM", "GPRS connected", 1);
      }
    else
    {
      if (!modem.gprsConnect(apn, gprsUser, gprsPass))
      {
        DebugMessage("initializeGSM", " fail connecting to " + String(apn), 1);
        gsmConnected = false;
        return;
      }
      else
      {
        DebugMessage("initializeGSM", " success connecting to " + String(apn), 2);
      }
    }
  }
  gsmConnected = true;
}

void updateGSMStatus()
{
  if (!noGSMHardware)
  {
    if(!mqttConnected)
    {
      connectToGSM();
    }
    DebugMessage("updateGSMStatus", "Current status: " + String(gsmConnected), 3);
  }
}

void initializeGSM()
{
  DebugMessage("initializeGSM", "Initializing GSM modem", 2);

  uint32_t modemSpeed = TinyGsmAutoBaud(SerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);
  //SerialAT.begin(9600);
  DebugMessage("initializeGSM","Initializing modem... " + String(modemSpeed),2);

  modem.restart();
  
  delay(1000);
  
  DebugMessage("initializeGSM", "Modem initialized", 2);
  

}

void activateInterrupts()
{
  zt3.configure(TC_CLOCK_PRESCALER_DIV8, // prescaler
                TC_COUNTER_SIZE_16BIT,   // bit width of timer/counter
                TC_WAVE_GENERATION_MATCH_PWM  // frequency || PWM mode
               );

  zt3.setCompare(0, 180000); // 1 match, channel 0
  zt3.setCallback(true, TC_CALLBACK_CC_CHANNEL0, accReadInterrupt_Handler);  // this one sets pin low
  zt3.enable(true);

  zt4.configure(TC_CLOCK_PRESCALER_DIV8, // prescaler
                TC_COUNTER_SIZE_16BIT,   // bit width of timer/counter
                TC_WAVE_GENERATION_MATCH_PWM   // match style
               );

  zt4.setCompare(0, 300000); // 1 match, channel 0
  zt4.setCallback(true, TC_CALLBACK_CC_CHANNEL0, inputReadInterrupt_Handler);  // set DAC in the callback
  zt4.enable(true);
}

void displayCredentials(void)
{
  DebugMessage("displayCredentials", "\nYour stored Credentials :", 3);

  for (int i = 0; i < NUM_MENU_ITEMS; i++)
  {
    DebugMessage("displayCredentials", String(myMenuItems[i].displayName) + " = " + myMenuItems[i].pdata, 3);
  }
}

int xPosDeviations = 0;
int yPosDeviations = 0;
int zPosDeviations = 0;

int xNegDeviations = 0;
int yNegDeviations = 0;
int zNegDeviations = 0;

void updateCpuCounter()
{
  cpuCycleCounter += 1;

  if (cpuCycleCounter > 100000)
  {
    cpuCycleCounter = 0;
  }
}

void loop()
{
  static bool displayedCredentials = false;

  if (!displayedCredentials)
  {
    for (int i = 0; i < NUM_MENU_ITEMS; i++)
    {
      if (!strlen(myMenuItems[i].pdata))
      {
        break;
      }

      if ( i == (NUM_MENU_ITEMS - 1) )
      {
        displayedCredentials = true;
        displayCredentials();
      }
    }
  }

  updateWifiStatus();
  updateEthernetStatus();
  updateGSMStatus();

  mqtt_loop();
  
  DebugMessage("loop", " Amount of free memory: " + String(freeMemory()), 3);
  DebugMessage("loop", " Loop done", 3);

  delay(1000);
}

void TC3_Handler() {
  Adafruit_ZeroTimer::timerHandler(3);
}

void TC4_Handler() {
  Adafruit_ZeroTimer::timerHandler(4);
}

void imu_read(float *ax, float *ay, float *az)
{
  float _ax, _ay, _az;

  IMU.readAcceleration(_ax, _ay, _az);

  *ax = _ax;
  *ay = _ay;
  *az = _az;
}

#define NUM_SAMPLES 30
#define NUM_AXES 3
// sometimes you may get "spikes" in the readings
// set a sensible value to truncate too large values
#define TRUNCATE_AT 20
#define ACCEL_THRESHOLD 0.1
double baseline[NUM_AXES];
double features[NUM_SAMPLES * NUM_AXES];

void calibrate()

{
  float ax, ay, az;

  for (int i = 0; i < 10; i++) {
    imu_read(&ax, &ay, &az);
    delay(100);
  }

  baseline[0] = ax;
  baseline[1] = ay;
  baseline[2] = az;
}

AccDataInput allAxles(10, 0.03, 10, 20, 10, true, true, true, true, true);

void accReadInterrupt_Handler()
{
  if (recordSamples)
  {
    if (samplesRequested <= samplesRecorded)
    {
      DebugMessage("displayCredentials", "Recording done", 2);
      recordSamples = false;
      return;
    }
    if (xSampleArray == NULL)
    {
      xSampleArray = new float[samplesRequested];
      ySampleArray = new float[samplesRequested];
      zSampleArray = new float[samplesRequested];
    }
    float ax, ay, az;

    imu_read(&ax, &ay, &az);

    ax = constrain(ax - baseline[0], -TRUNCATE_AT, TRUNCATE_AT);
    ay = constrain(ay - baseline[1], -TRUNCATE_AT, TRUNCATE_AT);
    az = constrain(az - baseline[2], -TRUNCATE_AT, TRUNCATE_AT);

    xSampleArray[samplesRecorded] = ax;
    ySampleArray[samplesRecorded] = ay;
    zSampleArray[samplesRecorded] = az;

    samplesRecorded++;
  }

  else
  {
    float ax, ay, az;

    imu_read(&ax, &ay, &az);


    double correctedAx = ax - baseline[0];
    double correctedAz = ay - baseline[1];
    double correctedAy = az - baseline[2];

    if(allAxles.addReading(correctedAx,correctedAy,correctedAz))
    {
      createEvent(allAxles.currentState, allAxles.inputID);
    }
    
    if (enableAccDataOutput)
    {
      Serial.print(correctedAx);
      Serial.print('\t');
      Serial.print(correctedAz);
      Serial.print('\t');
      Serial.print(correctedAy);
      Serial.print('\t');
      Serial.print(allAxles.lastValue);
      Serial.print('\t');
      Serial.println(allAxles.currentState);
    }

  }
}

void recordIMU() {
  float ax, ay, az;

  for (int i = 0; i < NUM_SAMPLES; i++) {
    imu_read(&ax, &ay, &az);

    ax = constrain(ax - baseline[0], -TRUNCATE_AT, TRUNCATE_AT);
    ay = constrain(ay - baseline[1], -TRUNCATE_AT, TRUNCATE_AT);
    az = constrain(az - baseline[2], -TRUNCATE_AT, TRUNCATE_AT);

    features[i * NUM_AXES + 0] = ax;
    features[i * NUM_AXES + 1] = ay;
    features[i * NUM_AXES + 2] = az;

    delay(10);
  }
}

void inputReadInterrupt_Handler()
{
  //YOUR CODE HERE
  readInputs();
  if(!setupStageActive){
    if (eventQueue.itemCount() > 0)
    {
      if (mqttConnected)
      {
        WIFI_LED_STATUS = LED_FASTBLINK;
        ERROR_LED_STATUS = LED_OFF;
      }
      else
      {
        WIFI_LED_STATUS = LED_OFF;
        ERROR_LED_STATUS = LED_FASTBLINK;
      }
    }
    else
    {
      if (mqttConnected)
      {
        WIFI_LED_STATUS = LED_ON;
        ERROR_LED_STATUS = LED_OFF;
      }
      else
      {
        WIFI_LED_STATUS = LED_OFF;
        ERROR_LED_STATUS = LED_SLOWBLINK;
      }
    }
  }

  //Update && set LEDs
  setLEDOutputs();

  //Update CpuCounter used to schedule functions
  updateCpuCounter();
}

void readInputs()
{
  D0_LAST_STATUS = readInput(D0_INPUT, D0_LAST_STATUS);
  D1_LAST_STATUS = readInput(D1_INPUT, D1_LAST_STATUS);
  D2_LAST_STATUS = readInput(D2_INPUT, D2_LAST_STATUS);
  D3_LAST_STATUS = readInput(D3_INPUT, D3_LAST_STATUS);
  D4_LAST_STATUS = readInput(D4_INPUT, D4_LAST_STATUS);
  D5_LAST_STATUS = readInput(D5_INPUT, D5_LAST_STATUS);
}

bool readInput(int inputID, bool lastStatus)
{
  bool currentStatus = !digitalRead(inputID);

  if (lastStatus != currentStatus)
  {
    createEvent(currentStatus, inputID);
  }
  return currentStatus;
}

void createEvent(bool flank, int inputID)
{
  InputEvent newEvent = InputEvent();
  newEvent.inputID = inputID;
  newEvent.flank = flank;
  newEvent.rtcEpoch = rtc.getEpoch();
  eventQueue.enqueue(newEvent);
  DebugMessage("createEvent", String(flank) + " event detected on input " + String(inputID), 1);
}

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}
