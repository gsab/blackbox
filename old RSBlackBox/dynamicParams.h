/****************************************************************************************************************************
   dynamicParams.h  for SAMD_WiFiNINA_MQTT.ino
   For SAMD boards using WiFiNINA modules/shields, using much less code to support boards with smaller memory

   WiFiManager_NINA_WM_Lite is a library for the Mega, Teensy, SAM DUE, SAMD && STM32 boards 
   (https://github.com/khoih-prog/WiFiManager_NINA_Lite) to enable store Credentials in EEPROM/LittleFS for easy 
   configuration/reconfiguration && autoconnect/autoreconnect of WiFi && other services without Hardcoding.

   Built by Khoi Hoang https://github.com/khoih-prog/WiFiManager_NINA_Lite
   Licensed under MIT license
   Version: 1.0.5

   Version Modified By   Date        Comments
   ------- -----------  ----------   -----------
   1.0.0   K Hoang      26/03/2020  Initial coding
   1.0.1   K Hoang      27/03/2020  Fix SAMD soft-reset bug. Add support to remaining boards
   1.0.2   K Hoang      15/04/2020  Fix bug. Add SAMD51 support.
   1.0.3   K Hoang      24/04/2020  Fix bug. Add nRF5 (Adafruit, NINA_B302_ublox, etc.) support. Add MultiWiFi, HostName capability.
                                    SSID password maxlen is 63 now. Permit special chars # && % in input data.
   1.0.4   K Hoang      04/05/2020  Add Configurable Config Portal Title, Default Config Data && DRD. Update examples.
   1.0.5   K Hoang      11/07/2020  Modify LOAD_DEFAULT_CONFIG_DATA logic. Enhance MultiWiFi connection logic. Add MQTT examples.         
 *****************************************************************************************************************************/

#define dynamicParams_h

#include "defines.h"

#define USE_DYNAMIC_PARAMETERS      true

/////////////// Start dynamic Credentials ///////////////

//Defined in <WiFiManager_NINA_Lite_SAMD.h>
/**************************************
  #define MAX_ID_LEN                5
  #define MAX_DISPLAY_NAME_LEN      16

  typedef struct
  {
  char id             [MAX_ID_LEN + 1];
  char displayName    [MAX_DISPLAY_NAME_LEN + 1];
  char *pdata;
  uint8_t maxlen;
  } MenuItem;
**************************************/

#define AIO_SERVER_LEN       60
#define AIO_SERVERPORT_LEN   6
#define INSTALLATION_LEN 10
#define APN_ADRESS_LEN 60
#define APN_USER_LEN 30
#define APN_PASSWORD_LEN 30


char AIO_SERVER     [AIO_SERVER_LEN + 1]        = "";
char AIO_SERVERPORT [AIO_SERVERPORT_LEN + 1]    = "1883";     //1883, || 8883 for SSL

char INSTALLATION        [INSTALLATION_LEN + 1]           = "";

char APN_ADRESS		[APN_ADRESS_LEN + 1] = "services.telenor.se";
char APN_USER		[APN_USER_LEN + 1] = "";
char APN_PASSWORD	[APN_PASSWORD_LEN + 1] = "";

MenuItem myMenuItems [] =
{
  { "svr", "RSProduction URL",      AIO_SERVER,     AIO_SERVER_LEN },
  { "prt", "MQTT Port",  AIO_SERVERPORT, AIO_SERVERPORT_LEN },
  { "apna", "APN Adress",  APN_ADRESS, APN_ADRESS_LEN },
  { "apnu", "APN User",  APN_USER, APN_USER_LEN },
  { "apnp", "APN Password",  APN_PASSWORD, APN_PASSWORD_LEN },
  { "ins", "INSTALLATION",         INSTALLATION,        INSTALLATION_LEN }
};

uint16_t NUM_MENU_ITEMS = sizeof(myMenuItems) / sizeof(MenuItem);  //MenuItemSize;

