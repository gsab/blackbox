/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <EthernetConnection.h>
#include <WifiConnection.h>
#include <GSMConnection.h>
#include <DebugService.h>
#include <WString.h>
#include <Arduino.h>
#include <IPAddress.h>

class ConnectionService {
public:
	void initialize();
	void loop();
	Client* getClient();
	bool getEpochTime(long* result);
	bool connected;
	void resetConnection();
	void handleMessage(char* topic, byte* payload, unsigned int length);
private:
	EthernetConnection ethernet = EthernetConnection();
	WifiConnection wifi = WifiConnection();
	GSMConnection gsm = GSMConnection();
};

