#include "EthernetConnection.h"

void EthernetConnection::initialize()
{
	byte mac[6] = { 0x90, 0xA2, 0xDA, 0x00, 0x00, 0x00 };

	//TODO Randomize and save mac adress

	Ethernet.init(10); //10 is the pinnumber for ethernet module
	unsigned long timeout = 15000UL;
	unsigned long responseTimeout = 400UL;
	Ethernet.begin(mac, timeout, responseTimeout);
}

void EthernetConnection::loop()
{
	if (this->connected && Ethernet.linkStatus() == LinkOFF)
	{
		this->connected = false;
	}

	if (Ethernet.linkStatus() != LinkOFF && Ethernet.hardwareStatus() != EthernetNoHardware)
	{
		int maintainResult = Ethernet.maintain();

		//0: nothing happened
		//1: renew failed
		//2: renew success
		//3: rebind fail
		//4: rebind success

		if (maintainResult == 1 || maintainResult == 3)
		{
			this->connected = false;
		}
		else
		{
			this->connected = true;
		}
	}
}
void EthernetConnection::resetConnection()
{

}
EthernetClient* EthernetConnection::getClient()
{
	return &this->client;
}


long EthernetConnection::getEpochTime()
{
	long timeRecieved = 0;
	if (!this->timeClient)
	{
		this->timeClient = new NTPClient(udpClient);
		this->timeClient->begin();
	}
	if (this->timeClient->update())
	{
		timeRecieved = timeClient->getEpochTime();
	}

	return timeRecieved;
}

