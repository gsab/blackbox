#ifndef SerialAT
#define TINY_GSM_MODEM_SIM800
#define SerialAT Serial1
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 9600
#endif // !SerialAT

#include "GSMConnection.h"


void GSMConnection::initialize()
{
	//DebugMessage("initializeGSM", "Initializing GSM modem", 2);

	uint32_t modemSpeed = TinyGsmAutoBaud(SerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);
	//SerialAT.begin(9600);
	DebugService::Information("initializeGSM", "Initializing modem... " + String(modemSpeed));

	this->modem.restart();

	String simCardCCID = this->modem.getSimCCID();

	if (simCardCCID == "ERROR" || simCardCCID == "")
	{
		noSimCard = true;
	}

	DebugService::Information("initializeGSM","Sim card CCID: " + simCardCCID);
}

void GSMConnection::resetConnection()
{
	this->connected = false;
}



void GSMConnection::loop(String apnAdress, String apnUser, String apnPassword)
{
	if (noSimCard)
	{
		this->connected = false;
		return;
	}

	modem.maintain();

	if (this->connected == false)
	{
		if (!this->modem.isGprsConnected())
		{
			DebugService::Information("GSMConnection::loop", " recycling connection!");
			if (!this->modem.isNetworkConnected())
			{
				DebugService::Information("GSMConnection::loop", " waiting for network!");
				//this->modem.waitForNetwork();
				DebugService::Warning("GSMConnection::loop", "Could not get network!");
				this->connected = false;

			}
			else {
				DebugService::Information("GSMConnection::loop", "Connecting to " + apnAdress);
				DebugService::Information("GSMConnection::loop", "Operator " + String(modem.getOperator()));
				DebugService::Information("GSMConnection::loop", "Signal strength " + String(modem.getSignalQuality()));

				if (this->modem.isGprsConnected())
				{


					DebugService::Information("GSMConnection::loop", "GPRS connected");
					this->connected = true;
				}
				else
				{
					this->client.setTimeout(30000);
					!this->modem.gprsConnect(apnAdress.c_str(), apnUser.c_str(), apnPassword.c_str());
				}
			}
		}
		else
		{
			failCounter = 10;
			this->connected = true;
		}

	}

	
}

TinyGsmClient* GSMConnection::getClient()
{
	
	return &this->client;
}

long GSMConnection::getEpochTime()
{
	int year3 = 0;
	int month3 = 0;
	int day3 = 0;
	int hour3 = 0;
	int min3 = 0;
	int sec3 = 0;
	float timezone = 0;
	if (modem.getNetworkTime(&year3, &month3, &day3, &hour3, &min3, &sec3,
		&timezone)) {
		DateTime currentTime = DateTime(year3, month3, day3, hour3, min3, sec3);
		return currentTime.unixtime();
	}
	return 0;
}