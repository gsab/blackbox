/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <EthernetENC.h>
#include <NTPClient.h>

class EthernetConnection {
public:
	void initialize();
	void loop();
	EthernetClient* getClient();
	long getEpochTime();
	bool connected;
	void resetConnection();
private:
	EthernetClient client = EthernetClient();
	EthernetUDP udpClient = EthernetUDP();
	NTPClient* timeClient;
};




