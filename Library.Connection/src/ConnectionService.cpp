/*
 Name:		Library.cpp
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "ConnectionService.h"

void ConnectionService::initialize()
{

	DebugService::Information("ConnectionService initialize", "Initialization started");
	if (ConfigurationService::ethernetActive) {
		this->ethernet.initialize();
		DebugService::Information("ConnectionService initialize", "Ethernet initialized");
	}
	
	this->wifi.initialize();
	DebugService::Information("ConnectionService initialize", "Wifi initialized");
	
	if (ConfigurationService::gsmActive) {
		this->gsm.initialize();
		DebugService::Information("ConnectionService initialize", "GSM initialized");
	}
}

void ConnectionService::loop()
{
	if (ConfigurationService::ethernetActive) {
		this->ethernet.loop();
	}
	this->wifi.loop();
	
	if (ConfigurationService::gsmActive) {
		this->gsm.loop(ConfigurationService::APNUrl, ConfigurationService::APNUser, ConfigurationService::APNPassword);
	}
	
	this->connected = ethernet.connected || wifi.connected || gsm.connected;
}
Client* ConnectionService::getClient()
{
	if (this->ethernet.connected)
	{
		//DebugService::Information("ConnectionService::getClient", "Ethernet");
		return this->ethernet.getClient();
	}
	if (this->wifi.connected)
	{
		//DebugService::Information("ConnectionService::getClient", "WiFi");
		return this->wifi.getClient();
	}
	if (this->gsm.connected)
	{
		//DebugService::Information("ConnectionService::getClient", "GSM");
		return this->gsm.getClient();
	}
	return NULL;
}

void ConnectionService::resetConnection()
{
	if (this->ethernet.connected)
	{
		return this->ethernet.resetConnection();
	}
	if (this->wifi.connected)
	{
		return this->wifi.resetConnection();
	}
	if (this->gsm.connected)
	{
		return this->gsm.resetConnection();
	}
}
void ConnectionService::handleMessage(char* topic, byte* payload, unsigned int length)
{
}

bool ConnectionService::getEpochTime(long* result)
{
	long currentTime;
	if (this->ethernet.connected)
	{
		currentTime = this->ethernet.getEpochTime();
	}
	else if (this->wifi.connected)
	{
		currentTime = this->wifi.getEpochTime();
	}
	else if (this->gsm.connected)
	{
		currentTime = this->gsm.getEpochTime();
	}
	else
	{
		return false;
	}

	DebugService::Information("ConnectionService::getEpochTime","Time recieved " + String(currentTime));

	if (currentTime != 0)
	{
		*result = currentTime;
		return true;
	}
	else
	{
		return false;
	}
}