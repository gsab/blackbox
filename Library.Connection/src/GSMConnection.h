/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#ifndef SerialAT
#define TINY_GSM_MODEM_SIM800
#define SerialAT Serial1
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 9600
#endif // !SerialAT


#include <Client.h>
#include <TinyGsmClient.h>
#include <RTCLib.h>
#include <DebugService.h>
#include <WString.h>

class GSMConnection {
public:
	void initialize();
	void loop(String apnAdress, String apnUser, String apnPassword);
	TinyGsmClient* getClient();
	long getEpochTime();
	bool connected;
	void resetConnection();
private:
	TinyGsm modem = TinyGsm(SerialAT);
	TinyGsmClient client = TinyGsmClient(modem);
	bool noSimCard = false;
	int failCounter = 0;
};
