/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WiFiNINA.h>
#include <NTPClient.h>
#include <WString.h>
#include <DebugService.h>
#include <ConfigurationService.h>
#include <UniqueIDService.h>

class WifiConnection{
public:
	void initialize();
	void loop();
	WiFiClient* getClient();
	long getEpochTime();
	bool connected;
	void resetConnection();
private:
	WiFiClient client = WiFiClient();
	WiFiUDP udpClient = WiFiUDP();
	NTPClient* timeClient;
	bool apMode = false;
	int failCounter = 0;
	void scanNetworks();
	bool networkAvailable = false;
	int apScanNetwork = 30;
};