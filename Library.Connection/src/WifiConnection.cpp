#include "WifiConnection.h"

void WifiConnection::initialize()
{
	WiFi.noLowPowerMode();
	DebugService::Information("WifiConnection::initialize", "FirmWare version: " + String(WiFi.firmwareVersion()));

	if (ConfigurationService::wifiSSID == "" || !ConfigurationService::wifiActive)
	{
		this->apMode = true;
	}
}

void WifiConnection::loop()
{
	if (this->apMode && ConfigurationService::wifiActive)
	{
		if (WiFi.status() != WL_AP_CONNECTED && WiFi.status() != WL_AP_LISTENING)
		{
			DebugService::Information("WifiConnection::loop", "No SSID set starting AP point");
			
			WiFi.beginAP(String("RSBlackBox_" + UniqueIDService::getUniqueID()).c_str(), "rsblackbox");
			WiFi.config(IPAddress(192, 168, 4, 1), IPAddress(192, 168, 4, 1));
		}

		if (WiFi.status() == WL_AP_LISTENING && ConfigurationService::wifiSSID != "")
		{
			apScanNetwork--;
			if (apScanNetwork == 0)
			{
				scanNetworks();

				if (this->networkAvailable)
				{
					this->apMode = false;
					WiFi.end();
				}
				apScanNetwork = 30;
			}
		}
		else
		{
			apScanNetwork = 30;
		}
	}
	else {
		if (WiFi.status() != WL_NO_MODULE) {
			if (WiFi.status() == WL_CONNECTED)
			{
				if (this->connected == false)
				{
					IPAddress localIP = WiFi.localIP();

					DebugService::Information("WifiConnection::loop", "Connected with IP: " + String(localIP[0]) + "." + String(localIP[1]) + "." + String(localIP[2]+ "." + String(localIP[3])));
				}

				this->connected = true;
				this->failCounter = 10;
			}
			else
			{
				this->failCounter--;

				if (this->failCounter > 0 && this->connected)
				{
					DebugService::Information("WifiConnection::loop", "Connection status = " + String(WiFi.status()) + " Failcounter = " + String(this->failCounter));
				}
				else
				{
					scanNetworks();

					if (this->networkAvailable)
					{
						DebugService::Information("WifiConnection::loop", "Connection status = " + String(WiFi.status()));

						if (this->connected)
						{
							DebugService::Information("WifiConnection::loop", "Lost connection!");
							WiFi.end();
						}

						DebugService::Information("WifiConnection::loop", "Wifi not connected, connection to " + ConfigurationService::wifiSSID);

						this->connected = false;

						WiFi.begin(ConfigurationService::wifiSSID.c_str(), ConfigurationService::wifiPass.c_str());
					}
					else
					{
						this->apMode = true;
					}
				}
			}
		}
	}
	//DebugMessage("updateWifiStatus", "Current status: " + String(this->connected), 3);
}

WiFiClient* WifiConnection::getClient()
{
	return &this->client;
}


void WifiConnection::resetConnection()
{
	DebugService::Information("WifiConnection::resetConnection", "Connection reset called!");

	WiFi.end();

	this->failCounter = 0;
}

long WifiConnection::getEpochTime()
{
	long timeRecieved = 0;
	if (!this->timeClient)
	{
		this->timeClient = new NTPClient(udpClient);
		
	}
	this->timeClient->begin();
	if (this->timeClient->update())
	{
		timeRecieved = timeClient->getEpochTime();
	}
	this->timeClient->end();
	return timeRecieved;
}

void WifiConnection::scanNetworks()
{
	if (ConfigurationService::wifiSSID != "")
	{
		DebugService::Information("WifiConnection::scanNetworks", "Scanning for networks");
		int8_t wifiNetworks = WiFi.scanNetworks();

		bool networkFound = false;

		for (int thisNet = 0; thisNet < wifiNetworks; thisNet++) {
			if (String(WiFi.SSID(thisNet)) == ConfigurationService::wifiSSID)
			{
				DebugService::Information("WifiConnection::scanNetworks", "Found network!");
				networkFound = true;
			}
		}

		this->networkAvailable = networkFound;
	}
}
