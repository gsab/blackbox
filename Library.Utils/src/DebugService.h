#pragma once
#include <WString.h>
#include <Arduino.h>

class DebugService
{
public:
	static void Information(String header, String message);
	static void Warning(String header, String message);
	static void Error(String header, String message);
};