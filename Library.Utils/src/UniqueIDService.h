#pragma once
#include <WString.h>
#include <ArduinoUniqueID.h>

class UniqueIDService
{
public:
	static String getUniqueID();
};