#include <UniqueIDService.h>

String UniqueIDService::getUniqueID()
{
	String result = "ID";
	for (size_t i = 0; i < UniqueIDsize; i++)
	{
		if (UniqueID[i] < 0x10)
		{
			result += "0";
		}
		result += String(UniqueID[i], HEX);
	}
	return result;
}