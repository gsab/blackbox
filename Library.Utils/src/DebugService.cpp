#include <DebugService.h>

void DebugService::Information(String header, String message)
{
	Serial.println("Information: " + header + ": " + message);
}
void DebugService::Warning(String header, String message) 
{
	Serial.println("Warning: " + header + ": " + message);
}
void DebugService::Error(String header, String message) 
{
	Serial.println("Error: " + header + ": " + message);
}
