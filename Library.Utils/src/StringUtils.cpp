/*
 Name:		Library.cpp
 Created:	11/18/2020 3:23:15 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "StringUtils.h"


static const char* convertStringToCharArray(String string)
{
	char __charArray[sizeof(string)];
	string.toCharArray(__charArray, sizeof(__charArray));
	return __charArray;
}