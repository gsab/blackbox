/*
 Name:		Library.h
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>

class VibrationService {

public:
	void initialize();
	bool evaluateData(double xAxleValue, double yAxleValue, double zAxleValue);
	void changeItem(double expectedCycletime, String identifier);

private:

};