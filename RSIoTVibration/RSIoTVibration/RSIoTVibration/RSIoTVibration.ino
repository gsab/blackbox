/*
 Name:		RSIoTVibration.ino
 Created:	2/2/2023 9:15:02 AM
 Author:	David
*/

#include <KX0231025IMU.h>
#include <WString.h>
#include <Library.Vibration.h>
#include <SPI.h>

//Accelerometer defines
#define KX0231025_RANGE_2G       0
#define KX0231025_RANGE_4G       1
#define KX0231025_RANGE_8G       2

#define KX0231025_DATARATE_12_5HZ  0
#define KX0231025_DATARATE_25HZ    1
#define KX0231025_DATARATE_50HZ    2
#define KX0231025_DATARATE_100HZ   3
#define KX0231025_DATARATE_200HZ   4
#define KX0231025_DATARATE_400HZ   5
#define KX0231025_DATARATE_800HZ   6
#define KX0231025_DATARATE_1600HZ  7
#define KX0231025_DATARATE_0_781HZ   8
#define KX0231025_DATARATE_1_563HZ   9
#define KX0231025_DATARATE_3_125HZ   10
#define KX0231025_DATARATE_6_25HZ  11

#define KX0231025_LOWPOWER_MODE    0X00
#define KX0231025_HIGHPOWER_MODE   0X40

SPIClass hspi(HSPI); //SPI-bus for accelerometer running on core 0
KX0231025Class* imu;
bool hardwareError = false;

VibrationService vibrationService = VibrationService();
float x, y, z;
TaskHandle_t core0ReadInputsTask;

void startReadInputWorkerTask()
{
	xTaskCreatePinnedToCore(
		readInputsTask, /* Function to implement the task */
		"ReadInputsTask", /* Name of the task */
		100000,  /* Stack size in words */
		NULL,  /* Task input parameter */
		10,  /* Priority of the task */
		&core0ReadInputsTask,  /* Task handle. */
		0); /* Core where the task should run */
}

void readInputsTask(void* parameter)
{
	int loops = 0;
	
	while (true)
	{
		delay(1);
		
	}
	vTaskDelete(NULL);
}


void startIMU()
{
	imu = new KX0231025Class(hspi, 15);

	int responce = imu->begin(KX0231025_HIGHPOWER_MODE, KX0231025_RANGE_2G, KX0231025_DATARATE_1600HZ);

	if (responce == 1)
	{
		Serial.println("IMU Started succesfully");

	}
	if (responce == 0)
	{
		Serial.println("IMU start failed!");
		hardwareError = true;
	}
}


// the setup function runs once when you press reset or power the board
void setup()
{
	Serial.begin(115200);
	Serial.println("Vibration testproject started");

	Serial.println("Initializing accelerometer");
	startIMU(); //Accelerometer chip start
	Serial.println("Initializing done");

	Serial.println("Initializing VibrationService");
	vibrationService.initialize();
	Serial.println("Initializing done");

	//startReadInputWorkerTask();
}

bool lastResult = false;

// the loop function runs over and over again until power down or reset
void loop()
{
	imu->readAcceleration(x, y, z);


	//Evaluate vibrationdata
	long startTime = millis();
	bool result = vibrationService.evaluateData(x, y, z);
	long endTime = millis();

	if (result != lastResult)
	{
		Serial.println("Evaluate data result changed to " + String(result));

		lastResult = result;
	}

	//Print vibrationdata to plot
	//Serial.print(x);
	//Serial.print("\t");
	//Serial.print(y);
	//Serial.print("\t");
	//Serial.println(z);

	delay(10);
}



