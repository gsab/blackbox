#include "EthernetConnection.h"

void EthernetConnection::initialize(bool active)
{
    if (!this->initialized)
    {
        DebugService::Information("EthernetConnection::initialize", "Initializing ethernet");
        Ethernet.init(4);
        delay(1000);
        generateMacFromID();
        this->initialized = true;
    }
        
    if (ConfigurationService::inTestMode)
    {
        DebugService::Information("EthernetConnection::initialize", "mac address " + StringUtils::macAddressToString(uniqueIDtoMAC));
        DebugService::Information("EthernetConnection::initialize", "Hardware status " + String(Ethernet.hardwareStatus()));
        DebugService::Information("EthernetConnection::initialize", "Link status " + String(Ethernet.linkStatus()));

        if (Ethernet.begin(uniqueIDtoMAC) == 0)
        {
			DebugService::Error("EthernetConnection::initialize", "Failed to configure Ethernet using DHCP");
		}
        else
        {
			DebugService::Information("EthernetConnection::initialize", "Ethernet connection succesful: " + Ethernet.localIP().toString());
		}
    }

    this->active = active;
}

void EthernetConnection::loop()
{
    if (this->active)
    {
        if (!this->connected)
        {
            if ((Ethernet.linkStatus() == LinkON || Ethernet.linkStatus() == Unknown))
            {
                DebugService::Information("EthernetConnection::loop", "Attempting to connect to Ethernet:");

                if (ConfigurationService::ethernetUseIPSettings)
                {
                    IPAddress ip, dns, gateway, subnet;

                    // Parsing IP Address
                    if (StringUtils::parseIPAddress(ConfigurationService::ethernetIPAdress, ip) == false) {
                        DebugService::Error("EthernetConnection::loop", "Failed to parse IP address.");
                        return;
                    }

                    // Parsing DNS (if provided)
                    bool hasDns = StringUtils::parseIPAddress(ConfigurationService::ethernetDNS, dns);

                    // Parsing Gateway (if provided)
                    bool hasGateway = StringUtils::parseIPAddress(ConfigurationService::ethernetStandardGateway, gateway);

                    // Parsing Subnet (if provided)
                    bool hasSubnet = StringUtils::parseIPAddress(ConfigurationService::ethernetSubnet, subnet);

                    // Determine which begin() method to use based on what's available
                    if (hasSubnet && hasGateway && hasDns) {
                        Ethernet.begin(uniqueIDtoMAC, ip, dns, gateway, subnet);
                    }
                    else if (hasGateway && hasDns) {
                        Ethernet.begin(uniqueIDtoMAC, ip, dns, gateway);
                    }
                    else if (hasDns) {
                        Ethernet.begin(uniqueIDtoMAC, ip, dns);
                    }
                    else if (hasGateway) {
                        Ethernet.begin(uniqueIDtoMAC, ip, gateway, gateway);
                    }
                    else {
                        Ethernet.begin(uniqueIDtoMAC, ip);
                    }

                    DebugService::Information("EthernetConnection::loop", "Static IP set: " + Ethernet.localIP().toString());
                }
                else if (Ethernet.begin(uniqueIDtoMAC) == 0)
                {
                    DebugService::Error("EthernetConnection::loop", "Failed to configure Ethernet using DHCP");
                    // Check for Ethernet hardware present
                    if (Ethernet.hardwareStatus() == EthernetNoHardware)
                    {
                        DebugService::Error("EthernetConnection::loop", "Ethernet shield was not found.  Sorry, can't run without hardware. :(");
                        return;
                    }
                    if (Ethernet.linkStatus() == LinkOFF)
                    {
                        DebugService::Warning("EthernetConnection::loop", "Ethernet cable is not connected.");
                    }
                    this->connected = false;
                }
                else
                {
                    DebugService::Information("EthernetConnection::loop", "DHCP assigned IP " + Ethernet.localIP().toString());

                    DebugService::Information("EthernetConnection::loop", "Hardware status: " + String(Ethernet.hardwareStatus()));
                    DebugService::Information("EthernetConnection::loop", "Link status: " + String(Ethernet.linkStatus()));
                    DebugService::Information("EthernetConnection::loop", "Gateway: " + Ethernet.gatewayIP().toString());
                    DebugService::Information("EthernetConnection::loop", "DNS: " + Ethernet.dnsServerIP().toString());
                    DebugService::Information("EthernetConnection::loop", "Subnet: " + Ethernet.subnetMask().toString());
                    delay(1000);
                    Ethernet.maintain();
                    this->connected = true;
                    DebugService::Information("EthernetConnection::loop", "Client connected status: " + String(client.connected()));
                    DebugService::Information("EthernetConnection::loop", "Client status: " + String(client.status()));
                }
            }
            else if (Ethernet.linkStatus() == LinkOFF)
            {
                DebugService::Warning("EthernetConnection::loop", "Ethernet cable is not connected.");
                this->connected = false;
            }
        }
    }
}

void EthernetConnection::maintain()
{
    if (this->connected)
    {
		int result = Ethernet.maintain();

        if (result == 1)
        {
			DebugService::Information("EthernetConnection::maintain", "Connection lost");
			this->connected = false;
		}
        else if (result == 2)
        {
			DebugService::Information("EthernetConnection::maintain", "Renewing IP");
		}
        else if (result == 3)
        {
			DebugService::Information("EthernetConnection::maintain", "Rebinding IP");
		}
        else if (result == 4)
        {
			DebugService::Information("EthernetConnection::maintain", "Lease failed");
            this->connected = false;
		}
	}
}

Client* EthernetConnection::getClient()
{
	return &this->client;
}

long EthernetConnection::getEpochTime()
{
        long timeRecieved = 0;
        if (!this->timeClient)
        {
            DebugService::Information("EthernetConnection::getEpochTime", "Synchronizing time with: " + ConfigurationService::NTPUrl);
            this->timeClient = new NTPClient(udpClient, ConfigurationService::NTPUrl.c_str());
        }
        this->timeClient->begin();
        if (this->timeClient->update())
        {
            timeRecieved = timeClient->getEpochTime();
        }
        else
        {
            DebugService::Information("EthernetConnection::getEpochTime", "Failed updating time");
        }
        this->timeClient->end();
        return timeRecieved;

}

void EthernetConnection::resetConnection()
{
    this->connected = false;
}

String EthernetConnection::getConnectionDetail()
{
    String details = "";  // Specify the connection type.

    if (this->connected)
    {
        IPAddress localIP = Ethernet.localIP();
        details += "Status: Connected, ";  // Show connection status.
        details += "IP Address: " + String(localIP[0]) + "." + String(localIP[1]) + "."
            + String(localIP[2]) + "." + String(localIP[3]);
    }
    else
    {
        details += "Status: Not connected";
    }

    return details;
}

int EthernetConnection::getStrength()
{
    return 100;
}

void EthernetConnection::generateMacFromID() {
    // Retrieve the unique ID
    String id = UniqueIDService::getUniqueID();

    // We'll skip the 'ID' prefix and take the last 12 characters (6 bytes worth) 
    // for our MAC address, as the latter part is more likely to be unique.
    id = id.substring(id.length() - 12);

    for (int i = 0; i < 6; i++) {
        // Each byte in MAC address is derived from two characters in the ID
        String bytePart = id.substring(i * 2, (i * 2) + 2);

        // Convert the 2-character substring to a byte
        uniqueIDtoMAC[i] = (byte)strtoul(bytePart.c_str(), NULL, 16);
    }

    // Ensure it's a locally administered address
    uniqueIDtoMAC[0] |= 0x02;
    uniqueIDtoMAC[0] &= ~0x01;
}
