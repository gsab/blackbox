#pragma once

#include <ConnectionReport.h>

void ConnectionReport::startSession(bool connected) {
    this->connected = connected;
    startTime = millis();
    connectionAttempts = 0;
    resets = 0;
    connectionTime = 0;
    bestStrength = std::numeric_limits<int>::min();
    worstStrength = std::numeric_limits<int>::max();
    totalStrength = 0;
    strengthCount = 0;
}

void ConnectionReport::endSession() {
    endTime = millis();
    logReport();
}

void ConnectionReport::addConnectionAttempt() {
    connectionAttempts++;
}

void ConnectionReport::addReset() {
    resets++;
}

void ConnectionReport::setConnectionStrength(int strength) {
    bestStrength = strength;
    worstStrength = strength;
    totalStrength = strength;
    strengthCount = 1;
}

void ConnectionReport::updateConnectionStrength(int strength) {
    if (strength > bestStrength) bestStrength = strength;
    if (strength < worstStrength) worstStrength = strength;
    totalStrength += strength;
    strengthCount++;
}

void ConnectionReport::logReport() {
    String report;
    if (connected) {
        unsigned long duration = (endTime - startTime) / 1000; // convert to seconds
        int averageStrength = strengthCount ? totalStrength / strengthCount : 0;
        report = "Connected for " + String(duration) + " seconds\n" +
            "Best connection strength: " + String(bestStrength) + "\n" +
            "Worst connection strength: " + String(worstStrength) + "\n" +
            "Average connection strength: " + String(averageStrength);
    }
    else {
        unsigned long reconnectDuration = (endTime - startTime) / 1000; // convert to seconds
        report = "Not connected\nAttempts: " + String(connectionAttempts) + "\n" + "Resets: " + String(resets) + "\n" + "Time to reconnect: " + String(reconnectDuration) + " seconds";
    }
    DebugService::Information("ConnectionService::logReport", report,true);
}