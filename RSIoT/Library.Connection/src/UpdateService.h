#pragma once
#include <DebugService.h>
#include <WString.h>
#include <Update.h>
#include <MemoryServiceFAT.h>
#include <SerialFlash.h>

class UpdateService {
public:
	static void execOTAFromMemory(Client* client, String host, int port, String installationNumber);
private:
	static String getHeaderValue(String header, String headerName);
	static void updateFromFS();
	static void performUpdate(Stream& updateSource, size_t updateSize);
	static void printPercent(uint32_t readLength, uint32_t contentLength);
};