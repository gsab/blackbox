/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#ifndef SerialAT
#define TINY_GSM_MODEM_SIM7080
#define SerialAT Serial2
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200

#ifndef TINY_GSM_RX_BUFFER
#define TINY_GSM_RX_BUFFER 2048
#endif
#endif // !SerialAT


#include <Client.h>
#include <TinyGsmClient.h>
#include <RTCLib.h>
#include <DebugService.h>
#include <ConfigurationService.h>
#include <WString.h>
#include <IOStatus.h>

class GSMConnection {
public:
	void initialize(bool active);
	bool RestartModem();
	void loop(String apnAdress, String apnUser, String apnPassword);
	void maintain();
	Client* getClient();
	long getEpochTime();
	int getStrength();
	bool connected;
	void resetConnection();
	String getConnectionDetail();
	bool active = false;
	
private:
	bool firstRun = true;
	bool noSimCard = false;
	bool modemRestarting = false;
	int currentReception = 0;
	bool networkAvailable = false;
	bool ntpTimeSuccess = false;
	bool forceReset = false;

	int loopCounter = 100;
	long lastConnectedOrReset = 0;
	
	int currentNetworkMode = 2;
	long getNTPTime();
	long getHttpTime();
	bool modemStarted = false;
	bool checkingTime = false;

	int maintainCounter = 0;

	long extractUnixTime(String data);
	const int udpCid = 11;
	void sendNTPRequestPacket();
	String sendATCommand(String command, int timeout, String expectedResult);
	uint32_t extractTime(const char* timeData);

	TinyGsm modem = TinyGsm(SerialAT);
	TinyGsmClient client = TinyGsmClient(modem);
	//TinyGsmClient httpClient = TinyGsmClient(modem);
	
};
