#include "UpdateService.h"

void UpdateService::execOTAFromMemory(Client* client, String host, int port, String installationNumber)
{
	String bin = "/" + installationNumber + "/data/download/rsiot.bin"; // bin file name with a slash in front.

	if (ConfigurationService::UpdateURLOverride != "")
	{
		int index = ConfigurationService::UpdateURLOverride.indexOf("://");  // Find the protocol separator
		if (index >= 0) {
			// Skip the protocol separator and find the next slash
			index = ConfigurationService::UpdateURLOverride.indexOf("/", index + 3);
			if (index >= 0) {
				// Extract the host and file-url
				host = ConfigurationService::UpdateURLOverride.substring(8, index);
				bin = ConfigurationService::UpdateURLOverride.substring(index);
			}
		}
	}
	DebugService::Information("UpdateService::execOTAFromMemory", "Hostname " + host);
	DebugService::Information("UpdateService::execOTAFromMemory", "Binpath " + bin);

	if (!client->connect(host.c_str(), port))
	{
		Serial.println(" fail");
		delay(10000);
		return;
	}
	Serial.println(" OK");

	client->print(String("GET ") + bin + " HTTP/1.0\r\n");
	client->print(String("Host: ") + host + "\r\n");
	client->print("Connection: close\r\n\r\n");

	long timeout = millis();
	while (client->available() == 0)
	{
		if (millis() - timeout > 5000L)
		{
			Serial.println(">>> Client Timeout !");
			client->stop();
			delay(10000L);
			return;
		}
	}

	Serial.println("Reading header");
	uint32_t contentLength = 1900000;

	while (client->available())
	{
		String line = client->readStringUntil('\n');
		line.trim();
		line.toLowerCase();
		if (line.startsWith("content-length:"))
		{
			contentLength = line.substring(line.lastIndexOf(':') + 1).toInt();
		}
		else if (line.length() == 0)
		{
			break;
		}
	}

	File file = MemoryServiceFAT::createOpenFile("/update.bin");

	try
	{
		Serial.println("File returned");
		timeout = millis();
		uint32_t readLength = 0;

		unsigned long timeElapsed = millis();

		unsigned int failCounter = 0;

		while (readLength < contentLength && client->connected() && millis() - timeout < 60000L)
		{
			int i = 0;
			while (client->available())
			{
				if (!file.print(char(client->read())))
				{
					failCounter++;
					/*if(failCounter % 10 || failCounter == 0)
						DebugService::Information("Update progress", "Failed during read " + String(failCounter) +" times");*/
				}
				else
				{
					readLength++;
				}
				if (readLength % (contentLength / 100) == 0)
				{
					DebugService::Information("Update progress", String(readLength) + "Bytes");
				}
				timeout = millis();
			}
			delay(10);
		}

		timeElapsed = millis() - timeElapsed;

		DebugService::Information("Update progress", "File downloaded in " + String(timeElapsed) + "ms");


		file.close();

		UpdateService::updateFromFS();
	}
	catch (const std::exception & ex)
	{
		DebugService::Error("Update progress", "Download failed!");

		file.close();

		MemoryServiceFAT::removeFile("/update.bin");

	}
}

// Utility to extract header value from headers
String UpdateService::getHeaderValue(String header, String headerName)
{
	return header.substring(strlen(headerName.c_str()));
}



void UpdateService::updateFromFS()
{
	File updateBin = MemoryServiceFAT::loadFile("/update.bin");
	if (updateBin)
	{
		size_t updateSize = updateBin.size();
		if (updateSize > 0)
		{
			Serial.println("Starting update");
			performUpdate(updateBin, updateSize);
		}
		else
		{
			Serial.println("Error, file is empty");
		}
		updateBin.close();

		MemoryServiceFAT::removeFile("/update.bin");

		DebugService::Warning("UpdateService::updateFromFS", "Reset triggered!");
		ESP.restart();
	}
	else
	{
		Serial.println("no such binary");
	}
}

void UpdateService::performUpdate(Stream& updateSource, size_t updateSize)
{
	if (Update.begin(updateSize))
	{
		size_t written = Update.writeStream(updateSource);
		if (written == updateSize)
		{
			Serial.println("Writes : " + String(written) + " successfully");
		}
		else
		{
			Serial.println("Written only : " + String(written) + "/" + String(updateSize) + ". Retry?");
		}
		if (Update.end())
		{
			Serial.println("OTA finished!");
			if (Update.isFinished())
			{
				Serial.println("Restart ESP device!");
				
			}
			else
			{
				Serial.println("OTA not fiished");
			}
		}
		else
		{
			Serial.println("Error occured #: " + String(Update.getError()));
		}
	}
	else
	{
		Serial.println("Cannot begin update");
	}
}

void UpdateService::printPercent(uint32_t readLength, uint32_t contentLength)
{
	// If we know the total length
	if (contentLength != -1)
	{
		Serial.print("\r ");
		Serial.print((100.0 * readLength) / contentLength);
		Serial.print('%');
	}
	else
	{
		Serial.println(readLength);
	}
}


