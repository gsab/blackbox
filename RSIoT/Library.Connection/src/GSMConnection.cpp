#ifndef SerialAT
#define TINY_GSM_MODEM_SIM7080
#define SerialAT Serial2
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200
#endif // !SerialAT

#include "GSMConnection.h"

void GSMConnection::initialize(bool active)
{
	//uint32_t modemSpeed = TinyGsmAutoBaud(SerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);
	
	//DebugService::Information("initializeGSM", "Initializing modem... ");

	this->active = active;
}

bool GSMConnection::RestartModem()
{
	this->connected = false;
	modemRestarting = true;

	DebugService::Information("initializeGSM", "Test AT ");
	if (!modem.testAT(1000))
	{
		DebugService::Information("initializeGSM", "Modem not started ");
		if (ConfigurationService::newHardware)
		{
			DebugService::Information("initializeGSM", "Send start ON ");
			IOStatus::Sim_Power = true;
			delay(1000);
			DebugService::Information("initializeGSM", "Send start OFF ");
			IOStatus::Sim_Power = false;
		}
		else
		{
			digitalWrite(4, HIGH);
			delay(1000);
			digitalWrite(4, LOW);
		}
		//delay(5000);
	}
	else
	{
		DebugService::Information("initializeGSM", "Modem is started ");

		if (!this->forceReset &&this->modem.isGprsConnected())
		{
			DebugService::Information("initializeGSM", "Modem is started and connected, using current connection.");
			modemStarted = true;
			networkAvailable = true;
			DebugService::GSMStatus = "Connected to internet";
			DebugService::Information("GSMConnection::loop", "Connected to GPRS!");
			modem.streamClear();
			this->connected = true;
			this->lastConnectedOrReset = millis();
			modemRestarting = false;
			return true;
		}
		else
		{
			this->modem.restart();
		}
		//delay(1000);
	}
	this->forceReset = false;
	int attempt = 20;

	while (!modem.testAT(1000))
	{
		delay(10);

		attempt -= 1;

		if (attempt == 0)
		{
			DebugService::Error("GSMConnection::RestartModem", "Cannot communicate with modem!");
			firstRun = true;
			modemRestarting = false;
			return false;
		}
	}
	modemStarted = true;

	String name = this->modem.getModemName();
	DebugService::Information("GSMConnection::loop Modem Name:", name);
	String modemInfo = modem.getModemInfo();
	DebugService::Information("initializeGSM", "Modem Info: " + modemInfo);

	DebugService::Information("initializeGSM", "SIM Status: " + String(modem.getSimStatus()));

	if (ConfigurationService::simPin && modem.getSimStatus() != 3)
	{ 
		DebugService::Information("initializeGSM", "Unlocking sim");
		modem.simUnlock(ConfigurationService::simPin.c_str()); 
	}

	DebugService::Information("initializeGSM", "SIM Status: " + String(modem.getSimStatus()));

	this->modem.factoryDefault();
	delay(1000);
	this->modem.setPreferredMode(1);
	delay(1000);
	this->modem.setNetworkMode(currentNetworkMode);
	delay(1000);
	

	String simCardCCID = this->modem.getSimCCID();

	if (simCardCCID == "ERROR")
	{
		noSimCard = true;
	}
	else
	{
		noSimCard = false;
	}
	DebugService::Information("initializeGSM", "Sim card CCID: " + simCardCCID);
	modemRestarting = false;
	return true;
}

void GSMConnection::resetConnection()
{
	lastConnectedOrReset = millis();

	DebugService::Warning("GSMConnection::resetConnection", "Reset connection called!");

	if (this->active)
	{

		if (this->currentNetworkMode != 38)
		{
			DebugService::Information("GSMConnection::resetConnection", "Switching to LTE");
			this->currentNetworkMode = 38;
		}
		else
		{
			DebugService::Information("GSMConnection::resetConnection", "Switching to 2G/3G");
			this->currentNetworkMode = 2;
		}


		this->connected = false;
		this->modemStarted = false;
		this->forceReset = true;
	}
}

String GSMConnection::getConnectionDetail()
{
	String statusMessage = "";

	if (!modemStarted)
	{
		return "Starting modem.";
	}

	if (noSimCard)
	{
		return "No SIM card detected.";
	}

	if (modemRestarting)
	{
		return "Modem restarting.";
	}

	if (!networkAvailable)
	{
		return "Looking for network";
	}

	if (connected)
	{
		statusMessage += "Connected!";
		

		// Mapping reception value to descriptive text
		String receptionDescription;
		if (currentReception <= 5) receptionDescription = "Extremely poor";
		else if (currentReception <= 10) receptionDescription = "Very weak";
		else if (currentReception <= 15) receptionDescription = "Weak";
		else if (currentReception <= 20) receptionDescription = "Good";
		else if (currentReception <= 30) receptionDescription = "Excellent";
		else receptionDescription = "outstanding";

		statusMessage += " Reception: " + receptionDescription;
		return statusMessage;

	}
	else
	{
		return "Not connected";
	}
}

void GSMConnection::loop(String apnAdress, String apnUser, String apnPassword)
{
	if (active)
	{
		if (this->firstRun)
		{
			DebugService::Information("initializeGSM", "Initializing modem... ");
			DebugService::GSMStatus = "Initializing modem...";
			SerialAT.begin(115200, SERIAL_8N1, 22, 5);
			DebugService::Information("initializeGSM", "Serial started ");
			if (!this->RestartModem())
			{
				DebugService::Error("GSMConnection::loop", "Cannot communicate with modem!");
				return;
			}
			this->firstRun = false;
		}

		if (!modemStarted)
		{
			this->RestartModem();
		}

		if (noSimCard)
		{
			DebugService::Warning("GSMConnection::loop", " No simcard!");
			DebugService::GSMStatus = "No simcard!";
			this->connected = false;
			sleep(5000);
			RestartModem();
			return;
		}

		if (this->connected == false)
		{

			if (loopCounter <= 0)
			{
				String registrationStatus = String(this->modem.getRegistrationStatus());
				DebugService::Information("GSMConnection::loop", "Trying to connect to network connected status: " + String(connected) + " isGprsConnected status : " + String(this->modem.isGprsConnected() + "  Network Mode: " + String(registrationStatus)));
			}
			DebugService::GSMStatus = "Waiting for network!";
			
			this->modem.maintain();

			if (!this->modem.isNetworkConnected())
			{
				this->connected = false;
				networkAvailable = false;
			}
			else {
				networkAvailable = true;
				DebugService::Information("GSMConnection::loop", "Connecting to " + apnAdress);
				DebugService::GSMStatus = "Connecting to " + apnAdress;
				DebugService::Information("GSMConnection::loop", "Operator " + String(this->modem.getOperator()));
				DebugService::GSMSignalNetworkName = String(this->modem.getOperator());

				this->currentReception = this->modem.getSignalQuality();
				DebugService::Information("GSMConnection::loop", "Signal strength " + String(this->currentReception));
				
				DebugService::GSMSignalStrength = String(this->currentReception);

				this->client.setTimeout(30000);
				if (this->modem.gprsConnect(apnAdress.c_str(), apnUser.c_str(), apnPassword.c_str()))
				{
					DebugService::GSMStatus = "Connected to internet";
					DebugService::Information("GSMConnection::loop", "Connected to GPRS!");
					modem.streamClear();
					this->connected = true;
					this->lastConnectedOrReset = millis();
				}
				else
				{
					DebugService::GSMStatus = "Failed to start gprs!";

					DebugService::Information("GSMConnection::loop", "Failed to start GPRS!");
					this->connected = false;
				}
			}

			int timeLeft = 600 - (TimeUtils::checkTimeDifferenceMillis(lastConnectedOrReset, millis()) / 1000);

			if (TimeUtils::checkTimeExpiredMillis(this->lastConnectedOrReset, millis(), 600000))
			{
				DebugService::Error("GSMConnection::loop", "Could not connect within 10min. restarting modem");
				this->resetConnection();

			}
			else if (this->connected == false && loopCounter <= 0)
			{
				DebugService::Information("GSMConnection::loop", "Could not connect. Modem reset will occur in " + String(600 - (TimeUtils::checkTimeDifferenceMillis(lastConnectedOrReset,millis())/1000)) + "s");
			}

		}
		else
		{
			this->lastConnectedOrReset = millis();
		}
		

	}
	if (loopCounter <= 0)
		loopCounter = 100;
	else
		loopCounter -= 1;
	
}

void GSMConnection::maintain()
{

	if (this->connected && this->modem.isGprsConnected())
	{
		this->modem.maintain();
		
		maintainCounter++;
		if (maintainCounter == 10)
		{
			this->currentReception = this->modem.getSignalQuality();

			DebugService::GSMSignalStrength = String(this->currentReception);

			maintainCounter = 0;
		}
	}
	else if(this->connected)
	{
		this->connected = false;
		
		DebugService::Information("GSMConnection::maintain", "Maintaining connection detected lost connection!");
	}

}

Client* GSMConnection::getClient()
{
	return &this->client;
}

long GSMConnection::extractUnixTime(String data) {
	// Find the start of the JSON part
	int jsonStart = data.indexOf('{');
	if (jsonStart == -1) return -1; // JSON start not found

	// Extract JSON substring
	String json = data.substring(jsonStart);

	// Find the "unixtime" field
	String unixTimeKey = "\"unixtime\":";
	int unixTimeStart = json.indexOf(unixTimeKey);
	if (unixTimeStart == -1) return -1; // Unix time key not found

	// Calculate the start position of the actual unix time value
	unixTimeStart += unixTimeKey.length();

	// Find the end of the number (comma or end of string)
	int unixTimeEnd = json.indexOf(',', unixTimeStart);
	if (unixTimeEnd == -1) unixTimeEnd = json.length();

	// Extract the unix time string
	String unixTimeString = json.substring(unixTimeStart, unixTimeEnd);

	// Convert to long
	long unixTime = unixTimeString.toInt();

	if (unixTime > 2051222400 || unixTime <= 0)
	{
		DebugService::Error("GSMConnection::getHTTPTime", "Epoch time is invalid, returning 0, http responce: " + data);

		return 0;
	}

	return unixTime;
}

long GSMConnection::getEpochTime()
{
	int year3 = 0;
	int month3 = 0;
	int day3 = 0;
	int hour3 = 0;
	int min3 = 0;
	int sec3 = 0;
	float timezone = 0;

	//String error = modem.ShowNTPError(modem.NTPServerSync(ConfigurationService::NTPUrl));

	long returnTime = getNTPTime();

	DebugService::Information("GSMConnection::getEpochTime", "return time " + String(returnTime) +" ntpTimeSuccess " + String(ntpTimeSuccess));

	if (returnTime <= 0 && !ntpTimeSuccess)
	{
		DebugService::Information("GSMConnection::getEpochTime", "Unable to sync time thru NTP.");

		return getHttpTime();
	}
	else
	{
		ntpTimeSuccess = true;
	}

	return returnTime;
}

int GSMConnection::getStrength()
{
	if (this->currentReception == 0)
	{
		return 0;
	}


	return round((this->currentReception/35.0)*100);
}

long GSMConnection::getHttpTime()
{
	String host = "worldtimeapi.org";
	String service = "/api/timezone/GMT";

	if (!client.connect(host.c_str(), 80))
	{
		Serial.println(" fail");
		delay(10000);
		return 0;
	}
	long start = millis();
	client.print(String("GET ") + service + " HTTP/1.0\r\n");
	client.print(String("Host: ") + host + "\r\n");
	client.print("Connection: close\r\n\r\n");

	long timeout = millis();
	while (client.available() == 0)
	{
		if (millis() - timeout > 5000L)
		{
			Serial.println(">>> Client Timeout !");
			client.stop();
			delay(10000L);
			return 0;
		}
	}
	long timeForAnswer = millis();
	String result = "";

	while (client.available())
	{
		result += client.readStringUntil('}');
	}
	DebugService::Information("GSMConnection::getHttpTime", "Recieved package: " + result);
	DebugService::Information("GSMConnection::getHttpTime", "Request took  " + String(timeForAnswer -start) + "ms read answer took  " + String(millis()- timeForAnswer) + "ms");

	return extractUnixTime(result);
}

long GSMConnection::getNTPTime() {
	uint32_t epochTime = 0;
	//Close connection if it is open from before
	sendATCommand("AT+CACLOSE=" + String(udpCid), 1000,"OK");
	//Register a connection ID
	sendATCommand("AT+CACID=" + String(udpCid), 1000, "OK");
	//Opening UDP connection...
	sendATCommand("AT+CAOPEN=" + String(udpCid) + ",0,\"UDP\",\"" + ConfigurationService::NTPUrl + "\"," + String(123), 1000, "OK");
	//Sending NTP request packet...
	sendATCommand("AT+CASEND=" + String(udpCid) + ",48", 1000,">");
	sendNTPRequestPacket();
	
	String response = "";
	bool foundTime = false;

	//Receiving NTP response packet...
	unsigned long startMillis = millis();
	while (millis() - startMillis < 5000 && millis() >= startMillis) { // 5 seconds timeout handle if request is done just when millis overflows
		response = sendATCommand("AT+CARECV=11,48", 100,"");
		if (response.indexOf("+CARECV: 48,") > 0) {
			foundTime = true;
			break;
		}
		delay(10);
	}

	if (foundTime)
	{
		//Succesfully returned ntp-packet
		int indexOfResponse = response.indexOf("+CARECV: 48,");
		//Extracting time from response
		String line = response.substring(indexOfResponse + 12);
		line.trim();
		epochTime = extractTime(line.c_str());
	}

	//Closing UDP connection...
	sendATCommand("AT+CACLOSE=" + String(udpCid), 100,"OK");

	if (epochTime <= 0)
	{
		DebugService::Error("GSMConnection::getNTPTime", "Unable to get NTP time, returning 0");
		return 0;
	}
	return epochTime;
}

void GSMConnection::sendNTPRequestPacket() {

	uint8_t ntpPacket[48] = { 0x1B, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	for (int i = 0; i < 48; i++) {
		SerialAT.write(ntpPacket[i]);
	}

}

String GSMConnection::sendATCommand(String command, int timeout, String expectedResult) {
	SerialAT.println(command);
	long int time = millis();
	String response = "";
	while ((time + timeout) > millis()) {
		while (SerialAT.available()) {
			char c = SerialAT.read();
			response += c;

			if (expectedResult != "" && response.indexOf(expectedResult) >= 0) {
				return response;
			}
		}
	}
	return response;
}

uint32_t GSMConnection::extractTime(const char* timeData) {
	const uint32_t seventyYears = 2208988800UL; // NTP epoch starts in 1900, Unix in 1970

	// The NTP time starts at byte 40 in the response
	unsigned long highWord = word(timeData[40], timeData[41]);
	unsigned long lowWord = word(timeData[42], timeData[43]);
	// combine the four bytes (two words) into a long integer
	// this is NTP time (seconds since Jan 1 1900):
	unsigned long secsSince1900 = highWord << 16 | lowWord;

	long epoch = secsSince1900 - seventyYears;


	uint32_t june2024 = 1717286400; // June 1, 2024
	uint32_t jan2035 = 2051222400; // January 1, 2035
	if (epoch >= june2024 && epoch <= jan2035) {
		//"Epoch time within valid range
	}
	else {
		DebugService::Error("GSMConnection::extractTime", "Epoch time from NTP out of valid range " + String(epoch));
		epoch = 0;
	}

	return epoch;
}