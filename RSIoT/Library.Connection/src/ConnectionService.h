/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <EthernetConnection.h>
#include <WifiConnection.h>
#include <GSMConnection.h>

#include <DebugService.h>
#include <WString.h>
#include <ArduinoJson.h>
#include <UpdateService.h>
#include <ConfigurationService.h>
#include <SSLClient.h>
#include <SSLCertificate.h>
#include <TimeUtils.h>
#include <IOStatus.h>
#include <ConnectionReport.h>

#define INTERFACE_ETHERNET		0
#define INTERFACE_WIFI			1
#define INTERFACE_GSM			2
#define INTERFACE_NONE			-1

class ConnectionService {
public:
	void initialize(String version, ESP32Time* rtc);
	void loop(bool onlyWifi = false);
	void maintain();
	Client* getClient();
	void createSSLClient(Client* newClient);
	bool getEpochTime(long* result);
	bool connected;
	void resetConnection();
	void handleMessage(String topic, String payload, unsigned int length);
	int getConnectedInterfaceType();
	String getConnectedInterfaceDetails();
private:
	String version = "";
	long lastCertificateCheck = -800000;
	EthernetConnection ethernetCon = EthernetConnection();
	WifiConnection wifi = WifiConnection();
	GSMConnection gsm = GSMConnection();
	bool wifiInitialized = false;
	bool ethernetInitialized = false;
	bool gsmInitialized = false;
	ConnectionReport connectionReport;
	ESP32Time* rtc;
	Client* activeClient;
	SSLClient* sslClient;
	
};

