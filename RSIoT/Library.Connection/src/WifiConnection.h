/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WiFi.h>
#include <NTPClient.h>
#include <WString.h>
#include <DebugService.h>
#include <ConfigurationService.h>
#include <UniqueIDService.h>
#include <DNSServer.h>

class WifiConnection{
public:
	void initialize(bool active);
	void loop();
	void handleConfigurationMode();
	void maintain();
	void enterConfigurationMode();
	void StartAP(String apName, String apPass, IPAddress localIP);
	void StopAP();
	Client* getClient();
	long getEpochTime();
	int getStrength();
	bool connected;
	void resetConnection();
	String getConnectionDetail();
	bool active = false;
	
private:
	WiFiClient client = WiFiClient();
	WiFiUDP udpClient = WiFiUDP();
	DNSServer dnsServer = DNSServer();
	NTPClient* timeClient;
	bool apModeActive = false;
	int failCounter = 10;
	void scanNetworks();
	bool networkAvailable = false;
	int apScanNetwork = 30;
	
	bool configAPActive = false;
};