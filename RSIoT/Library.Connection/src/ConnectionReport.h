#pragma once
#include <limits>
#include <WString.h>
#include <Arduino.h>
#include <DebugService.h>

class ConnectionReport {
public:
    void startSession(bool connected);
    void endSession();
    void addConnectionAttempt();
    void addReset();
    void setConnectionStrength(int strength);
    void updateConnectionStrength(int strength);
    void logReport();

private:
    bool connected = false;
    unsigned long startTime = 0;
    unsigned long endTime = 0;
    int connectionAttempts = 0;
    int resets = 0;
    unsigned long connectionTime = 0;
    int bestStrength = std::numeric_limits<int>::min();
    int worstStrength = std::numeric_limits<int>::max();
    int totalStrength = 0;
    int strengthCount = 0;
};