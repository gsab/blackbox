#pragma once

#include <ConnectionService.h>

void ConnectionService::initialize(String version, ESP32Time* rtc) {
    DebugService::Information("ConnectionService initialize", "Initialization started");
    this->rtc = rtc;
    this->version = version;
    if (ConfigurationService::newHardware) {
        this->ethernetCon.initialize(ConfigurationService::ethernetActive);
        DebugService::Information("ConnectionService initialize", "Ethernet initialized");
    }
    this->wifi.initialize(ConfigurationService::wifiActive);
    DebugService::Information("ConnectionService initialize", "Wifi initialized");
    this->gsm.initialize(ConfigurationService::gsmActive);
    DebugService::Information("ConnectionService initialize", "GSM initialized");

    connectionReport.startSession(false);
}

void ConnectionService::loop(bool onlyWifi) {
    bool wasConnected = connected;

    if (ConfigurationService::ethernetActive != ethernetCon.active) {
        ethernetCon.active = ConfigurationService::ethernetActive;
    }
    if (ConfigurationService::wifiActive != wifi.active) {
        wifi.active = ConfigurationService::wifiActive;
    }
    if (ConfigurationService::gsmActive != gsm.active) {
        gsm.active = ConfigurationService::gsmActive;
    }

    if (ConfigurationService::inTestMode || ConfigurationService::configurationWizardComplete ||
        ConfigurationService::VerifyConnection || ConfigurationService::currentConfigurationStep == 12) {

        if (ConfigurationService::ForceLostConnection) {
            this->connected = false;
            return;
        }

        this->wifi.loop();

        if (!onlyWifi) {
            this->gsm.loop(ConfigurationService::APNUrl, ConfigurationService::APNUser, ConfigurationService::APNPassword);
            this->ethernetCon.loop();
        }

        this->connected = ethernetCon.connected || wifi.connected || gsm.connected;

        if (!wasConnected && connected) {
            connectionReport.endSession();
            connectionReport.startSession(true);
            // Log the initial connection strength here
            if (ethernetCon.connected) {
                connectionReport.setConnectionStrength(ethernetCon.getStrength());
            }
            else if (wifi.connected) {
                connectionReport.setConnectionStrength(wifi.getStrength());
            }
            else if (gsm.connected) {
                connectionReport.setConnectionStrength(gsm.getStrength());
            }
        }
        else if (wasConnected && !connected) {
            connectionReport.endSession();
            connectionReport.startSession(false);
        }
        else if (connected) {
            // Update connection strength during the session
            if (ethernetCon.connected) {
                connectionReport.updateConnectionStrength(ethernetCon.getStrength());
            }
            else if (wifi.connected) {
                connectionReport.updateConnectionStrength(wifi.getStrength());
            }
            else if (gsm.connected) {
                connectionReport.updateConnectionStrength(gsm.getStrength());
            }
        }
        else {
            // Log connection attempts and resets during disconnected state
            connectionReport.addConnectionAttempt();
        }
    }
}

void ConnectionService::maintain() {
    if (this->ethernetCon.connected) {
        this->ethernetCon.maintain();
    }
    if (this->wifi.connected || ConfigurationService::inConfigurationMode) {
        this->wifi.maintain();
    }
    if (this->gsm.connected) {
        this->gsm.maintain();
    }
}

Client* ConnectionService::getClient() {
    Client* newClient = NULL;

    if (ConfigurationService::ForceLostConnection)
        return NULL;

    if (this->ethernetCon.connected) {
        newClient = this->ethernetCon.getClient();
    }
    else if (this->wifi.connected) {
        newClient = this->wifi.getClient();
    }
    else if (this->gsm.connected) {
        newClient = this->gsm.getClient();
    }

    if (newClient != NULL) {
        if (!ConfigurationService::DisableHttps) {
            createSSLClient(newClient);
            if (TimeUtils::checkTimeExpiredMillis(lastCertificateCheck, millis(), 600000)) {
                DebugService::Information("ConnectionService::getClient()", "Renew Certificate check date");
                lastCertificateCheck = millis();
                long epoch = rtc->getEpoch();
                if (epoch > 1685350181) {
                    sslClient->setVerificationTime(719528 + epoch / 86400 + 1, 0);
                }
            }
            return this->sslClient;
        }
        else {
            return newClient;
        }
    }
    return NULL;
}

void ConnectionService::createSSLClient(Client* newClient) {
    if (newClient != activeClient) {
        DebugService::Information("ConnectionService::createSSLClient()", "Create new SSL Client");
        this->sslClient = new SSLClient(*newClient, TAs, (size_t)TAs_NUM, 37, this->wifi.connected || this->ethernetCon.connected, 4);
        activeClient = newClient;
    }
}

void ConnectionService::resetConnection() {
    if (ConfigurationService::ForceLostConnection)
        return;

    DebugService::Information("ConnectionService::resetConnection()", "Resetting connection");

    this->ethernetCon.resetConnection();
    this->wifi.resetConnection();
    this->gsm.resetConnection();

    connectionReport.addReset();
}

void ConnectionService::handleMessage(String topic, String payload, unsigned int length) {
    if (String(topic).endsWith("update")) {
        StaticJsonDocument<256> doc;
        deserializeJson(doc, payload.c_str(), length);
        if (doc.containsKey("urloverride")) {
            String urlOverride = doc["urloverride"];
            if (urlOverride != "") {
                ConfigurationService::UpdateURLOverride = urlOverride;
            }
        }
        if (doc.containsKey("version")) {
            String toVersion = doc["version"];
            if (toVersion != version) {
                ConfigurationService::Update = true;
            }
        }
    }
}

int ConnectionService::getConnectedInterfaceType() {
    if (ConfigurationService::ForceLostConnection)
        return INTERFACE_NONE;

    if (this->ethernetCon.connected) {
        return INTERFACE_ETHERNET;
    }
    if (this->wifi.connected) {
        return INTERFACE_WIFI;
    }
    if (this->gsm.connected) {
        return INTERFACE_GSM;
    }
    return INTERFACE_NONE;
}

String ConnectionService::getConnectedInterfaceDetails() {
    if (ConfigurationService::ForceLostConnection)
        return "Not connected (FORCED)";

    if (this->ethernetCon.active) {
        return "Ethernet " + this->ethernetCon.getConnectionDetail();
    }
    if (this->wifi.active) {
        return "WiFi " + this->wifi.getConnectionDetail();
    }
    if (this->gsm.active) {
        return "4G " + this->gsm.getConnectionDetail();
    }
    return "No interfaced selected active";
}

bool ConnectionService::getEpochTime(long* result) {
    long currentTime;
    if (this->ethernetCon.connected) {
        currentTime = this->ethernetCon.getEpochTime();
    }
    else if (this->wifi.connected) {
        currentTime = this->wifi.getEpochTime();
    }
    else if (this->gsm.connected) {
        currentTime = this->gsm.getEpochTime();
    }
    else {
        return false;
    }

    DebugService::Information("ConnectionService::getEpochTime", "Time received " + String(currentTime));

    if (currentTime > 1685350181) {
        *result = currentTime;
        return true;
    }
    else {
        return false;
    }
}