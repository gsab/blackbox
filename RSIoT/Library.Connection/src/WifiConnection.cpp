#include "WifiConnection.h"

void WifiConnection::initialize(bool active)
{
	DebugService::Information("WifiConnection::initialize", "Initializing wifi");

	this->active = active;

	WiFi.setHostname(ConfigurationService::wifiHost.c_str()); //define hostname
	WiFi.config((u32_t)0UL, (u32_t)0UL, (u32_t)0UL, (u32_t)0UL);
	WiFi.setSleep(false);

	if (ConfigurationService::inConfigurationMode)
	{
		IPAddress ip(35, 214, 241, 143);
		this->StartAP(String("RSIoT_" + UniqueIDService::getUniqueID()), "",ip);
		configAPActive = true;
	}
	else
	{
		WiFi.mode(WIFI_MODE_STA);
	}
}

void WifiConnection::loop()
{
	handleConfigurationMode();

	if (this->active && !this->connected && ConfigurationService::wifiSSID != "")
	{
		if (WiFi.status() == WL_CONNECTED)
		{

			if (this->connected == false)
			{
				IPAddress localIP = WiFi.localIP();

				DebugService::Information("WifiConnection::loop", "Connected with IP: " + String(localIP[0]) + "." + String(localIP[1]) + "." + String(localIP[2]) + "." + String(localIP[3]));

				client.setNoDelay(true);
			}

			this->connected = true;
			this->failCounter = 10;
		}
		else
		{
			this->failCounter--;

			if (this->failCounter > 0 && this->connected)
			{
				DebugService::Information("WifiConnection::loop", "Connection status = " + String(WiFi.status()) + " Failcounter = " + String(this->failCounter));
				IPAddress localIP = WiFi.localIP();

				DebugService::WifiIP = String(localIP[0]) + "." + String(localIP[1]) + "." + String(localIP[2]) + "." + String(localIP[3]) + " ";
				DebugService::WifiStatus = "WiFi is connected ";

				
			}
			else
			{
				/*scanNetworks();

				if (this->networkAvailable)
				{*/
					DebugService::Information("WifiConnection::loop", "Connection status = " + String(WiFi.status()));

					if (this->connected)
					{
						DebugService::Information("WifiConnection::loop", "Lost connection!");
						WiFi.disconnect();
					}

					DebugService::Information("WifiConnection::loop", "Wifi not connected, connection to " + ConfigurationService::wifiSSID);

					this->connected = false;

					if (ConfigurationService::wifiUseIPSettings)
					{
						IPAddress ip = IPAddress();
						IPAddress gateway = IPAddress();
						IPAddress subnet = IPAddress();

						if (ip.fromString(ConfigurationService::wifiIPAdress) && gateway.fromString(ConfigurationService::wifiStandardGateway) && subnet.fromString(ConfigurationService::wifiSubnet))
						{
							WiFi.config(ip, gateway, subnet);
						}

					}



					DebugService::Information("Status from WiFi", String(WiFi.begin(ConfigurationService::wifiSSID.c_str(), ConfigurationService::wifiPass.c_str())));

					


					//
					WiFi.waitForConnectResult(20000);

					if (failCounter < -10)
					{
						this->resetConnection();
					
					}

					//delay(5000);
					// Add Connect
				/*}*/
			}
		}
	}
}

void WifiConnection::handleConfigurationMode()
{
	if (ConfigurationService::inConfigurationMode && !this->configAPActive)
	{

		if (apModeActive)
		{
			//DebugService::Information("WifiConnection::loop", "Stopping AP");
			this->StopAP();
			apModeActive = false;
		}

		enterConfigurationMode();
	}

	if (!ConfigurationService::inConfigurationMode && configAPActive)
	{
		this->StopAP();
		configAPActive = false;
	}

	if (!ConfigurationService::inConfigurationMode && ConfigurationService::MQTTBrokerActive && !apModeActive && !ConfigurationService::wifiActive)
	{
		IPAddress ip(192, 168, 4, 1);
		this->StartAP(ConfigurationService::MQTTBrokerSSID, ConfigurationService::MQTTBrokerPassword, ip);
		apModeActive = true;
	}
}

void WifiConnection::maintain()
{
	if (this->connected && WiFi.status() != WL_CONNECTED)
	{
		this->connected = false;
	}

	if (configAPActive)
	{
		dnsServer.processNextRequest();
	}
}

void WifiConnection::enterConfigurationMode()
{
	//DebugService::Information("WifiConnection::loop", "Starting config AP");
	IPAddress ip(35, 214, 241, 143);

	this->StartAP(String("RSIoT_" + UniqueIDService::getUniqueID()), "", ip);
	configAPActive = true;
	//DebugService::Information("WifiConnection::loop", "Starting AP done");
}

void WifiConnection::StartAP(String apName, String apPass, IPAddress localIP)
{
	WiFi.mode(WIFI_MODE_APSTA);
	WiFi.enableAP(true);
	IPAddress subnet(255, 255, 255, 0);
	DebugService::Information("WifiConnection::loop", "Start AP point " + apName);

	if (WiFi.softAPConfig(localIP, localIP, subnet))
	{
		DebugService::Information("WifiConnection::loop", "AP configuration successful.");
	}
	else
	{
		DebugService::Information("WifiConnection::loop", "AP configuration failed!");
	}

	

	bool apStarted = apPass == "" ? WiFi.softAP(apName.c_str()) : WiFi.softAP(apName.c_str(), apPass.c_str());
	if (apStarted)
	{
		DebugService::Information("WifiConnection::loop", "AP started successfully.");
	}
	else
	{
		DebugService::Information("WifiConnection::loop", "Failed to start AP!");
	}

	IPAddress IP = WiFi.softAPIP();
	DebugService::Information("WifiConnection::loop", "AP IP address: " + String(IP));

	if (dnsServer.start(53, "*", IP))
	{
		DebugService::Information("WifiConnection::loop", "DNS server started successfully on IP: " + String(IP));
	}
	else
	{
		DebugService::Information("WifiConnection::loop", "Failed to start DNS server!");
	}
}

void WifiConnection::StopAP()
{
	WiFi.softAPdisconnect();
	WiFi.enableAP(false);
	WiFi.mode(WIFI_MODE_STA);
}

Client* WifiConnection::getClient()
{
	return &this->client;
}


void WifiConnection::resetConnection()
{
	DebugService::Information("WifiConnection::resetConnection", "Connection reset called!");
	if (this->active)
	{
		if (!configAPActive)
		{
			WiFi.disconnect();

			WiFi.mode(WIFI_OFF);
			delay(1000);

			WiFi.mode(WIFI_MODE_STA);
		}
		this->failCounter = 0;
	}
}

String WifiConnection::getConnectionDetail()
{
	if (this->connected)
	{
		IPAddress localIP = WiFi.localIP();
		int rssi = WiFi.RSSI();  // Receive Signal Strength Indicator (RSSI)

		String signalStrength;
		if (rssi > -50)
			signalStrength = "Excellent";
		else if (rssi > -70)
			signalStrength = "Good";
		else if (rssi > -90)
			signalStrength = "Fair";
		else
			signalStrength = "Poor";

		return "Connected with IP: " + String(localIP[0]) + "." + String(localIP[1]) + "." + String(localIP[2]) + "." + String(localIP[3]) + ". Signal strength: " + signalStrength;
	}

	switch (WiFi.status())
	{
	case WL_NO_SHIELD:
		return "No WiFi shield/module present";
	case WL_IDLE_STATUS:
		return "WiFi idle";
	case WL_NO_SSID_AVAIL:
		return "No SSID available";
	case WL_SCAN_COMPLETED:
		return "Scan completed but not connected";
	case WL_CONNECT_FAILED:
		return "Connection failed, possible wrong password";
	case WL_CONNECTION_LOST:
		return "Connection lost";
	case WL_DISCONNECTED:
		return "Disconnected";
	default:
		return "Not connected";
	}
}

long WifiConnection::getEpochTime()
{
	long timeRecieved = 0;
	if (!this->timeClient)
	{
		DebugService::Information("WifiConnection::getEpochTime", "Synchronizing time with: " + ConfigurationService::NTPUrl);
		this->timeClient = new NTPClient(udpClient, ConfigurationService::NTPUrl.c_str());
	}
	this->timeClient->begin();
	if (this->timeClient->update())
	{
		timeRecieved = timeClient->getEpochTime();
	}
	this->timeClient->end();
	return timeRecieved;
}

int WifiConnection::getStrength()
{
	int rssi = WiFi.RSSI();
	int percentage = map(rssi, -90, -30, 0, 100);
	percentage = constrain(percentage, 0, 100);
	return percentage;
}

void WifiConnection::scanNetworks()
{
	if (ConfigurationService::wifiSSID != "")
	{
		DebugService::Information("WifiConnection::scanNetworks", "Scanning for networks");

		int8_t wifiNetworks = WiFi.scanNetworks();

		bool networkFound = false;

		for (int thisNet = 0; thisNet < wifiNetworks; thisNet++) {
			DebugService::Information("WifiConnection::scanNetworks", "SSID: " + String(WiFi.SSID(thisNet)));
			if (String(WiFi.SSID(thisNet)) == ConfigurationService::wifiSSID)
			{
				DebugService::Information("WifiConnection::scanNetworks", "Found network!");
				networkFound = true;
				break;
			}
		}

		this->networkAvailable = networkFound;
	}
}
