/*
 Name:		Library.h
 Created:	11/18/2020 3:21:10 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <EthernetWizNet.h>
#include <NTPClient.h>
#include <WString.h>
#include <DebugService.h>
#include <ConfigurationService.h>
#include <UniqueIDService.h>
#include <SPI.h>
#include <StringUtils.h>

class EthernetConnection {
public:
	void initialize(bool active);
	void loop();
	void maintain();
	Client* getClient();
	long getEpochTime();
	bool connected;
	void resetConnection();
	String getConnectionDetail();
	int getStrength();
	EthernetClient client;
	EthernetUDP udpClient;
	NTPClient* timeClient;
	bool active = false;
private:
	byte uniqueIDtoMAC[6];
	void generateMacFromID();
	bool initialized = false;
};