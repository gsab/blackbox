/*
 Name:		Library.cpp
 Created:	11/25/2020 1:20:38 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "WebServerService.h"

DNSServer dnsServer;

void recvMsg(uint8_t* data, size_t len) {
	WebSerial.println("Received Data...");
	String d = "";
	for (int i = 0; i < len; i++) {
		d += char(data[i]);
	}
	WebSerial.println(d);
}

void WebServerService::initialize(ConnectionService* connectionService, String currentVersion)
{
	this->connectionService = connectionService;
	this->currentVersion = currentVersion;

	//if DNSServer is started with "*" for domain name, it will reply with provided IP to all DNS request
	dnsServer.start(53, "*", WiFi.softAPIP());
	String version = DebugService::CurrentVersion;
	version.replace(".", "");
	String stylesheetname = String("/styles" + version + ".css");

	asyncServer.on(stylesheetname.c_str(), HTTP_GET, [](AsyncWebServerRequest* request) {
		AsyncResponseStream* response = request->beginResponseStream("text/css");
		DebugService::Information("WebServerService", "/styles.css requested");
		//DebugService::Information("WebServerService", styles);
		response->print(styles);
		request->send(response);
		});
	asyncServer.on("/api/status", HTTP_GET, [this](AsyncWebServerRequest* request) {
		apiStatusCallCount++;  // Increment the counter
		String jsonResponse = "{";

		// Assuming your connectionService has methods like isNetworkConnected() that return booleans
		jsonResponse += "\"interfaceStatus\": \"" + String(this->connectionService->getConnectedInterfaceDetails()) + "\",";
		jsonResponse += "\"networkStatus\": \"" + String(this->connectionService->connected ? "connected" : "disconnected") + "\",";
		jsonResponse += "\"rsproductionStatus\": \"" + String(MQTTService::current->connected ? "connected" : "disconnected") + "\",";
		jsonResponse += "\"timeserverStatus\": \"" + String(ConfigurationService::timeIsSet ? "connected" : "disconnected") + "\"";

		jsonResponse += "}";

		request->send(200, "application/json", jsonResponse);
		});

	asyncServer.on("/api/energy", HTTP_GET, [this](AsyncWebServerRequest* request) {
		apiEnergyCallCount++;  // Increment the counter
		DynamicJsonDocument doc(1024);
		if (ConfigurationService::newHardware)
		{
			JsonArray sensors = doc.createNestedArray("energySensors");
			// Sensor 1
			JsonObject sensor1 = sensors.createNestedObject();
			sensor1["inputStatus"] = EnergyService::current->energyDataTriggers[0].currentState;
			sensor1["currentValue"] = String(EnergyService::current->energyDataTriggers[0].lastValue, 1);
			sensor1["thresholdValue"] = EnergyService::current->energyDataTriggers[0].lowerThreshold;
			sensor1["ampereRating"] = EnergyService::current->energyDataTriggers[0].multiplier;
			sensor1["onThresholdCount"] = EnergyService::current->energyDataTriggers[0].onThresholdCount;
			sensor1["offThresholdCount"] = EnergyService::current->energyDataTriggers[0].offThresholdCount;
			sensor1["powerfactor"] = EnergyService::current->energyDataTriggers[0].powerfactor;
			sensor1["referencevoltage"] = EnergyService::current->energyDataTriggers[0].referencevoltage;

			// Sensor 2
			JsonObject sensor2 = sensors.createNestedObject();
			sensor2["inputStatus"] = EnergyService::current->energyDataTriggers[1].currentState;
			sensor2["currentValue"] = String(EnergyService::current->energyDataTriggers[1].lastValue, 1);
			sensor2["thresholdValue"] = EnergyService::current->energyDataTriggers[1].lowerThreshold;
			sensor2["ampereRating"] = EnergyService::current->energyDataTriggers[1].multiplier;
			sensor2["onThresholdCount"] = EnergyService::current->energyDataTriggers[1].onThresholdCount;
			sensor2["offThresholdCount"] = EnergyService::current->energyDataTriggers[1].offThresholdCount;
			sensor2["powerfactor"] = EnergyService::current->energyDataTriggers[1].powerfactor;
			sensor2["referencevoltage"] = EnergyService::current->energyDataTriggers[1].referencevoltage;

			// Sensor 3
			JsonObject sensor3 = sensors.createNestedObject();
			sensor3["inputStatus"] = EnergyService::current->energyDataTriggers[2].currentState;
			sensor3["currentValue"] = String(EnergyService::current->energyDataTriggers[2].lastValue, 1);
			sensor3["thresholdValue"] = EnergyService::current->energyDataTriggers[2].lowerThreshold;
			sensor3["ampereRating"] = EnergyService::current->energyDataTriggers[2].multiplier;
			sensor3["onThresholdCount"] = EnergyService::current->energyDataTriggers[2].onThresholdCount;
			sensor3["offThresholdCount"] = EnergyService::current->energyDataTriggers[2].offThresholdCount;
			sensor3["powerfactor"] = EnergyService::current->energyDataTriggers[2].powerfactor;
			sensor3["referencevoltage"] = EnergyService::current->energyDataTriggers[2].referencevoltage;

			// Serialize JSON document to String
			String jsonResponse;
			serializeJson(doc, jsonResponse);
		
			request->send(200, "application/json", jsonResponse);
		}
		});

	asyncServer.on("/api/inputs", HTTP_GET, [this](AsyncWebServerRequest* request) {
		apiInputsCallCount++;
		DynamicJsonDocument doc(1024);

		JsonArray inputs = doc.createNestedArray("inputs");

		JsonObject input1 = inputs.createNestedObject();
		input1["inputStatus"] = InputService::current->inputTriggers[0].lastKnownStatus;
		JsonObject input2 = inputs.createNestedObject();
		input2["inputStatus"] = InputService::current->inputTriggers[1].lastKnownStatus;
		JsonObject input3 = inputs.createNestedObject();
		input3["inputStatus"] = InputService::current->inputTriggers[2].lastKnownStatus;
		JsonObject input4 = inputs.createNestedObject();
		input4["inputStatus"] = InputService::current->inputTriggers[3].lastKnownStatus;
		JsonObject input5 = inputs.createNestedObject();
		input5["inputStatus"] = InputService::current->inputTriggers[4].lastKnownStatus;
		JsonObject input6 = inputs.createNestedObject();
		input6["inputStatus"] = InputService::current->inputTriggers[5].lastKnownStatus;
		JsonObject input7 = inputs.createNestedObject();
		input7["inputStatus"] = InputService::current->inputTriggers[6].lastKnownStatus;
		JsonObject input8 = inputs.createNestedObject();
		input8["inputStatus"] = InputService::current->inputTriggers[7].lastKnownStatus;


		// Serialize JSON document to String
		String jsonResponse;
		serializeJson(doc, jsonResponse);

		request->send(200, "application/json", jsonResponse);
		});

	asyncServer.on("/api/set", HTTP_POST, [](AsyncWebServerRequest* request) {},
		NULL,
		[this](AsyncWebServerRequest* request, uint8_t* data, size_t len, size_t index, size_t total) {
			// Ensure the incoming data is null-terminated
			apiSetCallCount++;
			uint8_t* data_end = (uint8_t*)malloc(len + 1);
			memcpy(data_end, data, len);
			data_end[len] = '\0'; // Null-terminate the data

			// Convert the raw data to a String, then parse as JSON
			String jsonStr = String((char*)data_end);
			free(data_end); // Free the allocated memory

			DynamicJsonDocument doc(1024);
			deserializeJson(doc, jsonStr);
			JsonObject jsonObj = doc.as<JsonObject>();

			String function = jsonObj["function"];
			String id = jsonObj["id"];

			DebugService::Information("WebServerService", "Function: " + function);
			if (ConfigurationService::newHardware)
			{
				if (function == "setAmpere")
				{
					String ampere = jsonObj["ampere"];
					if (id.startsWith("sensor1"))
					{
						EnergyService::current->energyDataTriggers[0].multiplier = ampere.toFloat();
					}
					else if (id.startsWith("sensor2"))
					{
						EnergyService::current->energyDataTriggers[1].multiplier = ampere.toFloat();
					}
					else if (id.startsWith("sensor3"))
					{
						EnergyService::current->energyDataTriggers[2].multiplier = ampere.toFloat();
					}

					EnergyService::current->sendSettingsToServer();
				}
				else if (function == "changeSetting")
				{
					String value = jsonObj["value"];
					if (id.startsWith("sensor1"))
					{
						setEnergySetting(0, id, value);
					}
					else if (id.startsWith("sensor2"))
					{
						setEnergySetting(1, id, value);
					}
					else if (id.startsWith("sensor3"))
					{
						setEnergySetting(2, id, value);
					}

					EnergyService::current->sendSettingsToServer();
				}
			}

			request->send(200, "text", "{\"result\": \"success\"}");
		});



	asyncServer.addHandler(new CaptiveRequestHandler());//only when requested from AP
	DebugService::AsyncServerInitialized = true;
	Serial.println("IoT Running...");
	asyncServer.begin();

}

void WebServerService::printApiCallMetrics() {

	int allCalls = apiStatusCallCount + apiEnergyCallCount + apiInputsCallCount + apiSetCallCount;

	if (allCalls != apiSummary)
	{
		Serial.println("API Call Metrics:");
		Serial.println(" /api/status: " + String(apiStatusCallCount) + " calls");
		Serial.println(" /api/energy: " + String(apiEnergyCallCount) + " calls");
		Serial.println(" /api/inputs: " + String(apiInputsCallCount) + " calls");
		Serial.println(" /api/set: " + String(apiSetCallCount) + " calls");
		apiSummary = allCalls;
	}
}

void WebServerService::loop()
{
	dnsServer.processNextRequest();

	loopCounter++;

	if (loopCounter == 100)
	{
		if(ConfigurationService::debugLevel >= 2)
			printApiCallMetrics();
		loopCounter = 0;
	}
}

void WebServerService::setEnergySetting(int trigger, String id, String value)
{
	if (ConfigurationService::newHardware)
	{
		if (id.endsWith("thresholdInput"))
		{
			EnergyService::current->energyDataTriggers[trigger].lowerThreshold = value.toDouble();
			EnergyService::current->energyDataTriggers[trigger].upperThreshold = value.toDouble() + 100000;
			EnergyService::current->energyDataTriggers[trigger].savedLocalNotSynced = true;
			DebugService::Information("WebServerService", id + " thresholdInput: " + value);
		}
		else if (id.endsWith("onDelayInput"))
		{
			EnergyService::current->energyDataTriggers[trigger].onThresholdCount = value.toInt();
			EnergyService::current->energyDataTriggers[trigger].savedLocalNotSynced = true;
			DebugService::Information("WebServerService", id + " onDelayInput: " + value + " value set " + String(EnergyService::current->energyDataTriggers[trigger].onThresholdCount));
		}
		else if (id.endsWith("offDelayInput"))
		{
			EnergyService::current->energyDataTriggers[trigger].offThresholdCount = value.toInt();
			EnergyService::current->energyDataTriggers[trigger].savedLocalNotSynced = true;
			DebugService::Information("WebServerService", id + " offDelayInput: " + value);
		}
		else if (id.endsWith("powerfactorInput"))
		{
			EnergyService::current->energyDataTriggers[trigger].powerfactor = value.toDouble();
			EnergyService::current->energyDataTriggers[trigger].savedLocalNotSynced = true;
			DebugService::Information("WebServerService", id + " powerfactor: " + value);
		}
		else if (id.endsWith("voltageInput"))
		{
			EnergyService::current->energyDataTriggers[trigger].referencevoltage = value.toDouble();
			EnergyService::current->energyDataTriggers[trigger].savedLocalNotSynced = true;
			DebugService::Information("WebServerService", id + " referencevoltage: " + value);
		}
	}
}
