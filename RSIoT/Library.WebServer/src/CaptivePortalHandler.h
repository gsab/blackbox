#pragma once
#include <WString.h>
#include <ConnectionService.h>
#include <MemoryService.h>
#include <MemoryServiceFAT.h>
#include <FFat.h>
#include <WiFi.h>
#include <DebugService.h>
#include <ConfigurationService.h>
#include <ESPAsyncWebServer.h>
#include <WebSerial.h>
#include <AsyncTCP.h>
#include <esp_wifi.h>
#include <ESPmDNS.h>
#include <WifiUtils.h>
#include "web_data.h"

class CaptiveRequestHandler : public AsyncWebHandler {
public:
	CaptiveRequestHandler() {}
	virtual ~CaptiveRequestHandler() {}

	bool canHandle(AsyncWebServerRequest* request) {
		//request->addInterestingHeader("ANY");
		return true;
	}

	struct Variable {
		String name;
		String value;
	};

	static String getNextLine(const char** pgm_ptr) {
		String line = "";
		char c;
		while (1) {
			c = pgm_read_byte(*pgm_ptr);
			(*pgm_ptr)++;  // Move to next character
			if (c == '\n' || c == '\0') {
				break;
			}
			line += c;
		}
		return line;
	}

	static void printWebPage(AsyncResponseStream* response, const char* webPageProgmem, const Variable variables[], const int varCount) {
		// Convert the entire PROGMEM string to a String object
		String pageContent = String(webPageProgmem);
		String version = DebugService::CurrentVersion;
		version.replace(".", "");
		String stylesheetname = String("/styles" + version + ".css");

		pageContent.replace("styles.css", stylesheetname);
		// Replace the variables in the entire content
		for (int i = 0; i < varCount; i++) {
			pageContent.replace(variables[i].name, variables[i].value);
		}

		//DebugService::Information("CaptiveRequestHandler::printWebPage", pageContent);
		response->print(pageContent);  // Send the entire content in one go
	}

	static void printWebPage(AsyncResponseStream* response, const char* webPageProgmem) {
		String line;
		const char* pgm_ptr = webPageProgmem;  // Point to the beginning of the PROGMEM page


		while (pgm_read_byte(pgm_ptr) != 0) { // While not end of PROGMEM string
			line = getNextLine(&pgm_ptr);
			response->println(line);
		}

	}

	void updateConfigurationStep(String navigate, bool& error, String& errorMessage)
	{
		switch (ConfigurationService::currentConfigurationStep)
		{
		case 0:
			if (navigate == "next")
			{
				ConfigurationService::currentConfigurationStep = 1;
				if (ConfigurationService::newHardware)
				{
					ConfigurationService::APNUrl = "iot.melita.io";
				}
			}
			break;
		case 1:
			if (navigate == "next" && StringUtils::isNumber(ConfigurationService::InstallationNumber, 6))
			{
				ConfigurationService::currentConfigurationStep = 2;
			}
			else if (navigate == "next")
			{
				//Handle error installationnumber is not in correct format
				error = true;
				errorMessage = "Installationnumber is not in correct format";
				ConfigurationService::currentConfigurationStep = 1;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 0;
			}
			break;
		case 2:
			if (navigate == "next")
			{
				ConfigurationService::currentConfigurationStep = 3;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 1;
			}
			break;

		case 3:
			if (navigate == "cloud")
			{
				ConfigurationService::currentConfigurationStep = 4;
				//TODO Set ports and adresses according to cloud connection
				ConfigurationService::MQTTUrl = ConfigurationService::InstallationNumber + ".rsproduction.se";
				ConfigurationService::MQTTPort = 8883;
				ConfigurationService::DisableHttps = false;
				ConfigurationService::UpdateURL = ConfigurationService::InstallationNumber + ".rsproduction.se";
				ConfigurationService::UpdatePort = 443;
			}
			else if (navigate == "local")
			{
				ConfigurationService::currentConfigurationStep = 5;
				//TODO Set ports and adresses according to local connection
				ConfigurationService::MQTTUrl = "";
				ConfigurationService::MQTTPort = 1883;
				ConfigurationService::DisableHttps = true;
				ConfigurationService::UpdateURL = "";
				ConfigurationService::UpdatePort = 80;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 2;
			}
			break;

		case 4:
			if (navigate == "4G")
			{
				ConfigurationService::currentConfigurationStep = 10;
				ConfigurationService::gsmActive = true;
				ConfigurationService::ethernetActive = false;
				ConfigurationService::wifiActive = false;
			}
			else if (navigate == "local")
			{
				if (ConfigurationService::newHardware)
				{
					ConfigurationService::currentConfigurationStep = 6;
				}
				else
				{
					ConfigurationService::ethernetActive = false;
					ConfigurationService::wifiActive = true;
					ConfigurationService::currentConfigurationStep = 7;
				}
				ConfigurationService::gsmActive = false;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 3;
			}
			break;
		case 5:
			if (navigate == "next")
			{
				ConfigurationService::currentConfigurationStep = ConfigurationService::newHardware? 6 : 7;
				ConfigurationService::UpdateURL = ConfigurationService::MQTTUrl;
				ConfigurationService::UpdatePort = 80;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 3;
			}
			break;
		case 6:
			if (navigate == "wifi")
			{
				ConfigurationService::currentConfigurationStep = 7;
				ConfigurationService::ethernetActive = false;
				ConfigurationService::wifiActive = true;
			}
			else if (navigate == "ethernet")
			{
				ConfigurationService::currentConfigurationStep = 9;
				ConfigurationService::ethernetActive = true;
				ConfigurationService::wifiActive = false;
			}
			else if (navigate == "back")
			{
				if (ConfigurationService::DisableHttps)
				{
					ConfigurationService::currentConfigurationStep = 5;
				}
				else
				{
					ConfigurationService::currentConfigurationStep = 4;
				}

			}
			break;
		case 7:
			if (navigate == "next")
			{
				ConfigurationService::currentConfigurationStep = 8;
			}
			else if (navigate == "back")
			{
				if (ConfigurationService::newHardware)
				{
					//Jump to Select WiFI or Ethernet
					ConfigurationService::currentConfigurationStep = 6;
				}
				else
				{
					//Jump to local server IP settings or Cloud connection settings
					if (ConfigurationService::DisableHttps)
					{
						ConfigurationService::currentConfigurationStep = 5;
					}
					else
					{
						ConfigurationService::currentConfigurationStep = 4;
					}
				}
			}

			break;
		case 8:
			if (navigate == "next")
			{
				ConfigurationService::currentConfigurationStep = 9;
			}
			else if (navigate == "back")
				ConfigurationService::currentConfigurationStep = 7;

			break;
		case 9:
			if (navigate == "no")
			{
				ConfigurationService::VerifyConnection = false;
				ConfigurationService::currentConfigurationStep = 10;
				ConfigurationService::wifiIPAdress = "";
				ConfigurationService::wifiSubnet = "";
				ConfigurationService::wifiStandardGateway = "";
				ConfigurationService::wifiDNS = "";
				ConfigurationService::ethernetIPAdress = "";
				ConfigurationService::ethernetSubnet = "";
				ConfigurationService::ethernetStandardGateway = "";
				ConfigurationService::ethernetDNS = "";
			}
			else if (navigate == "yes")
			{
				ConfigurationService::currentConfigurationStep = 11;
			}
			else if (navigate == "back")
			{
				if (ConfigurationService::wifiActive)
				{
					ConfigurationService::currentConfigurationStep = 8;
				}
				else
				{
					ConfigurationService::currentConfigurationStep = 6 ;
				}
			}

			break;
		case 10:
			if (navigate == "finish")
			{
				ConfigurationService::VerifyConnection = false;
				ConfigurationService::currentConfigurationStep = 12;
			}
			else if (navigate == "timeserver")
			{
				ConfigurationService::VerifyConnection = false;
				ConfigurationService::currentConfigurationStep = 13;
			}
			else if (navigate == "back")
			{
				ConfigurationService::VerifyConnection = false;
				if (ConfigurationService::gsmActive)
				{
					ConfigurationService::currentConfigurationStep = 4;
				}
				else
				{
					ConfigurationService::currentConfigurationStep = 9;
				}
			}

			break;
		case 11:
			if (navigate == "next")
			{
				ConfigurationService::VerifyConnection = false;
				ConfigurationService::currentConfigurationStep = 10;
				ConfigurationService::wifiIPAdress = ConfigurationService::ethernetIPAdress;
				ConfigurationService::wifiSubnet = ConfigurationService::ethernetSubnet;
				ConfigurationService::wifiStandardGateway = ConfigurationService::ethernetStandardGateway;
				ConfigurationService::wifiDNS = ConfigurationService::ethernetDNS;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 9;
			}

			break;
		case 12:
			if (navigate == "finish")
			{
				ConfigurationService::configurationWizardComplete = true;
				ConfigurationService::currentConfigurationStep = 99;
				
			}
			if (navigate == "mainmenu")
			{
				ConfigurationService::configurationWizardComplete = true;
				ConfigurationService::currentConfigurationStep = 99;
			}
			else if (navigate == "back")
			{
				ConfigurationService::currentConfigurationStep = 10;
			}

			break;
		}

	}

	String generateSSIDOptions(const String& ssids) {
		String options = "";
		int startIndex = 0, endIndex = 0;

		while ((endIndex = ssids.indexOf('\n', startIndex)) != -1) {
			options += "<option>" + ssids.substring(startIndex, endIndex) + "</option>";
			startIndex = endIndex + 1;
		}

		// Handling the last SSID (if not ending with \n)
		if (startIndex < ssids.length()) {
			options += "<option>" + ssids.substring(startIndex) + "</option>";
		}

		return options;
	}

	void handleRequest(AsyncWebServerRequest* request) {

		AsyncResponseStream* response = request->beginResponseStream("text/html");

		//Update ConfigurationTime so that it does not time out if user is active in using condiguration
		ConfigurationService::enteredConfigurationMode = millis();

		String readString = "";

		int x = 0;
		bool con = true;
		char c = request->client()->remoteIP();

		String url = request->host();

		int params = request->params();

		String navigate = "";

		String postData = "";

		for (int i = 0;i < params;i++) {
			AsyncWebParameter* p = request->getParam(i);
			
			Serial.printf("GET[%s]: %s\n", p->name().c_str(), p->value().c_str());
			
			if (String(p->name().c_str()) == "navigate")
			{
				navigate = String(p->value().c_str());

				DebugService::Information("WebServer", "Navigate to ---> " + navigate);
			}
			else
			{
				String data = String(p->name().c_str()) + "=" + String(p->value().c_str());

				if (postData != "")
				{
					postData += "&";
				}

				postData += data;

			}

		}

		if (postData != "")
		{

			DebugService::Information("Set Configuration", "Data from webinterface= " + postData);

			setConfiguration(postData);

			MemoryServiceFAT::saveConfiguration();

		}

		if (navigate == "reset")
		{
			DebugService::Information("WebServer", "Reset device");
			ConfigurationService::Reset = true;
			return;
		}

		if (navigate == "factoryreset")
		{
			DebugService::Information("WebServer", "Reset to factory settings");
			MemoryServiceFAT::removeFile("/configuration.conf");
			ConfigurationService::Reset = true;
			return;
		}

		while (con == true)
		{
			bool error = false;
			String errorMessage = "";
			if (ConfigurationService::currentConfigurationStep < 99)
			{
				//RS IoT is in configuration mode

				DebugService::Information("WebServer", "Current configuration step: " + String(ConfigurationService::currentConfigurationStep));
				DebugService::Information("WebServer", "navigate: " + navigate);
				updateConfigurationStep(navigate, error, errorMessage);
				if (navigate != "" && navigate != "reset" && navigate != "factoryreset")
				{
					MemoryServiceFAT::saveConfiguration();
				}
			}
			Variable variables[] = {
			{"%version%", String(DebugService::CurrentVersion)},
			{"%newhardware%", ConfigurationService::newHardware ? "" : "invisible-content"},
			{"%newhardwareBool%", ConfigurationService::newHardware ? "true" : "false"},
			{"%installationid%", ConfigurationService::InstallationNumber},
			{"%unitname%", ConfigurationService::UnitName},
			{"%wifissid%", ConfigurationService::wifiSSID},
			{"%wifipass%", ConfigurationService::wifiPass},
			{"%simpin%", ConfigurationService::simPin},
			{"%ethernetipadress%", ConfigurationService::ethernetIPAdress},
			{"%ethernetstandardgateway%", ConfigurationService::ethernetStandardGateway},
			{"%ethernetdns%", ConfigurationService::ethernetDNS},
			{"%ethernetsubnet%", ConfigurationService::ethernetSubnet},
			{"%ntpurl%", ConfigurationService::NTPUrl},
			{"%mqtturl%", ConfigurationService::MQTTUrl},
			{"%mqttport%", String(ConfigurationService::MQTTPort)},
			{"%wifioptions%", generateSSIDOptions(WifiUtils::getAvailableWifiNetworks())},
			{"%diagnosticcontent%", ""},
			{"%advancedsettingshtml%", ""},
			{"%localconnectionbuttontext%", ConfigurationService::newHardware?"Use WiFi or Ethernet":"Use WiFi"}
			};

			int variableCount = 19;

			switch (ConfigurationService::currentConfigurationStep)
			{
			case 0:
				printWebPage(response, start, variables, variableCount);
				break;
			case 1:
				printWebPage(response, installation, variables, variableCount);
				break;
			case 2:
				printWebPage(response, unitName, variables, variableCount);
				break;
			case 3:
				printWebPage(response, cloudOrLocal, variables, variableCount);
				break;
			case 4:
				printWebPage(response, cloudConnection, variables, variableCount);
				break;
			case 5:
				printWebPage(response, localServer, variables, variableCount);
				break;
			case 6:
				printWebPage(response, wifiEthernet, variables, variableCount);
				break;
			case 7:
				printWebPage(response, wifiSSID, variables, variableCount);
				break;
			case 8:
				printWebPage(response, wifiPass, variables, variableCount);
				break;
			case 9:
				printWebPage(response, staticIPQuestion, variables, variableCount);
				break;
			case 10:
				printWebPage(response, verification, variables, variableCount);
				break;
			case 11:
				printWebPage(response, staticIP, variables, variableCount);
				break;
			case 12:
				printWebPage(response, allDone, variables, variableCount);
				break;
			case 13:
				printWebPage(response, timeServer, variables, variableCount);
				break;
			case 99:
				if (navigate == "finish")
				{
					request->send(response);
					ConfigurationService::inConfigurationMode = false;
					return;

				}
				else if (navigate == "advanced")
				{
					variables[17].value = createSettingsData();
					DebugService::Debug("WebServer", "Show advanced");
					printWebPage(response, advanced, variables, variableCount);

				}
				else if (navigate == "diagnostic")
				{

					variables[16].value = createDiagnosticData();
					DebugService::Debug("WebServer", "Show diagnostics");
					printWebPage(response, diagnostic, variables, variableCount);

				}
				else if (navigate == "inputs")
				{

					DebugService::Debug("WebServer", "Show diagnostics");
					printWebPage(response, inputs, variables, variableCount);

				}
				else if (navigate == "ready")
				{
					DebugService::Debug("WebServer", "Show ready");
					printWebPage(response, ready, variables, variableCount);
				}
				else
				{
					DebugService::Debug("WebServer", "Show ready");
					printWebPage(response, ready, variables, variableCount);
				}
				break;
			}

			request->send(response);

			if (ConfigurationService::currentConfigurationStep == 10)
			{
				ConfigurationService::VerifyConnection = true;
			}
			break;
		}
	}


	String createSettingsData()
	{
		String settingsData = "";

		//Unit settings
		
		settingsData += ("<label for = \"UnitName\">Unit name : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"UnitName\" name = \"UnitName\" value=\"" + ConfigurationService::UnitName + "\"><br><br>");
		settingsData += ("<label for = \"mqtturl\">InstallationNumber : </label>");
		settingsData += ("<input type='hidden' id='chkAdvancedExists' name='chkAdvancedExists' value='exists'>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"InstallationNumber\" name = \"InstallationNumber\" value=\"" + ConfigurationService::InstallationNumber + "\"><br><br>");
		

		//GSM
		settingsData += ("<div class='collapsible-section'>");
		settingsData += ("<h2 class='collapsible-header'><span class='toggle-btn'>GSM Settings</span></h2>");
		settingsData += ("<div class='collapsible-content'>");
		settingsData += ("<label for = \"gsmenable\">Enabled : </label>");
		settingsData += ("<input type='hidden' id='chkgsmActiveExists' name='chkgsmActiveExists' value='exists'>");
		settingsData += ("<input type = \"checkbox\" id=\"gsmActive\" name=\"gsmActive\" " + String(ConfigurationService::gsmActive ? "checked" : "") + "><br><br>");
		settingsData += ("<label for = \"simpin\">SIM Pin : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0; margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"simPin\" name = \"simPin\" value=\"" + ConfigurationService::simPin + "\"><br><br>");
		settingsData += ("<label for = \"apnurl\">APN Url : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"APNUrl\" name = \"APNUrl\" value=\"" + ConfigurationService::APNUrl + "\"><br><br>");
		settingsData += ("<label for = \"apnuser\">APN User : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"APNUser\" name = \"APNUser\" value=\"" + ConfigurationService::APNUser + "\"><br><br>");
		settingsData += ("<label for = \"apnpass\">APN Password : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"APNPassword\" name = \"APNPassword\" value=\"" + ConfigurationService::APNPassword + "\"><br><br>");
		settingsData += ("</div>");
		settingsData += ("</div>");
		//MQTT
		settingsData += ("<div class='collapsible-section'>");
		settingsData += ("<h2 class='collapsible-header'><span class='toggle-btn'>MQTT Settings</span></h2>");
		settingsData += ("<div class='collapsible-content'>");
		settingsData += ("<label for = \"mqtturl\">MQTTServer URL : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"MQTTUrl\" name = \"MQTTUrl\" value=\"" + ConfigurationService::MQTTUrl + "\"><br><br>");
		settingsData += ("<label for = \"mqttport\">MQTTServer Port : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"MQTTPort\" name = \"MQTTPort\" value=\"" + String(ConfigurationService::MQTTPort) + "\"><br><br>");
		settingsData += ("<label for = \"mqtturl\">MQTTServer User : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"MQTTUser\" name = \"MQTTUser\" value=\"" + ConfigurationService::MQTTUser + "\"><br><br>");
		settingsData += ("<label for = \"mqttport\">MQTTServer Password : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"MQTTPassword\" name = \"MQTTPassword\" value=\"" + ConfigurationService::MQTTPassword + "\"><br><br>");
		settingsData += ("<input type='hidden' id='chkDisableHttpsExists' name='chkDisableHttpsExists' value='exists'>");
		settingsData += ("<label for = \"disablehttps\">Disable secure connection : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"checkbox\" id=\"DisableHttps\" name=\"DisableHttps\" " + String(ConfigurationService::DisableHttps ? "checked" : "") + "><br><br>");
		settingsData += ("<label for = \"NTPUrl\">NTP URL : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"NTPUrl\" name = \"NTPUrl\" value=\"" + ConfigurationService::NTPUrl + "\"><br><br>");

		settingsData += ("</div>");
		settingsData += ("</div>");
		//MQTT Host
		settingsData += ("<div class='collapsible-section'>");
		settingsData += ("<h2 class='collapsible-header'><span class='toggle-btn'>MQTT Broker Settings</span></h2>");
		settingsData += ("<div class='collapsible-content'>");
		settingsData += ("<input type='hidden' id='chkMQTTBrokerActiveExists' name='chkMQTTBrokerActiveExists' value='exists'>");
		settingsData += ("<label for = \"MQTTBrokerActive\">Enabled : </label>");
		settingsData += ("<input  type = \"checkbox\" id=\"MQTTBrokerActive\" name=\"MQTTBrokerActive\" " + String(ConfigurationService::MQTTBrokerActive ? "checked" : "") + "><br><br>");
		settingsData += ("<label for = \"MQTTBrokerSSID\">SSID : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"MQTTBrokerSSID\" name = \"MQTTBrokerSSID\" value=\"" + ConfigurationService::MQTTBrokerSSID + "\"><br><br>");
		settingsData += ("<label for = \"MQTTBrokerPassword\">Wifi key : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"MQTTBrokerPassword\" name = \"MQTTBrokerPassword\" value=\"" + ConfigurationService::MQTTBrokerPassword + "\"><br><br>");
		settingsData += ("<label for = \"MQTTBrokerPort\">MQTT Broker port : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"MQTTBrokerPort\" name = \"MQTTBrokerPort\" value=\"" + String(ConfigurationService::MQTTBrokerPort) + "\"><br><br>");
		settingsData += ("</div>");
		settingsData += ("</div>");
		//WIFI
		settingsData += ("<div class='collapsible-section'>");
		settingsData += ("<h2 class='collapsible-header'><span class='toggle-btn'>Wifi Settings</span></h2>");
		settingsData += ("<div class='collapsible-content'>");
		settingsData += ("<h2>Wifi Settings</h2>");
		settingsData += ("<input type='hidden' id='chkwifiActiveExists' name='chkwifiActiveExists' value='exists'>");
		settingsData += ("<label for = \"wifienable\">Enabled : </label>");
		settingsData += ("<input  type = \"checkbox\" id=\"wifiActive\" name=\"wifiActive\" " + String(ConfigurationService::wifiActive ? "checked" : "") + "><br><br>");
		settingsData += ("<label for = \"wifiSSID\">SSID : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"wifiSSID\" name = \"wifiSSID\" value=\"" + ConfigurationService::wifiSSID + "\"><br><br>");
		settingsData += ("<label for = \"wifiPass\">Wifi key : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"password\" id = \"wifiPass\" name = \"wifiPass\" value=\"" + ConfigurationService::wifiPass + "\"><br><br>");
		settingsData += ("<label for = \"wifiHost\">Wifi host name : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"wifiHost\" name = \"wifiHost\" value=\"" + ConfigurationService::wifiHost + "\"><br><br>");
		settingsData += ("<label for = \"ethenable\">Set static ip : </label>");
		settingsData += ("<input type='hidden' id='chkwifiUseIPSettingsExists' name='chkwifiUseIPSettingsExists' value='exists'>");
		settingsData += ("<input type = \"checkbox\" id=\"wifiUseIPSettings\" name=\"wifiUseIPSettings\" " + String(ConfigurationService::wifiUseIPSettings ? "checked" : "") + "><br><br>");
		settingsData += ("<label for = \"ethenable\">IP Adress : </label>");
		settingsData += ("<input id=\"wifiIPAdress\" style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"wifiIPAdress\" name = \"wifiIPAdress\" value=\"" + ConfigurationService::wifiIPAdress + "\"><br><br>");
		settingsData += ("<label for = \"ethenable\">Standard gateway : </label>");
		settingsData += ("<input id=\"wifiStandardGateway\" style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"wifiStandardGateway\" name = \"wifiStandardGateway\" value=\"" + ConfigurationService::wifiStandardGateway + "\"><br><br>");
		settingsData += ("<label for = \"ethenable\">Subnet : </label>");
		settingsData += ("<input id=\"wifiSubnet\" style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"wifiSubnet\" name = \"wifiSubnet\" value=\"" + ConfigurationService::wifiSubnet + "\"><br><br>");
		settingsData += ("</div>");
		settingsData += ("</div>");
		if (ConfigurationService::newHardware)
		{
			//Ethernet
			settingsData += ("<div class='collapsible-section'>");
			settingsData += ("<h2 class='collapsible-header'><span class='toggle-btn'>Ethernet Settings</span></h2>");
			settingsData += ("<div class='collapsible-content'>");
			settingsData += ("<input type='hidden' id='chkEthernetActiveExists' name='chkethernetActiveExists' value='exists'>");
			settingsData += ("<label for = \"ethernetenable\">Enabled : </label>");
			settingsData += ("<input  type = \"checkbox\" id=\"ethernetActive\" name=\"ethernetActive\" " + String(ConfigurationService::ethernetActive ? "checked" : "") + "><br><br>");
			settingsData += ("<label for = \"ethenable\">Set static ip : </label>");
			settingsData += ("<input type='hidden' id='chkethernetUseIPSettingsExists' name='chkethernetUseIPSettingsExists' value='exists'>");
			settingsData += ("<input type = \"checkbox\" id=\"ethernetUseIPSettings\" name=\"ethernetUseIPSettings\" " + String(ConfigurationService::ethernetUseIPSettings ? "checked" : "") + "><br><br>");
			settingsData += ("<label for = \"ethenable\">IP Adress : </label>");
			settingsData += ("<input id=\"ethIPAdress\" style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"ethernetIPAdress\" name = \"ethernetIPAdress\" value=\"" + ConfigurationService::ethernetIPAdress + "\"><br><br>");
			settingsData += ("<label for = \"ethenable\">Standard gateway : </label>");
			settingsData += ("<input id=\"ethStandardGateway\" style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"ethernetStandardGateway\" name = \"ethernetStandardGateway\" value=\"" + ConfigurationService::ethernetStandardGateway + "\"><br><br>");
			settingsData += ("<label for = \"ethenable\">Subnet : </label>");
			settingsData += ("<input id=\"ethSubnet\" style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px; font-weight: 400; font-family:\"Exo 2\" ' type = \"text\" id = \"ethernetSubnet\" name = \"ethernetSubnet\" value=\"" + ConfigurationService::ethernetSubnet + "\"><br><br>");
			settingsData += ("</div>");
			settingsData += ("</div>");
		}
		//UPDATE
		settingsData += ("<div class='collapsible-section'>");
		settingsData += ("<h2 class='collapsible-header'><span class='toggle-btn'>Update Settings</span></h2>");
		settingsData += ("<div class='collapsible-content'>");
		settingsData += ("<label for = \"mqtturl\">Update URL : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;	margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"UpdateUrl\" name = \"UpdateUrl\" value=\"" + ConfigurationService::UpdateURL + "\"><br><br>");
		settingsData += ("<label for = \"mqttport\">Update Port : </label>");
		settingsData += ("<input style = 'outline: 0;background: #d6d6d6;width: 100%;border : 0;margin: 0 0 15px;padding: 15px;	box-sizing: border-box;	font-size: 14px;' type = \"text\" id = \"UpdatePort\" name = \"UpdatePort\" value=\"" + String(ConfigurationService::UpdatePort) + "\"><br><br>");
		settingsData += ("</div>");
		settingsData += ("</div>");

		return settingsData;
	}

	String diagnosticEmail = "";

	String addDiagnosticHeader(String header)
	{
		return "<h2>" + header + "</h2><table>";
	}

	String addDiagnosticParameter(String parameterName, String parameterID, String parameterValue, bool lastParameter = false)
	{
		String result = "";

		result += "<tr>";
		result += "<td>" + parameterName + "</td>";
		result += "<td id=\"" + parameterID + "\">" + parameterValue + " </td>";
		result += "</tr>";

		diagnosticEmail += "`" + parameterName + " : " + parameterValue + "\\n" + "`";
		if (!lastParameter)
		{
			diagnosticEmail += " + \n";
		}

		return result;
	}

	String endDiagnosticHeader()
	{
		return "</table>";
	}

	String createDiagnosticData()
	{
		String diagnosticdata = "";
		diagnosticdata += addDiagnosticHeader("RSIoT device data");
		diagnosticdata += addDiagnosticParameter("Hardware version","hardwareVersion", ConfigurationService::newHardware ? "22.11.1":"21.11.3");
		diagnosticdata += addDiagnosticParameter("Software version", "softwareVersion", DebugService::CurrentVersion);
		diagnosticdata += addDiagnosticParameter("Installation number", "installationNumber", ConfigurationService::InstallationNumber);
		diagnosticdata += addDiagnosticParameter("Device ID", "deviceID", UniqueIDService::getUniqueID());
		diagnosticdata += addDiagnosticParameter("Device name", "unitName", ConfigurationService::UnitName);
		diagnosticdata += addDiagnosticParameter("Last restart", "lastRestart", DebugService::lastRestart);
		diagnosticdata += addDiagnosticParameter("Events in queue", "eventQueue", DebugService::eventQueueAmount);
		diagnosticdata += addDiagnosticParameter("Free memory", "freeMemory", DebugService::freeMemory);
		diagnosticdata += endDiagnosticHeader();

		diagnosticdata += addDiagnosticHeader("GSM Settings");
		diagnosticdata += addDiagnosticParameter("GSM active", "gsmActive", ConfigurationService::gsmActive ? "true" : "false");
		diagnosticdata += addDiagnosticParameter("SIM pin", "simPin", ConfigurationService::simPin);
		diagnosticdata += addDiagnosticParameter("APN", "apn", ConfigurationService::APNUrl);
		diagnosticdata += addDiagnosticParameter("APN user", "apnUser", ConfigurationService::APNUser);
		diagnosticdata += addDiagnosticParameter("APN password", "apnPassword", ConfigurationService::APNPassword);
		diagnosticdata += addDiagnosticParameter("GSM status", "gsmStatus", DebugService::GSMStatus);
		diagnosticdata += addDiagnosticParameter("GSM signal strength", "gsmSignalStrength", DebugService::GSMSignalStrength);
		diagnosticdata += addDiagnosticParameter("GSM network", "gsmNetwork", DebugService::GSMSignalNetworkName);
		diagnosticdata += endDiagnosticHeader();
		
		diagnosticdata += addDiagnosticHeader("WiFi Settings");
		diagnosticdata += addDiagnosticParameter("WiFi active", "wifiActive", ConfigurationService::wifiActive ? "true" : "false");
		diagnosticdata += addDiagnosticParameter("WiFi SSID", "wifiSSID", ConfigurationService::wifiSSID);
		diagnosticdata += addDiagnosticParameter("WiFi status", "wifiStatus", DebugService::WifiStatus);
		diagnosticdata += addDiagnosticParameter("WiFi signal strength", "wifiSignalStrength", DebugService::WifiStrength);
		diagnosticdata += addDiagnosticParameter("WiFi host name", "hostName", ConfigurationService::wifiHost);
		diagnosticdata += endDiagnosticHeader();

		diagnosticdata += addDiagnosticHeader("MQTT Settings");
		diagnosticdata += addDiagnosticParameter("MQTT server", "mqttServer", ConfigurationService::MQTTUrl);
		diagnosticdata += addDiagnosticParameter("MQTT port", "mqttPort", String(ConfigurationService::MQTTPort));
		diagnosticdata += addDiagnosticParameter("MQTT user", "mqttUser", ConfigurationService::MQTTUser);
		diagnosticdata += addDiagnosticParameter("MQTT password", "mqttPassword", ConfigurationService::MQTTPassword);
		diagnosticdata += addDiagnosticParameter("Disable secure connection", "secureConnection", ConfigurationService::DisableHttps ? "true" : "false");
		diagnosticdata += endDiagnosticHeader();

		diagnosticdata += addDiagnosticHeader("Update Settings");
		diagnosticdata += addDiagnosticParameter("Update server", "updateServer", ConfigurationService::UpdateURL);
		diagnosticdata += addDiagnosticParameter("Update port", "updatePort", String(ConfigurationService::UpdatePort));
		diagnosticdata += endDiagnosticHeader();

		diagnosticdata += addDiagnosticHeader("NTP Settings");
		diagnosticdata += addDiagnosticParameter("NTP Url", "ntp URL", ConfigurationService::NTPUrl);
		diagnosticdata += endDiagnosticHeader();

		diagnosticdata += addDiagnosticHeader("Connected clients info");
		diagnosticdata += addDiagnosticParameter("Connected clients", "conClients", WifiUtils::getConnectedClientsInfo());
		diagnosticdata += endDiagnosticHeader();

		return diagnosticdata;
	}

	void setConfiguration(String postData)
	{
		String chkwifiActive = parseData("chkwifiActiveExists=", postData);
		if (chkwifiActive != "�NoData�")
		{
			String wifiActive = parseData("wifiActive=", postData);
			if (wifiActive != "�NoData�")
			{
				ConfigurationService::wifiActive = true;
			}
			else
			{
				ConfigurationService::wifiActive = false;
			}
		}

		String chkEthernetActive = parseData("chkethernetActiveExists=", postData);
		if (chkEthernetActive != "�NoData�")
		{
			String ethernetActive = parseData("ethernetActive=", postData);
			if (ethernetActive != "�NoData�")
			{
				ConfigurationService::ethernetActive = true;
			}
			else
			{
				ConfigurationService::ethernetActive = false;
			}
		}

		String chkDisableHttps = parseData("chkDisableHttpsExists=", postData);
		if (chkDisableHttps != "�NoData�")
		{
			String DisableHttps = parseData("DisableHttps=", postData);
			if (DisableHttps != "�NoData�")

			{
				ConfigurationService::DisableHttps = true;
			}
			else
			{
				ConfigurationService::DisableHttps = false;
			}
		}
		String chkgsmActive = parseData("chkgsmActiveExists=", postData);
		if (chkgsmActive != "�NoData�")
		{
			String gsmActive = parseData("gsmActive=", postData);
			if (gsmActive != "�NoData�")
			{
				Serial.println("GSMACTIVE___________ " + gsmActive);

				ConfigurationService::gsmActive = true;
			}
			else
			{
				ConfigurationService::gsmActive = false;
			}
		}
		String wifiSSID = parseData("wifiSSID=", postData);
		if (wifiSSID != "�NoData�")
		{
			ConfigurationService::wifiSSID = wifiSSID;
		}
		String wifiPass = parseData("wifiPass=", postData);
		if (wifiPass != "�NoData�")
		{
			ConfigurationService::wifiPass = wifiPass;
		}
		String wifiHost = parseData("wifiHost=", postData);
		if (wifiHost != "�NoData�")
		{
			ConfigurationService::wifiHost = wifiHost;
		}
		String chkwifiUseIPSettings = parseData("chkwifiUseIPSettingsExists=", postData);
		if (chkwifiUseIPSettings != "�NoData�")
		{
			String wifiUseIPSettings = parseData("wifiUseIPSettings=", postData);
			if (wifiUseIPSettings != "�NoData�")
			{
				ConfigurationService::wifiUseIPSettings = true;
			}
			else
			{
				ConfigurationService::wifiUseIPSettings = false;
			}
		}
		String wifiIPAdress = parseData("wifiIPAdress=", postData);
		if (wifiIPAdress != "�NoData�")
		{
			ConfigurationService::wifiIPAdress = wifiIPAdress;
		}
		String wifiStandardGateway = parseData("wifiStandardGateway=", postData);
		if (wifiStandardGateway != "�NoData�")
		{
			ConfigurationService::wifiStandardGateway = wifiStandardGateway;
		}
		String wifiSubnet = parseData("wifiSubnet=", postData);
		if (wifiSubnet != "�NoData�")
		{
			ConfigurationService::wifiSubnet = wifiSubnet;
		}
		String chkethernetUseIPSettings = parseData("chkethernetUseIPSettingsExists=", postData);
		if (chkethernetUseIPSettings != "�NoData�")
		{
			String ethernetUseIPSettings = parseData("ethernetUseIPSettings=", postData);
			if (ethernetUseIPSettings != "�NoData�")
			{
				ConfigurationService::ethernetUseIPSettings = true;
			}
			else
			{
				ConfigurationService::ethernetUseIPSettings = false;
			}
		}
		String ethernetIPAdress = parseData("ethernetIPAdress=", postData);
		if (ethernetIPAdress != "�NoData�")
		{
			ConfigurationService::ethernetIPAdress = ethernetIPAdress;
		}
		String ethernetStandardGateway = parseData("ethernetStandardGateway=", postData);
		if (ethernetStandardGateway != "�NoData�")
		{
			ConfigurationService::ethernetStandardGateway = ethernetStandardGateway;
		}
		String ethernetSubnet = parseData("ethernetSubnet=", postData);
		if (ethernetSubnet != "�NoData�")
		{
			ConfigurationService::ethernetSubnet = ethernetSubnet;
		}
		String chkMQTTBrokerActive = parseData("chkMQTTBrokerActiveExists=", postData);
		if (chkMQTTBrokerActive != "�NoData�")
		{
			String MQTTBrokerActive = parseData("MQTTBrokerActive=", postData);
			if (MQTTBrokerActive != "�NoData�")
			{
				ConfigurationService::MQTTBrokerActive = true;
			}
			else
			{
				ConfigurationService::MQTTBrokerActive = false;
			}
		}
		String MQTTBrokerPort = parseData("MQTTBrokerPort=", postData);
		if (MQTTBrokerPort != "�NoData�")
		{
			ConfigurationService::MQTTBrokerPort = MQTTBrokerPort.toInt();
		}
		String MQTTBrokerSSID = parseData("MQTTBrokerSSID=", postData);
		if (MQTTBrokerSSID != "�NoData�")
		{
			ConfigurationService::MQTTBrokerSSID = MQTTBrokerSSID;
		}
		String MQTTBrokerPassword = parseData("MQTTBrokerPassword=", postData);
		if (MQTTBrokerPassword != "�NoData�")
		{
			ConfigurationService::MQTTBrokerPassword = MQTTBrokerPassword;
		}

		String APNUrl = parseData("APNUrl=", postData);
		if (APNUrl != "�NoData�")
		{
			ConfigurationService::APNUrl = APNUrl;
		}
		String APNUser = parseData("APNUser=", postData);
		if (APNUser != "�NoData�")
		{
			ConfigurationService::APNUser = APNUser;
		}
		String APNPassword = parseData("APNPassword=", postData);
		if (APNPassword != "�NoData�")
		{
			ConfigurationService::APNPassword = APNPassword;
		}
		String UnitName = parseData("UnitName=", postData);
		if (UnitName != "�NoData�")
		{
			ConfigurationService::UnitName = UnitName;
		}
		String MQTTUrl = parseData("MQTTUrl=", postData);
		if (MQTTUrl != "�NoData�")
		{
			ConfigurationService::MQTTUrl = MQTTUrl;
		}
		String MQTTPort = parseData("MQTTPort=", postData);
		if (MQTTPort != "�NoData�")
		{
			ConfigurationService::MQTTPort = MQTTPort.toInt();
		}
		String MQTTUser = parseData("MQTTUser=", postData);
		if (MQTTUser != "�NoData�")
		{
			ConfigurationService::MQTTUser = MQTTUser;
		}
		String MQTTPassword = parseData("MQTTPassword=", postData);
		if (MQTTPassword != "�NoData�")
		{
			ConfigurationService::MQTTPassword = MQTTPassword;
		}
		String UpdateUrl = parseData("UpdateUrl=", postData);
		if (UpdateUrl != "�NoData�")
		{
			ConfigurationService::UpdateURL = UpdateUrl;
		}
		String NTPUrl = parseData("NTPUrl=", postData);
		if (NTPUrl != "�NoData�")
		{
			ConfigurationService::NTPUrl = NTPUrl;
		}
		String UpdatePort = parseData("UpdatePort=", postData);
		if (UpdatePort != "�NoData�")
		{
			ConfigurationService::UpdatePort = UpdatePort.toInt();
		}
		String InstallationNumber = parseData("InstallationNumber=", postData);
		if (InstallationNumber != "�NoData�")
		{
			ConfigurationService::InstallationNumber = InstallationNumber;
		}
		
	}

	String parseData(String searchString, String postData)
	{
		int index = postData.indexOf(searchString);

		if (index >= 0)
		{
			String data = "";

			int currentIndex = index + searchString.length();

			while (currentIndex < postData.length())
			{
				char nextChar = postData[currentIndex];

				if (nextChar == '&')
				{
					break;
				}

				data += nextChar;

				currentIndex++;
			}
			return data;
		}
		return "�NoData�";
	}

	
};