const char advanced[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="header-text">Edit settings</h4>
                <form method="get" action="/navigate">
                    <div class="settings-content">
                    %advancedsettingshtml%
                    </div>
                    <input type="hidden" id="navigate" name="navigate" value="ready">
                    <button type="submit" class="btn btn-prefered">Save changes</button>
                </form>

                <br>
                <br>
                <br>
                <form method="get" action="/navigate">
                
                    <input type="hidden" id="navigate" name="navigate" value="reset">
                    <button type="submit" class="btn ">Restart RS IoT</button>
                </form>
                
                <br>
                <br>
                <form method="get" action="/navigate" id='factoryResetForm'>
                
                    <input type="hidden" id="navigate" name="navigate" value="factoryreset">
                    <button type="submit" class="btn ">Reset to factory settings</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        // Set wifiSSID textbox with the selected network from the dropdown
        document.getElementById("wifiSelect").addEventListener("change", function() {
            // Get the selected option text
            var selectedNetwork = this.options[this.selectedIndex].text;

            // Set the value of the wifiSSID textbox to the selected network
            if (selectedNetwork !== "Select WiFi network") {
                document.getElementById("wifiSSID").value = selectedNetwork;
            }
        });
    }

    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);

    var headers = document.querySelectorAll('.collapsible-header');
    headers.forEach(function(header) {
        header.addEventListener('click', function() {
            // If the clicked section is already expanded, collapse it
            if (this.classList.contains('expanded')) {
                this.nextElementSibling.style.display = 'none';
                this.classList.remove('expanded');
                return; // Exit the function early
            }
            // First, collapse all sections
            document.querySelectorAll('.collapsible-content').forEach(function(content) {
                content.style.display = 'none';
            });
            document.querySelectorAll('.collapsible-header').forEach(function(h) {
                h.classList.remove('expanded');
            });
            // Then, expand the clicked section
            this.nextElementSibling.style.display = 'block';
            this.classList.add('expanded');
        });
    });

    function confirmFactoryReset() {
        var confirmation = confirm('Are you sure you want to reset the device to factory settings? This action cannot be undone.');
        if (confirmation) {
            document.getElementById('factoryResetForm').submit();
        }
    }

    document.addEventListener('DOMContentLoaded', function() {
        updateIPSettings();  // call on page load
        document.getElementById('wifiUseIPSettings').addEventListener('change', updateIPSettings);  // call when the checkbox is changed
    });

    function updateIPSettings() {
        var checkbox = document.getElementById('wifiUseIPSettings');
        var ip = document.getElementById('wifiIPAdress');
        var gateway = document.getElementById('wifiStandardGateway');
        var subnet = document.getElementById('wifiSubnet');

        if (checkbox.checked) {
            ip.disabled = false;
            gateway.disabled = false;
            subnet.disabled = false;
        } else {
            ip.disabled = true;
            gateway.disabled = true;
            subnet.disabled = true;
        }
    }

    document.addEventListener('DOMContentLoaded', function() {
        updateEthernetIPSettings();  // call on page load
        document.getElementById('ethernetUseIPSettings').addEventListener('change', updateEthernetIPSettings);  // call when the checkbox is changed
    });

    function updateEthernetIPSettings() {
        var checkbox = document.getElementById('ethernetUseIPSettings');
        var ip = document.getElementById('ethIPAdress');
        var gateway = document.getElementById('ethStandardGateway');
        var subnet = document.getElementById('ethSubnet');

        if (checkbox.checked) {
            ip.disabled = false;
            gateway.disabled = false;
            subnet.disabled = false;
        } else {
            ip.disabled = true;
            gateway.disabled = true;
            subnet.disabled = true;
        }
    }
</script>)====";

const char allDone[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/back">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-header">Everything works!<br>Well done</h4>

                <h4 class="instruction-text">You can now close this set-up wizard.</h4>
                
                <form method="get" action="/finish">
                    <input type="hidden" id="navigate" name="navigate" value="finish">
                    <button type="submit" class="btn btn-prefered">Close</button>
                </form>

                <form method="get" action="/mainmenu">
                    <input type="hidden" id="navigate" name="navigate" value="mainmenu">
                    <button type="submit" class="btn">Main menu</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text-last">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

</script>)====";

const char cloudConnection[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-header">How should this device connect to RS Production cloud?</h4>
                <form method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="4G">
                    <button type="submit" class="btn btn-prefered">Use 4G</button>
                </form>
                <br>
                <br> 
                <form class="advanced-form" method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="local">
                    <button type="submit" class="btn">%localconnectionbuttontext%</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char cloudOrLocal[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
            <h4 class="instruction-header">Is your RS Production locally installed or hosted in the cloud?</h4>
            <form method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="cloud">
                <button type="submit" class="btn btn-prefered">Cloud</button>
            </form>
            <br>
            <br>
            <form class="advanced-form" method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="local">
                <button type="submit" class="btn">Local installation</button>
            </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char diagnostic[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="backbutton" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
            <h4 class="instruction-header">Diagnostic data</h4>
            <div class="diagnostic-content">
            %diagnosticcontent%
            </div>
            
            <form class="advanced-form" method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="ready">
                <button type="submit" class="btn" id="action-button">Go back</button>
            </form>

        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    function updateStatuses()
    {
        fetch('/api/status')
            .then(response => response.json())
            .then(data => {
                // Update classes for the statuses
                const networkElem = document.getElementById("network-status");
                const rsproductionElem = document.getElementById("rsproduction-status");
                const timeserverElem = document.getElementById("timeserver-status");
            
                networkElem.classList = data.networkStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                networkElem.querySelector(".status-text").textContent = data.networkStatus === "connected" ? "Connected" : "Not Connected";
                
                // Update RS Production status
                rsproductionElem.classList = data.rsproductionStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                rsproductionElem.querySelector(".status-text").textContent = data.rsproductionStatus === "connected" ? "Connected" : "Not Connected";

                // Update Timeserver status
                timeserverElem.classList = data.timeserverStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                timeserverElem.querySelector(".status-text").textContent = data.timeserverStatus === "connected" ? "Connected" : "Not Connected";
                

            })
            .catch(error => {
                console.error('Error fetching status:', error);
            });
    }

    updateStatuses(); // Call the function immediately once
    setInterval(updateStatuses, 1000); // Then update every second
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char inputs[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <!-- Error Modal -->
    <div id="errorModal" class="modal">
       <div class="modal-content">
           <span class="close">&times;</span>
           <p>%errormessage%</p>
       </div>
   </div>
   <div class="header-container">
       <form method="get" action="/navigate">
           <input type="hidden" id="backbutton" name="navigate" value="back">
           <button type="submit" class="btn-back">Back</button>
       </form>
       <h3 class="iot-title">RS IoT</h3>
   </div>
   <div class="page">
        <div class="form">
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input1-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 1</td>
                        <td class="energy-header"><button id="input1btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input1-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input1-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input2-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 2</td>
                        <td class="energy-header"><button id="input2btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input2-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input2-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input3-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 3</td>
                        <td class="energy-header"><button id="input3btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input3-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input3-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input4-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 4</td>
                        <td class="energy-header"><button id="input4btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input4-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input4-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input5-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 5</td>
                        <td class="energy-header"><button id="input5btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input5-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input5-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input6-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 6</td>
                        <td class="energy-header"><button id="input6btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input6-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input6-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input7-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 7</td>
                        <td class="energy-header"><button id="input7btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input7-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input7-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="input8-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Digital input 8</td>
                        <td class="energy-header"><button id="input8btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table id="input8-table" class="chart-container">
                    <tr><td colspan="5">
                        <svg class="input-line-chart" id="input8-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                            <g class="grid"></g>
                            <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                        </svg>
                    </td>
                    </tr>
                </table>
            </div>
            <div class="input-section %newhardware%">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="sensor1-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Energy input 1</td>
                        <td class="energy-header"><button id="energySensor1btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table class="energy-table" cellspacing="0">
                    <tr>
                        <td class="energy-type" id="sensor1-type"><span class="instruction-text"></span></td>
                        <td class="energy-power" id="sensor1-power"><span class="instruction-text"></span></td>
                    </tr>
                </table>
                <table id="sensor1-table" class="chart-container">
                    <tr>
                        <td colspan="5">
                            <svg class="line-chart" id="sensor1-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                                <g class="grid"></g>
                                <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                            </svg>
                        </td>
                    </tr>
                    <tr>
                        <td><button class="btn-type amp0" id="sensor1-amp-0" data-ampere="0">NC</button></td>
                    </tr>
                    <tr>
                        <td><button class="btn-type amp30" id="sensor1-amp-30" data-ampere="30">30A</button></td>
                        <td><button class="btn-type amp100" id="sensor1-amp-100" data-ampere="100">100A</button></td>
                        <td><button class="btn-type amp300" id="sensor1-amp-300" data-ampere="300">300A</button></td>
                        <td><button class="btn-type amp600" id="sensor1-amp-600" data-ampere="600">600A</button></td>
                        <td><button class="btn-type amp800" id="sensor1-amp-800" data-ampere="800">800A</button></td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status threshold</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor1-thresholdInput"></input>
                            <span class="setting-text">W</span>
                            <input type="hidden" id="sensor1-thresholdHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status on delay</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor1-onDelayInput"></input>
                            <span class="setting-text">s</span>
                            <input type="hidden" id="sensor1-onDelayHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status off delay</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor1-offDelayInput"></input>
                            <span class="setting-text">s</span>
                            <input type="hidden" id="sensor1-offDelayHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Power factor</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor1-powerfactorInput"></input>
                            <span class="setting-text">x</span>
                            <input type="hidden" id="sensor1-powerfactorHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Reference voltage</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor1-voltageInput"></input>
                            <span class="setting-text">V</span>
                            <input type="hidden" id="sensor1-voltageHidden">
                        </td>
                    </tr>
                </table>
            </div>
            
            <!-- Sensor 2 -->
            <div class="input-section %newhardware%">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="sensor2-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Energy input 2</td>
                        <td class="energy-header"><button id="energySensor2btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table class="energy-table" cellspacing="0">
                    <tr>
                        <td class="energy-type" id="sensor2-type"><span class="instruction-text"></span></td>
                        <td class="energy-power" id="sensor2-power"><span class="instruction-text"></span></td>
                    </tr>
                </table>
                <table id="sensor2-table" class="chart-container">
                    <tr>
                        <td colspan="5">
                            <svg class="line-chart" id="sensor2-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                                <g class="grid"></g>
                                <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                            </svg>
                        </td>
                    </tr>
                    <tr>
                        <td><button class="btn-type amp0" id="sensor2-amp-0" data-ampere="0">NC</button></td>
                    </tr>
                    <tr>
                        <td><button class="btn-type amp30" id="sensor2-amp-30" data-ampere="30">30A</button></td>
                        <td><button class="btn-type amp100" id="sensor2-amp-100" data-ampere="100">100A</button></td>
                        <td><button class="btn-type amp300" id="sensor2-amp-300" data-ampere="300">300A</button></td>
                        <td><button class="btn-type amp600" id="sensor2-amp-600" data-ampere="600">600A</button></td>
                        <td><button class="btn-type amp800" id="sensor2-amp-800" data-ampere="800">800A</button></td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status threshold</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor2-thresholdInput"></input>
                            <span class="setting-text">W</span>
                            <input type="hidden" id="sensor2-thresholdHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status on delay</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor2-onDelayInput"></input>
                            <span class="setting-text">s</span>
                            <input type="hidden" id="sensor2-onDelayHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status off delay</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor2-offDelayInput"></input>
                            <span class="setting-text">s</span>
                            <input type="hidden" id="sensor2-offDelayHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Power factor</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor2-powerfactorInput"></input>
                            <span class="setting-text">x</span>
                            <input type="hidden" id="sensor2-powerfactorHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Reference voltage</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor2-voltageInput"></input>
                            <span class="setting-text">V</span>
                            <input type="hidden" id="sensor2-voltageHidden">
                        </td>
                    </tr>
                </table>
            </div>
            
            <!-- Sensor 3 -->
            <div class="input-section %newhardware%">
                <table class="input-table" cellspacing="0">
                    <tr>
                        <td class="input-status" id="sensor3-status"><span class="instruction-text"></span></td>
                        <td colspan="2" class="energy-header">Energy input 3</td>
                        <td class="energy-header"><button id="energySensor3btn" class="expand-button">+</button></td>
                    </tr>
                </table>
                <table class="energy-table" cellspacing="0">
                    <tr>
                        <td class="energy-type" id="sensor3-type"><span class="instruction-text"></span></td>
                        <td class="energy-power" id="sensor3-power"><span class="instruction-text"></span></td>
                    </tr>
                </table>
                <table id="sensor3-table" class="chart-container">
                    <tr>
                        <td colspan="5">
                            <svg class="line-chart" id="sensor3-chart" width="100%" height="200" xmlns="http://www.w3.org/2000/svg">
                                <g class="grid"></g>
                                <path class="plot" d="" stroke="orange" stroke-width="3" fill="none"></path>
                            </svg>
                        </td>
                    </tr>
                    <tr>
                        <td><button class="btn-type amp0" id="sensor3-amp-0" data-ampere="0">NC</button></td>
                    </tr>
                    <tr>
                        <td><button class="btn-type amp30" id="sensor3-amp-30" data-ampere="30">30A</button></td>
                        <td><button class="btn-type amp100" id="sensor3-amp-100" data-ampere="100">100A</button></td>
                        <td><button class="btn-type amp300" id="sensor3-amp-300" data-ampere="300">300A</button></td>
                        <td><button class="btn-type amp600" id="sensor3-amp-600" data-ampere="600">600A</button></td>
                        <td><button class="btn-type amp800" id="sensor3-amp-800" data-ampere="800">800A</button></td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status threshold</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor3-thresholdInput"></input>
                            <span class="setting-text">W</span>
                            <input type="hidden" id="sensor3-thresholdHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status on delay</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor3-onDelayInput"></input>
                            <span class="setting-text">s</span>
                            <input type="hidden" id="sensor3-onDelayHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Status off delay</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor3-offDelayInput"></input>
                            <span class="setting-text">s</span>
                            <input type="hidden" id="sensor3-offDelayHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Power factor</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor3-powerfactorInput"></input>
                            <span class="setting-text">x</span>
                            <input type="hidden" id="sensor3-powerfactorHidden">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><span class="setting-text">Reference voltage</span></td>
                        <td colspan="2">
                            <input class="setting-input" type="number" id="sensor3-voltageInput"></input>
                            <span class="setting-text">V</span>
                            <input type="hidden" id="sensor3-voltageHidden">
                        </td>
                    </tr>
                </table>
            </div>
            
        </div>
    </div>

    <div class="text-footer">
        %version%
    </div>
</body>

</html>

<script>
    let svgWidth;
    let svgHeight;

    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    function toggleChart(chartId) {
            const chartContainer = document.querySelector(chartId);
            if (chartContainer) {
            chartContainer.classList.toggle('expandable-content');
            }
        }

    const maxDataPoints = 60;
    let data = new Array(maxDataPoints).fill(0);  // Start with an array of zeros.

    let sensorData = {
        sensor1: new Array(maxDataPoints).fill(0),
        sensor2: new Array(maxDataPoints).fill(0),
        sensor3: new Array(maxDataPoints).fill(0)
    };


    const maxInputDataPoints = 600;
    let inputData = {
        input1: new Array(maxInputDataPoints).fill(1/6),
        input2: new Array(maxInputDataPoints).fill(1/6),
        input3: new Array(maxInputDataPoints).fill(1/6),
        input4: new Array(maxInputDataPoints).fill(1/6),
        input5: new Array(maxInputDataPoints).fill(1/6),
        input6: new Array(maxInputDataPoints).fill(1/6),
        input7: new Array(maxInputDataPoints).fill(1/6),
        input8: new Array(maxInputDataPoints).fill(1/6)
    };
    // Create grid lines
    const svgs = document.querySelectorAll('.line-chart');

// Create grid lines for each SVG
    svgs.forEach((svg) => {
        svgWidth = svg.clientWidth;
        svgHeight = svg.clientHeight;
        const gridGroup = svg.querySelector('.grid');

        for (let i = 0; i <= 6; i++) {
            let line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            line.setAttribute('x1', '0');
            line.setAttribute('y1', (svgHeight / 6) * i);
            line.setAttribute('x2', svgWidth);
            line.setAttribute('y2', (svgHeight / 6) * i);
            gridGroup.appendChild(line);
        }

    });

    const inputSvgs = document.querySelectorAll('.input-line-chart');

    // Create grid lines for each SVG
    inputSvgs.forEach((svg) => {
        svgWidth = svg.clientWidth;
        svgHeight = svg.clientHeight;
        const gridGroup = svg.querySelector('.grid');

        let line0 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        line0.setAttribute('x1', '0');
        line0.setAttribute('y1', (svgHeight / 6) * 1);
        line0.setAttribute('x2', svgWidth);
        line0.setAttribute('y2', (svgHeight / 6) * 1);
        gridGroup.appendChild(line0);
        let line1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        line1.setAttribute('x1', '0');
        line1.setAttribute('y1', (svgHeight / 6) * 5);
        line1.setAttribute('x2', svgWidth);
        line1.setAttribute('y2', (svgHeight / 6) * 5);
        gridGroup.appendChild(line1);

    });

    toggleChart("#input1-table");
    toggleChart("#input2-table");
    toggleChart("#input3-table");
    toggleChart("#input4-table");
    toggleChart("#input5-table");
    toggleChart("#input6-table");
    toggleChart("#input7-table");
    toggleChart("#input8-table");

    toggleChart("#sensor1-table");
    toggleChart("#sensor2-table");
    toggleChart("#sensor3-table");

    async function fetchEnergyData() {
        if(%newhardwareBool%)
        {
            try {
                let response = await fetch('/api/energy');
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                let data = await response.json();
                return data;
            } catch (error) {
                console.error("There was an error fetching the data:", error);

                // Generate random power value based on ampere rating
                function getRandomPower(ampereRating) {
                    const maxPower = ampereRating * 230; // For a 10A rating, maximum power is 2300W
                    return Math.floor(Math.random() * (maxPower + 1));
                }

            }
        }
    }
    
    async function fetchInputData() {
        try {
            let response = await fetch('/api/inputs');
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            let data = await response.json();
            return data;
        } catch (error) {
            console.error("There was an error fetching the data:", error);

            return {
                inputs: [
                    {
                        inputStatus: false
                        
                    },
                    {
                        inputStatus: false
                    },
                    {
                        inputStatus: false
                    }
                    ,
                    {
                        inputStatus: false
                    }
                    ,
                    {
                        inputStatus: false
                    }
                    ,
                    {
                        inputStatus: false
                    }
                    ,
                    {
                        inputStatus: false
                    }
                    ,
                    {
                        inputStatus: false
                    }
                ]
            };
        }
    }

    function updateAmpereSelection(sensorData,sensorNumber) {
        
        const ampereRating = sensorData.ampereRating;

        // Define all possible ampere values
        const ampereValues = [0, 30, 100, 300, 600, 800];

        // Remove 'selected' class from all buttons for this sensor
        ampereValues.forEach(value => {
            const button = document.getElementById(`sensor${sensorNumber}-amp-${value}`);
            if (button) {
            button.classList.remove('selected');
            }
        });

        // Add 'selected' class to the button that matches the current ampere rating
        const selectedButton = document.getElementById(`sensor${sensorNumber}-amp-${ampereRating}`);
        if (selectedButton) {
            selectedButton.classList.add('selected');
        }
        }
    
    function plotData(dataTable, sensorIdentifier, dataPointsMax) {
        let pathData = '';
        dataTable[sensorIdentifier].forEach((value, index) => {
            let x = (svgWidth / dataPointsMax) * index;
            let y = svgHeight - (svgHeight * value);  // Assuming values are normalized between 0 and 1
            pathData += (index === 0 ? 'M' : 'L') + `${x},${y} `;
        });

        const plotPath = document.querySelector(`#${sensorIdentifier}-chart .plot`);
        plotPath.setAttribute('d', pathData);
    }

    function updateData(newValue, sensorIdentifier) {
        sensorData[sensorIdentifier].shift(); // Remove the first item.
        sensorData[sensorIdentifier].push(newValue); // Add the new value.
        plotData(sensorData, sensorIdentifier, maxDataPoints); // Update the specific plot.
    }

    function getColorByAmpereRating(ampereRating) {
        switch (ampereRating) {
            case 0:
                return { bgColor: 'gray', textColor: 'black' };
            case 30:
                return { bgColor: 'white', textColor: 'black' };
            case 100:
                return { bgColor: 'purple', textColor: 'white' };
            case 300:
                return { bgColor: 'red', textColor: 'white' };
            case 600:
                return { bgColor: 'green', textColor: 'white' };
            default:
                return { bgColor: 'yellow', textColor: 'black' }; // Default colors
        }
    }

    function updateSensorData(sensorData, sensorNumber)
    {
        document.getElementById(`sensor${sensorNumber}-status`).style.backgroundColor = sensorData.inputStatus === true ? 'GREEN' : 'RED';
        document.getElementById(`sensor${sensorNumber}-type`).querySelector(".instruction-text").textContent = sensorData.ampereRating + "A";
        document.getElementById(`sensor${sensorNumber}-power`).querySelector(".instruction-text").textContent = sensorData.currentValue + " W";
    
        const colors = getColorByAmpereRating(sensorData.ampereRating);
        const sensorTypeElement = document.getElementById(`sensor${sensorNumber}-type`);
        sensorTypeElement.style.backgroundColor = colors.bgColor;
        sensorTypeElement.querySelector(".instruction-text").style.color = colors.textColor;

        // Threshold input
        const thresholdInput = document.getElementById(`sensor${sensorNumber}-thresholdInput`);
        const thresholdHidden = document.getElementById(`sensor${sensorNumber}-thresholdHidden`);
        if(sensorData.thresholdValue != thresholdHidden.value || thresholdHidden.value == "") {
            thresholdInput.value = sensorData.thresholdValue;
            thresholdHidden.value = sensorData.thresholdValue; // Update hidden field
        }

        // On-threshold input
        const onThresholdInput = document.getElementById(`sensor${sensorNumber}-onDelayInput`);
        const onThresholdHidden = document.getElementById(`sensor${sensorNumber}-onDelayHidden`);
        if(sensorData.onThresholdCount != onThresholdHidden.value || onThresholdHidden.value == "") {
            onThresholdInput.value = sensorData.onThresholdCount;
            onThresholdHidden.value = sensorData.onThresholdCount; // Update hidden field
        }

        // Off-threshold input
        const offThresholdInput = document.getElementById(`sensor${sensorNumber}-offDelayInput`);
        const offThresholdHidden = document.getElementById(`sensor${sensorNumber}-offDelayHidden`);
        if(sensorData.offThresholdCount != offThresholdHidden.value || offThresholdHidden.value == "") {
            offThresholdInput.value = sensorData.offThresholdCount;
            offThresholdHidden.value = sensorData.offThresholdCount; // Update hidden field
        }

        // Power factor input
        const powerfactorInput = document.getElementById(`sensor${sensorNumber}-powerfactorInput`);
        const powerfactorHidden = document.getElementById(`sensor${sensorNumber}-powerfactorHidden`);
        if(sensorData.powerfactor != powerfactorHidden.value || powerfactorHidden.value == "") {
            powerfactorInput.value = sensorData.powerfactor;
            powerfactorHidden.value = sensorData.powerfactor; // Update hidden field
        }

        // Voltage input
        const voltageInput = document.getElementById(`sensor${sensorNumber}-voltageInput`);
        const voltageHidden = document.getElementById(`sensor${sensorNumber}-voltageHidden`);
        if(sensorData.referencevoltage != voltageHidden.value || voltageHidden.value == "") {
            voltageInput.value = sensorData.referencevoltage;
            voltageHidden.value = sensorData.referencevoltage; // Update hidden field
        }

        updateAmpereSelection(sensorData,sensorNumber);

    }

    function updateInputData(inputData, inputNumber)
    {
        document.getElementById(`input${inputNumber}-status`).style.backgroundColor = inputData.inputStatus === true ? 'GREEN' : 'RED';
    }

    function plotInputData(newValue, inputIdentifier) {

        let plotValue = 1/6;

        if(newValue)
        {
            plotValue = 1/6*5;
        }

        inputData[inputIdentifier].shift(); // Remove the first item.
        inputData[inputIdentifier].push(plotValue); // Add the new value.
        plotData(inputData, inputIdentifier, maxInputDataPoints); // Update the specific plot.
    }

    let isFetchingEnergyData = false;
let isFetchingInputData = false;
    async function handleDataUpdate() {
    if (!isFetchingEnergyData) {
        isFetchingEnergyData = true;
        try {
            let data = await fetchEnergyData(); // Awaiting the fetchData function here
            console.log(data);  // Let's log the data to see its structure

            for (let i = 0; i < data.energySensors.length; i++) {
                let sensorIdentifier = `sensor${i+1}`;  // Construct the sensor's identifier
                updateSensorData(data.energySensors[i], i+1);

                let maxPower = data.energySensors[i].ampereRating * 230;
                let normalizedEnergyPower = 0;
                if (maxPower > 0) {
                    normalizedEnergyPower = data.energySensors[i].currentValue / maxPower;
                }

                updateData(normalizedEnergyPower, sensorIdentifier);
            }
        } catch (error) {
            console.error(error);
        } finally {
            isFetchingEnergyData = false;
        }
    }
}

async function handleInputUpdate() {
    if (!isFetchingInputData) {
        isFetchingInputData = true;
        try {
            let inputData = await fetchInputData();

            for (let i = 0; i < inputData.inputs.length; i++) {
                let inputIdentifier = `input${i + 1}`;
                updateInputData(inputData.inputs[i], i + 1);

                plotInputData(inputData.inputs[i].inputStatus, inputIdentifier);
            }
        } catch (error) {
            console.error(error);
        } finally {
            isFetchingInputData = false;
        }
    }
}
    // Simulate adding new data every second
    setInterval(handleDataUpdate, 1000); // Update every second

    setInterval(handleInputUpdate, 500); // Update every second

    function makeApiCall(endpoint, data)
    {
        // Here you would implement the API call
        // For example, using fetch to make a POST request
        fetch(endpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
        console.log('Success:', data);
        })
        .catch((error) => {
        console.error('Error:', error);
        });
    }

    document.addEventListener('DOMContentLoaded', (event) => {
        // Function to toggle the chart visibility
        

        // Define the number of sensors
        const numberOfSensors = 3; // Update this if you have more sensors

        // Attach event listeners to the new buttons
        for (let i = 1; i <= numberOfSensors; i++) {
            const button = document.getElementById(`energySensor${i}btn`);
            if (button) {
            button.addEventListener('click', function () {
                toggleChart(`#sensor${i}-table`);
                button.textContent = button.textContent === '+' ? '-' : '+';
            });
            }
        }

        const numberOfInputs = 8; // Update this if you have more sensors

        // Attach event listeners to the new buttons
        for (let i = 1; i <= numberOfInputs; i++) {
            const button = document.getElementById(`input${i}btn`);
            if (button) {
            button.addEventListener('click', function () {
                toggleChart(`#input${i}-table`);
                button.textContent = button.textContent === '+' ? '-' : '+';
            });
            }
        }

        const buttons = document.querySelectorAll('.btn-type');
        buttons.forEach(button => {
            button.addEventListener('click', function() {
            const ampereValue = this.getAttribute('data-ampere');
            const inputId = this.id;
            console.log(`Button clicked: ID: ${inputId}, ${ampereValue}`);
            // Make an API call
            makeApiCall('/api/set', { function: "setAmpere", id:inputId, ampere: ampereValue });
            });
        });

        // Listener for input changes
        const inputs = document.querySelectorAll('.setting-input');
        inputs.forEach(input => {
            input.addEventListener('change', function() {
            const inputValue = this.value;
            const inputId = this.id;
            console.log(`Input changed - ID: ${inputId}, Value: ${inputValue}`);
            // Make an API call
            makeApiCall('/api/set', { function: "changeSetting", id: inputId, value: inputValue });
            });
        });
        });
</script>)====";

const char installation[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>



<body>
    <!-- Error Modal -->
    <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/back">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-header">Connect to your RS Production</h4>
                <h4 class="instruction-text">Enter your 6-digit RS Production installation number.</h4>
                <h4 class="instruction-text">Reach out to Support if you're unsure.</h4>
                <form method="get" action="/navigate">
                <input type="text" id="InstallationNumber" name="InstallationNumber" value="%installationid%" class="input-box" pattern="^[0-9]{6}$" maxlength="6" title="Enter a 6-digit number.">
                <br><br>
                
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn-prefered">Next</button>
                </form>
                <br>
                <br>
                <a href="tel:+46217881920" class="btn">Call RS Production support</a>
           
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char localServer[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
            <form method="get" action="/navigate">
                <h4 class="instruction-text">Enter the local IP address to your RS Production server.</h4>
                <input type="text" id="MQTTUrl" name="MQTTUrl" class="input-box" value="%mqtturl%">
                <br>
                <h4 class="instruction-text">Enter the port your RS Production uses. Default is 1883.</h4>
                <input type="text" id="MQTTPort" name="MQTTPort" class="input-box-alt" value="%mqttport%">
                <br>
                <h4 class="instruction-text">Enter adress to timeserver if internet is blocked.</h4>
                <input type="text" id="NTPUrl" name="NTPUrl" class="input-box-alt" value="%ntpurl%">
                <input type="hidden" id="navigate" name="navigate" value="next">
                <button type="submit" class="btn btn-prefered">Next</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script> )====";

const char ready[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form class="invisible-content" method="get" action="/navigate">
            <input type="hidden" id="backbutton" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
            <h4 class="instruction-header">RS IoT is configured</h4>
            <h4 class="instruction-text" id="interface-status">Interface message:<br> <span class="status-text">Checking...</span></h4>
            <h4 class="instruction-text" id="network-status">Network connectivity: <span class="status-text">Checking...</span></h4>
            <h4 class="instruction-text" id="rsproduction-status">RS Production connection: <span class="status-text">Checking...</span></h4>
            <h4 class="instruction-text" id="timeserver-status">Timeserver connection: <span class="status-text">Checking...</span></h4>
            
            <form class="advanced-form" method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="inputs">
                <button type="submit" class="btn" id="action-button">Show inputs</button>
            </form>


            <form class="advanced-form" method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="diagnostic">
                <button type="submit" class="btn" id="action-button">Show diagnostics</button>
            </form>

            <form class="advanced-form" method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="advanced">
                <button type="submit" class="btn" id="action-button">Change settings</button>
            </form>
            <br><br>
            <form class="advanced-form" method="get" action="/navigate">
                <input type="hidden" id="navigate" name="navigate" value="finish">
                <button type="submit" class="btn" id="action-button">Exit configuration mode</button>
            </form>

        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    function updateStatuses()
    {
        fetch('/api/status')
            .then(response => response.json())
            .then(data => {
                // Update classes for the statuses
                const networkElem = document.getElementById("network-status");
                const rsproductionElem = document.getElementById("rsproduction-status");
                const timeserverElem = document.getElementById("timeserver-status");
                
                const interfaceElem = document.getElementById("interface-status");
                interfaceElem.querySelector(".status-text").textContent = data.interfaceStatus || "Unknown";

                networkElem.classList = data.networkStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                networkElem.querySelector(".status-text").textContent = data.networkStatus === "connected" ? "Connected" : "Not Connected";
                
                // Update RS Production status
                rsproductionElem.classList = data.rsproductionStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                rsproductionElem.querySelector(".status-text").textContent = data.rsproductionStatus === "connected" ? "Connected" : "Not Connected";

                // Update Timeserver status
                timeserverElem.classList = data.timeserverStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                timeserverElem.querySelector(".status-text").textContent = data.timeserverStatus === "connected" ? "Connected" : "Not Connected";
                

            })
            .catch(error => {
                console.error('Error fetching status:', error);
            });
    }

    updateStatuses(); // Call the function immediately once
    setInterval(updateStatuses, 1000); // Then update every second
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char start[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form class="invisible-content" method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-header">RS IoT set-up wizard</h4>
                <h4 class="instruction-text">To get started with RS IoT, you'll first need to connect it to RS Production.</h4>
                <h4 class="instruction-text">Make sure to have your 6-digit RS Production installation number ready. </h4>
                <h4 class="instruction-text">You'll find this number in your RS IoT delivery box and it's the same number you use to log into RS Production.</h4>
                
                <form method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn-main">I've got the number.<br>Let's get started!</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>
</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char staticIP[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <form method="get" action="/navigate">
                <h4 class="instruction-header">Your static IP settings</h4>
                <h4 class="input-text">IP Number</h4>
                <input type="text" id="ethernetIPAdress" name="ethernetIPAdress" class="input-box" value="%ethernetipadress%">
                <h4 class="input-text">DNS Server</h4>
                <input type="text" id="ethernetDNS" name="ethernetDNS" class="input-box" value="%ethernetdns%">
                <h4 class="input-text">Standard gateway</h4>
                <input type="text" id="ethernetStandardGateway" name="gaethernetStandardGatewayteway" class="input-box" value="%ethernetstandardgateway%">
                <h4 class="input-text">Subnet</h4>
                <input type="text" id="ethernetSubnet" name="ethernetSubnet" class="input-box" value="%ethernetsubnet%">
                
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn">Next</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char staticIPQuestion[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-text">Does this RS IoT device need a static IP address on your network?</h4>
                <h4 class="instruction-text">Ask your local IT contact if you're unsure.</h4>
                <form method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="no">
                    <button type="submit" class="btn btn-prefered">No</button>
                </form>
                <br>
                <br>
                <form class="advanced-form" method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="yes">
                    <button type="submit" class="btn">Yes</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>

</html>)====";

const char styles[] PROGMEM = R"====(@import url('https://fonts.googleapis.com/css2?family=Exo+2:400');

body {
    background: #e85e43;
    font-family: 'Exo2', sans-serif;
    font-weight: 400;
    display: inline;
}

.page {
    width: 95%;
    padding: 2cap 0 0;
    margin: auto;
}
.input-section{
    width: 100%;
}
.input-table{
    width: 100%;
}

.input-status {
    width: 20px;  /* or any desired size */
    height: 20px;
    border-radius: 50%; /* makes it round */
    display: inline-block; /* allows you to set width/height */
    text-align: center; /* center the content horizontally */
    vertical-align: middle; /* center the content vertically */
    line-height: 20px; /* same as height, to center single line text/content vertically */
    margin:5px;
}


.energy-table{
    padding-top: 10px;
    width: 100%;
}

.energy-status {
    width: 20px;  /* or any desired size */
    height: 20px;
    border-radius: 50%; /* makes it round */
    display: inline-block; /* allows you to set width/height */
    text-align: center; /* center the content horizontally */
    vertical-align: middle; /* center the content vertically */
    line-height: 20px; /* same as height, to center single line text/content vertically */
    margin:5px;
}


.energy-type {
    padding: 5px 10px; /* Provides some space around the content */
    border-radius: 10px; /* Rounded edges */
    border-top-right-radius: 0; /* No rounding on top right corner */
    border-bottom-right-radius: 0; /* No rounding on bottom right corner */
    border: 2px solid rgba(0, 0, 0, 0.8); /* Black border with 50% transparency */
    display: inline-block; /* Allows you to set padding and maintain inline behavior */
    text-align: center; /* Center the content horizontally */
    margin:0px;
}

.btn-type {
    padding: 2px 6px; /* Provides some space around the content */
    border-radius: 10px; /* Rounded edges */
    border: 2px solid rgba(0, 0, 0, 0.8); /* Black border with 50% transparency */
    display: inline-block; /* Allows you to set padding and maintain inline behavior */
    text-align: center; /* Center the content horizontally */
    margin:0px;
}

.amp0 {
    background-color: gray;
    color:white;
}
.amp800 {
    background-color: yellow;
    color:black;
}
.amp100 {
    background-color: purple;
    color:white;
}
.amp300 {
    background-color: red;
    color:white;
}
.amp600 {
    background-color: green;
    color:white;
}

.setting-text {
    color:#000000;
    text-align: right;
    margin: 5px;
}
.setting-input {
    width:60px;
}

.selected{
    border-color: black;
    border-width: 8px;
}

.energy-power {
    padding: 5px 10px; /* Provides some space around the content */
    border-radius: 10px; /* Rounded edges */
    border-top-left-radius: 0; /* No rounding on top right corner */
    border-bottom-left-radius: 0; /* No rounding on bottom right corner */
    border: 2px solid rgba(0, 0, 0, 0.8); /* Black border with 50% transparency */
    display: inline-block; /* Allows you to set padding and maintain inline behavior */
    text-align: center; /* Center the content horizontally */
    width:150px;
    margin:0px;
    background-color: #FFFFFF;
    
}

.expand-button{
    background-color: #f9f9f9; /* Light grey background */
    color: #333; /* Dark text */
    text-align:center;
    font-size: 24px; /* Large font size for visibility */
    border: none;
    cursor: pointer;
    vertical-align: middle;
    border-radius: 10px;
    width:40px;
}


.energy-header {
   
    margin-top: 30px;
    margin-bottom: 30px;
    text-align: left;
    color: #FFFFFF;
    font-size: 20px;
}

.expandable-content {
    display: none;
}

.form {
    position: relative;
    z-index: 1;
    background: #fffffa;
    max-width: 95%;
    margin: 0 auto 20px;
    padding: 25px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.header-container {
    display: flex;
    align-items: center;
    justify-content: space-between; /* This spreads the child elements apart as much as possible */
    position: relative; /* So we can use absolute positioning for .iot-title */
    margin-bottom: 8%;
    margin-top: 8%;
}

.iot-title {
    position: absolute; /* Takes the title out of the normal flow */
    left: 50%; /* Center it relative to .header-container */
    transform: translateX(-50%); /* Ensures the title is truly centered, adjusting for its own width */
    text-align: center;
    color: #FFFFFA;
    font-size: 8vw;
}

.invisible-content {
    visibility: hidden;
}

.instruction-text {
    margin-top: 12%;
    text-align: center;
    color: #000000;
    font-size: 4vw;
}

.input-text {
    margin-top: 1%;
    text-align: left;
    color: #000000;
    font-size: 4vw;
}

.instruction-header {
    margin-top: 5%;
    text-align: center;
    color: #000000;
    font-size: 5.8vw;
}
.input-box,
.input-box-alt {
    outline: 0;
    background: #d6d6d6;
    width: 100%;
    border: 0;
    margin: 0 0 5px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 6vw;
    text-align: center;
}

.btn {
    text-transform: uppercase;
    outline: 0;
    background: #e85e43;
    width: 100%;
    border: 0;
    padding: 15px;
    color: #fcfcfc;
    margin-top: 20px;
    text-decoration: none;
    display: block;      /* Make the element a block element */
    box-sizing: border-box; /* Ensure padding and border are included in the width */
    text-align: center;  /* Center the text within the element */
}

.input-section{
    outline: 0;
    background: #e85e43;
    width: 100%;
    border: 0;
    padding: 15px;
    color: #fcfcfc;
    margin-top: 20px;
    text-decoration: none;
    display: block;      /* Make the element a block element */
    box-sizing: border-box; /* Ensure padding and border are included in the width */
}

.btn-back {
    background: #FFFFFA;
    border: #000000;
    margin: 0px;
    height: 40px;
    width: 90px;
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
    color: #000000;
}

.btn-prefered {
    height: 100px;
    font-size:x-large;
}

.text-footer {
    position: fixed; /* Use absolute if you don't want it to stick during scroll */
    bottom: 0; 
    left: 50%; /* Center it horizontally */
    transform: translateX(-50%); /* This ensures true centering */
    text-align: center; /* Center the text within the div */
    color: white; /* Set the text color to white */
}

.status-ok {
    color: green;
}

.status-error {
    color: red;
}
/* Modal Styles */
.modal {
    display: none; /* Hidden by default */
    position: fixed; 
    top: 0; 
    left: 0;
    width: 100%; 
    height: 100%; 
    overflow: auto;
    background-color: rgba(0,0,0,0.4); 
    z-index: 1000; /* This should be a high value to ensure the modal appears on top */
}

.diagnostic-content {
    text-align: left;
}
.settings-content {
    text-align: left;
}

.loading-overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(41, 41, 41, 0.5);
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 1000;
    color: white;
}

.loading-text {
    display: flex; /* Set to flex to line up our text and dots */
    justify-content: flex-start; /* Align children to the start */
    width: 100px; /* Fixed width to accommodate the word 'Loading' and three dots */
}

.loading-text-last {
    display: flex; /* Set to flex to line up our text and dots */
    justify-content: flex-start; /* Align children to the start */
}

.ellipsis {
    display: inline-block;
    width: 24px; /* Width set to ensure it doesn't shift */
    text-align: left;
    animation: ellipsisSteps 1s infinite;
}
.header-ellipsis {
    display: inline-block;
    width: 20px; /* adjust this value based on your preferred spacing */
    text-align: center;
}

@keyframes ellipsisSteps {
    0%, 100% { content: ''; }
    25%  { content: '.'; }
    50%  { content: '..'; }
    75%  { content: '...'; }
}

.ellipsis::before {
    content: '.';
}

.checkbox {
    width: 40px;
    height: 19px;
    color: #000000;
    font-family: 'Exo 2';
    font-weight: 400;
    font-size: 15px;
    text-align: left;
    margin-bottom: 15px;
}

.collapsible-content {
    display: none;
}

.toggle-btn::before {
    content: '+ ';
}

.expanded .toggle-btn::before {
    content: '- ';
}

.expanded .collapsible-content {
    display: block;
}

.collapsible-header {
    background-color: #e85e43;   /* orange background */
    padding: 10px;               /* some padding for aesthetics */
    color: #fff;                 /* white text color */
    text-align: left;          /* centered text */
}

.modal-content {
    background-color: #fefefe;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 70%;
}

.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

.chart-container {
    margin-top: 10px;
    width: 100%;
    background-color: #FFF;  /* Dark background for contrast */
    position: relative;
}

.line-chart {
    width: 100%;
    height: 100%;
}

.grid line {
    stroke: #777777;
    stroke-opacity: 0.7;
    shape-rendering: crispEdges;
}

/* Media query for screens wider than 768px (typical breakpoint for tablets and above) */
@media screen and (min-width: 768px) {
    .iot-title {
        font-size: 64px;  /* Adjust as needed */
    }

    .instruction-text {
        font-size: 32px;  /* Adjust as needed */
    }

    .instruction-header {
        font-size: 48px;  /* Adjust as needed */
    }

    .form {
        max-width: 500px; /* Adjust as needed */
    }
    .page {
        max-width: 1200px; /* Set this to whatever maximum size you prefer for desktop */
    }
    
})====";

const char timeServer[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-text">Could not connect to Time server.</h4>

                <h4 class="instruction-text">Enter the local address for your NTP Timeserver or contact Support.</h4>
                <form method="get" action="/navigate">
                <input type="text" id="NTPUrl" name="NTPUrl" value="%ntpserver%" class="input-box">
                <br><br>
                
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn-prefered">Test connection</button>
                </form>
                <br>
                <br>
                <form class="advanced-form" method="get" action="/callSupport">
                    <input type="hidden" id="navigate" name="navigate" value="callSupport">
                    <button type="submit" class="btn">Call RS Production support</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char unitName[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-header">Name this RS IoT device</h4>
                <h4 class="instruction-text">The name you give your RS IoT device now will be how you identify it later in RS Production office Tools.</h4>
                
                <form method="get" action="/navigate">
                <input type="text" id="UnitName" name="UnitName" class="input-box" value="%unitname%" pattern="^{4}$" maxlength="50" title="Enter minimum of 4 characters">
                <br><br>
                
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn-prefered">Save this name and move on</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char verification[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="backbutton" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
            <h4 class="instruction-header">Verifying everything<span class="header-ellipsis">.</span></h4>
            <h4 class="instruction-text" id="interface-status">Interface message:<br> <span class="status-text">Checking...</span></h4>
            <h4 class="instruction-text" id="network-status">Network connectivity: <span class="status-text">Checking...</span></h4>
            <h4 class="instruction-text" id="rsproduction-status">RS Production connection: <span class="status-text">Checking...</span></h4>
            <h4 class="instruction-text" id="timeserver-status">Timeserver connection: <span class="status-text">Checking...</span></h4>
          
          
            <form class="advanced-form" method="get" action="/navigate" id="action-form" style="display: none;">
                <input type="hidden" id="navigate" name="navigate" value="actionToDo">
                <button type="submit" class="btn btn-prefered" id="action-button">Action</button>
            </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    function updateStatuses()
    {
        fetch('/api/status')
            .then(response => response.json())
            .then(data => {
                // Update classes for the statuses
                const networkElem = document.getElementById("network-status");
                const rsproductionElem = document.getElementById("rsproduction-status");
                const timeserverElem = document.getElementById("timeserver-status");
                
                const interfaceElem = document.getElementById("interface-status");
                interfaceElem.querySelector(".status-text").textContent = data.interfaceStatus || "Unknown";

                networkElem.classList = data.networkStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                networkElem.querySelector(".status-text").textContent = data.networkStatus === "connected" ? "Connected" : "Not Connected";
                
                // Update RS Production status
                rsproductionElem.classList = data.rsproductionStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                rsproductionElem.querySelector(".status-text").textContent = data.rsproductionStatus === "connected" ? "Connected" : "Not Connected";

                // Update Timeserver status
                timeserverElem.classList = data.timeserverStatus === "connected" ? "instruction-text status-ok" : "instruction-text status-error";
                timeserverElem.querySelector(".status-text").textContent = data.timeserverStatus === "connected" ? "Connected" : "Not Connected";
                
                // Handle button visibility and the hidden field's value
                const form = document.getElementById('action-form');
                const button = document.getElementById('action-button');
                const hiddenField = document.getElementById('navigate');

                // Default
                form.style.display = "none";

                if (data.networkStatus === "connected" && data.rsproductionStatus === "connected" && data.timeserverStatus === "connected") {
                    button.textContent = "Finish";
                    hiddenField.value = "finish";
                    form.style.display = "block";
                } else if (!data.timeserverStatus) {
                    button.textContent = "Set Timeserver URL";
                    hiddenField.value = "timeserver";
                    form.style.display = "block";
                } else {
                    hiddenField.value = "noaction";
                }

            })
            .catch(error => {
                console.error('Error fetching status:', error);
        });
    }

    updateStatuses(); // Call the function immediately once
    setInterval(updateStatuses, 1000); // Then update every second
    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
    let headerEllipsisSpan = document.querySelector(".header-ellipsis");
    let headerEllipsisCount = 0;

    setInterval(function() {
        headerEllipsisCount = (headerEllipsisCount + 1) % 4;
        let dots = '.'.repeat(headerEllipsisCount);
        headerEllipsisSpan.textContent = dots;
    }, 1000);

     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char wifiEthernet[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <form method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="wifi">
                    <button type="submit" class="btn btn-prefered">Use WiFI</button>
                </form>
                <br>
                <br> 
                <form class="advanced-form" method="get" action="/navigate">
                    <input type="hidden" id="navigate" name="navigate" value="ethernet">
                    <button type="submit" class="btn btn-prefered">Use Ethernet</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
</script>)====";

const char wifiPass[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-text">Enter the password for your WiFi network.</h4>
                <form method="get" action="/navigate">
                <input type="text" id="wifiPass" name="wifiPass" class="input-box-alt" value="%wifipass%">
                
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn-prefered">Next</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });
    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

const char wifiSSID[] PROGMEM = R"====(<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
     <!-- Error Modal -->
     <div id="errorModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>%errormessage%</p>
        </div>
    </div>
    <div class="header-container">
        <form method="get" action="/navigate">
            <input type="hidden" id="navigate" name="navigate" value="back">
            <button type="submit" class="btn-back">Back</button>
        </form>
        <h3 class="iot-title">RS IoT</h3>
    </div>
    <div class="page">
        <div class="form">
                <h4 class="instruction-text">Select your WiFi network.<br>Or enter the network SSID.</h4>
                <select id="wifiSelect" name="wifiSelect" class="input-box">
                    <option>Select WiFi network</option>
                    %wifioptions%
                </select>
                <br><br>
                <form method="get" action="/navigate">
                <input type="text" id="wifiSSID" name="wifiSSID" class="input-box-alt" value="%wifissid%">
                
                    <input type="hidden" id="navigate" name="navigate" value="next">
                    <button type="submit" class="btn btn-prefered">Next</button>
                </form>
        </div>
    </div>
    <div class="text-footer">
        %version%
    </div>
    <div class="loading-overlay" style="display: none;">
    <div class="loading-text">
        Loading
        <span class="ellipsis">.</span>
    </div>
</div>
</body>

</html>

<script>
    window.onload = function() {
        var modal = document.getElementById("errorModal");
        var span = document.getElementsByClassName("close")[0];
        if (window.history.replaceState) {
            // Clear the URL parameters
            window.history.replaceState({}, document.title, window.location.pathname);
        }
        // Function to try and parse a string as a boolean
        function tryParseBool(str, defaultValue = false) {
            if (str === "true") return true;
            if (str === "false") return false;
            return defaultValue;
        }

        // Determine whether to show the error modal
        var showErrorModal = tryParseBool("%error%");

        // If showErrorModal is true, display the modal
        if (showErrorModal) {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        // Set wifiSSID textbox with the selected network from the dropdown
        document.getElementById("wifiSelect").addEventListener("change", function() {
            // Get the selected option text
            var selectedNetwork = this.options[this.selectedIndex].text;

            // Set the value of the wifiSSID textbox to the selected network
            if (selectedNetwork !== "Select WiFi network") {
                document.getElementById("wifiSSID").value = selectedNetwork;
            }
        });
    }

    var formButtons = document.querySelectorAll('form button');

    formButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var overlay = document.querySelector(".loading-overlay");
            if (overlay) {
                overlay.style.display = "flex"; // change to flex to match our CSS from earlier
            }
        });
    });

    
     // Ellipsis animation for loading text
     let ellipsisSpan = document.querySelector(".ellipsis");
    let ellipsisCount = 0;

    setInterval(function() {
        ellipsisCount = (ellipsisCount + 1) % 4;
        let dots = '.'.repeat(ellipsisCount);
        ellipsisSpan.textContent = dots;
    }, 1000);
</script>)====";

