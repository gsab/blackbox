/*
 Name:		Library.h
 Created:	11/25/2020 1:20:38 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#pragma once
#include <WString.h>
#include <ConnectionService.h>
#include <MemoryService.h>
#include <MemoryServiceFAT.h>
#include <FFat.h>
#include <WiFi.h>
#include <DebugService.h>
#include <ConfigurationService.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>
#include <CaptivePortalHandler.h>
#include <MQTTService.h>
#include <EnergyService.h>
#include <InputService.h>

class WebServerService {
public:
	void initialize(ConnectionService* connectionService, String currentVersion);
	void loop();
	void setEnergySetting(int trigger, String id, String value);
	
private:

	AsyncWebServer asyncServer = AsyncWebServer(80);
	int apiStatusCallCount = 0;
	int apiEnergyCallCount = 0;
	int apiInputsCallCount = 0;
	int apiSetCallCount = 0;
	int loopCounter = 0;
	int apiSummary = 0;

	void printApiCallMetrics();
	std::vector<String> collectPostParams(AsyncWebServerRequest* request);
	ConnectionService* connectionService;
	String currentVersion = "";

};


