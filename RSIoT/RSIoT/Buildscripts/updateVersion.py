import os

# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))

# Move up one directory to get to the solution directory from the Buildscript folder
solution_dir = os.path.dirname(script_dir)

# Path to the 'WebInterface' directory located in the solution directory
file_path = os.path.join(solution_dir, 'src','Version.h')

# Read the file
with open(file_path, 'r') as file:
    content = file.read()

# Extract the version string
version_start = content.find('"') + 1
version_end = content.rfind('"')
version_str = content[version_start:version_end]

# Split version string and increment the last number
parts = version_str.split('.')
parts[-1] = str(int(parts[-1]) + 1)
new_version_str = '.'.join(parts)

# Replace the old version string with the new one
content = content.replace(version_str, new_version_str)

# Write the modified content back to the file
with open(file_path, 'w') as file:
    file.write(content)

print(f"Version updated to: {new_version_str}")