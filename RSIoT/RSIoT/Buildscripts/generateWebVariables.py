import os

# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))

# Move up two directories to get to the solution directory from the Buildscript folder
solution_dir = os.path.dirname(os.path.dirname(script_dir))

# Path to the 'WebInterface' directory located in the solution directory
input_folder = os.path.join(solution_dir, 'WebInterface')

# Path to where the output C++ header file should be saved
output_file_path = os.path.join(solution_dir, 'Library.WebServer', 'src', 'web_data.h')

with open(output_file_path, 'w') as out_file:
    for filename in os.listdir(input_folder):
        if filename.endswith('.html') or filename.endswith('.css'):
            with open(os.path.join(input_folder, filename), 'r') as in_file:
                content = in_file.read()
                var_name = os.path.splitext(filename)[0]  # Remove extension
                var_name = var_name.replace('-', '_').replace(' ', '_')  # Convert invalid characters to underscore
                out_file.write(f'const char {var_name}[] PROGMEM = R"====({content})====";\n\n')