#include <TinyConsole.h>
#include <ssl_client.h>
#include <WiFiClientSecure.h>
#include <CircularBuffer.h>
#include <FS.h>
#include <FFat.h>
#include <SoftwareSerial.h>
#include <ESPAsyncWebServer.h>
#include <WebSerial.h>
#include <AsyncTCP.h>
#include <MD5Builder.h>
#include <SSLClient.h>
#include <DNSServer.h>
#include <HTTPClient.h>
#include <ESP32Time.h>
#include <KX0231025IMU.h>
#include <SPI.h>
#include <TLA2518.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <ArduinoUniqueID.h>
#include <WiFi.h>
#include <NTPClient.h>
#include <PubSubClient.h>
#include <RTCLib.h>
#include <SerialFlash.h>
#include <Update.h>
#include <Wire.h>
#include <TinyMqtt.h>
#include <time.h>
#include <EmonLib.h>     
#include <esp_wifi.h>
#include <ESPmDNS.h>
#include <vector>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <Arduino.h>
#include <EthernetWizNet.h>
#include <SdFat.h>

#define TINY_GSM_MODEM_SIM7080 //GSM model define must be before include
#include <TinyGsmClient.h>
#include <ArduinoQueue.h>

#include <WString.h>
#include <Dictionary.h>

//Internal libraries

#include <IOStatus.h>
#include <DebugService.h>
#include <DebugMessage.h>
#include <StringUtils.h>
#include <WifiUtils.h>
#include <InputEvent.h>
#include <ConfigurationService.h>
#include <MemoryService.h>
#include <MemoryServiceFAT.h>
#include <SDMemoryService.h>
#include <EEPROMMemoryService.h>
#include <TinyMqtt.h>
#include <MQTTService.h>
#include <VibrationService.h>
#include <ConnectionService.h>
#include <WebServerService.h>
#include <UpdateService.h>
#include <InputService.h>
#include <InputTrigger.h>
#include "src\Version.h"
#include <EventPackage.h>
#include <sd.h>
#include <EnergyService.h>
#include <EnergyDataTrigger.h>
#include <CalibrationPoint.h>
#include <CalibrationDataSet.h>
#include <IEventProcessor.h>
#include "esp_system.h"
#include "esp_efuse.h"
#include "esp_system.h"
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/efuse_reg.h"
//#include <QueuedMqttMessage.h>

// Define the bitmask for extracting the MTDI status (bit 5)
#define MTDI_STATUS_BIT  (1 << 5)

//Emon defines
#define SD_FAT_TYPE 3
#define SPI_CLOCK SD_SCK_MHZ(20)
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, SHARED_SPI, SPI_CLOCK)


EnergyMonitor emon1;
EnergyMonitor emon2;
EnergyMonitor emon3;
//#define ACTectionRange 30;    //set Non-invasive AC Current Sensor tection range (5A,10A,20A)
//#define VREF 3.3
//#define ADC_BITS    12
//#define ADC_COUNTS  (1<<ADC_BITS)

//GSM defines


#define SerialAT Serial2
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200
#ifndef TINY_GSM_RX_BUFFER
#define TINY_GSM_RX_BUFFER 2048
#endif

//Connectionservice defines
#define INTERFACE_ETHERNET		0
#define INTERFACE_WIFI			1
#define INTERFACE_GSM			2
#define INTERFACE_NONE			-1


//Accelerometer defines
#define KX0231025_RANGE_2G       0
#define KX0231025_RANGE_4G       1
#define KX0231025_RANGE_8G       2

#define KX0231025_DATARATE_12_5HZ  0
#define KX0231025_DATARATE_25HZ    1
#define KX0231025_DATARATE_50HZ    2
#define KX0231025_DATARATE_100HZ   3
#define KX0231025_DATARATE_200HZ   4
#define KX0231025_DATARATE_400HZ   5
#define KX0231025_DATARATE_800HZ   6
#define KX0231025_DATARATE_1600HZ  7
#define KX0231025_DATARATE_0_781HZ   8
#define KX0231025_DATARATE_1_563HZ   9
#define KX0231025_DATARATE_3_125HZ   10
#define KX0231025_DATARATE_6_25HZ  11

#define KX0231025_LOWPOWER_MODE    0X00
#define KX0231025_HIGHPOWER_MODE   0X40

#define SerialMon Serial
// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG SerialMon
#define DUMP_AT_COMMANDS

#define PORT 1883
MqttBroker* broker;


//MqttClient client(&broker,"Brokerclient");

#define VIB_SAMPLING_FREQ_HZ 100
#define CAL_CYCLES 12
#define SAMPLES_PER_CYCLE 25
#define PATTERN_MIN_LENGTH 8
#define PATTERN_MAX_LENGTH 14
#define PATTERNS_TO_CHECK 400
#define COV_CYCLES 3

int analogsample1 = 0;
int analogsample2 = 0;
int analogsample3 = 0;

class XYZ {
public:
	XYZ()
		: x(0), y(0), z(0) {};
	XYZ(float x, float y, float z)
		: x(x), y(y), z(z) {};
	float x, y, z;
};

//bool timeIsSet = false; // determines if time has been set since restart. This needs to be done to be able to send any events

bool hardwareError = false;
// Defining global variables

ESP32Time rtc;

bool D0_LAST_STATUS = false;
bool D1_LAST_STATUS = false;
bool D2_LAST_STATUS = false;
bool D3_LAST_STATUS = false;
bool D4_LAST_STATUS = false;
bool D5_LAST_STATUS = false;

bool X_LAST_STATUS = false;
bool Y_LAST_STATUS = false;
bool Z_LAST_STATUS = false;


const int8_t D0_INPUT = 36;
const int8_t D1_INPUT = 39;
const int8_t D2_INPUT = 34;
const int8_t D3_INPUT = 35;
const int8_t D4_INPUT = 32;
const int8_t D5_INPUT = 33;
const int8_t D6_INPUT = 25;
const int8_t D7_INPUT = 26;

double lastEnergyReading1 = 0;
double lastEnergyReading2 = 0;
double lastEnergyReading3 = 0;

//InterruptBuffert
long interruptBuffert_A[8];
long interruptBuffert_B[8];
bool interrupt_B_Buffert = false;

const int8_t BOOT_BUTTON = 0;

const int8_t SIM_PWR = 4;
const int8_t ETHERNET_CS = 4;

//const int8_t PWR_LED = 22;
const int8_t ERR_LED = 21;
const int8_t EXPANDER_CS = 21;
const int8_t NET_LED = 2;

const int8_t ETH_INT = 5;
const int8_t ETH_RES = 6;


const int8_t X_INPUT = 8;
const int8_t Y_INPUT = 9;
const int8_t Z_INPUT = 10;

int cpuCycleCounter = 0;

int loopCounter = 0;

long firstStartupDate = 0;

bool wifiActive = false;

int totalWatthSensorSamples = 0;

int emon1RemoveValue = 38;

ConnectionService connectionService = ConnectionService();
MQTTService mqttService = MQTTService();
EnergyService energyService = EnergyService();
WebServerService webServerService = WebServerService();
InputService inputService = InputService();
VibrationService<float, VIB_SAMPLING_FREQ_HZ, COV_CYCLES, CAL_CYCLES, SAMPLES_PER_CYCLE, PATTERN_MIN_LENGTH, PATTERN_MAX_LENGTH, PATTERNS_TO_CHECK> vibrationService;

CircularBuffer<XYZ, VIB_SAMPLING_FREQ_HZ * 20> accl;

TaskHandle_t core0ReadInputsTask;
TaskHandle_t core0ReadEnergyTask;
TaskHandle_t core1EvaluateVibrationsTask;
TaskHandle_t core1WebserverTask;
TaskHandle_t core1ConnectionServiceTask;

KX0231025Class* imu;
TLA2518Class* expander;


//Millis used to see that inputthread is running
long watchDogInputReadRunMillis = millis();

//Millis used to see that loopthread is running
long watchDogLoopRunMillis = millis();

//Millis used to see last contact with server
long lastContactMillis = millis();


SPIClass hspi(HSPI); //SPI-bus for accelerometer and IO-Expander
SPIClass vspi(VSPI); //SPI-bus for Ethernet and memory

int emonlCallbackHandler(unsigned int input)
{
	int value = expander->analogReadIO(input);

	if(value < 0)
	{
		return 0;
	}

	return value;   //read peak voltage
}

void loopMqttBroker()
{
	if (ConfigurationService::MQTTBrokerActive)
	{
		if (broker == NULL)
		{
			broker = new MqttBroker(ConfigurationService::MQTTBrokerPort);
			broker->begin();
		}

		broker->loop();
	}
}

hw_timer_t* readDataTimer = NULL;

TaskHandle_t taskHandleReadData = NULL;

const UBaseType_t xArrayIndex = 0;

int loopCount = 0;

void ReadAccelerationTask(void* arg) {
	Serial.println("Read acceleration task started");
	float x, y, z;
	uint32_t ulNotifiedValue;
	while (!ConfigurationService::Update) {
		ulNotifiedValue = ulTaskNotifyTakeIndexed(xArrayIndex,
			pdTRUE,
			portMAX_DELAY);

		if (ulNotifiedValue > 0) {
			if (ulNotifiedValue > 1) {
				/*DEBUG_PRINT("Bad, we are missing data or sampling to fast!");
				DEBUG_PRINT(" ");
				DEBUG_PRINT(ulNotifiedValue);
				DEBUG_PRINT("\n");*/
			}

			if (imu->readAcceleration(x, y, z) > 0) {
				loopCount++;
				accl.push(XYZ(x, y, z));

			}
			else {
				Serial.println("Cannot read acc");
				delay(1000);
			}
		}
		else {
			// This should not be possible
		}
	}
	vTaskDelete(NULL);
}

void IRAM_ATTR onReadTimer() {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	vTaskNotifyGiveFromISR(taskHandleReadData, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

bool lastResult = false;

void evaluateVibrationsTask(void* arg)
{
	Serial.println("Evaluate vibrations task started");
	while (!ConfigurationService::Update)
	{
		if (!accl.isEmpty()) {

			/*    DEBUG_PRINT("before available: ");
			DEBUG_PRINT(xAccl.available());
			*/
			// Get data from queue

			XYZ p = accl.pop();
			/*    DEBUG_PRINT("p ");
			DEBUG_PRINT(p.x);
			DEBUG_PRINT("\n");*/
			//Evaluate vibrationdata

			bool result = false;
			
			if (ConfigurationService::UseEnergyCycle)
			{
				result = vibrationService.evaluateData(analogsample1, analogsample2, analogsample3);
			}
			else
			{
				result = vibrationService.evaluateData(p.x, p.y, p.z);
			}

			if (loopCount == 100)
			{
				loopCount = 0;
			}

			if (result != lastResult) {
				Serial.println("Evaluate data result changed to " + String(result));

				lastResult = result;

				if (result)
				{
					InputEvent newEvent = InputEvent();
					newEvent.amount = 1;
					newEvent.flank = 1;
					newEvent.inputID = 8;
					newEvent.millis = rtc.getMillis();
					newEvent.rtcEpoch = rtc.getEpoch();

					mqttService.addEvent(newEvent);
				}
			}
			if (accl.available() <= VIB_SAMPLING_FREQ_HZ * 2) {
				/*DEBUG_PRINT("Available space in queue is getting low: ");
				DEBUG_PRINT(accl.available());
				DEBUG_PRINT("\n");*/
			}
		}
		else
		{
		}
		delay(1);
	}

	vTaskDelete(NULL);
}

void checkEFuse()
{
	// INITIAL SETUP...BURN THE EFUSE IF NECESSARY FOR PROPER OPERATION.
	// Force the FLASH voltage regulator to 3.3v, disabling "MTDI" strapping check at startup
	if ((REG_READ(EFUSE_BLK0_RDATA4_REG) & EFUSE_RD_SDIO_TIEH) == 0)
	{
		esp_efuse_reset();
		REG_WRITE(EFUSE_BLK0_WDATA4_REG, EFUSE_RD_SDIO_TIEH);
		esp_efuse_burn_new_values();
		DebugService::Information("setup eFuse", "---------------- Writing E-Fuse first startup SDIO_TIEH ---------------");
	} //burning SDIO_TIEH -> sets SDIO voltage regulator to pass-thru 3.3v from VDD
	if ((REG_READ(EFUSE_BLK0_RDATA4_REG) & EFUSE_RD_XPD_SDIO_REG) == 0)
	{
		esp_efuse_reset();
		REG_WRITE(EFUSE_BLK0_WDATA4_REG, EFUSE_RD_XPD_SDIO_REG);
		esp_efuse_burn_new_values();
		DebugService::Information("setup eFuse", "---------------- Writing E-Fuse first startup SDIO_REG ---------------");
	} //burning SDIO_REG -> enables SDIO voltage regulator (otherwise user must hardwire power to SDIO)
	if ((REG_READ(EFUSE_BLK0_RDATA4_REG) & EFUSE_RD_SDIO_FORCE) == 0)
	{
		esp_efuse_reset();
		REG_WRITE(EFUSE_BLK0_WDATA4_REG, EFUSE_RD_SDIO_FORCE);
		esp_efuse_burn_new_values();
		DebugService::Information("setup eFuse", "---------------- Writing E-Fuse first startup SDIO_FORCE ---------------");
	} //burning SDIO_FORCE -> enables SDIO_REG and SDIO_TIEH

	DebugService::Information("setup eFuse", "E-Fuse check done");
}

void setup()
{
	Serial.begin(115200);
	Serial.setDebugOutput(true);

	checkEFuse();
	

	DebugService::CurrentVersion = currentVersion;
	try
	{
		pinMode(15, OUTPUT);
		pinMode(27, OUTPUT);
		pinMode(4, OUTPUT);
		pinMode(21, OUTPUT);

		Serial.println("Verifying Hardware version");
		expander = new TLA2518Class(hspi, 21); //Accelerometer chip start
		if (expander->begin())
		{
			expander->reset();
			Serial.println("RSIoT Hardware version 2");

			//This is done before loading configuration so will only affect first startup
			
			
			ConfigurationService::newHardware = true;
			
			//Define IO
			expander->pinModeIO(0, OUTPUT);
			expander->pinModeIO(1, OUTPUT);
			expander->pinModeIO(2, ANALOG);
			expander->pinModeIO(3, ANALOG);
			expander->pinModeIO(4, ANALOG);
			expander->pinModeIO(5, OUTPUT);
			expander->pinModeIO(6, OUTPUT);
			expander->pinModeIO(7, OUTPUT);

			//Set outputs
			expander->digitalWriteIO(0, HIGH);
			expander->digitalWriteIO(1, HIGH);
			expander->digitalWriteIO(5, HIGH);
			expander->digitalWriteIO(6, HIGH);
			expander->digitalWriteIO(7, LOW);

			expander->setManualSequenceMode(true);
			expander->enableAveraging(true);
			expander->setAveragingParameters(OSC_SEL_ON, CLK_DIV_32);

			expander->calibrateADC();

			emon1.current(MANUAL_CHID_AIN4, 1);             // Current: input pin, calibration.
			emon1.setCallback(emonlCallbackHandler);
			emon2.current(MANUAL_CHID_AIN3, 1);             // Current: input pin, calibration.
			emon2.setCallback(emonlCallbackHandler);
			emon3.current(MANUAL_CHID_AIN2, 1);             // Current: input pin, calibration.
			emon3.setCallback(emonlCallbackHandler);

		}
		else
		{
			Serial.println("RSIoT Hardware version 1");

		}

		DebugService::Information("Startup", "---------------- RS-IoT starting up ----------------");
		DebugService::Information("Startup", "---------------- " + String(currentVersion) + " ----------------");

		DebugService::Information("Setup", "Initializing accelerometer");
		startIMU(); //Accelerometer chip start
		DebugService::Information("Setup", "Initializing done");

		DebugService::Information("Setup", "Configuring pins");
		configurePins(); //Configures the pins on the arduino cpu
		DebugService::Information("Setup", "Configuring pins done");

		DebugService::Information("Setup", "Initializing external memory");
		if (MemoryService::initialize(&vspi, &mqttService, 27, ConfigurationService::newHardware))
		{
			DebugService::Information("Setup", "Initializing done");
		}
		else
		{
			hardwareError = true;
		}

		DebugService::Information("Setup", "Initializing internal memory");
		if (MemoryServiceFAT::initialize())
		{
			DebugService::Information("Setup", "Initializing done");
		}
		else
		{
			hardwareError = true;
		}
		DebugService::Information("Setup", "Check configuration");
		if (!ConfigurationService::configurationWizardComplete)
		{
			if (ConfigurationService::currentConfigurationStep == 10)
			{
				ConfigurationService::VerifyConnection = true;
			}

			if (ConfigurationService::currentConfigurationStep == 0 && ConfigurationService::InstallationNumber != "" && (ConfigurationService::wifiActive || ConfigurationService::gsmActive))
			{
				//Old device that got updated but is configured
				ConfigurationService::configurationWizardComplete = true;
				ConfigurationService::currentConfigurationStep = 99;
				MemoryServiceFAT::saveConfiguration();
			}
			else
			{
				WifiUtils::getAvailableWifiNetworks();
				ConfigurationService::inConfigurationMode = true;
				ConfigurationService::enteredConfigurationMode = millis();
			}
		}
		//This is needed so that webserver will work even if WiFi-settings is changed during operation
		wifiActive = ConfigurationService::wifiActive;
		
		DebugService::Information("Setup", "Initializing input service");
		inputService.initialize(&rtc, &mqttService, &connectionService);
		DebugService::Information("Setup", "Initializing done");

		DebugService::Information("Setup", "Initializing connectionservice");
		connectionService.initialize(currentVersion,&rtc);
		DebugService::Information("Setup", "Initializing done");

		DebugService::Information("Setup", "Initializing mqttservice");
		mqttService.setCallback(mqttMessageHandler);
		mqttService.initialize(&connectionService, rtc);
		DebugService::Information("Setup", "Initializing done");

		DebugService::Information("Setup", "Load saved events");
		if (ConfigurationService::configurationWizardComplete)
		{
			MemoryService::processSavedEventsOnStartup();
		}
		DebugService::Information("Setup", "Starting eventreader thread");
		startInputReaderTask();
		DebugService::Information("Setup", "Done");

		DebugService::Information("Setup", "Starting vibration evaluator thread");
		startDataEvaluationWorkerTask();
		DebugService::Information("Setup", "Done");

		DebugService::Information("Setup", "Initializing webserverservice");
		webServerService.initialize(&connectionService, currentVersion);
		DebugService::Information("Setup", "Initializing done");
	}
	catch (const std::exception& ex)
	{
		DebugService::Error("Setup thread", ex.what());
		hardwareError = true;
	}
	catch (...)
	{
		DebugService::Error("Setup thread", "Unknown error!");
		hardwareError = true;
	}

	
	if (hardwareError)
	{
		DebugService::Error("RSIoT hardware", "Hardware is not working properly! See error messages!");
	}
	else
	{
		DebugService::Information("Setup", "Initializing vibration service");
		vibrationService.initialize();
		xTaskCreatePinnedToCore(ReadAccelerationTask, "ReadAccelerationTask", 4096, NULL, 10, &taskHandleReadData, 1);
		//// create timer hardware timer for reading acceleration
		readDataTimer = timerBegin(0, 80, true);
		timerAttachInterrupt(readDataTimer, &onReadTimer, true);
		timerAlarmWrite(readDataTimer, 1000000 / VIB_SAMPLING_FREQ_HZ, true);
		timerAlarmEnable(readDataTimer);

	}

	if (ConfigurationService::newHardware)
	{
		//Initialize energy sensors
		if(ConfigurationService::APNUrl == "data.rewicom.net")
			ConfigurationService::APNUrl = "iot.melita.io";

		DebugService::Information("Setup", "Initializing energyservice");
		energyService.initialize(&rtc, 9, &mqttService, &connectionService);
		startEnergyReaderTask();
		DebugService::Information("Setup", "Initializing done");
		
	}

	

	DebugService::Information("Setup", "Load saved events done");
	
	DebugService::Information("Setup", "---------------- RS-IoT startup done ----------------");
	startConnectionServiceThread();

	DebugService::Information("RSIoT ID", UniqueIDService::getUniqueID().c_str());
	DebugService::Information("RSIoT VERSION", String(currentVersion));
	DebugService::Information("RSIoT HARDWAREVERSION", ConfigurationService::newHardware?"RSIoT B":"RSIoT A");
}

void startInputReaderTask()
{
	xTaskCreatePinnedToCore(
		readInputsTask, /* Function to implement the task */
		"ReadInputsTask", /* Name of the task */
		10000,  /* Stack size in words */
		NULL,  /* Task input parameter */
		1,  /* Priority of the task */
		&core0ReadInputsTask,  /* Task handle. */
		0); /* Core where the task should run */
}

void startEnergyReaderTask()
{
	xTaskCreatePinnedToCore(
		readEnergyTask, /* Function to implement the task */
		"ReadEnergyTask", /* Name of the task */
		20000,  /* Stack size in words */
		NULL,  /* Task input parameter */
		5,  /* Priority of the task */
		&core0ReadEnergyTask,  /* Task handle. */
		0); /* Core where the task should run */
}

void startConnectionServiceThread()
{
	xTaskCreatePinnedToCore(
		connectionServiceTask, /* Function to implement the task */
		"connectionServiceTask", /* Name of the task */
		20000,  /* Stack size in words */
		NULL,  /* Task input parameter */
		0,  /* Priority of the task */
		&core1ConnectionServiceTask,  /* Task handle. */
		1); /* Core where the task should run */
}

void connectionServiceTask(void* parameter)
{
	Serial.println("ConnectionServiceTask start");

	while (!ConfigurationService::Update)
	{
		try
		{
		//	if (wifiActive && WifiUtils::numberOfConnectedClients() > 0)
		//	{
		//		//Do nothing cause otherwise it will block the connection
		//	}
		//	else
		//	{
		//		connectionService.loop();
		//	}


			loopMqttBroker();

			webServerService.loop();

		}
		catch (const std::exception& ex)
		{
			DebugService::Error("ConnectionService thread", ex.what());
		}
		catch (...)
		{
			DebugService::Error("ConnectionService thread", "Unknown error!");
		}

		delay(20);
	}

	DebugService::Information("ConnectionService", "connectionServiceTask stopped");
	vTaskDelete(NULL);
}



void startDataEvaluationWorkerTask()
{
	xTaskCreatePinnedToCore(
		evaluateVibrationsTask, /* Function to implement the task */
		"EvaluateVibrationsTask", /* Name of the task */
		40000,  /* Stack size in words */
		NULL,  /* Task input parameter */
		tskIDLE_PRIORITY,  /* Priority of the task */
		&core1EvaluateVibrationsTask,  /* Task handle. */
		0); /* Core where the task should run */
}




double correctReading(double reading, CalibrationDataSet knownValues) {
	int i = 0;

	reading = reading;

	if(knownValues.size == 0)
	{
		return 0;
	}
	// find the first known reading that's larger than the reading to correct
	while (i < knownValues.size && knownValues.data[i].reading < reading) {
		++i;
	}

	// if there's no larger reading, return the reading itself without any correction
	if (i == knownValues.size) {
		return reading;
	}

	// if there's no smaller reading, use the first known value for correction
	if (i == 0) {
		return knownValues.data[i].actual;
	}

	// interpolate between the two nearest known readings
	CalibrationPoint lower = knownValues.data[i - 1];
	CalibrationPoint upper = knownValues.data[i];
	return lower.actual + (upper.actual - lower.actual) * (reading - lower.reading) / (upper.reading - lower.reading);
}



CalibrationDataSet determineCalibrationPoint(int multiplier) {
	CalibrationDataSet result = CalibrationDataSet();
	switch (multiplier) {
	case 30:
		result.data = ConfigurationService::gsmActive ? knownValues30Clamp4G : knownValues30Clamp;
		result.size = 6;
		break;
	case 100:
		result.data = ConfigurationService::gsmActive ? knownValues100Clamp4G : knownValues100Clamp;
		result.size = 7;
		break;
	case 300:
		result.data = ConfigurationService::gsmActive ? knownValues300Clamp4G : knownValues300Clamp;
		result.size = 7;
		break;
	default:
		result.size = 0;
		break;
	}
	return result;
}

class LowPassFilter {
private:
	float alpha;
	float prevY;

public:
	// Constructor. alpha determines the cutoff frequency.
	LowPassFilter(float alphaVal) : alpha(alphaVal), prevY(0) {}

	// This function takes in a new input sample and returns the filtered output
	float filter(float x) {
		float y = alpha * x + (1 - alpha) * prevY;
		prevY = y;
		return y;
	}
};

double adjustValue(double value, double multiplier) {
	const double lowerThreshold = 0.013; // 1% tr�skel
	const double upperThreshold = 0.015; // 1.5% tr�skel
	const double maxValue = 1.0; // Maxv�rde f�r sensorn (100%)

	// Om v�rdet �r under den l�gre tr�skeln, s�tt det till 0
	if (value < lowerThreshold || multiplier == 0) {
		return 0;
	}
	// Om v�rdet �r mellan den l�gre och �vre tr�skeln, anv�nd en linj�r upprampning
	else if (value >= lowerThreshold && value < upperThreshold) {
		double normalizedPercentage = (value - lowerThreshold) / (upperThreshold - lowerThreshold);
		// Justera v�rdet linj�rt mellan 0 och 1.5A baserat p� dess procentandel mellan 1% till 1.5%
		return normalizedPercentage * upperThreshold;
	}
	// Om v�rdet �r �ver den �vre tr�skeln, anv�nd det som det �r
	return value;
}

void readEnergyTask(void* parameter)
{
	Serial.println("readEnergyTask start");
	const int n = 1000; 
	int analog1samples[n];
	int analog2samples[n];
	int analog3samples[n];

	const int meanCount = 5;

	double runningMean1[meanCount];
	double runningMean2[meanCount];
	double runningMean3[meanCount];

	int index = 0;
	int meanIndex = 0;
	int wattLoops = 0;

	long lastWattTime = millis();

	int lastRefValue = 0;

	LowPassFilter filter1(0.5);
	LowPassFilter filter2(0.5);
	LowPassFilter filter3(0.5);

	while (!ConfigurationService::Update)
	{
		while (index < n) {
			if (!ConfigurationService::TransmittingData)
			{
				int analogsampleReading1 = emonlCallbackHandler(MANUAL_CHID_AIN4);
				int analogsampleReading2 = emonlCallbackHandler(MANUAL_CHID_AIN3);
				int analogsampleReading3 = emonlCallbackHandler(MANUAL_CHID_AIN2);



				analogsample1 = analogsampleReading1;
				analogsample2 = analogsampleReading2;
				analogsample3 = analogsampleReading3;

				analog1samples[index] = analogsample1;
				analog2samples[index] = analogsample2;
				analog3samples[index] = analogsample3;

				index++;

				if (index == n)
				{
					/*ConfigurationService::outputRawData = true;
					Serial.print("1:");
					Serial.print(analogsample1);
					Serial.print('\t');
					Serial.print("2:");
					Serial.print(analogsample2);
					Serial.print('\t');
					Serial.print("3:");
					Serial.println(analogsample3);*/


					int input1Multipler = energyService.energyDataTriggers[0].multiplier;
					int input2Multipler = energyService.energyDataTriggers[1].multiplier;
					int input3Multipler = energyService.energyDataTriggers[2].multiplier;

					CalibrationDataSet input1Calibration = determineCalibrationPoint(input1Multipler);
					CalibrationDataSet input2Calibration = determineCalibrationPoint(input2Multipler);
					CalibrationDataSet input3Calibration = determineCalibrationPoint(input3Multipler);


					runningMean1[meanIndex] = emon1.calcIrms(analog1samples, n);// *input1Multipler;
					runningMean2[meanIndex] = emon2.calcIrms(analog2samples, n);// *input2Multipler;
					runningMean3[meanIndex] = emon3.calcIrms(analog3samples, n);// *input3Multipler;

					meanIndex++;
					if (meanIndex == meanCount)
					{
						meanIndex = 0;
					}

					double value1 = 0;
					double value2 = 0;
					double value3 = 0;

					for (int i = 0; i < meanCount; i++)
					{
						value1 += runningMean1[i];
						value2 += runningMean2[i];
						value3 += runningMean3[i];
					}

					value1 = value1 / meanCount;
					value2 = value2 / meanCount;
					value3 = value3 / meanCount;
					
					/*ConfigurationService::outputRawData = true;
					Serial.print("30A:");
					Serial.print(value1, 2);
					Serial.print('\t');
					Serial.print("100A:");
					Serial.print(value2, 2);
					Serial.print('\t');
					Serial.print("300A:");
					Serial.println(value3, 2);*/
					
					/*value1 = correctReading(value1, input1Calibration);
					value2 = correctReading(value2, input2Calibration);
					value3 = correctReading(value3, input3Calibration);*/
					value1 = adjustValue(value1,input1Multipler);
					value2 = adjustValue(value2,input2Multipler);
					value3 = adjustValue(value3,input3Multipler);

					value1 = value1 * input1Multipler;
					value2 = value2 * input2Multipler;
					value3 = value3 * input3Multipler;

					lastEnergyReading1 = value1;
					lastEnergyReading2 = value2;
					lastEnergyReading3 = value3;

					//wattLoops++;
					double timeSpent = TimeUtils::checkTimeDifferenceMillis(lastWattTime, millis()) / 1000.0;

					double input1Pf = energyService.energyDataTriggers[0].powerfactor;
					double input2Pf = energyService.energyDataTriggers[1].powerfactor;
					double input3Pf = energyService.energyDataTriggers[2].powerfactor;

					if (input1Pf == 0)
						input1Pf = 1;
					if (input2Pf == 0)
						input2Pf = 1;
					if (input3Pf == 0)
						input3Pf = 1;

					double input1voltage = energyService.energyDataTriggers[0].referencevoltage;
					double input2voltage = energyService.energyDataTriggers[1].referencevoltage;
					double input3voltage = energyService.energyDataTriggers[2].referencevoltage;

					if (input1voltage == 0)
						input1voltage = 230;
					if (input2voltage == 0)
						input2voltage = 230;
					if (input3voltage == 0)
						input3voltage = 230;


					double currentWatt1 = value1 * input1voltage * input1Pf;
					double currentWatt2 = value2 * input2voltage * input2Pf;
					double currentWatt3 = value3 * input3voltage * input3Pf;

					double totalWatthSensor1 = currentWatt1 * timeSpent / 3600.0;
					double totalWatthSensor2 = currentWatt2 * timeSpent / 3600.0;
					double totalWatthSensor3 = currentWatt3 * timeSpent / 3600.0;

					mqttService.addEnergyReadings(currentWatt1, totalWatthSensor1, currentWatt2, totalWatthSensor2, currentWatt3, totalWatthSensor3);
					energyService.addRecording(currentWatt1, currentWatt2, currentWatt3);
					lastWattTime = millis();

				}
			}
			delay(1);
		}
		index = 0;
		
	}

	DebugService::Information("readEnergyTask", "readInputsTask stopped");
	vTaskDelete(NULL);
}

unsigned long lastBlinkTime = millis();
bool blinkStatus = false;

bool lastNetStatusOld = LOW;
bool lastErrorStatusOld = LOW;
bool lastNetStatusNew = LOW;
bool lastErrorStatusNew = LOW;

void updateLEDs()
{
	// check state

	if (ConfigurationService::inConfigurationMode)
	{
		IOStatus::Net_Blink = true;
		IOStatus::Error_Blink = true;
	}
	else if (mqttService.connected)
	{
		IOStatus::Net = true;
		IOStatus::Net_Blink = false;
		IOStatus::Error_Blink = false;
	}
	else if (!mqttService.connected)
	{
		IOStatus::Net = false;
		IOStatus::Net_Blink = true;
		IOStatus::Error_Blink = false;
	}
	
	if (hardwareError)
	{
		IOStatus::Error = true;
	}
	else
	{
		IOStatus::Error = false;
	}

	if (ConfigurationService::Update)
	{
		IOStatus::Net_Blink = false;
		IOStatus::Error_Blink = false;
		IOStatus::Error = true;
		IOStatus::Net = true;
	}

	// check if 500ms has passed since the last blink
	if (millis() - lastBlinkTime >= 500)
	{
		blinkStatus = !blinkStatus;
		lastBlinkTime = millis();
	}

	// Depending on the hardware version
	if (ConfigurationService::newHardware)
	{
		// New hardware
		bool currentNetStatusNew = IOStatus::Net_Blink ? blinkStatus : IOStatus::Net;
		bool currentErrorStatusNew = IOStatus::Error_Blink ? blinkStatus : IOStatus::Error;

		if (currentNetStatusNew != lastNetStatusNew)
		{
			expander->digitalWriteIO(0, !currentNetStatusNew);
			lastNetStatusNew = currentNetStatusNew;
		}

		if (currentErrorStatusNew != lastErrorStatusNew)
		{
			expander->digitalWriteIO(1, !currentErrorStatusNew);
			lastErrorStatusNew = currentErrorStatusNew;
		}
	}
	else
	{
		// Old hardware
		bool currentNetStatusOld = IOStatus::Net_Blink ? blinkStatus : IOStatus::Net;
		bool currentErrorStatusOld = IOStatus::Error_Blink ? blinkStatus : IOStatus::Error;

		if (currentNetStatusOld != lastNetStatusOld)
		{
			digitalWrite(NET_LED, !currentNetStatusOld);
			lastNetStatusOld = currentNetStatusOld;
		}

		if (currentErrorStatusOld != lastErrorStatusOld)
		{
			digitalWrite(ERR_LED, !currentErrorStatusOld);
			lastErrorStatusOld = currentErrorStatusOld;
		}
	}
}

void readInputsTask(void* parameter)
{
	Serial.println("readInputsTask started");
	int loops = 0;
	watchDogInputReadRunMillis = millis();
	bool firstStart = true;
	
	bool simStatus = false;

	while (!ConfigurationService::Update)
	{
		try
		{
			interrupt_B_Buffert = !interrupt_B_Buffert;

			long interruptEvents[8];

			for (int i = 0;i < 8;i++)
			{
				if (interrupt_B_Buffert)
				{
					interruptEvents[i] = interruptBuffert_A[i];
					interruptBuffert_A[i] = 0;
				}
				else
				{
					interruptEvents[i] = interruptBuffert_B[i];
					interruptBuffert_B[i] = 0;
				}
			}

			inputService.loop(interruptEvents);
		}
		catch (const std::exception& ex)
		{
			DebugService::Error("Input read thread", ex.what());
		}
		catch (...)
		{
			DebugService::Error("Input read thread", "Unknown error!");
		}

		delay(1);
		if (loops == 1000)
		{
			watchDogInputReadRunMillis = millis();
			loops = 0;

			if (TimeUtils::checkTimeExpiredMillis(watchDogLoopRunMillis, millis(), 1000000))
			{
				DebugService::Error("Loop watchdog", "Loop thread has stopped working! Reset triggered!");
				ESP.restart();
			}
		}
		else
		{
			loops++;
		}

		if (ConfigurationService::newHardware)
		{
			
			if (IOStatus::Sim_Power != simStatus || firstStart)
			{
				expander->digitalWriteIO(7, IOStatus::Sim_Power);
				simStatus = IOStatus::Sim_Power;
				DebugService::Information("readInputsTask", "Simstatus changed to " + String(simStatus));
			}
			firstStart = false;
		}

		updateLEDs();
		
	}
	DebugService::Information("readInputsTask", "readInputsTask stopped");
	vTaskDelete(NULL);
}

void startIMU()
{
	imu = new KX0231025Class(hspi, 15);

	int responce = imu->begin(KX0231025_HIGHPOWER_MODE, KX0231025_RANGE_2G, KX0231025_DATARATE_1600HZ);

	if (responce == 1)
	{
		DebugService::Information("startIMU", "IMU Started succesfully");
		
	}
	if (responce == 0)
	{
		DebugService::Error("startIMU", "IMU start failed!");
		hardwareError = true;
	}
}

void activateConfigurationMode()
{
	if (!ConfigurationService::MQTTBrokerActive)
	{
		//DebugService::Information("configurationRequest", "Entering configuration mode");
		//delay(10);
		ConfigurationService::inConfigurationMode = true;
		ConfigurationService::enteredConfigurationMode = millis();
		//delay(10);
		//DebugService::Information("configurationRequest", "Initiating configuration mode done");
	}
}

void disableConfigurationMode()
{
	DebugService::Information("configurationRequest", "Disable configuration mode");
	ConfigurationService::inConfigurationMode = false;
}

long mqttLastReconnect = 0;

int lastBrokerClientsCount = 0;

void subscribeToMqtt()
{
	//if (mqttLastReconnect != mqttService.reconnectedEpoch || lastBrokerClientsCount != broker.clientsCount())
	//{
	//	lastBrokerClientsCount = broker.clientsCount();
	//	DebugService::Information("Loop", "Subscribing to connected broker client topics!");
	//	for (int i = 0; i < broker.getClients().size(); i++)
	//	{
	//		std::set<Topic>::iterator it;

	//		for (it = broker.getClients()[i]->.begin(); it != broker.getClients()[i]->subscriptions.end(); ++it) {
	//			Topic f = *it; // Note the "*" here
	//			//Subscribe!

	//			DebugService::Information("Loop", "Subscribing " + String(f.c_str()));
	//			mqttService.subscribeTo(String(f.c_str()));
	//		}
	//	}

	//	mqttLastReconnect = mqttService.reconnectedEpoch;
	//}
}

void IRAM_ATTR configurationRequest()
{
	if (ConfigurationService::InstallationNumber != 0 && ConfigurationService::inConfigurationMode && TimeUtils::checkTimeExpiredMillis(ConfigurationService::enteredConfigurationMode, millis(), 2000))
	{
		disableConfigurationMode();
	}
	else
	{
		activateConfigurationMode();
	}
}

void IRAM_ATTR inputInterrupt_0()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[0]++;
	}
	else
	{
		interruptBuffert_B[0]++;
	}
}
void IRAM_ATTR inputInterrupt_1()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[1]++;
	}
	else
	{
		interruptBuffert_B[1]++;
	}
}
void IRAM_ATTR inputInterrupt_2()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[2]++;
	}
	else
	{
		interruptBuffert_B[2]++;
	}
}
void IRAM_ATTR inputInterrupt_3()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[3]++;
	}
	else
	{
		interruptBuffert_B[3]++;
	}
}
void IRAM_ATTR inputInterrupt_4()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[4]++;
	}
	else
	{
		interruptBuffert_B[4]++;
	}
}
void IRAM_ATTR inputInterrupt_5()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[5]++;
	}
	else
	{
		interruptBuffert_B[5]++;
	}
}
void IRAM_ATTR inputInterrupt_6()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[6]++;
	}
	else
	{
		interruptBuffert_B[6]++;
	}
}
void IRAM_ATTR inputInterrupt_7()
{
	if (!interrupt_B_Buffert)
	{
		interruptBuffert_A[7]++;
	}
	else
	{
		interruptBuffert_B[7]++;
	}
}

void configurePins()
{
	
	if (ConfigurationService::newHardware)
	{
		
	}
	else
	{
		pinMode(ERR_LED, OUTPUT);
		pinMode(NET_LED, OUTPUT);
		pinMode(SIM_PWR, OUTPUT);
	}
	pinMode(D0_INPUT, INPUT_PULLUP);
	pinMode(D1_INPUT, INPUT_PULLUP);
	pinMode(D2_INPUT, INPUT_PULLUP);
	pinMode(D3_INPUT, INPUT_PULLUP);
	pinMode(D4_INPUT, INPUT_PULLUP);
	pinMode(D5_INPUT, INPUT_PULLUP);
	pinMode(D6_INPUT, INPUT_PULLUP);
	pinMode(D7_INPUT, INPUT_PULLUP);

	attachInterrupt(D0_INPUT, inputInterrupt_0, FALLING);
	attachInterrupt(D1_INPUT, inputInterrupt_1, FALLING);
	attachInterrupt(D2_INPUT, inputInterrupt_2, FALLING);
	attachInterrupt(D3_INPUT, inputInterrupt_3, FALLING);
	attachInterrupt(D4_INPUT, inputInterrupt_4, FALLING);
	attachInterrupt(D5_INPUT, inputInterrupt_5, FALLING);
	attachInterrupt(D6_INPUT, inputInterrupt_6, FALLING);
	attachInterrupt(D7_INPUT, inputInterrupt_7, FALLING);

	pinMode(BOOT_BUTTON, INPUT_PULLUP);
	attachInterrupt(BOOT_BUTTON, configurationRequest, FALLING);
}

void updateCpuCounter()
{
	cpuCycleCounter += 1;

	if (cpuCycleCounter > 100000)
	{
		cpuCycleCounter = 0;
	}
}

int lastFreeMemory = 0;

int loopSleepCounter = 0;

int serialErrorCounter = 0;

long millisLooped = 0;

long lastMillisValue = 0;

int energyCounter = 0;

void loop()
{
	if(ConfigurationService::ForceLostConnection && rtc.getEpoch() >= ConfigurationService::endForceLostConnectionEpoch)
	{
		if(ConfigurationService::restartAfterForcedLostConnection)
		{
			ESP.restart();
		}
		ConfigurationService::ForceLostConnection = false;
		ConfigurationService::endForceLostConnectionEpoch = 2147483647;
		
	}

	if (ConfigurationService::newHardware)
	{
		SDMemoryService::processDebugMessages();
	}

	try {
		watchDogLoopRunMillis = millis();

		int currentFreeMemory = freeMemory();

		DebugService::freeMemory = String(currentFreeMemory);

		if (lastMillisValue > millis())
		{
			millisLooped++;
		}
		lastMillisValue = millis();


		if (!ConfigurationService::timeIsSet)
		{
			DebugService::lastRestart = "Restart was " + String((millis() + 4294967 * millisLooped) / 1000.0) + "s ago";
		}
		else
		{
			DebugService::lastRestart = TimeUtils::getDateTimeString(firstStartupDate);
			DebugService::timeSynchronized = true;
		}

		if (!ConfigurationService::Update)
		{
			/*if (wifiActive && WifiUtils::numberOfConnectedClients() > 0)
			{
				*///Need to do connectionService.loop() here to keep the connection alive and not cause thread exceptions
				connectionService.loop();
			//}
			//connectionService.loop();
			connectionService.maintain();

			if (ConfigurationService::inTestMode || (ConfigurationService::configurationWizardComplete || ConfigurationService::VerifyConnection || ConfigurationService::currentConfigurationStep == 12) && loopSleepCounter == 0 && ConfigurationService::InstallationNumber != "" && (ConfigurationService::gsmActive != false || ConfigurationService::wifiActive != false || ConfigurationService::ethernetActive != false))
			{
				synchronizeTime();

				String vibrationStatusAnalyzis = "";

				if(vibrationService.isCalibrated || vibrationService.isCalibrating)
				{
					if(vibrationService.isCalibrating)
					{
						vibrationStatusAnalyzis = "Calibrating " + String(TimeUtils::checkTimeDifferenceMillis(vibrationService.evaluationStartMillis,millis())/1000);
					}
					else
					{
						vibrationStatusAnalyzis = "Calibrated ";
						if(vibrationService.isCalibrationDataLoaded)
						{
							vibrationStatusAnalyzis += "Data loaded";
						}
					}
				}
				else
				{
					vibrationStatusAnalyzis = "Not running";
				}

				mqttService.loop(currentFreeMemory, firstStartupDate, currentVersion, vibrationStatusAnalyzis);

				if (mqttService.connected)
				{
					subscribeToMqtt();

					lastContactMillis = millis();
				}

				DebugService::eventQueueAmount = String(mqttService.eventsInMQTTQueueCount());
			}

			

			loopSleepCounter++;

			if (loopSleepCounter >= 5)
			{
				loopSleepCounter = 0;

			}
			if (ConfigurationService::currentConfigurationStep == 13 && ConfigurationService::inConfigurationMode && TimeUtils::checkTimeExpiredMillis(ConfigurationService::enteredConfigurationMode, millis(), 500000))
			{
				disableConfigurationMode();

			}
			else if (ConfigurationService::InstallationNumber == 0)
			{
				ConfigurationService::enteredConfigurationMode = millis();
			}
		}
		else
		{
			mqttService.disconnect();
			WiFi.softAPdisconnect();
			delay(5000);

			UpdateService::execOTAFromMemory(connectionService.getClient(), ConfigurationService::UpdateURL, ConfigurationService::UpdatePort, ConfigurationService::InstallationNumber);

			DebugService::Warning("UpdateService::After update", "Reset triggered!");
			ESP.restart();
		}
		if (Serial.available() > 0 && Serial.available() < 20) {

			String serialData = Serial.readString(); // read the incoming byte:
			DebugService::Information("Serial data recieved", serialData);

			if (serialData.startsWith("SET "))
			{
				DebugService::Information("Serial data recieved", "SET message recieved :" + serialData);

			}
			if (serialData.startsWith("RESET"))
			{
				DebugService::Information("Serial data recieved", "RESET message recieved");

				ConfigurationService::Reset = true;
			}
			if (serialData.startsWith("UPDATE"))
			{
				DebugService::Information("Serial data recieved", "UPDATE message recieved");
				ConfigurationService::Update = true;
			}
			if (serialData.startsWith("FORCELOSTCONNECTION"))
			{
				DebugService::Information("Serial data recieved", "FORCELOSTCONNECTION message recieved");
				ConfigurationService::ForceLostConnection = !ConfigurationService::ForceLostConnection;
				DebugService::Information("Serial data recieved", "FORCELOSTCONNECTION set to " + String(ConfigurationService::ForceLostConnection));
			}
			if (serialData.startsWith("CONFIGURATIONMODE"))
			{
				DebugService::Information("Serial data recieved", "CONFIGURATIONMODE message recieved");
				activateConfigurationMode();
			}
			if (serialData.startsWith("TESTMODE"))
			{
				DebugService::Information("Serial data recieved", "TESTMODE message recieved");
				

				MemoryServiceFAT::removeFile("/configuration.conf");

				DebugService::Information("TEST Configuration", "Configuration files removed");

				if (ConfigurationService::newHardware)
				{
					DebugService::Information("TEST Energy sensor 1", "Energy sensor analog value: " + String(analogsample1));
					DebugService::Information("TEST Energy sensor 2", "Energy sensor analog value: " + String(analogsample2));
					DebugService::Information("TEST Energy sensor 3", "Energy sensor analog value: " + String(analogsample3));

					DebugService::Information("TEST Ethernet cable", String(Ethernet.linkStatus()));
				}
				DebugService::Information("TEST WiFi networks", String(WifiUtils::getAvailableWifiNetworks().indexOf("gsgbg") != -1));

				ConfigurationService::gsmActive = true;
				ConfigurationService::InstallationNumber = "40000";
				ConfigurationService::DisableHttps = false;
				ConfigurationService::MQTTPort = 8883;
				ConfigurationService::MQTTUrl = "40000.rsproduction.se";
				disableConfigurationMode();
				ConfigurationService::inTestMode = true;
				connectionService.initialize(currentVersion,&rtc);
				

				
			}
		}
		else if (Serial.available() > 20)
		{
			if (serialErrorCounter == 0)
			{
				DebugService::Error("Large amount of serial data recieved!", "Bytes recieved = " + String(Serial.available()));
			}

			serialErrorCounter++;

			if (serialErrorCounter == 50)
			{
				serialErrorCounter = 0;
			}
			int amount = Serial.available();
			for (int i = 0; i < amount; i++)
			{
				Serial.read();
			}
		}
		if (ConfigurationService::Reset)
		{
			DebugService::Warning("Loop", "Reset triggered!");
			ESP.restart();
		}
	}
	catch (const std::exception& ex)
	{
		DebugService::Error("loop thread", ex.what());
	}
	catch (...)
	{
		DebugService::Error("loop thread", "Unknown error!");
	}

	if (TimeUtils::checkTimeExpiredMillis(watchDogInputReadRunMillis, millis(), 120000))
	{
		DebugService::Error("Input read watchdog", "Input thread has stopped working!");
		ESP.restart();
	}
	

	

	delay(50);
}

void parseMessage(String message, double& num1, int& num2) {
	int commaIndex = message.indexOf(',');
	if (commaIndex == -1) {
		// Error: message is not in the correct format
		return;
	}
	num1 = message.substring(8, commaIndex).toDouble();
	num2 = message.substring(commaIndex + 1).toInt();
}

int wrongTimeInRowRecieved = 0;
int wrongTimeRecieved = 0;

int bigTimeDifferenceCount = 0;

void synchronizeTime()
{
	if (connectionService.connected && (!ConfigurationService::timeIsSet || loopCounter % 1000 == 0))
	{
		DebugService::Information("loop", "Synchronize time");
		long epochTime = 0;
		if (connectionService.getEpochTime(&epochTime))
		{
			DebugService::Information("loop", "Time recieved " + String(epochTime));

			
			wrongTimeInRowRecieved = 0;

			long timeDifference = epochTime - rtc.getEpoch();

			if (ConfigurationService::timeIsSet && timeDifference > 10000 && bigTimeDifferenceCount < 5)
			{
				bigTimeDifferenceCount++;
				DebugService::Warning("Time synchronization", "Big time difference found. Retrying timesynchronization. " + String(timeDifference) + "s difference");
			}
			else if(timeDifference != 1 && timeDifference != -1)
			{
				bigTimeDifferenceCount = 0;

				DebugService::Information("loop", "Time recieved deviation = " + String(timeDifference));

				rtc.setTime(epochTime);
			}

			if (!ConfigurationService::timeIsSet)
			{
				DebugService::Information("loop", "Adjusting time on existing events");

				mqttService.timeSynchronized(timeDifference);

				firstStartupDate = rtc.getEpoch();

				DebugService::Information("loop", "Time is synchronized");
				ConfigurationService::timeIsSet = true;
			}
		}
		else
		{
			wrongTimeInRowRecieved++;
			wrongTimeRecieved++;

			if (wrongTimeRecieved > 5)
			{
				DebugService::Error("Time synchronization", "Stuck in getting wrong time! Reseting connection!");
				connectionService.resetConnection();
				wrongTimeRecieved = 0;
			}
			if (wrongTimeInRowRecieved > 20)
			{
				DebugService::Error("Time synchronization", "Stuck in getting wrong time! Restarting!");
				ESP.restart();
			}
		}
	}

	loopCounter++;

	if (loopCounter >= 1000000)
	{
		loopCounter = 0;
	}
}

String currentArticleID = "";

void handleArticleChange(String topic, String payload, unsigned int length)
{
	StaticJsonDocument<256> doc;
	deserializeJson(doc, payload.c_str(), length);
	if (doc.containsKey("article"))
	{
		DebugService::Information("handleArticleChange", "Article message deserialized");
		String articleID = doc["article"];

		float cycleTime = doc["cycletime"].as<String>().toFloat();

		bool disableSavedProfile = false;

		if (doc.containsKey("disableSavedProfile"))
		{
			disableSavedProfile = doc["disableSavedProfile"];
		}

		if (disableSavedProfile)
		{
			vibrationService.changeItem(cycleTime, articleID, true);
		}
		else if (articleID != currentArticleID && articleID != "")
		{
			vibrationService.changeItem(cycleTime, articleID);
			currentArticleID = articleID;
		}
	}
}

void mqttMessageHandler(String topic, String payload, unsigned int length)
{
	//DebugService::Information("mqttMessageHandler", "Message recieved: " + topic + " payload " + payload);

	//vibrationService.handleMessage(topic, payload, length);
	mqttService.handleMessage(topic, payload, length);
	connectionService.handleMessage(topic, payload, length);
	inputService.handleMessage(topic, payload, length);

	if (topic.endsWith("time"))
	{
		long epochTime = payload.toInt();

		/*if (abs(epochTime - rtc.getEpoch()) > 20)
		{
			DebugService::Error("mqttMessageHandler", "Time difference is too big! " + String(epochTime - rtc.getEpoch()));

			connectionService.resetConnection();
		}*/

	}

	if (topic.endsWith("article"))
	{
		handleArticleChange(topic, payload,length);
	}

	if (ConfigurationService::newHardware)
	{
		energyService.handleMessage(topic, payload, length);
	}

	if(topic.endsWith("command"))
	{
		StaticJsonDocument<256> doc;
		deserializeJson(doc, payload.c_str(), length);
		if (doc.containsKey("command"))
		{
			if(doc["command"] == "reset")
			{
				mqttService.sendCommandResponse("Reset", "Resetting device");
				delay(1000);
				DebugService::Information("mqttMessageHandler", "Reset command recieved");
				ConfigurationService::Reset = true;
			}
			else if(doc["command"] == "update")
			{
				mqttService.sendCommandResponse("Update", "Updating device");
				delay(1000);
				DebugService::Information("mqttMessageHandler", "Update command recieved");
				ConfigurationService::Update = true;
			}
			else if(doc["command"] == "configuration")
			{
				mqttService.sendCommandResponse("Configuration", "Entering configuration mode");
				delay(1000);
				DebugService::Information("mqttMessageHandler", "Configuration command recieved");
				activateConfigurationMode();
			}
			else if(doc["command"] == "restart_analyzis")
			{
				mqttService.sendCommandResponse("restart_analyzis", "Restarting analysis on " + vibrationService.id + " with likely cycletime " + String(vibrationService.likelyCycletime));
				
				DebugService::Information("mqttMessageHandler", "restart_analyzis command recieved");
				vibrationService.changeItem(vibrationService.likelyCycletime, vibrationService.id, true);

			}
			else if(doc["command"] == "factory_reset" && doc.containsKey("confirm") && doc["confirm"] == "true")
			{
				mqttService.sendCommandResponse("factory_reset", "Factory reseting device");
				delay(1000);
				MemoryServiceFAT::removeFile("/configuration.conf");
				DebugService::Information("mqttMessageHandler", "Factory reset command recieved");
				ConfigurationService::Reset = true;
			}
			else if (doc["command"] == "getlog" && doc.containsKey("startEpoch") && doc.containsKey("endEpoch"))
			{
				//DebugService::Information("mqttMessageHandler", "Get log command recieved, startepoch= " + String(doc["startEpoch"]) + " endepoch= " + String(doc["endEpoch"]));
				MemoryService::activateSendLogEntries(doc["startEpoch"], doc["endEpoch"]);
				DebugService::Information("mqttMessageHandler", "Log activated startFile= " + SDMemoryService::startFile);
				mqttService.sendCommandResponse("getlog", "Starting to send log");
			}
			else if (doc["command"] == "forcelostconnection")
			{
				if (doc.containsKey("endEpoch"))
				{
					ConfigurationService::endForceLostConnectionEpoch = doc["endEpoch"];
				}
				else
				{
					ConfigurationService::endForceLostConnectionEpoch = rtc.getEpoch() + 300;
				}


				if (doc.containsKey("restart") && doc["restart"] == 1)
				{
					ConfigurationService::restartAfterForcedLostConnection = true;
				}
				else
				{
					ConfigurationService::restartAfterForcedLostConnection = false;
				}
				//DebugService::Information("mqttMessageHandler", "Get log command recieved, startepoch= " + String(doc["startEpoch"]) + " endepoch= " + String(doc["endEpoch"]));
				ConfigurationService::ForceLostConnection = true;
				DebugService::Information("mqttMessageHandler", "forcelostconnection activated, will end " + String(ConfigurationService::endForceLostConnectionEpoch));
				mqttService.sendCommandResponse("forcelostconnection", "forcelostconnection activated, will end " + String(ConfigurationService::endForceLostConnectionEpoch));
			}
			else
			{
				mqttService.sendCommandResponse("Unknown", "Unknown command");
			}
		}
		else
		{
			mqttService.sendCommandResponse("Unknown", "Invalid payload");
		}
	}

	//DebugService::Information("mqttMessageHandler", "Responce from broker " + String(client.publish(topic.c_str(), payload.c_str(), length)));
}

bool calibrated = false;

int freeMemory() {
	return ESP.getFreeHeap();
}