/*
 Name:		Library.cpp
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "SDMemoryService.h"

int SDMemoryService::_spiPin = -1;
SPIClass* SDMemoryService::_spi;
long SDMemoryService::savedEvents = 0;
FsFile SDMemoryService::root;
std::vector<String> SDMemoryService::logFiles;
std::vector<String> SDMemoryService::eventFiles;
IEventProcessor* SDMemoryService::eventProcessor;
String SDMemoryService::currentDebugFileName = "";
int SDMemoryService::debugFileMessageCount = 0;
String SDMemoryService::startFile = "";
int SDMemoryService::startLine = 0;
long SDMemoryService::startEpoch = 0;
long SDMemoryService::endEpoch = 0;
bool SDMemoryService::sendLogEntries = false;
int SDMemoryService::currentLine = 0;
String SDMemoryService::currentFileName = "";
unsigned long SDMemoryService::lastEpoch = 0;
bool SDMemoryService::syncedLastEpoch = false;
SdFat SDMemoryService::sd;

bool SDMemoryService::initialize(SPIClass* spi, int spiPin, IEventProcessor* eventProcessor)
{
	SDMemoryService::eventProcessor = eventProcessor;

	DebugService::Information("SDMemoryService::initialize", "Initializing SD card");
	bool result = sd.begin(SD_CONFIG);
	if (!result) {
		DebugService::Error("SDMemoryService::initialize", "SD card initialization failed");
	}
	SDMemoryService::_spiPin = spiPin;
	SDMemoryService::_spi = spi;
	//sd.end();
	return result;
}

String SDMemoryService::loadFileAsString(String fileName)
{
	if (fileName.length() > 0 && fileName[0] != '/') {
		fileName = "/" + fileName;
	}

	//sd.begin(SD_CONFIG);
	FsFile file = sd.open(fileName,  O_RDWR | O_CREAT);
	//sd.end();
	return file.readString();
}

bool SDMemoryService::saveFile(String fileName, String fileData)
{
	if (fileName.length() > 0 && fileName[0] != '/') {
		fileName = "/" + fileName;
	}

	//sd.begin(SD_CONFIG);
	FsFile file = sd.open(fileName, O_RDWR | O_CREAT);
	file.println(fileData);
	file.close();
	//sd.end();
	return true;
}

bool SDMemoryService::removeFile(String fileName)
{
	if (fileName.length() > 0 && fileName[0] != '/') {
		fileName = "/" + fileName;
	}

	//sd.begin(SD_CONFIG);
	sd.remove(fileName);
	//sd.end();
	return true;
}

void SDMemoryService::loadConfiguration()
{
	String data = loadFileAsString("/configuration.conf");

	if (data == "")
	{
		return;
	}

	ConfigurationService::LoadConfig(data);
}

bool SDMemoryService::hasSavedEvents() {
	bool foundEventFile = false;

	//sd.begin(SD_CONFIG);
	FsFile root = sd.open("/");

	while (true) {
		FsFile entry = root.openNextFile();
		if (!entry) {
			break; // No more files
		}

		if (!entry.isDirectory()) {
			char filename[12];

			entry.getName(filename,12);
			if (String(filename).endsWith(".evt")) {
				foundEventFile = true;
				entry.close();
				break; // Break out of loop as soon as one event FsFile is found
			}
		}

		entry.close();
	}

	//sd.end();

	return foundEventFile;
}

void SDMemoryService::clearEventMemory() {
	//sd.begin(SD_CONFIG);

	FsFile root = sd.open("/"); // Open the root directory
	while (true) {
		FsFile entry = root.openNextFile();
		if (!entry) {
			break; // No more files
		}
		if (!entry.isDirectory()) {
			char filename[12];

			entry.getName(filename, 12);
			if (String(filename).endsWith(".evt")) {
				DebugService::Information("SDMemoryService::clearEventMemory", "Removing eventFsFile from SD card " + String(filename));
				sd.remove("/" + String(filename)); // Remove the event file
			}
		}
		entry.close();
	}
	savedEvents = 0;
	eventFiles = listFiles(".evt","/");
	//sd.end();
}

void SDMemoryService::saveEvent(InputEvent inputEvent)
{
	DebugService::Information("SDMemoryService::saveEvent", "Saving event to SD card");
	////sd.begin(SD_CONFIG);
	// Convert rtcEpoch to "YYYY-MM-DD HH"
	tm dateTime = TimeUtils::epochToDateTime(inputEvent.rtcEpoch);
	String dateHour = String(dateTime.tm_mon) + String(dateTime.tm_mday) + String(dateTime.tm_hour); // Extracts "YYYY-MM-DD HH"
	
	String formattedFileName = "/" + dateHour + ".evt";


	Serial.println(formattedFileName);
	// Assuming the InputEvent has a method called serialize that returns a String representation of the event
	String serializedData = inputEvent.Serialize();

	//sd.begin(SD_CONFIG);
	FsFile file = sd.open(formattedFileName, O_RDWR | O_CREAT | O_AT_END); // Open the FsFile in append mode
	if (file)
	{
		file.println(serializedData); // Write the serialized data to the file
		file.close();
		savedEvents++;
	}
	else
	{
		// Error handling, in case FsFile can't be opened.
		DebugService::Error("SDMemoryService::saveEvent", "Failed to open FsFile for appending");
	}
	//sd.end();
}

void SDMemoryService::processSavedEventsOnStartup()
{
	int index = 0;

	long startEpoch = getLastEventEpoch();

	DebugService::Information("SDMemoryService::processSavedEventsOnStartup", "Processing saved events on startup, last epoch event " + String(startEpoch));
	eventFiles = listFiles(".evt","/");

	int eventsLoaded = 0;
	while (index < eventFiles.size()) {
		String nextEventFileContent = getNextEventFile(index);
		DebugService::Information("SDMemoryService::processSavedEventsOnStartup", "Processing FsFile at index: " + String(index) + " of " + String(eventFiles.size()));
		// Assuming each line in the FsFile represents one event
		int startPos = 0;
		int endPos = nextEventFileContent.indexOf('\n', startPos);
		//DebugService::Information("SDMemoryService::processSavedEventsOnStartup", nextEventFileContent);
		while (endPos != -1) {
			String serializedEvent = nextEventFileContent.substring(startPos, endPos);
			//DebugService::Information("SDMemoryService::processSavedEventsOnStartup", serializedEvent);
			InputEvent inputEvent(serializedEvent);

			if (inputEvent.rtcEpoch >= startEpoch)
			{
				//DebugService::Information("SDMemoryService::processSavedEventsOnStartup", "Event with epoch " + String(inputEvent.rtcEpoch) + " added");
				
				if (inputEvent.isValidEvent())
				{
					eventProcessor->addEvent(inputEvent);
					eventsLoaded++;
					savedEvents++;
				}
			}
			else
			{
				DebugService::Information("SDMemoryService::processSavedEventsOnStartup", "Event with epoch " + String(inputEvent.rtcEpoch) + " disgarded because of old epoch");
			}


			// Move to the next line
			startPos = endPos + 1;

			
			endPos = nextEventFileContent.indexOf('\n', startPos);
			
		}
		index++;
	}

	DebugService::Information("SDMemoryService::processSavedEventsOnStartup", "Events loaded: " + String(eventsLoaded));
}

bool SDMemoryService::sendLogActive()
{
	return sendLogEntries;
}

void SDMemoryService::saveLastEventEpoch(long epoch)
{
	//sd.begin(SD_CONFIG);
	
	String formattedFileName = "/lastEventEpoch.txt";
	//sd.begin(SD_CONFIG);

	if(sd.exists(formattedFileName))
		sd.remove(formattedFileName);

	FsFile file = sd.open(formattedFileName, O_RDWR | O_CREAT); // Open the FsFile in write mode
	if (file)
	{
		file.println(epoch); // Write the serialized data to the file
		file.close();
	}
	else
	{
		// Error handling, in case FsFile can't be opened.
		DebugService::Error("SDMemoryService::saveLastEventEpoch", "Failed to open FsFile for writing");
	}
	//sd.end();
}

long SDMemoryService::getLastEventEpoch()
{
	//sd.begin(SD_CONFIG);
	long epoch = 0;
	String formattedFileName = "/lastEventEpoch.txt";
	FsFile file = sd.open(formattedFileName, O_RDWR | O_CREAT); // Open the FsFile in read mode
	if (file)
	{
		String epochString = file.readStringUntil('\n');
		epoch = epochString.toInt();

		DebugService::Information("SDMemoryService::getLastEventEpoch", "Found lastEvent epoch " + String(epoch));
		file.close();
	}
	else
	{
		// Error handling, in case FsFile can't be opened.
		DebugService::Error("SDMemoryService::getLastEventEpoch", "Failed to open FsFile for reading");
	}
	//sd.end();
	return epoch;
}

long SDMemoryService::amountOfSavedEvents()
{
	return savedEvents;
}

bool SDMemoryService::isDigit(char c) {
	return c >= '0' && c <= '9';
}

int SDMemoryService::extractNumber(const String& filename) {
	int number = 0;
	int i = 0;

	// Find the first digit in the string
	while (i < filename.length() && !isDigit(filename[i])) {
		i++;
	}

	// Extract the number from the string
	while (i < filename.length() && isDigit(filename[i])) {
		number = number * 10 + (filename[i] - '0');
		i++;
	}

	return number;
}

// Custom comparator to compare filenames by their numeric part
bool SDMemoryService::compareFiles(const String& a, const String& b) {
	int numA = extractNumber(a);
	int numB = extractNumber(b);
	return numA < numB;
}


std::vector<String> SDMemoryService::listFiles(String extension, String folder) {
	//sd.begin(SD_CONFIG);
	
	root = sd.open(folder);

	std::vector<String> files;
	while (true) {
		FsFile entry = root.openNextFile();
		if (!entry) {
			break; // No more files
		}
		if (!entry.isDirectory()) {
			char filename[12];

			entry.getName(filename, 12);
			if (String(filename).endsWith(extension)) {
				files.push_back(filename);
			}
		}
		entry.close();
	}
	std::sort(files.begin(), files.end(), compareFiles);
	//sd.end();
	return files;
}

unsigned long SDMemoryService::getLastLogEpoch() {
	std::vector<String> logFiles = listFiles(".log", "/logs");
	if (logFiles.empty()) {
		return -1;
	}

	// Get the last log file
	String lastLogFile = logFiles.back();
	//sd.begin(SD_CONFIG);
	long result = getFirstOrLastTimestamp(lastLogFile, "/logs", false);
	//sd.end();
	return result;
}

void SDMemoryService::processDebugMessages()
{
	while (DebugService::debugMessagesToSave.itemCount() > 0)
	{
		DebugMessage message = DebugService::debugMessagesToSave.dequeue();
		unsigned long messageEpoch = message.time;

		//Serial.println("Dequing log message");
		// Check if the current epoch is synchronized
		if (messageEpoch < 1000000000) { // Threshold for unsynchronized time
			
			if (!syncedLastEpoch)
			{
				syncedLastEpoch = true;
				
				lastEpoch = getLastLogEpoch();
				Serial.println("Last epoch: " + String(lastEpoch));
			}
			if (lastEpoch > 0) {
				messageEpoch = lastEpoch + message.time; // Use the last epoch + 1 second as the new epoch
			}
			
		}

		String messageString = String(messageEpoch) + ":" + String(message.debugLevel == 1 ? "ERROR" : message.debugLevel == 2 ? "WARNING" : "INFORMATION") + ":" + message.message;

		int maxFileSize = 10000;
		//Serial.print(messageString);
		if (SDMemoryService::currentDebugFileName == "" || SDMemoryService::debugFileMessageCount > maxFileSize)
		{
			// Count the number of log files in the "logs" directory
			//sd.begin(SD_CONFIG);

			// Check if the directory exists
			if (!sd.exists("/logs")) {
				// Create the directory
				if (sd.mkdir("/logs")) {
					Serial.println("Directory created");
				}
				else {
					Serial.println("Failed to create directory");
				}
			}

			FsFile dir = sd.open("/logs");
			std::vector<int> fileNumbers;
			while (true) {
				FsFile entry = dir.openNextFile();
				if (!entry) {
					// no more files
					break;
				}
				if (!entry.isDirectory()) {
					char fileNamechar[32]; // assuming the filename won't exceed 31 characters
					entry.getName(fileNamechar, sizeof(fileNamechar)); // Using getName instead of name
					String fileName = String(fileNamechar);
					fileName.replace("/logs/", "");
					fileName.replace(".log", "");
					fileNumbers.push_back(fileName.toInt());
				}
				entry.close();
			}
			dir.close();

			// Sort the file numbers to find the oldest one
			std::sort(fileNumbers.begin(), fileNumbers.end());

			if (fileNumbers.size() >= 200) {
				// Delete the oldest file
				String oldestFileName = "/logs/" + String(fileNumbers[0]) + ".log";
				if (sd.remove(oldestFileName.c_str())) {
					DebugService::Information("ProcessLogMessages","Deleted oldest log file: " + oldestFileName);
				}
				else {
					DebugService::Warning("ProcessLogMessages", "Failed to delete oldest log file: " + oldestFileName);
				}
			}
			int lastFileNumber = 0;
			if (!fileNumbers.empty()) {
				lastFileNumber = fileNumbers.back(); // Get the last file name
			}

			// Generate new FsFile name based on the count
			String newFileName = "/logs/" + String(lastFileNumber + 1) + ".log";

			DebugService::Information("ProcessLogMessages", "New logFsFile created with FsFile name: " + newFileName);

			SDMemoryService::currentDebugFileName = newFileName;
			SDMemoryService::debugFileMessageCount = 0;
		}

		if (SDMemoryService::saveFile(SDMemoryService::currentDebugFileName, messageString))
		{
			SDMemoryService::debugFileMessageCount += 1;
		}
	}
}

// Utility to get the first or last timestamp from a file
unsigned long SDMemoryService::getFirstOrLastTimestamp(const String& filename, const String& folder, bool first) {
	Serial.println("Getting first or last timestamp from file: " + folder + "/" + filename);
	FsFile file = sd.open(folder + "/" + filename);
	if (!file)
	{
		Serial.println("Failed to open file");
		return 0;
	}

	unsigned long timestamp = 0;
	if (first) {
		String line = file.readStringUntil('\n');
		timestamp = getTimestampFromLine(line);
	}
	else {
		// Move to the end and read backwards to find the last line (more complex logic required for actual implementation)
		// Here, we'll simplify by reading the entire file, which is not efficient for large files
		String lastLine;
		while (file.available()) {
			lastLine = file.readStringUntil('\n');
		}
		timestamp = getTimestampFromLine(lastLine);
	}
	file.close();
	Serial.println("Timestamp: " + String(timestamp));
	return timestamp;
}

int SDMemoryService::findLineInFile(const String& filename, const String& folder, unsigned long targetEpoch, bool findStart) {
	FsFile file = sd.open(folder + "/" + filename);
	if (!file) return -1; // FsFile couldn't be opened

	String line;
	int lineNum = 0;
	int resultLineNum = -1;
	while (file.available()) {
		line = file.readStringUntil('\n');
		lineNum++;
		unsigned long timestamp = getTimestampFromLine(line);
		if (findStart) {
			if (timestamp >= targetEpoch) {
				resultLineNum = lineNum;
				Serial.println("Found start line: " + line + " at line number: " + String(lineNum) + " with timestamp: " + String(timestamp) + " and target epoch: " + String(targetEpoch));
				break; // Found the starting line
			}
		}
		else {
			if (timestamp <= targetEpoch) {
				resultLineNum = lineNum; // Update to the latest line that meets the condition
			}
			else {
				Serial.println("Found end line: " + line + " at line number: " + String(lineNum) + " with timestamp: " + String(timestamp) + " and target epoch: " + String(targetEpoch));
				break; // Passed the target epoch, no need to search further
			}
		}
	}
	file.close();
	return resultLineNum;
}

unsigned long SDMemoryService::getTimestampFromLine(const String& line) {
	// Assumes the timestamp is at the start of the line, followed by a :
	int index = line.indexOf(':');
	if (index == -1) return 0;
	return line.substring(0, index).toInt();
}

int SDMemoryService::binarySearchForFile(const std::vector<String>& files, const String& folder, unsigned long targetEpoch, bool findStart) {
	int left = 0;
	int right = files.size() - 1;
	while (left <= right) {
		int mid = left + (right - left) / 2;
		unsigned long firstTimestamp = getFirstOrLastTimestamp(files[mid],folder, true);
		unsigned long lastTimestamp = getFirstOrLastTimestamp(files[mid], folder, false);

		if (targetEpoch >= firstTimestamp && targetEpoch <= lastTimestamp) {
			if (findStart) {
				// Check if this or any previous FsFile should be the start
				if (mid == 0 || getFirstOrLastTimestamp(files[mid - 1], folder, false) < targetEpoch) {
					return mid;
				}
				else {
					right = mid - 1;
				}
			}
			else {
				// Check if this or any next FsFile should be the end
				if (mid == files.size() - 1 || getFirstOrLastTimestamp(files[mid + 1], folder, true) > targetEpoch) {
					return mid;
				}
				else {
					left = mid + 1;
				}
			}
		}
		else if (targetEpoch < firstTimestamp) {
			right = mid - 1;
		}
		else {
			left = mid + 1;
		}
	}
	return -1; // Not found
}

void SDMemoryService::activateSendLogEntries(unsigned long startEpoch, unsigned long endEpoch) {
	// Ensure eventFiles is populated and sorted
	logFiles = listFiles(".log","/logs");
	//sd.begin(SD_CONFIG);
	int startIndex = binarySearchForFile(logFiles,"/logs", startEpoch, true);

	startFile = startIndex != -1 ? logFiles[startIndex] : "";
	startLine = startIndex != -1 ? findLineInFile(startFile,"/logs", startEpoch, true) : -1;

	SDMemoryService::startEpoch = startEpoch;
	SDMemoryService::endEpoch = endEpoch;

	//sd.end();
	currentFileName = startFile;
	currentLine = startLine;
	sendLogEntries = true;
}

void SDMemoryService::deactivateSendLogEntries() {
	sendLogEntries = false;
}

String SDMemoryService::findStringOnLineInFile(const String& filename, const String& folder,int lineNumber)
{
	String lineValue;
	//sd.begin(SD_CONFIG);
	bool lineFound = false;
	// Read the next line from the current file
	FsFile file = sd.open(folder +"/" +filename);
	if (!file) {
		Serial.println("findStringOnLineInfile Failed to open file");
		deactivateSendLogEntries();
		return "";
	}

	int lineNum = 0;

	while (file.available()) {
		lineValue = file.readStringUntil('\n');
		lineNum++;
		if (lineNum == lineNumber) {
			lineFound = true;
			break;
		}
	}
	file.close();
	//sd.end();

	if (!lineFound)
		return "";
	return lineValue;
}

DebugMessage SDMemoryService::getNextLogEntry() {
	
	if (!sendLogEntries) {
		return DebugMessage();
	}
	String line;
	line = findStringOnLineInFile(currentFileName, "/logs", currentLine);
	// Update the current FsFile and line

	if (getTimestampFromLine(line) > endEpoch) {
		deactivateSendLogEntries();
		Serial.println("End epoch reached " + String(endEpoch) + " in file: " + currentFileName + " at line: " + String(currentLine));
		return DebugMessage();
	}

	if (line != "") {
		currentLine++;
		Serial.println("Line: " + line);
	}
	else {
		// Move to the next file
		Serial.println("Moving to the next file");
		auto it = std::find(logFiles.begin(), logFiles.end(), currentFileName);
		if (it != logFiles.end()) {
			int currentIndex = std::distance(logFiles.begin(), it);

			// Check if there's a next FsFile in the vector
			if (currentIndex < logFiles.size() - 1) {
				String nextFileName = logFiles[currentIndex + 1];
				Serial.println("Next file: " + nextFileName);
				currentFileName = nextFileName;
				currentLine = 1;
				line = findStringOnLineInFile(currentFileName, "/logs", currentLine);
				if (line != "") {
					currentLine++;
					Serial.println("Line: " + line);
				}
				else {
					Serial.println("Error reading next file.");
					deactivateSendLogEntries();
					return DebugMessage();
				}
			}
			else {
				Serial.println("No next FsFile found.");
				deactivateSendLogEntries();
				return DebugMessage();
			}
		}
		else {
			Serial.println("Current FsFile not found.");
			deactivateSendLogEntries();
			return DebugMessage();
		}
		
	}

	return DebugMessage(line);
}

String SDMemoryService::getNextEventFile(int index) {

	DebugService::Information("SDMemoryService::getNextEventFile", "Getting next event FsFile at index: " + String(index));
	if (eventFiles.empty()) {
		eventFiles = listFiles(".evt","/"); // Populate eventFiles vector if it's empty
	}

	if (eventFiles.size() == 0 || eventFiles.size() < index) {
		DebugService::Information("SDMemoryService::getNextEventFile", "No more files!");
		return ""; // No more files left
	}

	
	String filename = eventFiles[index];
	DebugService::Information("SDMemoryService::getNextEventFile", "Reading FsFile " + filename);
	//sd.begin(SD_CONFIG);
	sd.open("/");
	FsFile file = sd.open("/" + filename, O_RDWR);
	if (!file) {
		DebugService::Error("SDMemoryService::getNextEventFile", "Failed to open file: " + filename);
		return "";
	}
	String fileContent = file.readString();
	file.close();
	//sd.end();
	//DebugService::Information("SDMemoryService::getNextEventFile", "FsFile content: " + fileContent);
	return fileContent;
}
