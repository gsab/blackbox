/*
 Name:		Library.cpp
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "EEPROMMemoryService.h"

long EEPROMMemoryService::savedEvents = 0;
IEventProcessor* EEPROMMemoryService::eventProcessor;
int32_t EEPROMMemoryService::nextAdress = 0;
bool* EEPROMMemoryService::metadata = NULL;

bool EEPROMMemoryService::initialize(SPIClass* spi, int spiPin, IEventProcessor* eventProcessor)
{
	EEPROMMemoryService::eventProcessor = eventProcessor;

	EEPROMMemoryService::metadata = (bool*)ps_malloc(MEMORY_SIZE/BLOCK_SIZE);

	if (SerialFlash.begin(*spi, spiPin))
	{
		Serial.println("Memory init succesful");

	}
	else
	{
		DebugService::Error("MemoryService::Initialization", "Memory init fail");
		return false;
	}

	return true;
}

uint32_t EEPROMMemoryService::findRandomAvailableBlock(uint32_t startBlock, uint32_t maxBlocks, int blockSize) {
	uint32_t address;
	bool isAvailable;

	do {
		// Generate a random block within the specified range
		address = (rand() % maxBlocks + startBlock) * blockSize;

		// Check if the address is free
		isAvailable = !metadata[address / blockSize];

		if (!isAvailable)
			Serial.println("Randomized to existing adress!");

	} while (!isAvailable);

	return address;
}

void EEPROMMemoryService::writeEventToFlash(uint32_t address, const InputEvent& event, uint32_t nextAddress) {
	uint8_t buffer[16] = { 0 };

	// Serialize event data into buffer
	buffer[0] = event.inputID;
	buffer[1] = event.flank ? 1 : 0;
	buffer[2] = (event.amount >> 8) & 0xFF;
	buffer[3] = event.amount & 0xFF;
	buffer[4] = (event.rtcEpoch >> 24) & 0xFF;
	buffer[5] = (event.rtcEpoch >> 16) & 0xFF;
	buffer[6] = (event.rtcEpoch >> 8) & 0xFF;
	buffer[7] = event.rtcEpoch & 0xFF;
	buffer[8] = (event.millis >> 8) & 0xFF;
	buffer[9] = event.millis & 0xFF;
	buffer[10] = (event.duration >> 8) & 0xFF;
	buffer[11] = event.duration & 0xFF;
	buffer[12] = (nextAddress >> 24) & 0xFF;
	buffer[13] = (nextAddress >> 16) & 0xFF;
	buffer[14] = (nextAddress >> 8) & 0xFF;
	buffer[15] = nextAddress & 0xFF;

	// Write buffer to flash memory
	SerialFlash.write(address, buffer, 16);
	while (!SerialFlash.ready()); // wait

	// Verify the data by reading it back
	uint32_t verifyNextAddress;
	InputEvent verifyEvent = readEventFromFlash(address, verifyNextAddress);

	// Check if the written event matches the read event
	if (event.inputID != verifyEvent.inputID ||
		event.flank != verifyEvent.flank ||
		event.amount != verifyEvent.amount ||
		event.rtcEpoch != verifyEvent.rtcEpoch ||
		event.millis != verifyEvent.millis ||
		event.duration != verifyEvent.duration ||
		nextAddress != verifyNextAddress) {

		DebugService::Error("writeEventToFlash", "Data verification failed. Written data does not match read data.");
		//Serial.println("Error!!!!!!!!!: Data verification failed. Written data does not match read data.");
		// Handle the error appropriately (e.g., retry write, log error, etc.)
	}

	// Debug output
	/*Serial.print("Writing to Flash at Address: "); Serial.println(address, HEX);
	Serial.print("Buffer: ");
	for (int i = 0; i < 16; i++) {
		Serial.print(buffer[i], HEX);
		Serial.print(" ");
	}
	Serial.println();*/
}

InputEvent EEPROMMemoryService::readEventFromFlash(uint32_t address, uint32_t& nextAddress) {
	uint8_t buffer[16];

	// Read the buffer from flash memory
	SerialFlash.read(address, buffer, 16);

	// Deserialize the buffer into an InputEvent object
	InputEvent event;
	event.inputID = buffer[0];
	event.flank = buffer[1] ? true : false;
	event.amount = (buffer[2] << 8) | buffer[3];
	event.rtcEpoch = ((unsigned long)buffer[4] << 24) | ((unsigned long)buffer[5] << 16) | ((unsigned long)buffer[6] << 8) | (unsigned long)buffer[7];
	event.millis = (buffer[8] << 8) | buffer[9];
	event.duration = (buffer[10] << 8) | buffer[11];
	nextAddress = ((uint32_t)buffer[12] << 24) | ((uint32_t)buffer[13] << 16) | ((uint32_t)buffer[14] << 8) | (uint32_t)buffer[15];

	/*Serial.print("Reading from Flash at Address: ");
	Serial.print(address, HEX);
	Serial.print(" Next event at: ");
	Serial.println(nextAddress, HEX);*/

	// Mark the entire block as used
	uint32_t blockIndex = address / BLOCK_SIZE;
	if (!metadata[blockIndex]) {
		metadata[blockIndex] = true;
	}

	return event;
}

String EEPROMMemoryService::readMemory(uint32_t address, uint32_t& length) {
	uint8_t* buffer = new uint8_t[length];  // Allocate buffer to hold the data

	// Read the data from the SerialFlash
	SerialFlash.read(address, buffer, length);

	// Convert the read bytes into a String
	String data = "";
	for (uint32_t i = 0; i < length; i++) {
		data += (char)buffer[i];  // Append each byte to the String as a character
	}

	delete[] buffer;  // Clean up the allocated buffer
	return data;
}

void EEPROMMemoryService::writeMemory(uint32_t address, const String& content) {
	uint8_t* buffer = new uint8_t[content.length()];  // Allocate buffer to hold the data

	// Copy the content of the String to the buffer
	for (size_t i = 0; i < content.length(); i++) {
		buffer[i] = (uint8_t)content[i];  // Copy each character as a byte
	}

	// Write the buffer to the SerialFlash
	SerialFlash.write(address, buffer, content.length());

	delete[] buffer;  // Clean up the allocated buffer
}


void EEPROMMemoryService::clearEventMemory() {
	// Clear metadata for the first 1 block
	SerialFlash.eraseBlock(0);

	memset(EEPROMMemoryService::metadata, 0, MEMORY_SIZE / BLOCK_SIZE);

	savedEvents = 0;  // Reset the count of saved events
	DebugService::Information("clearEventMemory", "Event memory cleared");
}

void EEPROMMemoryService::saveEvent(InputEvent inputEvent)
{
	if (savedEvents == 0)
	{
		//Find a startadress within the first 100 blocks
		nextAdress = findRandomAvailableBlock(0, 4096 / 16, 16);
	}

	uint32_t currentAddress = nextAdress;

	if (savedEvents == 0 || savedEvents % 256 == 0)
	{
		//Find a new adress after the first 100 blocks
		nextAdress = findRandomAvailableBlock(4096, MEMORY_SIZE / BLOCK_SIZE);

		DebugService::Information("EEPROMMemoryService::saveEvent", "Next area will be starting on " + String(nextAdress));
		//Erase the block from previous events
		SerialFlash.eraseBlock(nextAdress);

		metadata[nextAdress / BLOCK_SIZE] = true;
	}
	else
	{
		nextAdress += 16;
	}

	writeEventToFlash(currentAddress, inputEvent, nextAdress);

	
	savedEvents += 1;

}



void EEPROMMemoryService::processSavedEventsOnStartup() {
	DebugService::Information("processSavedEventsOnStartup", "Looking for saved events on startup");

	uint32_t nextLoadAddress = 0;
	bool foundEvent = false;
	// Search through the first 100 blocks for a valid event
	for (int i = 0; i < 256; ++i) {
		uint32_t checkAddress = i * 16;
		InputEvent potentialEvent = readEventFromFlash(checkAddress, nextLoadAddress);

		if (potentialEvent.isValidEvent()) {
			DebugService::Information("processSavedEventsOnStartup", "Found valid event at block " + String(i));
			foundEvent = true;
			eventProcessor->addEvent(potentialEvent);
			savedEvents++;
			break;  // Exit loop once the first valid event is found
		}
	}

	if (!foundEvent) {
		DebugService::Information("processSavedEventsOnStartup", "No valid events found on startup");

		clearEventMemory();  // Clear the memory after processing the events
		return;
	}

	// Process events starting from the found address
	while (nextLoadAddress != 0xFFFFFFFF && nextLoadAddress != 0) {  // Assuming 0xFFFFFFFF or 0 as the end marker
		InputEvent event = readEventFromFlash(nextLoadAddress, nextLoadAddress);
		// Do something with the event, such as log or process it

		if (event.isValidEvent())
		{
			eventProcessor->addEvent(event);
			savedEvents++;
			nextAdress = nextLoadAddress;
		}
		else
		{
			DebugService::Information("processSavedEventsOnStartup", "Last event " + event.Serialize() + ", next adress " + String(nextLoadAddress));
			DebugService::Information("processSavedEventsOnStartup", "No more events");
			break;
		}
		
	}

	DebugService::Information("processSavedEventsOnStartup", "Event processing complete, events loaded: " + String(savedEvents));
}

long EEPROMMemoryService::amountOfSavedEvents()
{
	return savedEvents;
}

void EEPROMMemoryService::activateSendLogEntries(unsigned long startEpoch, unsigned long endEpoch)
{
}

DebugMessage EEPROMMemoryService::getNextLogEntry()
{
	return DebugMessage();
}

void EEPROMMemoryService::deactivateSendLogEntries()
{
}


bool EEPROMMemoryService::sendLogActive()
{
	return false;
}

void EEPROMMemoryService::saveLastEventEpoch(long epoch)
{
}

long EEPROMMemoryService::getLastEventEpoch()
{
	return 0;
}

