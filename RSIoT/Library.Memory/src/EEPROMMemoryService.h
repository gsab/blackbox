/*
 Name:		Library.h
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>
#include <SerialFlash.h>
#include <StringUtils.h>
#include <ConfigurationService.h>
#include <InputEvent.h>
#include <IEventProcessor.h>

#define MEMORY_SIZE 0x800000 // 8MB
#define BLOCK_SIZE 4096
#define INITIAL_BLOCKS 20

class EEPROMMemoryService {

public:
	static bool initialize(SPIClass* spi, int spiPin, IEventProcessor* eventProcessor);
	static void clearEventMemory();
	static void saveEvent(InputEvent inputEvent);
	static void processSavedEventsOnStartup();
	static long amountOfSavedEvents();
	static void activateSendLogEntries(unsigned long startEpoch, unsigned long endEpoch);
	static DebugMessage getNextLogEntry();
	static void deactivateSendLogEntries();
	static bool sendLogActive();
	static void saveLastEventEpoch(long epoch);
	static long getLastEventEpoch();
private:
	static long savedEvents;
	static IEventProcessor* eventProcessor;
	static void writeEventToFlash(uint32_t address, const InputEvent& event, uint32_t nextAddress);
	static InputEvent readEventFromFlash(uint32_t address, uint32_t& nextAddress);
	static uint32_t findRandomAvailableBlock(uint32_t startBlock, uint32_t maxBlocks, int blockSize = BLOCK_SIZE);
	static String readMemory(uint32_t address, uint32_t& length);
	static void writeMemory(uint32_t address, const String& content);
	static int32_t nextAdress;
	static bool*  metadata;
	
};