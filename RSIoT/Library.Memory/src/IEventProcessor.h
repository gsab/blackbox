#pragma once
#include <InputEvent.h>

class IEventProcessor {
public:
	virtual void addEvent(InputEvent inputEvent) = 0;
};