/*
 Name:		Library.h
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>
#include <StringUtils.h>
#include <InputEvent.h>
#include <SPI.h>
#include <IOStatus.h>
#include <SDMemoryService.h>
#include <EEPROMMemoryService.h>
#include <IEventProcessor.h>


class MemoryService {

public:
	static bool initialize(SPIClass* spi, IEventProcessor* eventProcessor, int spiPin, bool newHardware);
	static String loadFileAsString(String fileName);
	static bool saveFile(String fileName, String fileData);
	static bool removeFile(String fileName);
	static void loadConfiguration();
	static String getNextEventFile();
	static bool hasSavedEvents();
	static void clearEventMemory();
	static void saveEvent(InputEvent inputEvent);
	static void processSavedEventsOnStartup();
	static long amountofSavedEvents();
	static void activateSendLogEntries(unsigned long startEpoch, unsigned long endEpoch);
	static DebugMessage getNextLogEntry();
	static void deactivateSendLogEntries();
	static bool sendLogActive();
	static void saveLastEventEpoch(long epoch);
	static long getLastEventEpoch();
private:
	static bool newHardware;
	static SemaphoreHandle_t mutex;

	class ScopedLock
	{
	public:
		ScopedLock(SemaphoreHandle_t mtx) : mtx(mtx)
		{
			xSemaphoreTake(mtx, portMAX_DELAY);
		}

		~ScopedLock()
		{
			xSemaphoreGive(mtx);
		}

	private:
		SemaphoreHandle_t mtx;
	};
};