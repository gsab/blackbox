/*
 Name:		Library.h
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>
#include <SdFat.h>
#include "sdios.h"
#include <SPI.h>
#include <StringUtils.h>
#include <ConfigurationService.h>
#include <InputEvent.h>
#include <IEventProcessor.h>
#include <string>
#include <vector>

#ifndef SD_FAT_TYPE
	#define SD_FAT_TYPE 3
	const uint8_t SD_CS_PIN = 27;
	#define SPI_CLOCK SD_SCK_MHZ(20)
	#define SD_CONFIG SdSpiConfig(SD_CS_PIN, SHARED_SPI, SPI_CLOCK)
#endif


class SDMemoryService{

public:
	static bool initialize(SPIClass* spi, int spiPin, IEventProcessor* eventProcessor);
	static String loadFileAsString(String fileName);
	static bool saveFile(String fileName, String fileData);
	static bool removeFile(String fileName);
	static void loadConfiguration();
	static String getNextEventFile(int index);
	static bool hasSavedEvents();
	static void clearEventMemory();
	static void saveEvent(InputEvent inputEvent);
	static void processSavedEventsOnStartup();
	static long amountOfSavedEvents();
	static void processDebugMessages();
	static String currentDebugFileName;
	static int debugFileMessageCount;
	static void activateSendLogEntries(unsigned long startEpoch, unsigned long endEpoch);
	static DebugMessage getNextLogEntry();
	static void deactivateSendLogEntries();
	static bool sendLogActive();
	static String startFile;
	static int startLine;
	static long startEpoch;
	static long endEpoch;
	static bool sendLogEntries;
	static int currentLine;
	static String currentFileName;
	static void saveLastEventEpoch(long epoch);
	static long getLastEventEpoch();
private:
	static SdFat sd;
	static long savedEvents;
	static unsigned long lastEpoch;
	static int _spiPin;
	static SPIClass* _spi;
	static FsFile root;
	static FsFile currentFile;
	static std::vector<String> eventFiles;
	static std::vector<String> logFiles;
	static std::vector<String> listFiles(String extension,String folder);
	static IEventProcessor* eventProcessor;
	static unsigned long getFirstOrLastTimestamp(const String& filename, const String& folder, bool first);
	static int binarySearchForFile(const std::vector<String>& files, const String& folder, unsigned long targetEpoch, bool findStart);
	static unsigned long getTimestampFromLine(const String& line);
	static int findLineInFile(const String& filename, const String& folder, unsigned long targetEpoch, bool findStart);
	static String findStringOnLineInFile(const String& filename, const String& folder, int lineNumber);
	static unsigned long getLastLogEpoch();
	static bool syncedLastEpoch;
	static int extractNumber(const String& filename);
	static bool compareFiles(const String& a, const String& b);
	static bool isDigit(char c);
	
};