/*
 Name:		Library.cpp
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "MemoryServiceFAT.h"

bool MemoryServiceFAT::initialize()
{
	FFat.begin(true);
	bool configured = FFat.exists("/configuration.conf");

	/*FFat.end();*/

	if (!configured)
	{
		DebugService::Information("MemoryServiceFAT::initialize", "Not configured formatting FAT");
		/*FFat.begin();*/
		//FFat.end();
		FFat.end();

		FFat.format(true);

		FFat.begin(true);
		//FFat.begin(true);
		/*FFat.end();*/

		//Load old configuration if possible
		/*DebugService::Information("MemoryServiceFAT::initialize", "Load configuration from external memory");
		MemoryService::loadConfiguration();
		DebugService::Information("MemoryServiceFAT::initialize", "Load configuration from external memory done");*/
		DebugService::Information("MemoryServiceFAT::initialize", "Save configuration");
		if (MemoryServiceFAT::saveConfiguration())
		{
			//MemoryService::removeFile("configuration.conf");
			DebugService::Information("MemoryServiceFAT::initialize", "Configuration initialized");
		}
		else
		{
			//This is not good!
			DebugService::Error("MemoryServiceFAT::initialize", "Could not save configurationfile!");
			return false;
		}
	}
	else
	{
		MemoryServiceFAT::loadConfiguration();
		DebugService::Information("MemoryServiceFAT::initialize", "Configuration loaded");
	}
	return true;
}



File MemoryServiceFAT::createOpenFile(String fileName)
{
	//noInterrupts();
	/*if (!FFat.begin())
	{
		DebugService::Error("MemoryServiceFAT::createOpenFile", "Mount failed!");
		return File();
	}*/
	DebugService::Information("MemoryServiceFAT", "CreateOpenFile!");
	const char* fileNameChar = fileName.c_str();

	//Remove file if it exists
	File file = FFat.open(fileName.c_str(), FILE_WRITE);
	//FFat.end();
	//interrupts();
	return file;
}

File MemoryServiceFAT::loadFile(String fileName)
{
	/*if (!FFat.begin())
	{
		DebugService::Error("MemoryServiceFAT::loadFile", "Mount failed!");
		return File();
	}*/
	DebugService::Information("MemoryServiceFAT", "OpenFile!");
	const char* fileNameChar = fileName.c_str();

	//Remove file if it exists
	File file = FFat.open(fileName.c_str());

	return file;
}

String MemoryServiceFAT::loadFileAsString(String fileName)
{
	// Add a "/" at the beginning of the fileName if it's missing
	if (fileName.length() > 0 && fileName[0] != '/') {
		fileName = "/" + fileName;
	}

	DebugService::Information("MemoryServiceFAT", "loadFileAsString");
	/*if (!FFat.begin())
	{
		DebugService::Error("MemoryServiceFAT::loadFileAsString", "Mount failed!");
		return "";
	}*/
	File file = FFat.open(fileName);

	if (file == NULL)
	{
		/*FFat.end();*/
		DebugService::Error("MemoryServiceFAT::loadFileAsString", "File open failed!");
		return "";
	}

	String resultString = "";
	unsigned long startTime = millis();  // Start time for timeout calculation
	while (file.available()) {
		resultString += file.readString();
		if (millis() - startTime > 10000) {  // 10,000 milliseconds = 10 seconds
			DebugService::Warning("MemoryServiceFAT", "loadFileAsString timeout!");
			break;  // Exit the loop if more than 10 seconds have passed
		}
	}
	file.close();
	/*FFat.end();*/
	DebugService::Information("MemoryServiceFAT", "loadFileAsString loaded data:" + resultString);
	return resultString;
}

bool MemoryServiceFAT::saveFile(String fileName, String fileData)
{

	if (fileName.length() > 0 && fileName[0] != '/') {
		fileName = "/" + fileName;
	}

	DebugService::Information("MemoryServiceFAT", "saveFile");
	/*if (!FFat.begin())
	{
		DebugService::Error("MemoryServiceFAT::saveFile", "Mount failed!");
		
		return false;
	}*/
	DebugService::Information("MemoryServiceFAT::saveFile", "removing file");
	if (!FFat.remove(fileName) && FFat.exists(fileName))
	{
		DebugService::Error("MemoryServiceFAT::saveFile", "Failed to remove the file even if it exists!");

	}

	File file = FFat.open(fileName, FILE_WRITE);

	if (file != NULL)
	{
		int position = 0;
		while (position < fileData.length())
		{
			file.print(fileData.charAt(position));
			position += 1;
		}
		file.flush();
		file.close();
		/*FFat.end();*/
		DebugService::Information("MemoryServiceFAT::saveFile", "File saved!");
		return true;
		
	}
	DebugService::Information("MemoryServiceFAT::saveFile", "could not save file! Could not open new file");

	/*FFat.end();*/
	return false;
}

bool MemoryServiceFAT::removeFile(String fileName)
{
	DebugService::Information("MemoryServiceFAT", "removeFile");

	if (fileName.length() > 0 && fileName[0] != '/') {
		fileName = "/" + fileName;
	}

	/*if (!FFat.begin())
	{
		DebugService::Error("MemoryServiceFAT::removeFile", "Mount failed!");
		return false;
	}*/
	FFat.remove(fileName.c_str());
	DebugService::Information("MemoryServiceFAT", "removeFile done");
	/*FFat.end();*/
	return true;
}

void MemoryServiceFAT::loadConfiguration()
{
	String data = loadFileAsString("/configuration.conf");

	if (data == "")
	{
		DebugService::Warning("MemoryService::loadConfiguration", "Configurationfile is empty!");

		FFat.end();

		FFat.format(true);

		FFat.begin(true);

		saveConfiguration();

		return;
	}

	ConfigurationService::LoadConfig(data);

}

bool MemoryServiceFAT::saveConfiguration()
{
	String data = ConfigurationService::SerializeConfig();

	String dataSaved = loadFileAsString("/configuration.conf");

	if (data != dataSaved)
	{
		DebugService::Information("MemoryService::saveConfiguration", "Saving configuration to file " + data);

		//Save Backup
		//MemoryService::saveFile("configuration.conf", data);

		return saveFile("/configuration.conf", data);
	}
	else
	{
		DebugService::Information("MemoryService::saveConfiguration", "Configuration already saved!");
	}
	return true;
}