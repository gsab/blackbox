/*
 Name:		Library.h
 Created:	11/18/2020 9:44:17 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#pragma once
#include <WString.h>
#include <FFat.h>
#include <StringUtils.h>
#include <ConfigurationService.h>
#include <MemoryService.h>
#include <IOStatus.h>
#include <IEventProcessor.h>



class MemoryServiceFAT {

public:
	static bool initialize();
	static String loadFileAsString(String fileName);
	static bool saveFile(String fileName, String fileData);
	static bool removeFile(String fileName);
	static File createOpenFile(String fileName);
	static File loadFile(String fileName);
	static void loadConfiguration();
	static bool saveConfiguration();
	static bool FatFS_initialize(bool make_fs);
};