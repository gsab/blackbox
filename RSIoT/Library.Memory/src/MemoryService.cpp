#include "MemoryService.h"

bool MemoryService::newHardware = false;
SemaphoreHandle_t MemoryService::mutex = xSemaphoreCreateMutex();

bool MemoryService::initialize(SPIClass* spi, IEventProcessor* eventProcessor, int spiPin, bool newHardware)
{
    ScopedLock lock(mutex);

    MemoryService::newHardware = newHardware;

    if (MemoryService::newHardware)
    {
        return SDMemoryService::initialize(spi, spiPin, eventProcessor);
    }
    else
    {
        return EEPROMMemoryService::initialize(spi, spiPin, eventProcessor);
    }
}

void MemoryService::clearEventMemory()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        SDMemoryService::clearEventMemory();
    }
    else
    {
        EEPROMMemoryService::clearEventMemory();
    }
}

void MemoryService::saveEvent(InputEvent inputEvent)
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        SDMemoryService::saveEvent(inputEvent);
    }
    else
    {
        EEPROMMemoryService::saveEvent(inputEvent);
    }
}

void MemoryService::processSavedEventsOnStartup()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        SDMemoryService::processSavedEventsOnStartup();
    }
    else
    {
        EEPROMMemoryService::processSavedEventsOnStartup();
    }
}

long MemoryService::amountofSavedEvents()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        return SDMemoryService::amountOfSavedEvents();
    }
    else
    {
        return EEPROMMemoryService::amountOfSavedEvents();
    }
}

void MemoryService::activateSendLogEntries(unsigned long startEpoch, unsigned long endEpoch)
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        SDMemoryService::activateSendLogEntries(startEpoch, endEpoch);
    }
    else
    {
        EEPROMMemoryService::activateSendLogEntries(startEpoch, endEpoch);
    }
}

DebugMessage MemoryService::getNextLogEntry()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        return SDMemoryService::getNextLogEntry();
    }
    else
    {
        return EEPROMMemoryService::getNextLogEntry();
    }
}

void MemoryService::deactivateSendLogEntries()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        SDMemoryService::deactivateSendLogEntries();
    }
    else
    {
        EEPROMMemoryService::deactivateSendLogEntries();
    }
}

bool MemoryService::sendLogActive()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        return SDMemoryService::sendLogActive();
    }
    else
    {
        return EEPROMMemoryService::sendLogActive();
    }
}

void MemoryService::saveLastEventEpoch(long epoch)
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        SDMemoryService::saveLastEventEpoch(epoch);
    }
    else
    {
        EEPROMMemoryService::saveLastEventEpoch(epoch);
    }
}

long MemoryService::getLastEventEpoch()
{
    ScopedLock lock(mutex);

    if (MemoryService::newHardware)
    {
        return SDMemoryService::getLastEventEpoch();
    }
    else
    {
        return EEPROMMemoryService::getLastEventEpoch();
    }
}

