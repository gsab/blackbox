/*
 Name:		Library.h
 Created:	11/30/2020 1:05:32 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>
#include <Arduino.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <DebugService.h>
#include <UniqueIDService.h>

class ConfigurationService {
public:
	static bool ethernetActive;
	static bool wifiActive;
	static bool gsmActive;
	static String simPin;
	static String wifiSSID;
	static bool wifiUseIPSettings;
	static bool ethernetUseIPSettings;
	static String wifiPass;
	static String wifiHost;
	static String wifiIPAdress;
	static String wifiStandardGateway;
	static String wifiDNS;
	static String wifiSubnet;
	static String ethernetIPAdress;
	static String ethernetStandardGateway;
	static String ethernetDNS;
	static String ethernetSubnet;
	static String NTPUrl;
	static String APNUrl;
	static String APNUser;
	static String APNPassword;
	static String MQTTUrl;
	static int MQTTPort;
	static String MQTTUser;
	static String MQTTPassword;
	static String UpdateURL;
	static String UpdateURLOverride;
	static int UpdatePort;
	static String InstallationNumber;
	static void LoadConfig(String data);
	static String SerializeConfig();
	static bool Reset;
	static bool Update;
	static bool MQTTBrokerActive;
	static bool VerifyConnection;
	static int MQTTBrokerPort;
	static String MQTTBrokerSSID;
	static String MQTTBrokerPassword;
	static int MQTTBrokerLimiter;
	static bool sendDebugMessages;
	static int8_t debugLevel;
	static bool DisableHttps;
	static bool ConfigurationLoaded;
	static int16_t FFTSamples;
	static void SetConfiguration(String data);
	static String inputSettingsHash;
	static String vibrationSettingsHash;
	static bool outputRawData;
	static bool inConfigurationMode;
	static long enteredConfigurationMode;
	static bool inTestMode;
	static bool newHardware;
	static String UnitName;
	static bool TransmittingData;
	static bool UseEnergyCycle;
	static bool ForceLostConnection;
	static long endForceLostConnectionEpoch;
	static bool restartAfterForcedLostConnection;
	static int currentConfigurationStep;
	static bool timeIsSet;
	static bool configurationWizardComplete;
	static bool simulateDataOnInputs;
	//Emeter
	static bool sensor1Active;
	static bool sensor2Active;
	static bool sensor3Active;

	static int calibrationValue1;
	static int calibrationValue2;
	static int calibrationValue3;

	static bool savePending;
};
