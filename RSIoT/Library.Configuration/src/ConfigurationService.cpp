/*
 Name:		Library.cpp
 Created:	11/30/2020 1:05:32 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "ConfigurationService.h"
bool ConfigurationService::ethernetActive = false;
bool ConfigurationService::wifiActive = false;
bool ConfigurationService::gsmActive = false;
String ConfigurationService::simPin = "";
String ConfigurationService::wifiSSID = "";
String ConfigurationService::wifiPass = "";
String ConfigurationService::wifiHost = "RSIoT-" + UniqueIDService::getUniqueID();
String ConfigurationService::NTPUrl = "gsab.pool.ntp.org";
String ConfigurationService::APNUrl = "data.rewicom.net";
String ConfigurationService::APNUser = "";
String ConfigurationService::APNPassword = "";
String ConfigurationService::MQTTUrl = "";
int ConfigurationService::MQTTPort = 1883;
String ConfigurationService::MQTTUser = "";
String ConfigurationService::MQTTPassword = "";
String ConfigurationService::UpdateURL = "";
String ConfigurationService::UpdateURLOverride = "";
int ConfigurationService::UpdatePort = 443;
String ConfigurationService::InstallationNumber = "";
bool ConfigurationService::Reset = false;
bool ConfigurationService::Update = false;
bool ConfigurationService::sendDebugMessages = true;
bool ConfigurationService::outputRawData = false;
int8_t ConfigurationService::debugLevel = 1;
bool ConfigurationService::DisableHttps = false;
bool  ConfigurationService::wifiUseIPSettings = false;
String  ConfigurationService::wifiIPAdress = "";
String  ConfigurationService::wifiStandardGateway = "";
String  ConfigurationService::wifiDNS = "";
String  ConfigurationService::wifiSubnet = "";
bool  ConfigurationService::ethernetUseIPSettings = false;
String  ConfigurationService::ethernetIPAdress = "";
String  ConfigurationService::ethernetStandardGateway = "";
String  ConfigurationService::ethernetDNS = "";
String  ConfigurationService::ethernetSubnet = "";
int16_t ConfigurationService::FFTSamples = 100;
String ConfigurationService::inputSettingsHash = "";
String ConfigurationService::vibrationSettingsHash = "0bf53df4aae1c9c88093d4752bb695c5";
bool ConfigurationService::inConfigurationMode = false;
long ConfigurationService::enteredConfigurationMode = 0;
int ConfigurationService::currentConfigurationStep = 0;
bool ConfigurationService::inTestMode = false;
String ConfigurationService::UnitName = "";
bool ConfigurationService::MQTTBrokerActive = false;
int ConfigurationService::MQTTBrokerPort = 1883;
String ConfigurationService::MQTTBrokerSSID = "RSIoT-MQTT";
String ConfigurationService::MQTTBrokerPassword = "rsiotmqtt";
bool ConfigurationService::newHardware = false;
int ConfigurationService::MQTTBrokerLimiter = 10;
bool ConfigurationService::TransmittingData = false;
bool ConfigurationService::UseEnergyCycle = false;
bool ConfigurationService::ForceLostConnection = false;
bool ConfigurationService::configurationWizardComplete = false;
bool ConfigurationService::VerifyConnection = false;
//Emeter
bool ConfigurationService::sensor1Active = true;
bool ConfigurationService::sensor2Active = true;
bool ConfigurationService::sensor3Active = true;

int ConfigurationService::calibrationValue1 = 100;
int ConfigurationService::calibrationValue2 = 300;
int ConfigurationService::calibrationValue3 = 100;

bool ConfigurationService::timeIsSet = false;

bool ConfigurationService::savePending = false;
bool ConfigurationService::simulateDataOnInputs = false;
long ConfigurationService::endForceLostConnectionEpoch = 2147483647;
bool ConfigurationService::restartAfterForcedLostConnection = false;

void ConfigurationService::LoadConfig(String data)
{
	ConfigurationService::SetConfiguration(data);
}
String ConfigurationService::SerializeConfig()
{
	StaticJsonDocument<2048> doc;
	DebugService::Information("Configuration::LoadConfig", "Serialized configuration");
	doc["wifiActive"] = ConfigurationService::wifiActive ? "True" : "False";
	doc["ethernetActive"] = ConfigurationService::ethernetActive ? "True" : "False";
	doc["gsmActive"] = ConfigurationService::gsmActive ? "True" : "False";
	doc["simPin"] = ConfigurationService::simPin;
	doc["wifiSSID"] = ConfigurationService::wifiSSID;
	doc["wifiPass"] = ConfigurationService::wifiPass;
	doc["wifiHost"] = ConfigurationService::wifiHost;
	doc["NTPUrl"] = ConfigurationService::NTPUrl;
	doc["APNUrl"] = ConfigurationService::APNUrl;
	doc["APNUser"] = ConfigurationService::APNUser;
	doc["APNPassword"] = ConfigurationService::APNPassword;
	doc["MQTTUrl"] = ConfigurationService::MQTTUrl;
	doc["MQTTPort"] = ConfigurationService::MQTTPort;
	doc["MQTTUser"] = ConfigurationService::MQTTUser;
	doc["MQTTPassword"] = ConfigurationService::MQTTPassword;
	doc["UpdateURL"] = ConfigurationService::UpdateURL;
	doc["UpdatePort"] = ConfigurationService::UpdatePort;
	doc["InstallationNumber"] = ConfigurationService::InstallationNumber;
	doc["wifiUseIPSettings"] = ConfigurationService::wifiUseIPSettings ? "True" : "False";
	doc["wifiIPAdress"] = ConfigurationService::wifiIPAdress;
	doc["wifiStandardGateway"] = ConfigurationService::wifiStandardGateway;
	doc["wifiSubnet"] = ConfigurationService::wifiSubnet;
	doc["ethernetUseIPSettings"] = ConfigurationService::ethernetUseIPSettings ? "True" : "False";
	doc["ethernetIPAdress"] = ConfigurationService::ethernetIPAdress;
	doc["ethernetStandardGateway"] = ConfigurationService::ethernetStandardGateway;
	doc["ethernetSubnet"] = ConfigurationService::ethernetSubnet;
	doc["DisableHttps"] = ConfigurationService::DisableHttps ? "True" : "False";
	doc["FFTSamples"] = ConfigurationService::FFTSamples;
	doc["UnitName"] = ConfigurationService::UnitName;
	doc["MQTTBrokerPort"] = ConfigurationService::MQTTBrokerPort;
	doc["MQTTBrokerActive"] = ConfigurationService::MQTTBrokerActive ? "True" : "False";
	doc["MQTTBrokerSSID"] = ConfigurationService::MQTTBrokerSSID;
	doc["MQTTBrokerPassword"] = ConfigurationService::MQTTBrokerPassword;
	doc["MQTTBrokerLimiter"] = ConfigurationService::MQTTBrokerLimiter;
	doc["currentConfigurationStep"] = ConfigurationService::currentConfigurationStep;
	doc["configurationWizardComplete"] = ConfigurationService::configurationWizardComplete ? "True" : "False";

	char buffer[2048];
	serializeJson(doc, buffer, 2048);
	DebugService::Information("Configuration::LoadConfig", String(buffer));
	return String(buffer);
}

void ConfigurationService::SetConfiguration(String data)
{
	StaticJsonDocument<2048> doc;
	deserializeJson(doc, data.c_str(), data.length());

	DebugService::Information("Configuration::SetConfiguration", "Deserialized configuration");

	DebugService::Information("Configuration::SetConfiguration", data);

	if (doc.containsKey("wifiActive"))
	{
		ConfigurationService::wifiActive = doc["wifiActive"].as<String>() == "True";
	}
	if (doc.containsKey("gsmActive"))
	{
		ConfigurationService::gsmActive = doc["gsmActive"].as<String>() == "True";
	}
	if (doc.containsKey("ethernetActive"))
	{
		ConfigurationService::ethernetActive = doc["ethernetActive"].as<String>() == "True";
	}
	if (doc.containsKey("simPin"))
	{
		ConfigurationService::simPin = doc["simPin"].as<String>();
	}
	if (doc.containsKey("wifiSSID"))
	{
		ConfigurationService::wifiSSID = doc["wifiSSID"].as<String>();
	}
	if (doc.containsKey("wifiPass"))
	{
		ConfigurationService::wifiPass = doc["wifiPass"].as<String>();
	}
	if (doc.containsKey("wifiHost") && doc["wifiHost"].as<String>() != "RSIoT-ID000000000000")
	{
		ConfigurationService::wifiHost = doc["wifiHost"].as<String>();
	}
	else
	{
		ConfigurationService::wifiHost = "RSIoT-" + UniqueIDService::getUniqueID();
	}

	if (doc.containsKey("APNUrl"))
	{
		ConfigurationService::APNUrl = doc["APNUrl"].as<String>();
	}

	if (doc.containsKey("debugLevel"))
	{
		ConfigurationService::debugLevel = doc["debugLevel"].as<String>().toInt();
	}

	if (doc.containsKey("NTPUrl"))
	{
		ConfigurationService::NTPUrl = doc["NTPUrl"].as<String>();
	}
	if (doc.containsKey("APNUser"))
	{
		ConfigurationService::APNUser = doc["APNUser"].as<String>();
	}
	if (doc.containsKey("APNPassword"))
	{
		ConfigurationService::APNPassword = doc["APNPassword"].as<String>();
	}
	if (doc.containsKey("MQTTUrl"))
	{
		ConfigurationService::MQTTUrl = doc["MQTTUrl"].as<String>();
	}
	if (doc.containsKey("MQTTPort"))
	{
		ConfigurationService::MQTTPort = (doc["MQTTPort"].as<String>()).toInt();
	}
	if (doc.containsKey("MQTTPassword"))
	{
		ConfigurationService::MQTTPassword = doc["MQTTPassword"].as<String>();
	}
	if (doc.containsKey("UpdateURL"))
	{
		ConfigurationService::UpdateURL = doc["UpdateURL"].as<String>();
	}
	if (doc.containsKey("UpdatePort"))
	{
		ConfigurationService::UpdatePort = (doc["UpdatePort"].as<String>()).toInt();
	}
	if (doc.containsKey("InstallationNumber"))
	{
		ConfigurationService::InstallationNumber = doc["InstallationNumber"].as<String>();
	}
	if (doc.containsKey("wifiUseIPSettings"))
	{
		ConfigurationService::wifiUseIPSettings = doc["wifiUseIPSettings"].as<String>() == "True";
	}
	if (doc.containsKey("wifiIPAdress"))
	{
		ConfigurationService::wifiIPAdress = doc["wifiIPAdress"].as<String>();
	}
	if (doc.containsKey("wifiStandardGateway"))
	{
		ConfigurationService::wifiStandardGateway = doc["wifiStandardGateway"].as<String>();
	}
	if (doc.containsKey("wifiSubnet"))
	{
		ConfigurationService::wifiSubnet = doc["wifiSubnet"].as<String>();
	}
	if (doc.containsKey("ethernetUseIPSettings"))
	{
		ConfigurationService::ethernetUseIPSettings = doc["ethernetUseIPSettings"].as<String>() == "True";
	}
	if (doc.containsKey("ethernetIPAdress"))
	{
		ConfigurationService::ethernetIPAdress = doc["ethernetIPAdress"].as<String>();
	}
	if (doc.containsKey("ethernetStandardGateway"))
	{
		ConfigurationService::ethernetStandardGateway = doc["ethernetStandardGateway"].as<String>();
	}
	if (doc.containsKey("ethernetSubnet"))
	{
		ConfigurationService::ethernetSubnet = doc["ethernetSubnet"].as<String>();
	}
	if (doc.containsKey("DisableHttps"))
	{
		ConfigurationService::DisableHttps = doc["DisableHttps"].as<String>() == "True";
	}
	if (doc.containsKey("FFTSamples"))
	{
		ConfigurationService::FFTSamples = doc["FFTSamples"];
	}
	if (doc.containsKey("currentConfigurationStep"))
	{
		ConfigurationService::currentConfigurationStep = (doc["currentConfigurationStep"].as<String>()).toInt();
	}
	if (doc.containsKey("UnitName"))
	{
		ConfigurationService::UnitName = doc["UnitName"].as<String>();
	}
	if (doc.containsKey("MQTTBrokerActive"))
	{
		ConfigurationService::MQTTBrokerActive = doc["MQTTBrokerActive"].as<String>() == "True";
	}
	if (doc.containsKey("MQTTBrokerPort"))
	{
		ConfigurationService::MQTTBrokerPort = doc["MQTTBrokerPort"].as<String>().toInt();
	}
	if (doc.containsKey("MQTTBrokerSSID"))
	{
		ConfigurationService::MQTTBrokerSSID = doc["MQTTBrokerSSID"].as<String>();
	}
	if (doc.containsKey("MQTTBrokerPassword"))
	{
		ConfigurationService::MQTTBrokerPassword = doc["MQTTBrokerPassword"].as<String>();
	}
	if (doc.containsKey("MQTTBrokerLimiter"))
	{
		ConfigurationService::MQTTBrokerLimiter = doc["MQTTBrokerLimiter"].as<String>().toInt();;
	}
	if (doc.containsKey("configurationWizardComplete"))
	{
		ConfigurationService::configurationWizardComplete = doc["configurationWizardComplete"].as<String>() == "True";
	}

	DebugService::Information("Configuration::SetConfiguration", data);

}


