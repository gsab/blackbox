template<typename T, size_t S, typename IT>
Buffer<T, S, IT>::Buffer()
  : buffer{}, count(0) {
}

template<typename T, size_t S, typename IT>
Buffer<T, S, IT>::Buffer(std::initializer_list<T> list)
  : buffer{}, count(list.size()) {
  int i = 0;
  for (auto&& e : list) {
    buffer[i] = e;
    ++i;
  }
}

template<typename T, size_t S, typename IT>
bool Buffer<T, S, IT>::push(T&& value) {
  if (count == capacity) {
    return false;
  }
  buffer[count] = value;
  ++count;
  return true;
}

template<typename T, size_t S, typename IT>
bool Buffer<T, S, IT>::push(const T& value) {
  if (count == capacity) {
    return false;
  }
  buffer[count] = value;
  ++count;
  return true;
}

template<typename T, size_t S, typename IT>
T Buffer<T, S, IT>::pop() {
  if (count == 0) {
    return buffer[0];
  }
  --count;
  return buffer[count];
}

template<typename T, size_t S, typename IT>
T inline Buffer<T, S, IT>::last() const {
  if (count == capacity) {
    return buffer[capacity - 1];
  }
  return buffer[count - 1];
}

template<typename T, size_t S, typename IT>
T Buffer<T, S, IT>::operator[](IT index) const {
  if (index >= count) {
    return buffer[capacity - 1];
  }
  return buffer[index];
}


template<typename T, size_t S, typename IT>
IT inline Buffer<T, S, IT>::size() const {
  return count;
}

template<typename T, size_t S, typename IT>
bool inline Buffer<T, S, IT>::isEmpty() const {
  return count == 0;
}

template<typename T, size_t S, typename IT>
bool inline Buffer<T, S, IT>::isFull() const {
  return count == capacity;
}

template<typename T, size_t S, typename IT>
void inline Buffer<T, S, IT>::clear() {
  count = 0;
}
