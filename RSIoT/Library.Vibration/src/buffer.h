// ArduinoCpp.h
#pragma once


#include <CircularBuffer.h>
#include <array>

template<typename T, size_t S, typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type>
class Buffer {

public:
  static constexpr IT capacity = static_cast<IT>(S);
  using index_t = IT;
  explicit Buffer();
  explicit Buffer(const std::initializer_list<T> list);

  Buffer(const Buffer&) = delete;
  Buffer(Buffer&&) = delete;
  Buffer& operator=(const Buffer&) = delete;
  Buffer& operator=(Buffer&&) = delete;

  bool push(T&& value);
  bool push(const T& value);
  T pop();
  T inline last() const;
  T operator[](IT index) const;
  IT inline size() const;
  IT inline available() const;
  bool inline isEmpty() const;
  bool inline isFull() const;
  void inline clear();
  T* begin() {
    return &buffer[0];
  }
  T* end() {
    return &buffer[count];
  }
  const T* cbegin() const {
    return &buffer[0];
  }
  const T* cend() const {
    return &buffer[count];
  }
  const T* begin() const {
    return cbegin();
  }
  const T* end() const {
    return cend();
  }

private:
  T buffer[S];
  IT count;
};

#include <buffer.tpp>
