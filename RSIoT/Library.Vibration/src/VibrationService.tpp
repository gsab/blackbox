//#include "VibrationService.h"
#include <tuple>
#include <stdlib.h>
#include <limits>
#include <bitset>
#include <math.h>
#include <algorithm>
#include <numeric>
#include "SymmetricEigensolver3x3.h"

#define DEBUG_PRINT(x) Serial.print(x);
#define DEBUG_PRINT_2(x, y) \
  Serial.print(x); \
  Serial.print(" "); \
  Serial.println(y);
#define DEBUG_PRINT_2f(x, y, v) \
  Serial.print(x); \
  Serial.print(" "); \
  Serial.println(y, v);
#define DEBUG_PRINT_3(x, y, z) \
  Serial.print(x); \
  Serial.print(" "); \
  Serial.print(y); \
  Serial.print(" "); \
  Serial.println(z);
#define DEBUG_PRINT_ITER(s, buf) \
  DEBUG_PRINT(s); \
  for (auto&& i : buf) { \
    DEBUG_PRINT(i); \
    DEBUG_PRINT(" "); \
  } \
  DEBUG_PRINT("\n");



template<typename T,
         typename std::enable_if<std::is_signed<T>::value>::type* = nullptr>
int inline sign(T val) {
  return (T(0) < val) - (val < T(0));
}


template<typename T,
         typename std::enable_if<std::is_floating_point<T>::value>::type* = nullptr>
T inline calcEnergy(T x, T y, T z, T meanX, T meanY, T meanZ) {
  return pow(x - meanX, 2) + pow(y - meanY, 2) + pow(z - meanZ, 2);
}


template<typename T,
         size_t S1,
         size_t S2,
         typename IT = typename Helper::Index<(S1 <= UINT8_MAX), (S1 <= UINT16_MAX)>::Type,
         typename std::enable_if<std::is_signed<T>::value>::type* = nullptr>
void zeroCrossings(const Buffer<T, S1>& buffer, T offset, Buffer<IT, S2>& crossings) {
  crossings.clear();
  T prev = sign(buffer[0] - offset);
  for (IT i = 1; i < buffer.size(); i++) {
    T curr = sign(buffer[i] - offset);
    if (curr != prev) {
      crossings.push(i - 1);
      prev = curr;
    }
  }
}


template<typename T1,
         typename T2,
         size_t S,
         typename std::enable_if<std::is_signed<T2>::value>::type* = nullptr>
void inline diff(const Buffer<T1, S>& buff, Buffer<T2, S - 1>& diffs) {
  diffs.clear();
  for (int i = 0; i < buff.size() - 1; i++) {
    diffs.push(buff[i + 1] - buff[i]);
  }
}


template<typename T1,
         typename T2,
         size_t S1,
         size_t S2,
         typename IT = typename Helper::Index<(S2 <= UINT8_MAX), (S2 <= UINT16_MAX)>::Type>
void zeroRuns(const Buffer<T1, S1>& buff, Buffer<T2, S2, IT>& runs) {
  Buffer<int, S1 + 2> isZero;
  isZero.push(0);
  for (auto&& i : buff) {
    if (i == 0) {
      isZero.push(1);
    } else {
      isZero.push(0);
    }
  }
  isZero.push(0);
  Buffer<int, isZero.capacity - 1> diffs;
  diff(isZero, diffs);
  for (IT i = 0; i < diffs.size(); i++) {
    if (abs(diffs[i]) == 1) {
      runs.push(i);
    }
  }
}


template<typename T,
         size_t S>
std::tuple<T, T> inline min_max(const Buffer<T, S>& buff) {
  T min_v = std::numeric_limits<T>::max();
  T max_v = std::numeric_limits<T>::lowest();
  for (auto&& i : buff) {
    min_v = i < min_v ? i : min_v;
    max_v = i > max_v ? i : max_v;
  }
  return { min_v, max_v };
}


template<typename T,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type,
         typename std::enable_if<std::is_signed<T>::value>::type* = nullptr>
bool isSignsCorrect(const Buffer<T, S, IT>& buff, bool alt) {
  IT i = 0;
  IT n = buff.size();
  while (i < n) {
    IT j = 0;
    while (((i + j) < n) && (buff[i + j] == 0)) {
      alt = !alt;
      ++j;
    }
    i += j;
    if (i >= n) {
      break;
    }
    if (!alt) {
      if (buff[i] > 0) {
        return false;
      }
      alt = !alt;
    } else {
      if (buff[i] < 0) {
        return false;
      }
      alt = !alt;
    }
    ++i;
  }

  return true;
}

template<typename T1,
         typename T2,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type,
         typename std::enable_if<std::is_signed<T2>::value>::type* = nullptr>
void pointwiseDiff(const Buffer<T1, S, IT>& a1, const Buffer<T1, S, IT>& a2, Buffer<T2, S, IT>& out) {
  out.clear();
  if (a1.size() != a2.size()) {
    return;
  }

  for (IT i = 0; i < a1.size(); ++i) {
    out.push((T2)a1[i] - (T2)a2[i]);
  }
}

template<template<class, size_t, class> class C1,
         template<class, size_t, class> class C2,
         typename T,
         size_t S1,
         size_t S2,
         typename IT1 = typename Helper::Index<(S1 <= UINT8_MAX), (S1 <= UINT16_MAX)>::Type,
         typename IT2 = typename Helper::Index<(S2 <= UINT8_MAX), (S2 <= UINT16_MAX)>::Type>
void risingEdge(const C1<T, S1, IT1>& scores, T offset, C2<IT1, S2, IT2>& edges, int hyster = -1) {
  if (scores.isEmpty()) {
    return;
  }
  edges.clear();
  T prev = scores[0];
  int last_edge = -1;
  IT1 i = 0;
  for (auto&& s : scores) {
    if ((s > prev) && (prev < offset) && (s > offset)) {
      if (last_edge >= 0) {
        if (i - last_edge >= hyster) {
          last_edge = i;
          edges.push(i);
        }
      } else {
        last_edge = i;
        edges.push(i);
      }
    }
    prev = s;
    ++i;
  }
}


template<template<class, size_t, class> class C,
         typename T,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type>
T meanPeakWidths(const C<T, S, IT>& scores, T offset) {
  T s = T(0);
  Buffer<IT, S> zc;
  zeroCrossings(scores, offset, zc);
  IT p_start;
  IT crossings = 0;
  bool found_start = false;
  DEBUG_PRINT_ITER("zc mpw ", zc);
  for (auto&& i : zc) {

    if (scores[i] < scores[i + 1]) {
      p_start = i;
      found_start = true;
    } else if (found_start && (scores[i] > scores[i + 1])) {
      s += i - p_start;
      ++crossings;
    }
  }
  return s / (T)crossings;
}

template<byte nr_min_peaks,
         byte nr_max_peaks,
         byte SAMPLES_PER_CYCLE = 20,
         byte BINS = 29,
         template<class, size_t, class> class C,
         typename T,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type>
std::tuple<IT, IT, T, T, T> findPrimaryPeaks(const C<T, S, IT>& scores) {
  std::tuple<IT, IT, T, T, T> bestPeaks{ IT(0), IT(0), std::numeric_limits<T>::lowest(), T(-1), T(-1) };
  if (scores.size() <= 3) {
    return bestPeaks;
  }
  T s_max;
  T s_min;
  std::tie(s_min, s_max) = min_max(scores);
  T tot_h = s_max - s_min;
  T start_h = (s_min + tot_h * T(0.05));
  T step = ((s_max - T(0.05) * tot_h) - start_h) / (T)(BINS - 1);
  Buffer<IT, S> zc;
  Buffer<IT, S> zc_test;
  Buffer<int, S> zc_diff;
  Buffer<T, BINS> peak_range;
  Buffer<IT, BINS> peak_counts;
  for (byte i = 0; i < BINS; i++) {
    T offset = start_h + i * step;
    peak_range.push(offset);
    zeroCrossings(scores, offset, zc);
    //DEBUG_PRINT_ITER("zc: ", zc);
    IT nr_zc = 0;
    if (!zc.isEmpty()) {
      IT counted = 0;
      if ((scores[zc[0] + 1] - offset) <= T(0)) {
        ++nr_zc;
        ++counted;
      }
      if ((scores[zc.last() + 1] - offset) >= T(0)) {
        ++nr_zc;
        ++counted;
      }
      nr_zc += (zc.size() - counted) / 2;
    }
    peak_counts.push(nr_zc);
  }
  //DEBUG_PRINT_ITER("peak_range: ", peak_range);
  //DEBUG_PRINT_ITER("peak_counts: ", peak_counts);


  Buffer<int, peak_counts.capacity - 1> peak_diff;
  diff(peak_counts, peak_diff);
  Buffer<IT, peak_diff.capacity> zr;
  zeroRuns(peak_diff, zr);
  //DEBUG_PRINT_ITER("zr:", zr);
  //using index_t = decltype(zr)::index_t;
  for (auto i = 0; i < zr.size() - 1; i += 2) {
    IT bin_max_i = zr[i + 1];
    IT nr_peaks = peak_counts[bin_max_i];

    if ((nr_min_peaks <= nr_peaks) && (nr_peaks <= nr_max_peaks)) {

      IT bin_min_i = zr[i];
      /*DEBUG_PRINT_3("bin_i", bin_min_i, bin_max_i);
      DEBUG_PRINT_3("peaks_i", peak_counts[bin_min_i], peak_counts[bin_max_i]);*/
      IT bin_diff = bin_max_i - bin_min_i;
      if (bin_diff < std::get<0>(bestPeaks)) {
        // DEBUG_PRINT_3("skip", bin_min_i, bin_max_i);
        continue;
      }

      zeroCrossings(scores, peak_range[bin_max_i], zc);
      zeroCrossings(scores, peak_range[bin_min_i], zc_test);
      // align zero crossings for the lower and upper bin
      while (true) {
        while (zc.size() != zc_test.size()) {
          ++bin_min_i;
          zeroCrossings(scores, peak_range[bin_min_i], zc_test);
        }
        pointwiseDiff(zc_test, zc, zc_diff);
        if (isSignsCorrect(zc_diff, false)) {
          break;
        }
        if (isSignsCorrect(zc_diff, true)) {
          break;
        }
        ++bin_min_i;
        zeroCrossings(scores, peak_range[bin_min_i], zc_test);
      }
      bin_diff = bin_max_i - bin_min_i;

      if (bin_diff >= 2) {

        T bin_min_v = peak_range[bin_min_i];
        T bin_max_v = peak_range[bin_max_i];


        T mid = bin_min_v + (bin_max_v - bin_min_v) * T(0.5);
        Buffer<IT, nr_max_peaks> edges;
        Buffer<int, nr_max_peaks - 1> cycle_times;
        risingEdge(scores, mid, edges);
        //DEBUG_PRINT_ITER("edges: ", edges);
        diff(edges, cycle_times);
        T abs_diff = T(0);
        for (auto&& i : cycle_times) {
          abs_diff += abs(i - SAMPLES_PER_CYCLE);
        }
        T spread = abs_diff / cycle_times.size();
        if (spread > T(5)) {
          //DEBUG_PRINT_3("To much spread", bin_diff, spread);
          continue;
        }
        std::tuple<IT, IT, T, T, T> score{ bin_diff, nr_peaks, -spread, bin_max_v, bin_min_v };
        DEBUG_PRINT_2("Good spread", spread);
        if (score > bestPeaks) {
          /*DEBUG_PRINT_3("Best", bin_diff, p_len);
          DEBUG_PRINT_3("range", peak_range[bin_min_i], peak_range[bin_max_i]);*/
          bestPeaks = score;
        }
      }
    }
    // DEBUG_PRINT_3("bin_i", bin_max_i, bin_min_i);
  }
  return bestPeaks;
}


template<typename T>
T inline score_func(T v1, T v2) {
  T d = v1 - v2;
  return -(d * d);
}


template<template<class, size_t, class> class C1,
         template<class, size_t, class> class C2,
         typename T,
         size_t S1,
         size_t S2,
         typename IT1 = typename Helper::Index<(S1 <= UINT8_MAX), (S1 <= UINT16_MAX)>::Type,
         typename IT2 = typename Helper::Index<(S2 <= UINT8_MAX), (S2 <= UINT16_MAX)>::Type,
         typename std::enable_if<std::is_signed<T>::value>::type* = nullptr>
void matchPattern(const C1<T, S1, IT1>& a, IT1 p_start, IT1 p_len, C2<T, S2, IT2>& scores) {
  scores.clear();
  const T frac = T(1) / (T)p_len;
  for (IT1 i = 0; i <= (a.size() - p_len); ++i) {
    T score = T(0);
    for (IT1 j = 0; j < p_len; ++j) {
      score += score_func(a[i + j], a[p_start + j]);
    }
    scores.push(score * frac);
  }
}


template<template<class, size_t, class> class C,
         typename T,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type>
T calcHighCutoff(const C<T, S, IT>& a, float q) {
  if (a.isEmpty()) {
    return 0.0f;
  }
  q = std::min(std::max(0.0f, q), 1.0f);
  // Round down, that is good enough
  IT ni = (IT)((a.size() - 1) * q);
  Buffer<T, S> tmp;

  for (IT i = 0; i < a.size(); ++i) {
    tmp.push(a[i]);
  }
  std::sort(tmp.begin(), tmp.end());
  return std::max(tmp[ni], tmp.last() * T(0.15));
}


template<template<class, size_t, class> class C1,
         template<class, size_t, class> class C2,
         typename T,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type>
void minimum(const C1<T, S, IT>& a, T v, C2<T, S, IT>& out) {
  out.clear();
  for (IT i = 0; i < a.size(); ++i) {
    out.push(std::min(a[i], v));
  }
}


template<size_t N,
         typename T,
         typename IT = typename Helper::Index<(N <= UINT8_MAX), (N <= UINT16_MAX)>::Type>
void shuffle(T* a) {

  for (IT i = 0; i < N - 2; ++i) {
    IT j = random(i, N);
    std::swap(a[i], a[j]);
  }
}


template<byte CALIBRATION_CYCLES,
         size_t P_LEN_MIN,
         size_t P_LEN_MAX,
         size_t PATTERNS_TO_CHECK = 100,
         byte SAMPLES_PER_CYCLE = 20,
         template<class, size_t, class> class B,
         typename T,
         size_t S,
         typename IT = typename Helper::Index<(S <= UINT8_MAX), (S <= UINT16_MAX)>::Type>
std::tuple<bool, IT, IT, T, T, T, T> findPattern(const B<T, S, IT>& a) {
  constexpr std::tuple<bool, IT, IT, T, T, T, T> null_score{ false, IT(0), IT(0), T(0), T(0), T(0), T(0) };
  if (a.size() < P_LEN_MAX) {
    // Too little data
    return null_score;
  }

  T p_score, spread;
  IT nr_peaks;
  T bin_min_v;
  T bin_max_v;


  const IT n = a.size();
  constexpr byte nr_min_peaks = CALIBRATION_CYCLES - 1;
  constexpr byte nr_max_peaks = CALIBRATION_CYCLES + 1;
  constexpr size_t max_patterns_to_check = PATTERNS_TO_CHECK <= (S - P_LEN_MIN + 1) ? PATTERNS_TO_CHECK : (S - P_LEN_MIN + 1);
  constexpr IT max_pattern_i = S - P_LEN_MIN + 1;
  constexpr byte p_range = P_LEN_MAX - P_LEN_MIN + 1;
  using PeakCount = typename Helper::Index<((max_patterns_to_check * p_range) <= UINT8_MAX), ((max_patterns_to_check * p_range) <= UINT16_MAX)>::Type;

  //DEBUG_PRINT_2("max_pattern_i", max_pattern_i);
  //DEBUG_PRINT_2("max_patterns_to_check", max_patterns_to_check);

  Buffer<T, (S - P_LEN_MIN + 1)> scores;
  PeakCount peak_counts[nr_max_peaks - nr_min_peaks + 1] = { 0 };
  std::tuple<T, T, IT, IT, T, T> best_pattern[nr_max_peaks - nr_min_peaks + 1];

  IT p_starts[max_pattern_i];
  std::iota(std::begin(p_starts), std::end(p_starts), 0);

  for (IT p_len = P_LEN_MIN; p_len <= P_LEN_MAX; ++p_len) {
    IT p_start_end = n - p_len + 1;
    IT p_nr_offset = 0;
    shuffle<max_pattern_i>(p_starts);
    // Check no more then all possible patterns
    //IT patterns_to_check = PATTERNS_TO_CHECK <= (S - p_len + 1) ? PATTERNS_TO_CHECK : (S - p_len + 1);
    IT patterns_to_check = std::min(PATTERNS_TO_CHECK, a.size() - p_len + 1u);
    DEBUG_PRINT_3("patterns_to_check", patterns_to_check, p_len);
    for (IT p_nr = 0; p_nr < patterns_to_check; ++p_nr) {
      IT p_start = p_starts[p_nr + p_nr_offset];
      //IT p_start = p_nr;
      while (p_start >= p_start_end) {
        ++p_nr_offset;
        //DEBUG_PRINT_3("skipp: ", p_start, p_len);
        p_start = p_starts[p_nr + p_nr_offset];
      }
      matchPattern(a, p_start, p_len, scores);
      std::tie(p_score, nr_peaks, spread, bin_max_v, bin_min_v) = findPrimaryPeaks<nr_min_peaks, nr_max_peaks, SAMPLES_PER_CYCLE>(scores);
      //DEBUG_PRINT_3("score", p_score, nr_peaks);
      //DEBUG_PRINT_3("spread", spread, bin_min_v);
      if (p_score > 0) {
        std::tuple<T, T, IT, IT, T, T> score_tuple{ p_score, spread, p_len, p_start, bin_max_v, bin_min_v };
        IT p_idx = nr_peaks - nr_min_peaks;
        if (peak_counts[p_idx]++ == 0) {
          /*DEBUG_PRINT_3("BEst peaks", p_idx, nr_peaks);
          DEBUG_PRINT_3("BEst spread", p_score, spread);*/
          best_pattern[p_idx] = score_tuple;
        } else if (score_tuple > best_pattern[p_idx]) {
          /*DEBUG_PRINT_3("NEW BEst peaks", p_idx, nr_peaks);
          DEBUG_PRINT_3("NEW BEst spread", p_score, spread);*/
          best_pattern[p_idx] = score_tuple;
        }
      }
    }
  }

  PeakCount most_peaks = std::numeric_limits<PeakCount>::lowest();
  nr_peaks = nr_min_peaks;
  IT p_idx = IT(0);
  for (auto&& count : peak_counts) {
    //DEBUG_PRINT_3("peak_counts", nr_peaks, count);
    if (count > most_peaks) {
      most_peaks = count;
      p_idx = nr_peaks - nr_min_peaks;
    }
    ++nr_peaks;
  }
  if (most_peaks == std::numeric_limits<PeakCount>::lowest()) {
    // No pattern found
    return null_score;
  }

  IT p_start, p_len;
  std::tie(p_score, spread, p_len, p_start, bin_max_v, bin_min_v) = best_pattern[p_idx];

  return { true, p_start, p_len, bin_min_v, bin_max_v, spread, p_score };
}


template<typename T,
         size_t SAMPLE_FREQ,
         size_t COV_CYCLES,
         size_t CALIBRATION_CYCLES,
         size_t SAMPLES_PER_CYCLE,
         byte MIN_PATTERN_LEN,
         byte MAX_PATTERN_LEN,
         size_t PATTERNS_TO_CHECK>
T inline VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::wma(T x, T old_avarage) {
  // Average over 3 seconds
  constexpr T w = exp(T(-1) / (T(3) * T(SAMPLE_FREQ)));
  return x + w * (old_avarage - x);
}


template<typename T,
         size_t SAMPLE_FREQ,
         size_t COV_CYCLES,
         size_t CALIBRATION_CYCLES,
         size_t SAMPLES_PER_CYCLE,
         byte MIN_PATTERN_LEN,
         byte MAX_PATTERN_LEN,
         size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::initialize() {
  foundPattern = false;
  likelyCycletime = 0;
  wSize = 0;
  highCutoff = 0;
}

template<typename T,
         size_t SAMPLE_FREQ,
         size_t COV_CYCLES,
         size_t CALIBRATION_CYCLES,
         size_t SAMPLES_PER_CYCLE,
         byte MIN_PATTERN_LEN,
         byte MAX_PATTERN_LEN,
         size_t PATTERNS_TO_CHECK>
bool VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::evaluateData(T xAxleValue, T yAxleValue, T zAxleValue) {

  /*  Matrix<double, 1, 3> m1;
  Matrix<double, 3, 1> m1_t;
  Matrix<double, 3, 3> res;
  m1[0][0] = 0.73150514;
  m1[0][1] = 0.56701366;
  m1[0][2] = 0.44455946;

  m1.transpose(m1_t).dot(m1, res);
*/
  bool new_cycle = false;
  constexpr size_t buffer_size = CALIBRATION_CYCLES * SAMPLES_PER_CYCLE;

  T x = xAxleValue;
  T y = yAxleValue;
  T z = zAxleValue;

  meanX = wma(x, meanX);
  meanY = wma(y, meanY);
  meanZ = wma(z, meanZ);



  //energys.push(energy);
  if (likelyCycletime > 0) {
    if (foundBasis) {
      // PCA transform the data
      Matrix<float, 3, 1> tmpVec({ x - meanX, y - meanY, z - meanZ });
      Matrix<float, 3, 1> resVec = eigVecs.dot(tmpVec);

/*      for (auto i = 0; i < 3; ++i) {
        DEBUG_PRINT(resVec(i, 0));
        DEBUG_PRINT(" ");
      }
      DEBUG_PRINT("\n");*/
      xs.push(resVec(0, 0));
      ys.push(resVec(1, 0));
      zs.push(resVec(2, 0));

      bool new_data = false;
      if (xs.size() >= wSize) {
        auto j = 0;
        T totx = T(0);
        T toty = T(0);
        T totz = T(0);
        while (j < wSize) {
          totx += pow(xs.shift(), 2);
          toty += pow(ys.shift(), 2);
          totz += pow(zs.shift(), 2);
          ++j;
        }
        //DEBUG_PRINT_2("-------energys size: ", energys.size());
        // downsample to each cycle to ~ samples_per_cycle
        xDetectionBuffer.push(totx / (T)wSize);
        yDetectionBuffer.push(toty / (T)wSize);
        zDetectionBuffer.push(totz / (T)wSize);
        new_data = true;
      }
      //DEBUG_PRINT_3("-------detectionBuffer size: ", xDetectionBuffer.size(), buffer_size);
      //if (true && calibrationCooldown <=0) {
      if (calibrationCooldown <= 0 && !foundPattern && xDetectionBuffer.size() >= (buffer_size)) {  // calibation
        DEBUG_PRINT_2("-------calibration size: ", xDetectionBuffer.size());
        DEBUG_PRINT_2("buffer_size", buffer_size);
        Buffer<T, buffer_size> tmp;
        using index_t = typename decltype(tmp)::index_t;
        long startTime = millis();
        // Determine the optimal axis combination
        Buffer<std::tuple<bool, bool, bool>, 7> combos{
          { true, false, false },
          { false, true, false },
          { false, false, true },
          { true, true, false },
          { false, true, true },
          { true, false, true },
          { true, true, true },
        };
        bool foundCombo = false;
        std::tuple<T, T> best{ std::numeric_limits<T>::lowest(), std::numeric_limits<T>::lowest() };
        Buffer<T, CALIBRATION_CYCLES * SAMPLES_PER_CYCLE> energy;
        bool p_found;
        index_t p_start, p_len;
        T bin_min_v, bin_max_v;
        T spread, p_score;
        for (auto&& combo : combos) {
          energy.clear();
          for (auto i = 0; i < xDetectionBuffer.size(); ++i) {
            T tot = T(0);
            if (std::get<0>(combo)) {
              tot += xDetectionBuffer[i];
            }
            if (std::get<1>(combo)) {
              tot += yDetectionBuffer[i];
            }
            if (std::get<2>(combo)) {
              tot += zDetectionBuffer[i];
            }

            energy.push(tot);
          }
          highCutoff = calcHighCutoff(energy, 0.85f);
          minimum(energy, highCutoff, tmp);
          std::tie(p_found, p_start, p_len, bin_min_v, bin_max_v, spread, p_score) = findPattern<CALIBRATION_CYCLES, SAMPLES_PER_CYCLE / 2 + 1, SAMPLES_PER_CYCLE / 2 + 1, PATTERNS_TO_CHECK, SAMPLES_PER_CYCLE>(tmp);
          if (p_found) {
            std::tuple<T, T> score{ -spread, p_score };
            if ((spread < 5) && (score > best)) {
              best = score;
              bestCombo = combo;
              foundCombo = true;
            }
          }
        }
        if (foundCombo) {
          energy.clear();
          for (auto i = 0; i < xDetectionBuffer.size(); ++i) {
            T tot = T(0);
            if (std::get<0>(bestCombo)) {
              tot += xDetectionBuffer[i];
            }
            if (std::get<1>(bestCombo)) {
              tot += yDetectionBuffer[i];
            }
            if (std::get<2>(bestCombo)) {
              tot += zDetectionBuffer[i];
            }
            energy.push(tot);
          }
          highCutoff = calcHighCutoff(energy, 0.85f);
          minimum(energy, highCutoff, tmp);
          std::tie(p_found, p_start, p_len, bin_min_v, bin_max_v, spread, p_score) = findPattern<CALIBRATION_CYCLES, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK, SAMPLES_PER_CYCLE>(tmp);
          if (p_found) {
            DEBUG_PRINT_3("Found", p_start, p_len);
            Buffer<T, buffer_size - MIN_PATTERN_LEN> scores;
            Buffer<index_t, CALIBRATION_CYCLES + 1> edges;
            Buffer<int, CALIBRATION_CYCLES> diffs;
            matchPattern(tmp, p_start, p_len, scores);
            pattern.clear();
            for (index_t i = p_start; i < p_start + p_len; ++i) {
              pattern.push(tmp[i]);
            }
            risingEdge(scores, bin_max_v, edges);
            //DEBUG_PRINT_ITER("edges ", edges);
            diff(edges, diffs);
            std::sort(diffs.begin(), diffs.end());
            DEBUG_PRINT_ITER("diffs ", diffs);
            // take the median as the found cycle time
            foundCycleTime = diffs[diffs.size() / 2u];
            DEBUG_PRINT_2("wSize", wSize);
            //TODO Alert the user if this is not within spec
            // Run changeItem with better estimate?
            DEBUG_PRINT_3("foundCycleTime", foundCycleTime, foundCycleTime * wSize);
            // determine the detection threshold
            T mpw = meanPeakWidths(scores, bin_min_v);
            DEBUG_PRINT_2f("mpw", mpw, 4);
            T threshold = T(1) - std::min(std::max((foundCycleTime - T(3) - mpw) / T(SAMPLES_PER_CYCLE), T(0)), T(1));
            DEBUG_PRINT_2f("threshold", threshold, 4);
            detectionThreshold = (bin_max_v - bin_min_v) * threshold + bin_min_v;
            DEBUG_PRINT_2f("detectionThreshold", detectionThreshold, 4);
            foundPattern = true;
            timeSinceLastCycle = 0;
            DebugService::Information("VibrationService", "Pattern found after " + String((millis() - evaluationStartMillis) / 1000) + "s, cycletime " + String((int)foundCycleTime), true);
            MQTTService::current->reportPatternFound((int)foundCycleTime, id, evaluationStartMillis);
            this->isCalibrated = true;
            this->isCalibrating = false;
            saveCalibrationData();

          }
        }


        long endTime = millis();
        float delta_t = (endTime - startTime) / 1000.0f;
        DEBUG_PRINT_2("findPattern t:", delta_t);
        // Wait a "few" samples before next calibration attempt.
        // The sample buffer has to be big enough to handle this amount of samples.
        // The time to calibrate is ~7s with 25 samples per cycle and 15 calibration cycles p_len [8-14]
        calibrationCooldown = (int)(delta_t * 1.5 * SAMPLE_FREQ);
      } else if (new_data && foundPattern && xDetectionBuffer.size() >= pattern.size()) {  // detection
        T score = T(0);
        using index_t = typename decltype(xDetectionBuffer)::index_t;
        index_t j = 0;
        for (index_t i = xDetectionBuffer.size() - pattern.size(); i < xDetectionBuffer.size(); ++i) {
          // match against last part of the detection buffers
          T tot = T(0);
          if (std::get<0>(bestCombo)) {
            tot += xDetectionBuffer[i];
          }
          if (std::get<1>(bestCombo)) {
            tot += yDetectionBuffer[i];
          }
          if (std::get<2>(bestCombo)) {
            tot += zDetectionBuffer[i];
          }

          //score += score_func(std::min(detectionBuffer[i], highCutoff), pattern[j]);
          score += score_func(std::min(tot, highCutoff), pattern[j]);
          ++j;
        }

        score /= pattern.size();
        if ((score > lastScore) && (lastScore < detectionThreshold) && (score >= detectionThreshold)) {
          if (timeSinceLastCycle > (int)(foundCycleTime * 0.7)) {
            timeSinceLastCycle = -1;
            new_cycle = true;
            DEBUG_PRINT("New cycle!\n");
          }
        }

        lastScore = score;
        ++timeSinceLastCycle;
      }
    } else {
      //DEBUG_PRINT("Find basis!\n");
      Matrix<double, 1, 3> tmpVec({ xs.last(), ys.last(), zs.last() });
      Matrix<double, 3, 3> tmpMat = tmpVec.transpose().dot(tmpVec);
      accCov += tmpMat;
      nAccCov += 1;
      if (nAccCov >= (likelyCycletime * SAMPLE_FREQ * COV_CYCLES)) {
        DEBUG_PRINT("calc eigvalues!\n");
        Matrix<double, 3, 3> cov = accCov.div(nAccCov);
        gte::SymmetricEigensolver3x3<double> solver;
        std::array<double, 3> eigValues;
        std::array<std::array<double, 3>, 3> tmpEigVecs;
        solver(cov[0][0], cov[0][1], cov[0][2], cov[1][1], cov[1][2], cov[2][2], false, -1, eigValues, tmpEigVecs);
        for (auto i = 0; i < 3; i++) {
          DEBUG_PRINT(eigValues[i]);
          DEBUG_PRINT(" ");
        }
        DEBUG_PRINT("\n");
        DEBUG_PRINT("calc eigvecs!\n");
        for (auto i = 0; i < 3; i++) {
          for (auto j = 0; j < 3; j++) {
            eigVecs[i][j] = tmpEigVecs[i][j];
            DEBUG_PRINT(tmpEigVecs[i][j]);
            DEBUG_PRINT(" ");
          }
          DEBUG_PRINT("\n");
        }
        DEBUG_PRINT("\n");
        foundBasis = true;
      }
    }
  }

  //DEBUG_PRINT_2("energy available:", energys.available());
  calibrationCooldown = std::max(0, calibrationCooldown - 1);
  return new_cycle;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::generateFilenameFromGUID(const String& guid) const {
  String filename = "";
  // Start from the end of the GUID string and move backward
  for (int i = guid.length() - 1; i >= 0 && filename.length() < 8; --i) {
    if (guid[i] != '-') { // Skip hyphens
      filename = guid[i] + filename; // Prepend to keep the order
    }
  }
  return filename;
}

template<typename T,
         size_t SAMPLE_FREQ,
         size_t COV_CYCLES,
         size_t CALIBRATION_CYCLES,
         size_t SAMPLES_PER_CYCLE,
         byte MIN_PATTERN_LEN,
         byte MAX_PATTERN_LEN,
         size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::changeItem(T expectedCycletime, String identifier, bool ignoreSavedprofile) {
  
  DebugService::Information("VibrationService", "Starting new vibrationanalyzis on " + identifier + " with cycletime " + String(expectedCycletime),true);
  this->isCalibrated = false;
  this->isCalibrating = false;
  this->isCalibrationDataLoaded = false;
  likelyCycletime = expectedCycletime;
  id = identifier;
  evaluationStartMillis = millis();
  // there should be a minimum number of samples requierd depending on sampling_freq and expectedCycletime
  wSize = (int)((expectedCycletime * SAMPLE_FREQ) / T(SAMPLES_PER_CYCLE));
  if (wSize <= 0) {
    // This is not ideal. There there should preferably be more then SAMPLES_PER_CYCLE samples per cycle
    DEBUG_PRINT_2("wSize is less then 1!", wSize);
    wSize = 1;
  }
  clearData();

  if(!ignoreSavedprofile)
  {
      if (!loadCalibrationData(expectedCycletime, String(identifier)))
      {
          clearData(); //Just to be sure no data was actually loaded
      }
  }
  if (!isCalibrationDataLoaded)
  {
      this->isCalibrating = true;
  }
  else
  {
      this->isCalibrated = true;
  }
}
template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::clearData()
{
    foundPattern = false;
    foundBasis = false;
    calibrationCooldown = 0;
    accCov.zero();
    xs.clear();
    ys.clear();
    zs.clear();
    xDetectionBuffer.clear();
    yDetectionBuffer.clear();
    zDetectionBuffer.clear();
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serializeCircularBuffer(const CircularBuffer<T, CALIBRATION_CYCLES * SAMPLES_PER_CYCLE>& buffer) const {
  String data = "";
  for (size_t i = 0; i < buffer.size(); ++i) {
    if (i > 0) data += ",";
    data += String(buffer[i],SERIAL_PRECISION);
  }
  return data;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serialize() const {
  String data = "";
  
  // Serialize basic types with appropriate precision for floats
  data += "meanX:" + String(meanX, SERIAL_PRECISION) + "\n";
  data += "meanY:" + String(meanY, SERIAL_PRECISION) + "\n";
  data += "meanZ:" + String(meanZ, SERIAL_PRECISION) + "\n";
  data += "likelyCycletime:" + String(likelyCycletime, SERIAL_PRECISION) + "\n";

  // Serialize String type as is
  data += "id:" + id + "\n";

  // Serialize integer types directly
  data += "wSize:" + String(wSize) + "\n";
  data += "foundPattern:" + String(foundPattern) + "\n"; // Assuming foundPattern is a bool
  data += "foundBasis:" + String(foundBasis) + "\n"; // Assuming foundBasis is a bool
  data += "foundCycleTime:" + String(foundCycleTime) + "\n"; // Assuming foundCycleTime is a byte

  // Serialize floating point types with precision
  data += "detectionThreshold:" + String(detectionThreshold, SERIAL_PRECISION) + "\n";
  data += "highCutoff:" + String(highCutoff, SERIAL_PRECISION) + "\n";

  // Serialize size_t directly
  data += "timeSinceLastCycle:" + String(timeSinceLastCycle) + "\n";

  // Serialize last floating point type with precision
  data += "lastScore:" + String(lastScore, SERIAL_PRECISION) + "\n";

  // Serialize integers directly
  data += "calibrationCooldown:" + String(calibrationCooldown) + "\n";
  
  // Serialize CircularBuffers
  data += "xDetectionBuffer:" + serializeCircularBuffer(xDetectionBuffer) + "\n";
  data += "yDetectionBuffer:" + serializeCircularBuffer(yDetectionBuffer) + "\n";
  data += "zDetectionBuffer:" + serializeCircularBuffer(zDetectionBuffer) + "\n";
   // Serialize Buffer
  data += "pattern:" + serializeBuffer(pattern) + "\n";
  
  // Serialize Matrix<double, 3, 3>
  data += "accCov:" + serializeDoubleMatrix(accCov) + "\n";
  
  // Serialize size_t
  data += "nAccCov:" + serializeSizeT(nAccCov) + "\n";
  
  // Serialize Matrix<float, 3, 3>
  data += "eigVecs:" + serializeFloatMatrix(eigVecs) + "\n";
  
  // Serialize tuple
  data += "bestCombo:" + serializeTuple(bestCombo) + "\n";

  return data;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
bool VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::deserialize(const String& data, T expectedCycletime){
  // Split the data by lines
  int start = 0;
  int end = data.indexOf('\n');
  while (end != -1) {
    String line = data.substring(start, end);
    // Extract key and value
    int separatorPos = line.indexOf(':');
    String key = line.substring(0, separatorPos);
    String value = line.substring(separatorPos + 1);

    if (key == "likelyCycletime") {
      T cycletime = value.toFloat();
      if (cycletime == expectedCycletime) {
        DebugService::Information("vibrationService::loadCalibrationData","Found correct calibration file!",true);
        likelyCycletime = cycletime;
      } else {
        // If cycletime does not match, we should not proceed further
        DebugService::Information("vibrationService::loadCalibrationData","Wrong cycletime in calibration data!",true);
        return false;
      }
    } else if (key == "foundPattern") {
      foundPattern = value.toInt(); // Assuming value is "1" or "0"
    } else if (key == "foundBasis") {
      foundBasis = value.toInt();
    } else if (key == "foundCycleTime") {
      foundCycleTime = value.toInt();
    } else if (key == "xDetectionBuffer") {
      deserializeCircularBuffer(value, xDetectionBuffer);
    } else if (key == "yDetectionBuffer") {
      deserializeCircularBuffer(value, yDetectionBuffer);
    } else if (key == "zDetectionBuffer") {
      deserializeCircularBuffer(value, zDetectionBuffer);
    } else if (key == "meanX") {
      meanX = value.toFloat();
    } else if (key == "meanY") {
      meanY = value.toFloat();
    } else if (key == "meanZ") {
      meanZ = value.toFloat();
    } else if (key == "wSize") {
      wSize = value.toInt();
    } else if (key == "detectionThreshold") {
      detectionThreshold = value.toFloat();
    } else if (key == "highCutoff") {
      highCutoff = value.toFloat();
    } else if (key == "timeSinceLastCycle") {
      timeSinceLastCycle = static_cast<size_t>(value.toInt());
    } else if (key == "lastScore") {
      lastScore = value.toFloat();
    } else if (key == "calibrationCooldown") {
      calibrationCooldown = value.toInt();
    } else if (key == "pattern") {
      deserializeBuffer(value, pattern); // You need to define this method
    } else if (key == "accCov") {
      deserializeDoubleMatrix(value, accCov); // You need to define this method
    } else if (key == "nAccCov") {
      nAccCov = static_cast<size_t>(value.toInt());
    } else if (key == "eigVecs") {
      deserializeFloatMatrix(value, eigVecs); // You need to define this method
    } else if (key == "bestCombo") {
      deserializeTuple(value, bestCombo); // You need to define this method
    }
    // Prepare for the next iteration
    start = end + 1;
    end = data.indexOf('\n', start);

  }

  return true;
}

// Helper function to deserialize CircularBuffer from a string
template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::deserializeCircularBuffer(const String& data, CircularBuffer<T, CALIBRATION_CYCLES * SAMPLES_PER_CYCLE>& buffer) const {
  int start = 0;
  int end = data.indexOf(',');
  while (end != -1) {
    T value = data.substring(start, end).toFloat();
    buffer.push(value);
    start = end + 1;
    end = data.indexOf(',', start);
  }
  // Don't forget the last value if there's no trailing comma
  if (start < data.length()) {
    T value = data.substring(start).toFloat();
    buffer.push(value);
  }
}

// Integration with loading the calibration data
template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
bool VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::loadCalibrationData(T expectedCycletime, const String& identifier) {
  String fileName = generateFilenameFromGUID(identifier) + ".cl1";
  String data = MemoryServiceFAT::loadFileAsString(fileName);
  if (data.length() > 0)
  {
      DebugService::Information("vibrationService::loadCalibrationData", "Deserializing data", true);

      if (deserialize(data, expectedCycletime))
      {
          this->isCalibrationDataLoaded = true;
          return true;
      }
  }
  else
  {
    DebugService::Information("vibrationService::loadCalibrationData","No calibration data found!",true);
  }
  return false;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::saveCalibrationData(){
  // First, serialize the current calibration data
  String serializedData = this->serialize();

  // Generate a filename from the last 8 non-hyphen characters of the GUID
  String filename = generateFilenameFromGUID(id) + ".cl1";

  // Use your file service to write the serialized data to the file
  bool writeSuccess = MemoryServiceFAT::saveFile(filename, serializedData);

  // Handle the case where the file could not be written
  if (!writeSuccess) {
    // Handle error, e.g., by logging or retrying
      DebugService::Error("vibrationService::saveCalibrationData", "Unable to save calibration data to file.", true);
  }
  else
  {
    DebugService::Information("vibrationService::saveCalibrationData","Calibration data saved to file.",true);
  }
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serializeBuffer(const Buffer<T, MAX_PATTERN_LEN>& buffer) const {
  String data = "";
  for (size_t i = 0; i < buffer.size(); ++i) {
    if (i > 0) data += ",";
    data += String(buffer[i], SERIAL_PRECISION); // SERIAL_PRECISION is a defined macro for the number of decimal places
  }
  return data;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serializeDoubleMatrix(const Matrix<double, 3, 3>& matrix) const {
  String data = "";
  for (size_t i = 0; i < 3; ++i) {
    for (size_t j = 0; j < 3; ++j) {
      if (i > 0 || j > 0) data += ",";
      data += String(matrix(i, j), SERIAL_PRECISION);
    }
  }
  return data;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serializeFloatMatrix(const Matrix<float, 3, 3>& matrix) const {
  String data = "";
  for (size_t i = 0; i < 3; ++i) {
    for (size_t j = 0; j < 3; ++j) {
      if (i > 0 || j > 0) data += ",";
      data += String(matrix(i, j), SERIAL_PRECISION);
    }
  }
  return data;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serializeTuple(const std::tuple<bool, bool, bool>& tuple) const {
  String data = "";
  data += String(std::get<0>(tuple) ? "1" : "0") + ",";
  data += String(std::get<1>(tuple) ? "1" : "0") + ",";
  data += String(std::get<2>(tuple) ? "1" : "0");
  return data;
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
String VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::serializeSizeT(size_t value) const {
  return String(value);
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::deserializeBuffer(const String& data, Buffer<T, MAX_PATTERN_LEN>& buffer) const{
  buffer.clear();
  int start = 0;
  int end = data.indexOf(',');
  while (end != -1) {
    buffer.push(data.substring(start, end).toFloat());
    start = end + 1;
    end = data.indexOf(',', start);
  }
  if (start < data.length()) {
    buffer.push(data.substring(start).toFloat());
  }
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::deserializeTuple(const String& data, std::tuple<bool, bool, bool>& tuple) {
  int firstComma = data.indexOf(',');
  int secondComma = data.indexOf(',', firstComma + 1);

  bool first = data.substring(0, firstComma).toInt();
  bool second = data.substring(firstComma + 1, secondComma).toInt();
  bool third = data.substring(secondComma + 1).toInt();

  tuple = std::make_tuple(first, second, third);
}

template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::deserializeDoubleMatrix(const String& data, Matrix<double, 3, 3>& matrix) {
  int start = 0;
  int end = data.indexOf(',');
  size_t row = 0, col = 0;

  while (end != -1) {
    T value = data.substring(start, end).toDouble();
    matrix(row, col) = value;
    start = end + 1;
    end = data.indexOf(',', start);

    if (++col >= 3) {
      col = 0;
      ++row;
    }
  }
}

  template<typename T, size_t SAMPLE_FREQ, size_t COV_CYCLES, size_t CALIBRATION_CYCLES, size_t SAMPLES_PER_CYCLE, byte MIN_PATTERN_LEN, byte MAX_PATTERN_LEN, size_t PATTERNS_TO_CHECK>
void VibrationService<T, SAMPLE_FREQ, COV_CYCLES, CALIBRATION_CYCLES, SAMPLES_PER_CYCLE, MIN_PATTERN_LEN, MAX_PATTERN_LEN, PATTERNS_TO_CHECK>::deserializeFloatMatrix(const String& data, Matrix<float, 3, 3>& matrix) {
  int start = 0;
  int end = data.indexOf(',');
  size_t row = 0, col = 0;

  while (end != -1) {
    T value = data.substring(start, end).toFloat();
    matrix(row, col) = value;
    start = end + 1;
    end = data.indexOf(',', start);

    if (++col >= 3) {
      col = 0;
      ++row;
    }
  }
  
  // Don't forget the last value if there's no trailing comma
  if (start < data.length()) {
    T value = data.substring(start).toFloat();
    matrix(row, col) = value;
  }
}