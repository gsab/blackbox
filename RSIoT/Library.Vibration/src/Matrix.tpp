
#define DEBUG_PRINT(x) Serial.print(x);
#define DEBUG_PRINT_2(x, y) \
  Serial.print(x); \
  Serial.print(" "); \
  Serial.println(y);
#define DEBUG_PRINT_2f(x, y, v) \
  Serial.print(x); \
  Serial.print(" "); \
  Serial.println(y, v);
#define DEBUG_PRINT_3(x, y, z) \
  Serial.print(x); \
  Serial.print(" "); \
  Serial.print(y); \
  Serial.print(" "); \
  Serial.println(z);
#define DEBUG_PRINT_ITER(s, buf) \
  DEBUG_PRINT(s); \
  for (auto&& i : buf) { \
    DEBUG_PRINT(i); \
    DEBUG_PRINT(" "); \
  } \
  DEBUG_PRINT("\n");

template<typename T, size_t M, size_t N>
Matrix<T, M, N>::Matrix() {
}

template<typename T, size_t M, size_t N>
Matrix<T, M, N>::Matrix(const T (&list)[M * N]) {
  for (auto i = 0; i < M * N; ++i) {
    size_t row = i / N;
    size_t col = i % N;
    data[row][col] = list[i];
  }
}


template<typename T, size_t M, size_t N>
T Matrix<T, M, N>::operator()(size_t x, size_t y) const {
  return data[x][y];
}


template<typename T, size_t M, size_t N>
T& Matrix<T, M, N>::operator()(size_t x, size_t y) {
  return data[x][y];
}

template<typename T, size_t M, size_t N>
T* Matrix<T, M, N>::operator[](size_t index) {
  return data[index];
}

template<typename T, size_t M, size_t N>
Matrix<T, M, N>& Matrix<T, M, N>::operator+=(const Matrix<T, M, N>& other) {
  for (auto i = 0; i < M; ++i) {
    for (auto j = 0; j < N; ++j) {
      data[j][i] += other.data[i][j];
    }
  }
  return *this;
}

template<typename T, size_t M, size_t N>
template<size_t K>
//Matrix<T, M, K>& Matrix<T, M, N>::dot(const Matrix<T, N, K>& other, Matrix<T, M, K>& res) {
Matrix<T, M, K> Matrix<T, M, N>::dot(const Matrix<T, N, K>& other) {
  Matrix<T, M, K> res;
  for (auto i = 0; i < M; ++i) {
    for (auto k = 0; k < K; ++k) {
      res.data[i][k] = T(0);
      for (auto j = 0; j < N; ++j) {
        res.data[i][k] += data[i][j] * other.data[j][k];
      }
    }
  }

  return res;
}




template<typename T, size_t M, size_t N>
//Matrix<T, N, M>& Matrix<T, M, N>::transpose(Matrix<T, N, M>& res) {
Matrix<T, N, M> Matrix<T, M, N>::transpose() {
  Matrix<T, N, M> res;
  for (auto i = 0; i < M; ++i) {
    for (auto j = 0; j < N; ++j) {
      res.data[j][i] = data[i][j];
    }
  }
  return res;
}


template<typename T, size_t M, size_t N>
Matrix<T, N, M> Matrix<T, M, N>::div(T value) {
  //Matrix<T, N, M>& Matrix<T, M, N>::div(T value, Matrix<T, N, M>& res) {
  //res.zero();
  Matrix<T, N, M> res;
  for (auto i = 0; i < M; ++i) {
    for (auto j = 0; j < N; ++j) {
      res.data[j][i] = data[i][j] / value;
    }
  }

  return res;
}


template<typename T, size_t M, size_t N>
void Matrix<T, M, N>::zero() {
  for (auto i = 0; i < M; ++i) {
    for (auto j = 0; j < N; ++j) {
      data[i][j] = T(0);
    }
  }
}