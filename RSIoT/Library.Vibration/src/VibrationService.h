// VibrationService.h
#pragma once

#include "Arduino.h"
#include <WString.h>
#include <CircularBuffer.h>
#include <vector>
#include "buffer.h"
#include "Matrix.h"
#include <MQTTService.h>
#include <MemoryServiceFAT.h>
#include <DebugService.h>

#define SERIAL_PRECISION 12

template<typename T,
         size_t SAMPLE_FREQ,
         size_t COV_CYCLES = 3,
         size_t CALIBRATION_CYCLES = 12,
         size_t SAMPLES_PER_CYCLE = 25,
         byte MIN_PATTERN_LEN = 8,
         byte MAX_PATTERN_LEN = 14,
         size_t PATTERNS_TO_CHECK = 3>
class VibrationService {

public:
  VibrationService()
    : meanX(T(0)), meanY(T(0)), meanZ(T(1)), likelyCycletime(0), wSize(0), foundPattern(false), foundBasis(false), foundCycleTime(0), detectionThreshold(0), highCutoff(0), timeSinceLastCycle(0), lastScore(0), calibrationCooldown(0) {}
  T inline wma(T x, T old_avarage);
  void initialize();
  bool evaluateData(T xAxleValue, T yAxleValue, T zAxleValue);
  void changeItem(T expectedCycletime, String identifier, bool ignoreSavedprofile = false);
  String serialize() const;
  bool deserialize(const String& data, T expectedCycletime);
  String generateFilenameFromGUID(const String& guid) const;
  void saveCalibrationData();
  bool loadCalibrationData(T expectedCycletime, const String& identifier);
  void clearData();
  void deserializeCircularBuffer(const String& data, CircularBuffer<T, CALIBRATION_CYCLES* SAMPLES_PER_CYCLE>& buffer) const;
  String serializeCircularBuffer(const CircularBuffer<T, CALIBRATION_CYCLES* SAMPLES_PER_CYCLE>& buffer) const;
  void deserializeTuple(const String& data, std::tuple<bool, bool, bool>& tuple);
  void deserializeBuffer(const String& data, Buffer<T, MAX_PATTERN_LEN>& buffer) const;
  void deserializeDoubleMatrix(const String& data, Matrix<double, 3, 3>& matrix);
  void deserializeFloatMatrix(const String& data, Matrix<float, 3, 3>& matrix);
  String serializeBuffer(const Buffer<T, MAX_PATTERN_LEN>& buffer) const;
  String serializeSizeT(size_t value) const;
  String serializeTuple(const std::tuple<bool, bool, bool>& tuple) const;
  String serializeDoubleMatrix(const Matrix<double, 3, 3>& matrix) const;
  String serializeFloatMatrix(const Matrix<float, 3, 3>& matrix) const;
  T likelyCycletime;  // in seconds
  String id;
  bool isCalibrated;
  bool isCalibrating;
  bool isCalibrationDataLoaded;
  long evaluationStartMillis = 0;
private:
  T meanX;
  T meanY;
  T meanZ;
  int wSize;
  bool foundPattern;
  bool foundBasis;
  byte foundCycleTime;  // in detection samples
  T detectionThreshold;
  T highCutoff;
  size_t timeSinceLastCycle;  // in detection samples
  T lastScore;
  int calibrationCooldown;  // in samples
  
  //TODO the size of this buffer is dependent on the maximum likelyCycletime
  // this size will be enough for cycles that are 512 * SAMPLES_PER_CYCLE / SAMPLE_FREQ second long
  CircularBuffer<T, 512> xs;
  CircularBuffer<T, 512> ys;
  CircularBuffer<T, 512> zs;
  CircularBuffer<T, CALIBRATION_CYCLES * SAMPLES_PER_CYCLE> xDetectionBuffer;
  CircularBuffer<T, CALIBRATION_CYCLES * SAMPLES_PER_CYCLE> yDetectionBuffer;
  CircularBuffer<T, CALIBRATION_CYCLES * SAMPLES_PER_CYCLE> zDetectionBuffer;
  Buffer<T, MAX_PATTERN_LEN> pattern;
  Matrix<double, 3, 3> accCov;
  size_t nAccCov;
  Matrix<float, 3, 3> eigVecs;
  std::tuple<bool, bool, bool> bestCombo;
};

#include <VibrationService.tpp>
