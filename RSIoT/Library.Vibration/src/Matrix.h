#pragma once

template<typename T, size_t M, size_t N>
class Matrix {
public:
  explicit Matrix();
  explicit Matrix(const T (&list)[M * N]);

  Matrix(const Matrix&) = delete;
  Matrix(Matrix&&) = default;

  T operator()(size_t x, size_t y) const;
  T& operator()(size_t x, size_t y);
  T* operator[](size_t index);
  Matrix<T, M, N>& operator+=(const Matrix<T, M, N>& other);

  template<size_t K>
  Matrix<T, M, K> dot(const Matrix<T, N, K>& other);
  Matrix<T, N, M> transpose();
  Matrix<T, N, M> div(T value);

  void zero();

private:
  template<typename U, size_t N_, size_t M_>
  friend class Matrix;
  T data[M][N];
};

#include <Matrix.tpp>
