# CMake generated Testfile for 
# Source directory: C:/Users/Ludwig/Documents/blackbox/RSIoT/Libraries/ArduinoJson/6.17.1/ArduinoJson
# Build directory: C:/Users/Ludwig/Documents/blackbox/RSIoT/Libraries/ArduinoJson/6.17.1/ArduinoJson/out/build/x64-Debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("extras/tests")
subdirs("extras/fuzzing")
