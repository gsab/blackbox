# CMake generated Testfile for 
# Source directory: C:/Users/Ludwig/Documents/blackbox/RSIoT/Libraries/ArduinoJson/6.17.1/ArduinoJson/extras/tests
# Build directory: C:/Users/Ludwig/Documents/blackbox/RSIoT/Libraries/ArduinoJson/6.17.1/ArduinoJson/out/build/x64-Debug/extras/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("catch")
subdirs("FailingBuilds")
subdirs("IntegrationTests")
subdirs("JsonArray")
subdirs("JsonDeserializer")
subdirs("JsonDocument")
subdirs("JsonObject")
subdirs("JsonSerializer")
subdirs("JsonVariant")
subdirs("MemoryPool")
subdirs("Misc")
subdirs("MixedConfiguration")
subdirs("MsgPackDeserializer")
subdirs("MsgPackSerializer")
subdirs("Numbers")
subdirs("TextFormatter")
