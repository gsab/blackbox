#include <DebugService.h>

ESP32Time DebugService::rtc;
bool DebugService::AsyncServerInitialized = false;
ArduinoQueue<DebugMessage> DebugService::debugMessages = ArduinoQueue<DebugMessage>(100);
String DebugService::CurrentVersion = "";
String DebugService::WifiStatus = "No connection";
String DebugService::WifiStrength = "";
String DebugService::WifiIP = "";
String DebugService::GSMStatus = "No connection";
String DebugService::GSMSignalStrength = "";
String DebugService::GSMSignalNetworkName = "";
String DebugService::MQTTStatus = "No connection";
String DebugService::lastRestart = "No data";
String DebugService::eventQueueAmount = "0";
bool DebugService::timeSynchronized = "false";
String DebugService::freeMemory = "0";
int DebugService::fileMessageCount = 0;
String DebugService::currentFileName = "";

int DebugService::nextArrayposition = 0;
int DebugService::messageCount = 200;

ArduinoQueue<DebugMessage> DebugService::debugMessagesToSave = ArduinoQueue<DebugMessage>(500);

void DebugService::Debug(String header, String message, bool sendToServer)
{
	if (ConfigurationService::debugLevel >= 4)
	{
		Serial.println("Debug: " + header + ": " + message);
		if (ConfigurationService::sendDebugMessages || sendToServer)
			debugMessages.enqueue(DebugMessage(4, DebugService::rtc.getEpoch(), "Debug: " + header + ": " + message));
	}
}

void DebugService::Information(String header, String message, bool sendToServer)
{
	if(!ConfigurationService::outputRawData)
		Serial.println("Information: " + header + ": " + message);
	if(ConfigurationService::sendDebugMessages && ConfigurationService::debugLevel >= 3 || sendToServer)
		debugMessages.enqueue(DebugMessage(3, DebugService::rtc.getEpoch(), "Information: " + header + ": " + message));
	if(ConfigurationService::newHardware)
		debugMessagesToSave.enqueue(DebugMessage(3, DebugService::rtc.getEpoch(), header + ": " + message));
}
void DebugService::Warning(String header, String message, bool sendToServer)
{
	if (!ConfigurationService::outputRawData)
		Serial.println("Warning: " + header + ": " + message);
	if (ConfigurationService::sendDebugMessages && ConfigurationService::debugLevel >= 2 || sendToServer)
		debugMessages.enqueue(DebugMessage(2, DebugService::rtc.getEpoch(), "Warning: " + header + ": " + message));
	
	if (ConfigurationService::newHardware)
		debugMessagesToSave.enqueue(DebugMessage(2, DebugService::rtc.getEpoch(), header + ": " + message));

}
void DebugService::Error(String header, String message, bool sendToServer)
{
	if (!ConfigurationService::outputRawData)
		Serial.println("Error: " + header + ": " + message);
	if (ConfigurationService::sendDebugMessages && ConfigurationService::debugLevel >= 1 || sendToServer)
		debugMessages.enqueue(DebugMessage(2, DebugService::rtc.getEpoch(), "Warning: " + header + ": " + message));
	
	if (ConfigurationService::newHardware)
		debugMessagesToSave.enqueue(DebugMessage(1, DebugService::rtc.getEpoch(), header + ": " + message));

}

void DebugService::addDebugMessageToLog(DebugMessage message)
{
	/*if (ConfigurationService::newHardware)
	{

		String messageString = String(message.time) + ":" + String(message.debugLevel==1?"ERROR": message.debugLevel == 2 ? "WARNING": "INFORMATION") + ":" + message.message;

		int maxFileSize = 100000;

		if (DebugService::currentFileName == "" || messageString.length() + DebugService::fileMessageCount > maxFileSize)
		{
			DebugService::currentFileName = DebugService::rtc.getEpoch() + ".log";
			DebugService::fileMessageCount = 0;
		}

		if (SDMemoryService::saveFile(DebugService::currentFileName, messageString))
		{
			DebugService::fileMessageCount += messageString.length();
		}
	}*/
}

String DebugService::getCurrentLog()
{
	int currentPosition = nextArrayposition - 1;

	String result = "";

	//for (int i = 0; i < messageCount; i++)
	//{
	//	if (currentPosition<0)
	//		currentPosition = messageCount - 1;

	//	if (debugArray[currentPosition].message != "")
	//	{
	//		result += "`";
	//		result += TimeUtils::getDateTimeString(debugArray[currentPosition].time);

	//		result += " - "  + debugArray[currentPosition].message +"\\n` + " + "\n";
	//	}
	//	currentPosition--;
	//}

	return result;
}
