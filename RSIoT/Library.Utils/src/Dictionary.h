#pragma once
#include "Arduino.h"

class Dictionary {
private:
    int max_entries;
    String* keys;
    long* values;
    int num_entries = 0;

public:
    Dictionary(int size);
    void add_entry(String key, long value);
    long get_value(String key);
    bool containsKey(String key);
};