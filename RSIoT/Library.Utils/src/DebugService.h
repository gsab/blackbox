#pragma once
#include <WString.h>
#include <Arduino.h>
#include <ArduinoQueue.h>
#include <DebugMessage.h>
#include <ESP32Time.h>
#include <ConfigurationService.h>
#include <WebSerial.h>
#include <TimeUtils.h>
//#include <SDMemoryService.h>


class DebugService
{
public:
	static bool AsyncServerInitialized;
	static void Debug(String header, String message, bool sendToServer = false);
	static void Information(String header, String message, bool sendToServer = false);
	static void Warning(String header, String message, bool sendToServer = false);
	static void Error(String header, String message, bool sendToServer = false);
	static ArduinoQueue<DebugMessage> debugMessagesToSave;
	static ArduinoQueue<DebugMessage> debugMessages;
	static ESP32Time rtc;
	static String CurrentVersion;
	static String WifiStatus;
	static String WifiStrength;
	static String WifiIP;
	static String GSMStatus;
	static String GSMSignalStrength;
	static String GSMSignalNetworkName;
	static String MQTTStatus;
	static String lastRestart;
	static String eventQueueAmount;
	static String freeMemory;
	static bool timeSynchronized;
	static void addDebugMessageToLog(DebugMessage message);
	static String getCurrentLog();
	static int nextArrayposition;
	static int messageCount;
private:
	static int fileMessageCount;
	static String currentFileName;
};