#pragma once
#include <WString.h>
#include <Arduino.h>

class DebugMessage
{
public:
	DebugMessage();
	DebugMessage(String inputString);
	DebugMessage(int8_t debugLevel, long time, String message);
	int8_t debugLevel;
	long time;
	String message;
};