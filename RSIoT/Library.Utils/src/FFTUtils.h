#pragma once
#include <Arduino.h>

class FFTUtils
{
	public:
		static float Approx_FFT(int in[], int N, float Frequency);
		static int fast_sine(int Amp, int th);
		static int fast_cosine(int Amp, int th);
		static int fastRSS(int a, int b);
	private:
		static byte isin_data[128];
		static unsigned int Pow2[14];
		static byte RSSdata[20];

};
