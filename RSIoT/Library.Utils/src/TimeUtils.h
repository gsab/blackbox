#pragma once
#include <time.h>
#include <WString.h>

class TimeUtils {

public:
	static bool checkTimeExpiredMillis(int startTime, int endTime, int amountOfTime);
	static int checkTimeDifferenceMillis(int startTime, int endTime);

	static tm epochToDateTime(time_t epoch);

	static String getDateTimeString(time_t epoch);
};
