#include "Dictionary.h"

Dictionary::Dictionary(int size)
{
    max_entries = size;
    keys = new String[size];
    values = new long[size];
}

void Dictionary::add_entry(String key, long value) {
    if (num_entries >= max_entries) {
        return;
    }

    int index = -1;
    for (int i = 0; i < num_entries; i++) {
        if (keys[i] == key) {
            index = i;
            break;
        }
    }

    if (index >= 0) {
        // key already exists, update the value
        values[index] = value;
    }
    else {
        // key doesn't exist, add new entry
        keys[num_entries] = key;
        values[num_entries] = value;
        num_entries++;
    }
}

long Dictionary::get_value(String key) {
    int index = -1;
    for (int i = 0; i < num_entries; i++) {
        if (keys[i] == key) {
            index = i;
            break;
        }
    }

    if (index >= 0) {
        return values[index];
    }
    else {
        return long(); // return default value if key not found
    }
}

bool Dictionary::containsKey(String key) {
    for (int i = 0; i < num_entries; i++) {
        if (keys[i] == key) {
            return true;
        }
    }

    return false;
}