/*
 Name:		Library.cpp
 Created:	11/18/2020 3:23:15 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "StringUtils.h"


const char* StringUtils::convertStringToCharArray(String string)
{
	char __charArray[sizeof(string)];
	string.toCharArray(__charArray, sizeof(__charArray));
	return __charArray;
}

String StringUtils::getSplitValue(String data, char separator, int index)
{
	int found = 0;
	int strIndex[] = { 0, -1 };
	int maxIndex = data.length() - 1;

	for (int i = 0; i <= maxIndex && found <= index; i++) {
		if (data.charAt(i) == separator || i == maxIndex) {
			found++;
			strIndex[0] = strIndex[1] + 1;
			strIndex[1] = (i == maxIndex) ? i + 1 : i;
		}
	}
	return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

bool StringUtils::parseIPAddress(const String& ipStr, IPAddress& ip) {
    int parts[4] = { 0, 0, 0, 0 };
    int maxDots = 3;
    int dotCount = 0;
    String temp = ipStr;

    for (int i = 0; i < maxDots; i++) {
        int dotPos = temp.indexOf('.');
        if (dotPos == -1) {
            return false;  // Not enough dots
        }
        parts[i] = temp.substring(0, dotPos).toInt();
        if (parts[i] > 255) {
            return false;  // Each byte should be in the range [0, 255]
        }
        temp = temp.substring(dotPos + 1);
        dotCount++;
    }

    if (temp.indexOf('.') != -1) {
        return false;  // Too many dots
    }

    parts[3] = temp.toInt();
    if (parts[3] > 255) {
        return false;  // Each byte should be in the range [0, 255]
    }

    ip = IPAddress(parts[0], parts[1], parts[2], parts[3]);
    return true;
}

String StringUtils::macAddressToString(byte mac[6]) {
    char buffer[18];
    snprintf(buffer, sizeof(buffer), "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    return String(buffer);
}

bool StringUtils::isDigit(char c) {
    return c >= '0' && c <= '9';
}

bool StringUtils::isNumber(const String& str, int digits) {
    // Check if the length is 6
    if (str.length() != digits) {
        return false;
    }

    // Check each character of the string
    for (int i = 0; i < digits; i++) {
        if (!isDigit(str[i])) {
            return false;  // Return false if any character is not a digit
        }
    }

    return true;
}