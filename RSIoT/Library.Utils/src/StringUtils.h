/*
 Name:		Library.h
 Created:	11/18/2020 3:23:15 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include "WString.h"
#include <EthernetWizNet.h>
class StringUtils {

public:
	static const char* convertStringToCharArray(String string);
	static String getSplitValue(String data, char separator, int index);
	static bool parseIPAddress(const String& ipStr, IPAddress& ip);
	static String macAddressToString(byte mac[6]);
	static bool isDigit(char c);
	static bool isNumber(const String& str, int digits);
};