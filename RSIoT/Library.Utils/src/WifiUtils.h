/*
 Name:		Library.h
 Created:	11/18/2020 3:23:15 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include "WString.h"
#include <esp_wifi.h>
#include <WiFi.h>
#include <ESPmDNS.h>

class WifiUtils {

public:
	static String getConnectedClientsInfo();
	static String resolveHostname(const IPAddress& ip);
	static int numberOfConnectedClients();
	static String getAvailableWifiNetworks();
	static int numberOfAvailableNetworks;
private:
	static String wifiNetworks;
};