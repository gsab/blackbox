#include <TimeUtils.h>

bool TimeUtils::checkTimeExpiredMillis(int startTime, int endTime, int amountOfTime)
{
	return checkTimeDifferenceMillis(startTime, endTime) > amountOfTime;
}

int TimeUtils::checkTimeDifferenceMillis(int startTime, int endTime)
{
	if (startTime > endTime)
	{
		//4294967 milliseconds is the esp32 rollover value for millis();
		endTime += 4294967;
	}

	return endTime - startTime;
}

tm TimeUtils::epochToDateTime(time_t epoch)
{
	tm result;
	const tm* temp = gmtime(&epoch);
	if (temp) {
		result = *temp;
	}
	return result;
}

String TimeUtils::getDateTimeString(time_t epoch)
{
	tm result = epochToDateTime(epoch);

	return String(result.tm_year + 1900) + "-" + String(result.tm_mon + 1) + "-" + String(result.tm_mday) + " " + String(result.tm_hour) + ":" + String(result.tm_min) + ":" + String(result.tm_sec) + " ";
}
