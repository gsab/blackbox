#include "DebugMessage.h"

DebugMessage::DebugMessage()
{
}

DebugMessage::DebugMessage(String inputString)
{
	// Extract time
	int firstColon = inputString.indexOf(':');
	this->time = inputString.substring(0, firstColon).toInt();

	// Extract debug level
	int secondColon = inputString.indexOf(':', firstColon + 1);
	String levelStr = inputString.substring(firstColon + 1, secondColon);
	debugLevel = 0; // Default to INFORMATION
	if (levelStr == "ERROR") this->debugLevel = 1;
	else if (levelStr == "WARNING") this->debugLevel = 2;
	// Assuming INFORMATION is 0 or default

	// Extract message
	this->message = inputString.substring(secondColon + 1);
}

DebugMessage::DebugMessage(int8_t debugLevel, long time, String message)
{
	this->debugLevel = debugLevel;
	this->time = time;
	this->message = message;
}
