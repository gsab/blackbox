#include "WifiUtils.h"

String WifiUtils::wifiNetworks = "";
int WifiUtils::numberOfAvailableNetworks = 0;

String WifiUtils::getConnectedClientsInfo() {
	wifi_sta_list_t stationList;
	tcpip_adapter_sta_list_t tcpipList;
	String clientInfo = "";

	// Get the list of connected stations
	esp_wifi_ap_get_sta_list(&stationList);
	tcpip_adapter_get_sta_list(&stationList, &tcpipList);

	// Add the number of connected clients to the string
	clientInfo += "Number of connected clients: ";
	clientInfo += String(stationList.num) + "\n";

	// Add the IP addresses, hostnames, and other information for each client to the string
	for (int i = 0; i < stationList.num; i++) {
		IPAddress clientIP = IPAddress(tcpipList.sta[i].ip.addr);
		String hostname = resolveHostname(clientIP);

		clientInfo += "Client " + String(i + 1) + " - IP: ";
		clientInfo += clientIP.toString();
		clientInfo += ", Hostname: " + (hostname.length() > 0 ? hostname : "Unknown");
		clientInfo += ", MAC: ";

		for (int j = 0; j < 6; j++) {
			clientInfo += String(stationList.sta[i].mac[j], HEX);
			if (j < 5) {
				clientInfo += ":";
			}
		}

		clientInfo += "\n";
	}

	return clientInfo;
}

String WifiUtils::resolveHostname(const IPAddress& ip) {
	String hostname = "";

	// Add '.local' to the end of the IP address
	String domain = ip.toString() + ".local";

	// Perform an mDNS query for the hostname
	if (MDNS.queryHost(domain.c_str(), 2000)) {
		IPAddress resolvedIP = MDNS.IP(0);
		if (resolvedIP == ip) {
			hostname = MDNS.hostname(0);
		}
	}

	return hostname;
}

int WifiUtils::numberOfConnectedClients()
{
	wifi_sta_list_t stationList;
	tcpip_adapter_sta_list_t tcpipList;
	String clientInfo = "";

	// Get the list of connected stations
	esp_wifi_ap_get_sta_list(&stationList);
	tcpip_adapter_get_sta_list(&stationList, &tcpipList);

	return stationList.num;
}

String WifiUtils::getAvailableWifiNetworks()
{
	if (WifiUtils::numberOfConnectedClients() == 0 && !WiFi.isConnected())
	{
		String networks = "";
		numberOfAvailableNetworks = WiFi.scanNetworks();
		for (int i = 0; i < numberOfAvailableNetworks; i++)
		{
			networks += WiFi.SSID(i) + "\n";
		}
		WifiUtils::wifiNetworks = networks;
		
	}
	return WifiUtils::wifiNetworks;
}
