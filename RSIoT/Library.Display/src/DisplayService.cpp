/*
 Name:		Library.cpp
 Created:	3/26/2021 9:49:55 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "DisplayService.h"


DisplayService* DisplayService::current = NULL;

DisplayService::DisplayService()
{
	DisplayService:current = this;
}

void DisplayService::initialize(SPIClass* spi)
{
	tft = new Adafruit_ST7735(spi, TFT_CS, TFT_DC, TFT_RST);
	tft->initR(INITR_MINI160x80);
	tft->setRotation(3);
	tft->invertDisplay(1);
	tft->fillScreen(ST7735_ORANGE);
	drawText("RS-IoT", ST77XX_BLACK, 30, 30, 3);
	//drawBitmap(0, 0, gsabLogo, 160, 80, ST77XX_WHITE);
}


void DisplayService::loop()
{
	if (!ConfigurationService::ScreenDebug)
	{
		tft->invertDisplay(0);

		uint16_t textColor = ST77XX_WHITE;
		if (this->mqttConnected)
		{
			tft->fillScreen(ST7735_GREEN);
			textColor = ST77XX_BLACK;
		}
		else
		{
			tft->fillScreen(ST7735_BLUE);
		}
		
		drawText("RS-IoT", textColor, 40, 10, 2);

		int currentInterface = this->currentInterface;
		if (this->interfaceConnected)
		{
			if (currentInterface == INTERFACE_ETHERNET)
			{
				drawText("Ethernet", textColor, 1, 50, 1);

				drawText(this->currentInterfaceDetails, textColor, 1, 60, 1);
			}
			if (currentInterface == INTERFACE_WIFI)
			{
				drawText("WiFi", textColor, 1, 50, 1);

				drawText(this->currentInterfaceDetails, textColor, 1, 60, 1);
			}
			if (currentInterface == INTERFACE_GSM)
			{
				drawText("GSM", textColor, 1, 50, 1);
				
				drawText(this->currentInterfaceDetails, textColor, 1, 60, 1);
			}
			if (this->mqttConnected)
			{
				drawText("MQTT Connected", textColor, 1, 70, 1);
			}
			else
			{
				drawText("MQTT Not Connected", textColor, 1, 70, 1);
			}
		}
		else
		{
			drawText("No connection", textColor, 1, 70, 1);
		}
	}
}

void DisplayService::drawText(String text, uint16_t color, uint16_t xPos, uint16_t yPos, uint16_t size)
{
	DisplayService::tft->setTextWrap(false);
	DisplayService::tft->setTextSize(size);
	DisplayService::tft->setTextColor(color);
	DisplayService::tft->setCursor(xPos,yPos);
	DisplayService::tft->print(text);
}

void DisplayService::drawBitmap(int16_t x, int16_t y, const uint8_t* bitmap, int16_t w, int16_t h, uint16_t color) {

	int16_t i, j, byteWidth = (w + 7) / 8;
	uint8_t byte;

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			if (i & 7) byte <<= 1;
			else      byte = pgm_read_byte(bitmap + j * byteWidth + i / 8);
			if (byte & 0x80) this->tft->drawPixel(x + i, y + j, color);
		}
	}
}

void DisplayService::clearDisplay()
{
	DisplayService::tft->fillScreen(ST77XX_WHITE);
}

void DisplayService::addDebugMessage(String text)
{
		textBuffer[DisplayService::bufferPosition] = text;

		if (ConfigurationService::ScreenDebug) {
			DisplayService::clearDisplay();
			int printPosition = DisplayService::bufferPosition;
			for (int i = 0; i < 10; i++)
			{

				DisplayService::drawText(DisplayService::textBuffer[printPosition], ST7735_ORANGE, 1, (10 - i) * 8, 1);
				printPosition -= 1;

				if (printPosition < 0)
				{
					printPosition = 9;
				}
			}
		}
		DisplayService::bufferPosition += 1;
		if (DisplayService::bufferPosition == 10)
		{
			DisplayService::bufferPosition = 0;
		}
}

void DisplayService::setConnectionStatus(bool mqttConnected, bool interfaceConnected, uint16_t currentInterface, String details)
{
	this->interfaceConnected = interfaceConnected;
	this->mqttConnected = mqttConnected;
	this->currentInterface = currentInterface;
	this->currentInterfaceDetails = details;
}

void DisplayService::setMachineStatus(bool active)
{
}
