/*
 Name:		Library.h
 Created:	3/26/2021 9:49:55 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SPI.h>
#include <ConfigurationService.h>
#include <gsabLogo.h>

#define INTERFACE_ETHERNET		0
#define INTERFACE_WIFI			1
#define INTERFACE_GSM			2
#define INTERFACE_NONE			-1

#define TFT_CS         26
#define TFT_RST        4
#define TFT_DC         32

class DisplayService {
public:
	DisplayService();
	void initialize(SPIClass* spi);
	void loop();
	static DisplayService* current;
	void addDebugMessage(String text);
	void setConnectionStatus(bool mqttConnected, bool interfaceConnected, uint16_t currentInterface, String details); //interface 0:Ethernet,1:WiFi,2:GSM
	void setMachineStatus(bool active);
private:
	Adafruit_ST7735* tft;
	float p = 3.1415926;
	void drawText(String text, uint16_t color, uint16_t xPos, uint16_t yPos, uint16_t size = 1);
	void drawBitmap(int16_t x, int16_t y, const uint8_t* bitmap, int16_t w, int16_t h, uint16_t color);
	void clearDisplay();
	String textBuffer[10];
	int bufferPosition = 0;
	bool interfaceConnected = false;
	bool mqttConnected = false;
	uint16_t currentInterface = -1;
	String currentInterfaceDetails = "Not connected";
};

