#pragma once
#include <ESP32Time.h>
#include <MemoryServiceFAT.h>
#include <MQTTService.h>
#include <DebugService.h>
#include <WString.h>
#include <ArduinoJson.h>
#include <ArduinoQueue.h>
#include <SerialFlash.h>
#include <InputTrigger.h>
#include <MD5Builder.h>
#include <ConfigurationService.h>

class InputService {

public:
	static InputService* current;
	InputService();
	void initialize(ESP32Time* rtcTimer, MQTTService* mqttService, ConnectionService* connectionService);
	void loop(long interruptsEvents[]);
	void handleMessage(String topic, String payload, unsigned int length);
	void saveInputSettings();
	InputTrigger inputTriggers[8];
	
private:
	
	ESP32Time* rtcTimer;
	MQTTService* mqttService;
	ConnectionService* connectionService;
	void saveInputSettings(InputTrigger dataTriggerSettings);
	void loadInputSettings();
	String fileName = "InputTrigger_";
	String fileType = ".json";
	void calculateSettingsHash();
};
