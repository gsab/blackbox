#include "InputTrigger.h"


int InputTrigger::GPIOArray[]{ 36,39,34,35,32,33,25,26 };
ulong interruptCounter = 0;

InputTrigger::InputTrigger()
{
}

InputTrigger::InputTrigger(MQTTService* mqttService, ESP32Time* rtc, int inputID)
{
	this->initialize(mqttService, rtc, inputID, false, 5000, 10000);
}

InputTrigger::InputTrigger(MQTTService* mqttService, ESP32Time* rtc, int inputID, bool isCounterInput, int16_t counterMaxUpdateTime, int16_t filterTime)
{
	this->initialize(mqttService, rtc, inputID, isCounterInput, counterMaxUpdateTime, filterTime);
}

void InputTrigger::checkInputStatus(long interruptEvents)
{

	int currentCheckTime = millis();
	if (filterTime != 0 || !isCounterInput)
	{
		//Read current status of input. The input is inverted
		bool currentStatus = digitalRead(this->GPIOArray[this->inputID]) == LOW;


		//currentStatus = rtc->getMillis() < 500;

		if (currentStatus != this->lastKnownStatus)
		{
			
			this->inputInChangeCounter += TimeUtils::checkTimeDifferenceMillis(this->lastCheckInputStatusMillis, currentCheckTime);
			
		}
		else
		{
			this->inputInChangeCounter = 0;
		}

		if (this->inputInChangeCounter >= this->filterTime && currentStatus != this->lastKnownStatus)
		{
			if (isCounterInput && currentStatus == HIGH)
			{
				addToCounterBuffer(currentCheckTime);
			}
			else if(!isCounterInput)
			{
				this->createEvent(currentStatus,0, this->inputID,0);
			}
			this->lastKnownStatus = currentStatus;
			this->lastChangeMillis = currentCheckTime;
			this->inputInChangeCounter = 0;
		}
		this->lastCheckInputStatusMillis = millis();
	}
	else
	{
		interruptCounter += interruptEvents;
	}

	if(isCounterInput)
		handleCounterEvents(currentCheckTime);
	
}

void InputTrigger::handleCounterEvents(int now)
{
	if (this->filterTime == 0 && this->isCounterInput)
	{
		while (InputTrigger::interruptCounter > 0)
		{
			InputTrigger::interruptCounter--;
			addToCounterBuffer(now);
		}
	}

	if (TimeUtils::checkTimeExpiredMillis(this->lastCounterMessageMillis,now, this->counterMaxUpdateTime))
	{
		if ((this->useBuffer1 && this->counterBuffer1 > 0) || (!this->useBuffer1 && this->counterBuffer2 > 0))
		{
			//Send amounts
			if (useBuffer1)
			{
				useBuffer1 = false;
				mqttService->createEvent(true, this->counterBuffer1, this->inputID, TimeUtils::checkTimeDifferenceMillis(this->firstCycleCounted, this->lastCycleCounted),firstCycleEpoch,firstCycleMillis);
				this->counterBuffer1 = 0;
				this->firstCycleCounted = 0;
			}
			else
			{
				useBuffer1 = true;
				mqttService->createEvent(true, this->counterBuffer2, this->inputID, TimeUtils::checkTimeDifferenceMillis(this->firstCycleCounted, this->lastCycleCounted), firstCycleEpoch, firstCycleMillis);
				this->counterBuffer2 = 0;
				this->firstCycleCounted = 0;
			}

			lastCounterMessageMillis = millis();
		}
	}
}

String InputTrigger::serialize()
{
	DynamicJsonDocument doc(256);
	doc["inputID"] = this->inputID;
	doc["filterTime"] = String(this->filterTime);
	doc["counterMaxUpdateTime"] = this->counterMaxUpdateTime;
	doc["isCounterInput"] = String(this->isCounterInput);
	char buffer[256];
	serializeJson(doc, buffer);
	return String(buffer);
}

void InputTrigger::addToCounterBuffer(int time)
{
	if (this->firstCycleCounted == 0)
	{
		this->firstCycleCounted = time;
		this->firstCycleEpoch = this->rtc->getEpoch();
		this->firstCycleMillis = this->rtc->getMillis();
	}
	this->lastCycleCounted = time;
	if (this->useBuffer1)
	{
		this->counterBuffer1++;
	}
	else
	{
		this->counterBuffer2++;
	}
}

void InputTrigger::initialize(MQTTService* mqttService, ESP32Time* rtc, int8_t inputID, bool isCounterInput, int16_t counterMaxUpdateTime, int16_t filterTime)
{
	this->mqttService = mqttService;
	this->rtc = rtc;
	this->inputID = inputID;
	this->isCounterInput = isCounterInput;
	this->counterMaxUpdateTime = counterMaxUpdateTime;
	this->filterTime = filterTime;
	this->lastKnownStatus = digitalRead(this->GPIOArray[this->inputID]) == LOW;
}

void InputTrigger::createEvent(bool flank, int16_t amount, int8_t inputID, int duration)
{
	mqttService->createEvent(flank, amount, inputID, duration, this->rtc->getEpoch(), this->rtc->getMillis());
}
