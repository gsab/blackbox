#pragma once
#include <stdlib.h>
#include <WString.h>
#include <Arduino.h>
#include <ArduinoJson.h>
#include <FunctionalInterrupt.h>
#include <ESP32Time.h>
#include <MQTTService.h>
#include <TimeUtils.h>
#include <DebugService.h>

class InputTrigger {

public:
	InputTrigger();
	InputTrigger(MQTTService* mqttService, ESP32Time* rtc, int inputID);
	InputTrigger(MQTTService* mqttService, ESP32Time* rtc, int inputID, bool isCounterInput, int16_t counterMaxUpdateTime, int16_t filterTime);
	void checkInputStatus(long interruptEvents);
	int8_t inputID;
	int16_t counterMaxUpdateTime = 5000;
	int16_t filterTime = 200;
	int lastChangeMillis = 0;
	int lastCounterMessageMillis = 0;
	bool lastKnownStatus = false;
	bool isCounterInput = false;
	String serialize();
	volatile ulong interruptCounter = 0;
private:
	void initialize(MQTTService* mqttService, ESP32Time* rtc, int8_t inputID, bool isCounterInput, int16_t counterMaxUpdateTime, int16_t filterTime);
	void handleCounterEvents(int now);
	void createEvent(bool flank, int16_t amount, int8_t inputID, int duration);
	MQTTService* mqttService;
	ESP32Time* rtc;
	static void IRAM_ATTR Input_Low_Interrupt(void *arg);
	void addToCounterBuffer(int time);
	int counterBuffer1 = 0;
	int counterBuffer2 = 0;
	int firstCycleCounted = 0;
	long firstCycleEpoch = 0;
	int firstCycleMillis = 0;
	int lastCycleCounted = 0;
	bool useBuffer1 = true;
	int16_t inputInChangeCounter = 0;
	int lastCheckInputStatusMillis = 0;
	static int GPIOArray[];
};
