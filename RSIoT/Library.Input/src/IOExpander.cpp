#include "IOExpander.h"


void IOExpander::begin(void)
{
	_address = TCA6408_ADDR2; // use default
	Wire.begin();
}

void IOExpander::begin(uint8_t addr)
{
	_address = addr;
	Wire.begin();
}


void IOExpander::writeByte(uint8_t data, uint8_t reg) {
	Wire.beginTransmission(_address);
	I2CWRITE((uint8_t)reg);
	I2CWRITE((uint8_t)data);
	Wire.endTransmission();

	return;
}

bool IOExpander::readByte(uint8_t* data, uint8_t reg) {
	Wire.beginTransmission(_address);
	I2CWRITE((uint8_t)reg);
	Wire.endTransmission();
	uint8_t timeout = 0;

	Wire.requestFrom(_address, (uint8_t)0x01);
	while (Wire.available() < 1) {
		timeout++;
		if (timeout > I2CTIMEOUT) {
			return(true);
		}
		delay(1);
	}

	*data = I2CREAD();
	return(false);
}

uint8_t IOExpander::readInput(uint8_t ch) {
	uint8_t inputState = 255;
	readByte(&inputState, TCA6408_INPUT);
	return(inputState);
}