#include "InputService.h"

InputService* InputService::current = NULL;

InputService::InputService()
{
}

void InputService::initialize(ESP32Time* rtcTimer, MQTTService* mqttService, ConnectionService* connectionService)
{
	this->rtcTimer = rtcTimer;
	this->mqttService = mqttService;
	this->connectionService = connectionService;

	this->loadInputSettings();

	InputService::current = this;
}

void InputService::loop(long interruptsEvents[])
{
	bool currentInputStatus[8];

	for (int i = 0; i < 8;i++)
	{
		inputTriggers[i].checkInputStatus(interruptsEvents[i]);
		currentInputStatus[i] = inputTriggers[i].lastKnownStatus;
	}


	this->mqttService->updateInputStatus(0, 8, currentInputStatus);
}

void InputService::handleMessage(String topic, String payload, unsigned int length)
{
	if (String(topic).endsWith("inputTrigger"))
	{
		StaticJsonDocument<256> doc;
		deserializeJson(doc, payload.c_str(), length);
		if (doc.containsKey("filterTime"))
		{
			DebugService::Information("InputService::handleMessage", "InputTrigger message deserialized");
			int inputID = doc["inputNumber"];

			if (inputID < 9)
			{
				DebugService::Information("InputService::handleMessage", "Getting settings data");
				inputTriggers[inputID].filterTime = doc["filterTime"];
				inputTriggers[inputID].counterMaxUpdateTime = doc["counterMaxUpdateTime"];
				inputTriggers[inputID].isCounterInput = doc["isCounterInput"];

				InputService::saveInputSettings(inputTriggers[inputID]);
				DebugService::Information("InputService::handleMessage", "Settings data saved");
				calculateSettingsHash();
			}
			else
			{
				DebugService::Warning("InputService::handleMessage", "Invalid inputnumber detected! " + String(inputID));
			}
		}
	}
}

void InputService::saveInputSettings()
{
	for (int i = 0;i < 8;i++)
	{
		saveInputSettings(inputTriggers[i]);
	}

	calculateSettingsHash();
}

void InputService::saveInputSettings(InputTrigger dataTriggerSettings)
{
	DebugService::Information("InputService::saveInputSettings", "Saving InputTrigger " + String(dataTriggerSettings.inputID));

	String currentFileName = "/" + this->fileName + String(dataTriggerSettings.inputID) + this->fileType;

	MemoryServiceFAT::saveFile(currentFileName, dataTriggerSettings.serialize());
}

void InputService::loadInputSettings()
{
	for (int i = 0; i < 8;i++)
	{
		String currentFileName = this->fileName + String(i) + this->fileType;

		String fileContent = MemoryServiceFAT::loadFileAsString("/" + currentFileName);

		DebugService::Information("InputService::loadInputSettings", "Loaded file content for input + " + String(i) + " content: " + fileContent);

		if (fileContent == "")
		{
			//No setting file found, create trigger with standard values
			DebugService::Information("InputService::loadInputSettings", "No file found, initializing with standard parameters");
			this->inputTriggers[i] = InputTrigger(mqttService,rtcTimer, i);
		}
		else
		{
			const int size = 1024;
			StaticJsonDocument<size> doc;
			deserializeJson(doc, fileContent.c_str(), size);
			if (doc.containsKey("filterTime"))
			{
				InputTrigger trigger = InputTrigger(mqttService, rtcTimer, i);
				trigger.filterTime = doc["filterTime"];
				trigger.counterMaxUpdateTime = doc["counterMaxUpdateTime"];
				trigger.isCounterInput = doc["isCounterInput"] == "1";

				this->inputTriggers[i] = trigger;
			}
			else {
				//No setting file found or corrupt file, create trigger with standard values
				DebugService::Information("InputService::loadInputSettings", "File could not be deserialized");
				this->inputTriggers[i] = InputTrigger(mqttService, rtcTimer, i);
			}
		}
	}
	calculateSettingsHash();
}

void InputService::calculateSettingsHash()
{
	String totalString = "";
	for (int i = 0; i < 8;i++)
	{
		totalString += String(inputTriggers[i].inputID);
		totalString += String(inputTriggers[i].filterTime);
		totalString += String(inputTriggers[i].counterMaxUpdateTime);
		totalString += String(inputTriggers[i].isCounterInput);
	}

	DebugService::Information("InputService::calculateSettingsHash", totalString);

	MD5Builder md5;
	md5.begin();
	md5.add(totalString);
	md5.calculate();
	ConfigurationService::inputSettingsHash = md5.toString();
	DebugService::Information("InputService::calculateSettingsHash", ConfigurationService::inputSettingsHash);
	
}
