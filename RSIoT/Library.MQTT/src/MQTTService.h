/*
 Name:		Library.h
 Created:	11/20/2020 1:53:43 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <ConnectionService.h>
#include <MemoryService.h>
#include <PubSubClient.h>
#include <ArduinoQueue.h>
#include <WString.h>
#include <ArduinoUniqueID.h>
#include <InputEvent.h>
#include <stdio.h>
#include <UniqueIDService.h>
#include <DebugService.h>
#include <DebugMessage.h>
#include <MD5Builder.h>
#include <ESP32Time.h>
#include <EventPackage.h>
#include <Dictionary.h>
#include <WifiUtils.h>
#include <QueuedMqttMessage.h>
#include <IEventProcessor.h>

#define MQTTService_CALLBACK_SIGNATURE void (*callbackMethod)(String, String, unsigned int)

class MQTTService : public IEventProcessor
{
public:
	MQTTService();
	static MQTTService* current;
	void initialize(ConnectionService* connectionService, ESP32Time rtcClock);
	void disconnect();
	void loop(long currentFreeMemory, long firstStartupDate, String currentVersion, String vibrationAnalyzisStatus);
	bool connected;
	void addEvent(InputEvent inputEvent);
	void timeSynchronized(long timeDifference);
	unsigned int eventsInMQTTQueueCount();
	void handleMessage(String topic, String payload, unsigned int length);
	void setCallback(MQTTService_CALLBACK_SIGNATURE);
	bool sendAccData(const char* payload);
	bool sendAccDataSetting(const char* payload);
	bool connect();
	void resetConnection(int countReset);
	void updateInputStatus(int offset, int amount, bool statusArray[]);
	void sendMessage(String topic, String payload);
	void subscribeTo(String topic);
	void createEvent(bool flank, int16_t amount, int8_t inputID, int duration, long epoch, int millis);
	long reconnectedEpoch = 0;
	void addEnergyReadings(double sensor1Watt, double sensor1Watth, double sensor2Watt, double sensor2Watth, double sensor3Watt, double sensor3Watth);
	void reportPatternFound(int foundCycleTime, String designator, long evaluationStartMillis);
	void sendCommandResponse(String command, String response);
private:
	String usedInstallationNumber;
	String MQTT_Status_Topic;
	String MQTT_Input_Event_Topic;
	String MQTT_Debug_Message_Topic;
	String MQTT_AccData_Topic;
	String MQTT_AccDataSetting_Topic;
	String MQTT_Connect_Topic;
	String MQTT_Configuration_Topic;
	String MQTT_Input_EventPackage_Topic;
	String MQTT_Input_Energy_Topic_Base;
	String MQTT_Cycle_Pattern_Topic_Base;
	String MQTT_Command_Response_Topic_Base;
	static void callback(char* topic, byte* payload, unsigned int length);
	void sendEmeterMessage(double currentWatt, double totalWatt, int inputNumber);
	String MQTT_Base = "rsproduction/rsiot/";
	String MQTT_Server_Base = "rsproduction/server/";
	int publishMQTTCounter;
	long timeCompensation;
	bool timeIsSyncronized;
	unsigned int mqttArrayIndex = 0;
	unsigned int mqttArrayItemCount = 0;
	unsigned int mqttArraySyncIndex = 0;
	int failCounter;
	MQTTService_CALLBACK_SIGNATURE;
	bool firstConnect = true;
	void sendConnectedMessage();
	bool currentInputStatuses[12];
	const size_t MQTT_ARRAY_SIZE = 200000;
	long lastConnectedMillis = 0;
	ESP32Time rtc;
	int verifiedPackageNumber = -1;
	bool useLegacyEvent = false;
	void handleEventPackage();
	void sendEventPackage();
	void verifyEventPackage(int packageID);
	double totalWatthSensor1 = 0;
	double totalWatthSensor2 = 0;
	double totalWatthSensor3 = 0;

	double currentWattSensor1 = 0;
	double currentWattSensor2 = 0;
	double currentWattSensor3 = 0;
	bool emeterValuesChanged = false;
	long lastMemory = 0;
	int lastMqttQueue = 0;
	Dictionary topicPublishLimiter = Dictionary(100);
	EventPackage* activeEventPackage;
	PubSubClient* mqtt;
	ConnectionService* connectionService;
	Client* activeClient;
	SSLClient* sslClient;
	static InputEvent* mqttArray;
	
};