#pragma once
#include <WString.h>
#include <StringUtils.h>

class EventPackage {

public:
	EventPackage(int startIndex, int amount);
	int packageID;
	int startIndex;
	int amount;
	long lastSent;
	static int currentPackageID;
};


