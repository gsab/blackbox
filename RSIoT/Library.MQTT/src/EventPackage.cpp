#include "EventPackage.h"

int EventPackage::currentPackageID = 0;

EventPackage::EventPackage(int startIndex, int amount)
{
	this->startIndex = startIndex;
	this->amount = amount;
	this->packageID = this->currentPackageID;
	this->currentPackageID += 1;

	if (this->currentPackageID > 200000)
		this->currentPackageID = 0;
}
