#pragma once
#include <WString.h>
#include <MemoryService.h>
#include <StringUtils.h>

class QueuedMqttMessage {

public:
	String topic;
	String payload;
};
