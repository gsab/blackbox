#include <InputEvent.h>

InputEvent::InputEvent()
{

}

InputEvent::InputEvent(String data)
{
	this->inputID = StringUtils::getSplitValue(data, ',', 0).toInt();
	this->flank = StringUtils::getSplitValue(data, ',', 1) == "1";
	this->rtcEpoch = stringToLong(StringUtils::getSplitValue(data, ',', 2).c_str());
	this->millis = StringUtils::getSplitValue(data, ',', 3).toInt();
	this->amount = StringUtils::getSplitValue(data, ',', 4).toInt();
	this->duration = StringUtils::getSplitValue(data, ',', 5).toInt();
}

String InputEvent::Serialize()
{
	return (String(this->inputID) + "," + String(this->flank) + "," + String(this->rtcEpoch) +"," + String(this->millis) + "," + String(this->amount) + "," + String(this->duration));
}

InputEvent InputEvent::Copy()
{
	InputEvent copy = InputEvent();
	copy.amount = this->amount;
	copy.duration = this->duration;
	copy.flank = this->flank;
	copy.inputID = this->inputID;
	copy.millis = this->millis;
	copy.rtcEpoch = this->rtcEpoch;
	copy.saved = this->saved;

	return copy;
}

unsigned long InputEvent::stringToLong(String str) {
	long result = 0;
	//DebugService::Information("InputEvent::stringToLong", "String: " + str);
	for (const char& c : str) {
		if (c < '0' || c > '9') {
			// Not a valid digit, you can handle this error as you see fit.
			// For now, we'll just return the current result.
			return result;
		}
		result = (result * 10) + (c - '0');
	}
	//DebugService::Information("InputEvent::stringToLong", "Result: " + String(result));
	return result;
}

bool InputEvent::isValidEvent()
{
	return this->inputID >= 0 && this->inputID <= 12 && this->rtcEpoch > 1720000000 && this->rtcEpoch < 2524657503 && this->millis < 1000 && this->millis >= 0;  // Example condition, adapt based on actual validation needs
}
