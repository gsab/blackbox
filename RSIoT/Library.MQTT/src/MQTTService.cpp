/*
 Name:		Library.cpp
 Created:	11/20/2020 1:53:43 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "MQTTService.h"

MQTTService* MQTTService::current = NULL;

InputEvent* MQTTService::mqttArray = NULL;

MQTTService::MQTTService()
{
	MQTTService::current = this;
}

void MQTTService::initialize(ConnectionService* connectionService, ESP32Time rtcClock)
{
	this->connectionService = connectionService;

	this->rtc = rtcClock;

	MQTTService::mqttArray = (InputEvent*)ps_malloc(MQTT_ARRAY_SIZE * sizeof(InputEvent));

}

void MQTTService::disconnect()
{
	mqtt->disconnect();
}

void MQTTService::loop(long currentFreeMemory, long firstStartupDate, String currentVersion, String vibrationAnalyzisStatus)
{

	if (publishMQTTCounter == 0)
	{
		if (currentFreeMemory != lastMemory || mqttArrayItemCount != lastMqttQueue)
		{
			DebugService::Information("*----- Running status update -----*", "Current free memory " + String(currentFreeMemory) + ", MQTT Queue " + String(mqttArrayItemCount));

			lastMemory = currentFreeMemory;
			lastMqttQueue = mqttArrayItemCount;
		}
	}

	if (ConfigurationService::InstallationNumber != usedInstallationNumber)
	{
		DebugService::Information("MQTTService::connect", "Changing topics after installationnumber has been changed");

		this->MQTT_Status_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Status";

		this->MQTT_Input_Event_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Input_Event";

		this->MQTT_Input_EventPackage_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Input_EventPackage";

		this->MQTT_Debug_Message_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Debug_Message";

		this->MQTT_AccData_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/AccData";

		this->MQTT_AccDataSetting_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/AccDataSetting";

		this->MQTT_Connect_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Connect";

		this->MQTT_Configuration_Topic = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Configuration";

		this->MQTT_Input_Energy_Topic_Base = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/emeter/";

		this->MQTT_Cycle_Pattern_Topic_Base = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/CyclePatternFound";

		this->MQTT_Command_Response_Topic_Base = this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/Command";

		usedInstallationNumber = ConfigurationService::InstallationNumber;
	}

	bool result = connect();

	//if (result != this->connected && result)
	//{
	//	DebugService::Information("MQTTService::loop", "Clearing disk event memory");
	//	//MemoryService::clearEventMemory();
	//	
	//}

	if (result && mqttArrayItemCount == 0 && MemoryService::amountofSavedEvents() > 0)
	{
		DebugService::Information("MQTTService::loop", "Remove events from disk");
		MemoryService::clearEventMemory();
	}

	if (!result && TimeUtils::checkTimeExpiredMillis(lastConnectedMillis, millis(), 30000) && ConfigurationService::configurationWizardComplete)
	{
		//DebugService::Information("MQTTService::loop", "*------------------------- Saving events to disk --------------------------*");

		if (eventsInMQTTQueueCount() > MemoryService::amountofSavedEvents())
		{
			DebugService::Information("MQTTService::loop", "There is " + String(eventsInMQTTQueueCount() - MemoryService::amountofSavedEvents()) + " events to save");
			int amountSaved = 0;
			int maxAmount = 50;
			DebugService::Information("MQTTService::loop", "Getting amounts saved");
			

			while (MemoryService::amountofSavedEvents() < eventsInMQTTQueueCount())
			{
				int nextIndex = mqttArrayIndex - (mqttArrayItemCount - MemoryService::amountofSavedEvents());

				if (nextIndex < 0)
				{
					nextIndex = nextIndex + MQTT_ARRAY_SIZE;
				}

				InputEvent nextEvent = mqttArray[nextIndex];

				MemoryService::saveEvent(nextEvent);
				amountSaved++;

				delay(1);

				if (amountSaved > maxAmount)
				{
					DebugService::Information("MQTTService::loop", "Reached maxAmount. Breaking the loop.");
					break;
				}
			}
		}
	}

	this->connected = result;

	if (this->connected)
	{
		this->lastConnectedMillis = millis();

		mqtt->loop();

		if (this->timeIsSyncronized)
		{
			bool hasPublishedEvents = false;

			if (mqttArrayItemCount > 0)
			{
				int maxAmount = 50;
				//DebugService::Information("publishMQTT", "Publish events to MQTTServer :" + String(mqttArrayItemCount));

				if (useLegacyEvent)
				{
					while (mqttArrayItemCount > 0 && maxAmount > 0)
					{

						hasPublishedEvents = true;
						int nextIndex = mqttArrayIndex - mqttArrayItemCount;

						if (nextIndex < 0)
						{
							nextIndex = nextIndex + MQTT_ARRAY_SIZE;
						}

						InputEvent nextEvent = mqttArray[nextIndex];
						long epochTime = nextEvent.rtcEpoch;

						if (nextIndex < this->mqttArraySyncIndex && !mqttArray[nextIndex].compensated && epochTime < 1695100000)
						{
							//Event that is collected before timesynchonization

							DebugService::Information("publishMQTT", "Compensating time. Time recorded is : " + String(epochTime));
							epochTime += this->timeCompensation;
						}
						else
						{
							DebugService::Information("publishMQTT", "Did not compensate time. Time recorded is : " + String(epochTime));
						}
						String message = String("{\"inputID\" : " + String(nextEvent.inputID) + ", \"flank\" : " + String(nextEvent.flank) + ", \"epoch\" : " + String(epochTime) + ", \"milli\" : " + String(nextEvent.millis) + ", \"duration\" : " + String(nextEvent.duration) + ", \"amount\" : " + String(nextEvent.amount) + "}");
						//DebugService::Information("publishMQTT", MQTT_Input_Event_Topic);
						//DebugService::Information("publishMQTT", message.c_str());
						ConfigurationService::TransmittingData = true;
						if (mqtt->publish(MQTT_Input_Event_Topic.c_str(), message.c_str(), true) && mqtt->connected())
						{
							mqttArrayItemCount--;
						}
						else
						{
							ConfigurationService::TransmittingData = false;
							DebugService::Information("publishMQTT", "Publish of event failed!");
							break;
						}
						ConfigurationService::TransmittingData = false;
						maxAmount--;
						delay(10);
					}
				}
				else
				{
					handleEventPackage();
				}
			}

			int maxSendAmount = 10;
			while (DebugService::debugMessages.itemCount() > 0 )
			{
				DebugMessage dMessage = DebugService::debugMessages.getHead();

				String message = String("{\"level\" : " + String(dMessage.debugLevel) + ", \"time\" : " + String(dMessage.time) + ", \"message\" : \"" + String(dMessage.message) + "\"}");
				ConfigurationService::TransmittingData = true;
				if (mqtt->publish(MQTT_Debug_Message_Topic.c_str(), message.c_str()))
				{
					DebugService::debugMessages.dequeue();
				}
				else
				{
					ConfigurationService::TransmittingData = false;
					break;
				}
				maxSendAmount--;
				if (maxSendAmount == 0)
				{
					
					break;
				}
				ConfigurationService::TransmittingData = false;
			}


			maxSendAmount = 10;

			while (MemoryService::sendLogActive())
			{
				DebugMessage dMessage = MemoryService::getNextLogEntry();

				String message = String("{\"history\" : \"true\", \"level\" : " + String(dMessage.debugLevel) + ", \"time\" : " + String(dMessage.time) + ", \"message\" : \"" + String(dMessage.message) + "\"}");
				ConfigurationService::TransmittingData = true;
				mqtt->publish(MQTT_Debug_Message_Topic.c_str(), message.c_str());
				ConfigurationService::TransmittingData = false;

				maxSendAmount--;
				if (maxSendAmount == 0)
				{
					break;
				}
			}

			
			if (publishMQTTCounter % 10 == 0)
			{
				ConfigurationService::TransmittingData = true;
				sendEmeterMessage(currentWattSensor1, totalWatthSensor1,0);
				sendEmeterMessage(currentWattSensor2, totalWatthSensor2,1);
				sendEmeterMessage(currentWattSensor3, totalWatthSensor3,2);
				ConfigurationService::TransmittingData = false;
			}

			if (publishMQTTCounter == 0)
			{
				//Publish a status update to server

				String ioMessage = "";

				if (mqttArrayItemCount == 0 && !hasPublishedEvents)
				{
					//Eventlist is empty so publish current known status

					ioMessage = "\"io\" : ";

					int bitValue = 1;

					int resultSum = 0;

					for (int i = 0; i < 12; i++)
					{
						if (this->currentInputStatuses[i])
						{
							resultSum += bitValue;
						}

						bitValue = bitValue * 2;
					}

					ioMessage += String(resultSum) + " ,";
				}

				long lastStatusEpoch = rtc.getEpoch();

				if (mqttArrayItemCount > 0)
				{
					//There is cycles in the list. Do not want a statusupdate that is futher away because that can create an illusion of a stoppage

					int nextIndex = mqttArrayIndex - mqttArrayItemCount;

					if (nextIndex < 0)
					{
						nextIndex = nextIndex + MQTT_ARRAY_SIZE;
					}

					InputEvent nextEvent = mqttArray[nextIndex];
					lastStatusEpoch = nextEvent.rtcEpoch;
				}

				String message = String("{\"m\" : " + String(currentFreeMemory) + ",\"s\" : " + String(firstStartupDate) + ",\"mqb\" : " + String(WifiUtils::numberOfConnectedClients()) + ",\"v\" : \"" + currentVersion + "\",\"t\" : " + String(lastStatusEpoch) + "," + ioMessage + " \"c\":\"" + connectionService->getConnectedInterfaceDetails() + "\", \"cal\":\"" + vibrationAnalyzisStatus + "\"}");

				ConfigurationService::TransmittingData = true;
				if (mqtt->publish(MQTT_Status_Topic.c_str(), message.c_str()))
				{

				}
				ConfigurationService::TransmittingData = false;
			}

			
		}
		else
		{
			DebugService::Warning("publishMQTT", "Time is not yet synchronized");
		}
	}

	publishMQTTCounter++;
	if (publishMQTTCounter == 20)
	{
		publishMQTTCounter = 0;
	}

}
void MQTTService::addEvent(InputEvent inputEvent)
{

	mqttArray[mqttArrayIndex] = inputEvent;
	mqttArrayIndex++;
	mqttArrayItemCount++;

	if (mqttArrayIndex >= MQTT_ARRAY_SIZE)
	{
		mqttArrayIndex = 0;
		DebugService::Warning("MQTTService::moveToMqttQueue", "mqttArray cycled over");
	}
	if (mqttArrayItemCount > MQTT_ARRAY_SIZE)
	{
		mqttArrayItemCount = MQTT_ARRAY_SIZE;
		DebugService::Warning("MQTTService::moveToMqttQueue", "mqttArray is overflowing!");
	}
}

void MQTTService::createEvent(bool flank, int16_t amount, int8_t inputID, int duration, long epoch, int millis)
{
	InputEvent newEvent = InputEvent();

	newEvent.flank = flank;
	newEvent.amount = amount;
	newEvent.inputID = inputID;
	newEvent.rtcEpoch = epoch;
	newEvent.millis = millis;
	newEvent.duration = duration;

	this->addEvent(newEvent);
}

void MQTTService::addEnergyReadings(double sensor1Watt, double sensor1Watth, double sensor2Watt, double sensor2Watth, double sensor3Watt, double sensor3Watth)
{
	bool setChangedValues = false;
	if (sensor1Watth > 0 || sensor2Watth > 0 || sensor3Watth > 0 || sensor1Watt != totalWatthSensor1 || sensor2Watt != currentWattSensor2 || sensor3Watt != currentWattSensor3)
	{
		setChangedValues = true;
	}

	this->totalWatthSensor1 += sensor1Watth;
	this->totalWatthSensor2 += sensor2Watth;
	this->totalWatthSensor3 += sensor3Watth;

	this->currentWattSensor1 = sensor1Watt;
	this->currentWattSensor2 = sensor2Watt;
	this->currentWattSensor3 = sensor3Watt;

	if (setChangedValues)
		this->emeterValuesChanged = true;
}

void MQTTService::reportPatternFound(int foundCycleTime, String designator, long evaluationStartMillis)
{
	String message = String("{\"foundCycleTime\" : " + String(foundCycleTime) + ",\"id\" : \"" + designator + "\",\"evaluationTime\" : \"" + String(TimeUtils::checkTimeDifferenceMillis(evaluationStartMillis,millis())) + "\"}");

	sendMessage(MQTT_Cycle_Pattern_Topic_Base, message);
}

void MQTTService::sendCommandResponse(String command, String response)
{
	String message = String("{\"command\" : \"" + command + "\",\"response\" : \"" + response + "\"}");

	sendMessage(MQTT_Command_Response_Topic_Base, message);
}

unsigned int MQTTService::eventsInMQTTQueueCount()
{
	return this->mqttArrayItemCount;
}

void MQTTService::handleMessage(String topic, String payload, unsigned int length)
{
	if (String(topic).endsWith("setconfiguration"))
	{
		ConfigurationService::SetConfiguration(payload);

		MemoryServiceFAT::saveConfiguration();
	}
	if (String(topic).endsWith("getconfiguration"))
	{
		mqtt->publish(MQTT_Configuration_Topic.c_str(), ConfigurationService::SerializeConfig().c_str());
	}
	if (String(topic).endsWith("eventpackageresponce"))
	{
		DebugService::Information("MQTTService::handleMessage", "MQTT Server event package responce packageID = " + payload);

		int packageID = payload.toInt();

		DebugService::Information("MQTTService::handleMessage", "Package ID converted to int");


		verifyEventPackage(payload.toInt());
	}
	if (String(topic).endsWith("serverversion"))
	{
		DebugService::Information("MQTTService::handleMessage", "Serverversion message recieved");
		
	}
}

void MQTTService::setCallback(MQTTService_CALLBACK_SIGNATURE)
{
	MQTTService::callbackMethod = callbackMethod;
}

bool MQTTService::sendAccData(const char* payload)
{
	if (mqtt && mqtt->connected())
	{
		if (mqtt->publish(MQTT_AccData_Topic.c_str(), payload))
		{
			payload = NULL;
			return true;
		}
	}
	payload = NULL;
	return false;
}

bool MQTTService::sendAccDataSetting(const char* payload)
{
	if (mqtt && mqtt->connected())
	{
		DebugService::Information("MQTTService::sendAccDataSetting", "Sending AccDataSetting");
		DebugService::Information("MQTTService::sendAccDataSetting", payload);
		if (mqtt->publish(MQTT_AccDataSetting_Topic.c_str(), payload))
		{
			payload = NULL;
			return true;
		}
	}
	payload = NULL;
	return false;
}

void MQTTService::callback(char* topic, byte* payload, unsigned int length)
{
	
	MQTTService::current->callbackMethod(String(topic), String((char*) payload).substring(0,length), length);
}

void MQTTService::sendEmeterMessage(double currentWatt, double totalWatt, int inputNumber)
{
	if (mqtt && mqtt->connected() && ConfigurationService::newHardware && emeterValuesChanged)
	{
		String message = String(currentWatt);
		if (mqtt->publish((MQTT_Input_Energy_Topic_Base + String(inputNumber) + "/power").c_str(), message.c_str()))
		{
		}

		message = String(totalWatt);
		if (mqtt->publish((MQTT_Input_Energy_Topic_Base + String(inputNumber) + "/total").c_str(), message.c_str()))
		{
		}
	}
}

void MQTTService::timeSynchronized(long timeDifference)
{
	//All events before this index has old time
	this->mqttArraySyncIndex = this->mqttArrayIndex;
	this->timeCompensation = timeDifference;
	this->timeIsSyncronized = true;
}

bool MQTTService::connect()
{
	Client* connectedClient = this->connectionService->getClient();

	if (connectedClient != NULL && connectionService->connected)
	{
		if (!this->mqtt)
		{
			DebugService::Information("MQTTService::connect", "Creating MQTT instance");

			this->mqtt = new PubSubClient;

			this->mqtt->setServer(ConfigurationService::MQTTUrl.c_str(), ConfigurationService::MQTTPort);
			//this->mqtt->
			this->mqtt->setCallback(callback);

			this->mqtt->setBufferSize(1024);
		}

		
		
		if (this->activeClient != connectedClient)
		{
			DebugService::Information("MQTTService::connect", "Changing connected client");
			if (mqtt->connected())
			{
				mqtt->disconnect();
			}
			this->activeClient = connectedClient;
			mqtt->setClient(*connectedClient);

		}

		if (this->connected) {
			mqtt->loop();

			if (mqtt->connected())
			{
				return true;
			}
		}
		else
		{
			if (mqtt->connected())
			{
				DebugService::Information("MQTT_connect", "MQTT was connected even if client was not!");
				mqtt->disconnect();
			}
		}

		String userName = ConfigurationService::MQTTUser;
		String password = ConfigurationService::MQTTPassword;

		if (userName == "")
		{
			userName = "RSIoT" + ConfigurationService::InstallationNumber;
			MD5Builder md5;
			md5.begin();
			md5.add("RSIoTmqttRandomPasswordGenerator" + ConfigurationService::InstallationNumber);
			md5.calculate();
			password = md5.toString();
			DebugService::Information("MQTT_connect", "Setting username and password " + password);
		}

		DebugService::Information("MQTT_connect", "Connecting to " + String(ConfigurationService::MQTTUrl) + " port " + String(ConfigurationService::MQTTPort));
		connectionService->maintain();
		if (connectionService->connected && mqtt->connect(UniqueIDService::getUniqueID().c_str(), userName.c_str(), password.c_str()))
		{
			DebugService::Information("MQTT_connect", "Connected");

			DebugService::MQTTStatus = "Connected to MQTT Server";

			DebugService::Information("MQTT_connect", "Subscribing to topics");

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/learn").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/accDataTrigger").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/inputTrigger").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/getcurrentsettings").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/update").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/setconfiguration").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/getconfiguration").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/eventpackageresponce").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/serverversion").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/article").c_str());

			mqtt->subscribe(String(this->MQTT_Base + ConfigurationService::InstallationNumber + "/" + UniqueIDService::getUniqueID() + "/command").c_str());

			mqtt->subscribe(String(this->MQTT_Server_Base + ConfigurationService::InstallationNumber  + "/time").c_str());

			this->reconnectedEpoch = rtc.getEpoch();

			failCounter = 0;

			this->sendConnectedMessage();

			return true;
		}
		else
		{
			failCounter++;
			DebugService::Warning("MQTT_connect", "Connection failed");
			DebugService::MQTTStatus = "Connection failed";

			mqtt->disconnect();

			if(this->activeClient->connected())
			{
				DebugService::Warning("MQTT_connect", "Client is connected");
				DebugService::Warning("MQTT_connect", "Current writeerror " + String(this->activeClient->getWriteError()));

				this->activeClient->clearWriteError();
				this->activeClient->flush();
			}
			else
			{
				DebugService::Warning("MQTT_connect", "Client is not connected");
				this->activeClient->stop();
			}

			resetConnection(10);
			return false;
		}
	}
	else
	{
		
		////DebugService::Information("MQTTService::connect", "No connected client");
		//if (mqtt && mqtt->connected())
		//{
		//	DebugService::Information("MQTT_connect", "MQTT lost connection because of client lost connection");
		//	mqtt->disconnect();
		//}


		failCounter++;
		resetConnection(1000);
		return false;
	}
}

void MQTTService::resetConnection(int countReset)
{
	if (failCounter >= countReset)
	{
		DebugService::Warning("MQTT_connect", "Connection failed 1000 times, resetting network connection");

		this->connectionService->resetConnection();

		failCounter = 0;
	}
}

void MQTTService::updateInputStatus(int offset, int amount, bool statusArray[])
{
	for (int i = 0; i < amount;i++)
	{
		currentInputStatuses[i+offset] = statusArray[i];

	}
}

void MQTTService::sendConnectedMessage()
{
	String message = String("{\"reset\" : " + String(firstConnect) + ",\"inputHash\" : \"" + ConfigurationService::inputSettingsHash + "\",\"vibrationHash\" : \"" + ConfigurationService::vibrationSettingsHash + "\",\"unitname\" : \"" + ConfigurationService::UnitName + "\",\"hardwareVersion\" : " + (ConfigurationService::newHardware ? "2":"1") + "}");
	ConfigurationService::TransmittingData = true;
	mqtt->publish(MQTT_Connect_Topic.c_str(), message.c_str());
	ConfigurationService::TransmittingData = false;
	this->firstConnect = false;
}

void MQTTService::sendMessage(String topic, String payload)
{
	if (mqtt && connected && activeClient && activeClient->connected())
	{

		if (!topicPublishLimiter.containsKey(topic) || topicPublishLimiter.get_value(topic) < rtc.getEpoch() - ConfigurationService::MQTTBrokerLimiter)
		{
			DebugService::Information("MQTTService::sendMessage", "Publishing topic from external client " + topic);
			ConfigurationService::TransmittingData = true;
			mqtt->publish(topic.c_str(), payload.c_str());
			ConfigurationService::TransmittingData = false;
			topicPublishLimiter.add_entry(topic, rtc.getEpoch());

		}
	}
}

void MQTTService::subscribeTo(String topic)
{
	if (mqtt && connected)
	{
		DebugService::Information("MQTTService::subscribeTo", "Confirming subscribe to topic " + topic);
		mqtt->subscribe(topic.c_str());
	}
}

void MQTTService::handleEventPackage()
{
	if (this->activeEventPackage != NULL && this->verifiedPackageNumber == this->activeEventPackage->packageID)
	{
		//Save last verified epoch so that we can continue from this point if RS IoT loose power while sending old saved data.
		int startIndex = mqttArrayIndex - mqttArrayItemCount;
		int lastindex = startIndex + activeEventPackage->amount - 1;
		long epoch = mqttArray[lastindex].rtcEpoch;

		MemoryService::saveLastEventEpoch(epoch);

		mqttArrayItemCount -= activeEventPackage->amount;
		delete activeEventPackage;
		this->activeEventPackage = NULL;
	}

	if (this->activeEventPackage == NULL)
	{
		if (mqttArrayItemCount > 0)
		{
			int amount = mqttArrayItemCount;
			if (amount > 20)
			{
				amount = 20;
			}
			int startIndex = mqttArrayIndex;

			this->activeEventPackage = new EventPackage(startIndex, amount);

			sendEventPackage();
		}
	}
	else if (TimeUtils::checkTimeExpiredMillis(this->activeEventPackage->lastSent, millis(), 10000))
	{

		DebugService::Warning("MQTTService::handleEventPackage", "Resending event package");
		sendEventPackage();
	}
}

void MQTTService::sendEventPackage()
{
	if (this->activeEventPackage != NULL)
	{
		String message = "{\"id\" : " + String(this->activeEventPackage->packageID) + ",\"events\" : \"";

		int startIndex = mqttArrayIndex - mqttArrayItemCount;
		for (int i = 0; i < this->activeEventPackage->amount; i++)
		{
			int currentIndex = startIndex + i;
			
			if (currentIndex < 0)
			{
				currentIndex = currentIndex + MQTT_ARRAY_SIZE;
			}

			long epochTime = mqttArray[currentIndex].rtcEpoch;

			if (currentIndex < mqttArraySyncIndex && !mqttArray[currentIndex].compensated && epochTime < 1695100000)
			{
				mqttArray[currentIndex].rtcEpoch += this->timeCompensation;
				mqttArray[currentIndex].compensated = true;
			}
			else
			{
				//Timesynchronization is done
				mqttArraySyncIndex = 0;
			}


			//DebugService::Information("MQTTService::sendEventPackage", "Sending cycle index " + String(currentIndex));

			message += mqttArray[currentIndex].Serialize() + ";";
		}
		message += "\"}";

		//DebugService::Information("MQTTService::sendEventPackage", "Sending package " + message);
		ConfigurationService::TransmittingData = true;
		mqtt->publish(MQTT_Input_EventPackage_Topic.c_str(), message.c_str());
		ConfigurationService::TransmittingData = false;
		this->activeEventPackage->lastSent = millis();
	}
}

void MQTTService::verifyEventPackage(int packageID)
{

	DebugService::Information("MQTTService::verifyEventPackage", "Event package verified. Package ID = " + String(packageID));
		
	this->verifiedPackageNumber = packageID;
	
}
