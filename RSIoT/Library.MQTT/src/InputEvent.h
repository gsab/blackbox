#pragma once
#include <WString.h>
#include <StringUtils.h>
#include <IOStatus.h>
#include <DebugService.h>

class InputEvent {

public:
	InputEvent();
	InputEvent(String data);
	int8_t inputID = 0;
	bool flank = false;
	int16_t amount = 0;
	unsigned long rtcEpoch = 0;
	int16_t millis = 0;
	int16_t duration = 0;
	bool saved = false;
	bool compensated = false;
	String Serialize();
	InputEvent Copy();
	unsigned long stringToLong(String str);
	bool isValidEvent();
};

