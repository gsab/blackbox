/*
 Name:		Library.h
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <ESP32Time.h>
#include <MemoryService.h>
#include <MQTTService.h>
#include <DebugService.h>
#include <EnergyDataTrigger.h>
#include <WString.h>
#include <ArduinoJson.h>
#include <ArduinoQueue.h>
#include <MD5Builder.h>

class EnergyService {

public:
	static EnergyService* current;
	void initialize(ESP32Time* rtcTimer, int inputNumberOffset, MQTTService* mqttService, ConnectionService* connectionService);
	void addRecording(double sensor1value, double sensor2value, double sensor3value);
	void loop();
	void createEvent(bool flank, int inputID);
	void handleMessage(String topic, String payload,  int length);
	void getAccTriggerSetting(String payload, int length);
	EnergyDataTrigger energyDataTriggers[3];
	void sendSettingsToServer();
private:
	ESP32Time* rtcTimer;
	MQTTService* mqttService;
	ConnectionService* connectionService;
	void setInputSettings(EnergyDataTrigger dataTriggerSettings);
	void saveInputSettings(EnergyDataTrigger dataTriggerSettings);
	void loadInputSettings();
	void calculateSettingsHash();
	String fileName = "EnergyDataInput_";
	String fileType = ".json";
	int inputNumberOffset = 0;
	int currentPackageID = 0;
	bool firstRun = false;
};