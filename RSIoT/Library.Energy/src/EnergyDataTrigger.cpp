/*
 Name:		Library.cpp
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#include "EnergyDataTrigger.h"



EnergyDataTrigger::EnergyDataTrigger()
{
}

EnergyDataTrigger::EnergyDataTrigger(int inputID)
{
	this->initialize(inputID, 0,0, 0, 0, 0,1,230,false);
}

EnergyDataTrigger::EnergyDataTrigger(int inputID, double  lowerThreshold, double upperThreshold, int onThresholdCount, int offThresholdCount, int multiplier, double powerfactor, double referencevoltage, bool savedLocalNotSynced)
{
	this->initialize(inputID, lowerThreshold, upperThreshold, onThresholdCount, offThresholdCount,multiplier, powerfactor, referencevoltage, savedLocalNotSynced);
}

String EnergyDataTrigger::serialize()
{
	DynamicJsonDocument doc(256);
	doc["inputNumber"] = this->inputID;
	doc["upperThreshold"] = this->upperThreshold;
	doc["lowerThreshold"] = this->lowerThreshold;
	doc["onThresholdCount"] = this->onThresholdCount;
	doc["offThresholdCount"] = this->offThresholdCount;
	doc["multiplier"] = this->multiplier;
	doc["powerfactor"] = this->powerfactor;
	doc["referencevoltage"] = this->referencevoltage;
	doc["savedLocalNotSynced"] = this->savedLocalNotSynced?"True":"False";
	char buffer[256];
	serializeJson(doc, buffer);
	return String(buffer);
}

void EnergyDataTrigger::initialize(int inputID, double lowerThreshold, double upperThreshold, int onThresholdCount, int offThresholdCount, int multiplier, double powerfactor, double referencevoltage, bool savedLocalNotSynced)
{
	this->inputID = inputID;
	this->lowerThreshold = lowerThreshold;
	this->upperThreshold = upperThreshold;
	this->onThresholdCount = onThresholdCount;
	this->offThresholdCount = offThresholdCount;
	this->currentStateCount = onThresholdCount;
	this->powerfactor = powerfactor;
	this->referencevoltage = referencevoltage;
	this->multiplier = multiplier;
}


bool EnergyDataTrigger::addReading(double value)
{
	lastValue = value;

	if (upperThreshold <= 0 || onThresholdCount <= 0 || offThresholdCount <= 0)
	{
		return false;
	}

	bool newState = lastValue >= this->lowerThreshold && lastValue <= this->upperThreshold;
	
	if (newState != this->currentState)
	{
		this->currentStateCount -= 1;
		if (this->currentStateCount <= 0)
		{
			this->currentState = newState;

			if (this->currentState)
			{
				this->currentStateCount = this->offThresholdCount;
			}
			else
			{
				this->currentStateCount = this->onThresholdCount;
			}
			return true;
		}
	}
	else
	{
		if (this->currentState)
		{
			this->currentStateCount = this->offThresholdCount;
		}
		else
		{
			this->currentStateCount = this->onThresholdCount;
		}
	}

	return false; // State did not change
}