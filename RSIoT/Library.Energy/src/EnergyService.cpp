#include "EnergyService.h"

EnergyService* EnergyService::current = NULL;

void EnergyService::initialize(ESP32Time* rtcTimer, int inputNumberOffset, MQTTService* mqttService, ConnectionService* connectionService)
{
	EnergyService::current = this;
	this->rtcTimer = rtcTimer;
	this->mqttService = mqttService;
	this->connectionService = connectionService;
	this->inputNumberOffset = inputNumberOffset;

	this->loadInputSettings();

}

void EnergyService::addRecording(double sensor1value, double sensor2value, double sensor3value)
{
	bool dataChanged = false;

	if (this->energyDataTriggers[0].addReading(sensor1value))
	{
		dataChanged = true;
		createEvent(this->energyDataTriggers[0].currentState, 0 + inputNumberOffset);
	}

	if (this->energyDataTriggers[1].addReading(sensor2value))
	{
		dataChanged = true;
		createEvent(this->energyDataTriggers[1].currentState, 1 + inputNumberOffset);
	}
	if (this->energyDataTriggers[2].addReading(sensor3value))
	{
		dataChanged = true;
		createEvent(this->energyDataTriggers[2].currentState, 2 + inputNumberOffset);
	}

	if (dataChanged || firstRun)
	{
		bool statusArray[3];
		statusArray[0] = this->energyDataTriggers[0].currentState;
		statusArray[1] = this->energyDataTriggers[1].currentState;
		statusArray[2] = this->energyDataTriggers[2].currentState;
		mqttService->updateInputStatus(inputNumberOffset, 3, statusArray);
		firstRun = false;
	}
}


void EnergyService::loop()
{
	
}

void EnergyService::createEvent(bool flank, int inputID)
{
	InputEvent newEvent = InputEvent();
	newEvent.inputID = inputID;
	newEvent.flank = flank;
	newEvent.rtcEpoch = this->rtcTimer->getEpoch();
	this->mqttService->addEvent(newEvent);
	DebugService::Information("EnergyService::createEvent", String(flank) + " event detected on input " + String(inputID));
}

void EnergyService::handleMessage(String topic, String payload, int length)
{
	
	
	if (topic.endsWith("accDataTrigger"))
	{
		DebugService::Information("EnergyService::handleMessage", "callback topic " + topic + " payload " + payload);
		getAccTriggerSetting(payload, length);
	}
	if (topic.endsWith("getcurrentsettings"))
	{
		//sendAccTriggerSetting(payload, length);
		DebugService::Information("EnergyService::handleMessage", "callback topic " + topic + " payload " + payload);
		for (int i = 0; i < 3;i++)
		{
			mqttService->sendAccDataSetting(energyDataTriggers[i].serialize().c_str());
		}
	}
}

void EnergyService::getAccTriggerSetting(String payload, int length)
{
	StaticJsonDocument<512> doc;
	deserializeJson(doc, payload.c_str(), length);
	if (doc.containsKey("multiplier"))
	{
		DebugService::Information("EnergyService::handleMessage", "EnergyDataTrigger message deserialized");
		int inputID = doc["inputNumber"];

		int offsetInputID = inputID - inputNumberOffset;

		if (offsetInputID >= 0 && offsetInputID < 4)
		{
			if(energyDataTriggers[offsetInputID].savedLocalNotSynced == false)
			{ 
				DebugService::Information("EnergyService::getAccTriggerSetting", "Getting settings data");
				energyDataTriggers[offsetInputID].upperThreshold = doc["upperThreshold"];
				energyDataTriggers[offsetInputID].lowerThreshold = doc["lowerThreshold"];
				energyDataTriggers[offsetInputID].onThresholdCount = doc["onThresholdCount"];
				energyDataTriggers[offsetInputID].offThresholdCount = doc["offThresholdCount"];
				energyDataTriggers[offsetInputID].multiplier = doc["multiplier"];
				if (doc.containsKey("powerfactor"))
				{
					DebugService::Information("EnergyService::getAccTriggerSetting", "Found powerfactor key");
					energyDataTriggers[offsetInputID].powerfactor = doc["powerfactor"];
					energyDataTriggers[offsetInputID].referencevoltage = doc["referencevoltage"];
				}

				EnergyService::saveInputSettings(energyDataTriggers[offsetInputID]);
				DebugService::Information("EnergyService::getAccTriggerSetting", "Settings data saved");
			}
			else
			{
				if (energyDataTriggers[offsetInputID].upperThreshold == doc["upperThreshold"] &&
					energyDataTriggers[offsetInputID].lowerThreshold == doc["lowerThreshold"] &&
					energyDataTriggers[offsetInputID].onThresholdCount == doc["onThresholdCount"] &&
					energyDataTriggers[offsetInputID].offThresholdCount == doc["offThresholdCount"] &&
					energyDataTriggers[offsetInputID].multiplier == doc["multiplier"] &&
					(!doc.containsKey("powerfactor") || (energyDataTriggers[offsetInputID].powerfactor == doc["powerfactor"] && energyDataTriggers[offsetInputID].referencevoltage == doc["referencevoltage"])))
				{
					energyDataTriggers[offsetInputID].savedLocalNotSynced = false;
					EnergyService::saveInputSettings(energyDataTriggers[offsetInputID]);
					DebugService::Information("EnergyService::getAccTriggerSetting", "Settings data synced");
				}
				else
				{
					DebugService::Error("EnergyService::getAccTriggerSetting", "Settings data not saved, local settings not synced");
				}
			}
		}
	}
}

void EnergyService::sendSettingsToServer()
{
	for (int i = 0; i < 3; i++)
	{
		mqttService->sendAccDataSetting(energyDataTriggers[i].serialize().c_str());
	}
}


void EnergyService::setInputSettings(EnergyDataTrigger dataTriggerSettings)
{DebugService::Information("EnergyService::setInputSettings", "Setting EnergyDataTrigger " + String(dataTriggerSettings.inputID));
	int offsetInputID = dataTriggerSettings.inputID - inputNumberOffset;
	if (offsetInputID >= 0 && offsetInputID < 3)
	{
		this->energyDataTriggers[offsetInputID] = dataTriggerSettings;
		this->saveInputSettings(dataTriggerSettings);
	}
	
}

void EnergyService::saveInputSettings(EnergyDataTrigger dataTrigger)
{
	DebugService::Information("EnergyService::saveInputSettings", "Saving EnergyDataTrigger " + String(dataTrigger.inputID));
	
	String currentFileName = this->fileName + String(dataTrigger.inputID) + this->fileType;

	MemoryServiceFAT::saveFile(currentFileName, dataTrigger.serialize());

	DebugService::Information("EnergyService::saveInputSettings",MemoryServiceFAT::loadFileAsString(currentFileName));

	calculateSettingsHash();
}

void EnergyService::loadInputSettings()
{
	for (int i = 0; i < 3;i++)
	{
		String currentFileName = this->fileName + String(i + (this->inputNumberOffset)) + this->fileType;

		String fileContent = MemoryServiceFAT::loadFileAsString(currentFileName);

		DebugService::Information("EnergyService::loadInputSettings", "Loaded file content for input + " + String(i + (this->inputNumberOffset)) + " content: " + fileContent);

		if (fileContent == "")
		{
			//No setting file found, create trigger with standard values
			DebugService::Information("EnergyService::loadInputSettings", "No file found, initializing with standard parameters");
			this->energyDataTriggers[i] = EnergyDataTrigger(i + (this->inputNumberOffset));
		}
		else
		{
			const int size = 1024;
			StaticJsonDocument<size> doc;
			deserializeJson(doc, fileContent.c_str(), size);
			if (doc.containsKey("upperThreshold"))
			{
				EnergyDataTrigger trigger = EnergyDataTrigger();
				trigger.inputID = i + this->inputNumberOffset;
				trigger.upperThreshold = doc["upperThreshold"];
				trigger.lowerThreshold = doc["upperThreshold"];
				trigger.onThresholdCount = doc["onThresholdCount"];
				trigger.offThresholdCount = doc["offThresholdCount"];
				trigger.multiplier = doc["multiplier"];
				trigger.savedLocalNotSynced = doc["savedLocalNotSynced"] == "True";
				if (doc.containsKey("powerfactor"))
				{
					trigger.powerfactor = doc["powerfactor"];
					trigger.referencevoltage = doc["referencevoltage"];
				}
				this->energyDataTriggers[i] = trigger;
			}
			else {
				//No setting file found or corrupt file, create trigger with standard values
				DebugService::Information("EnergyService::loadInputSettings", "File could not be deserialized");
				this->energyDataTriggers[i] = EnergyDataTrigger(i + this->inputNumberOffset);
			}
		}
	}
	calculateSettingsHash();
}

void EnergyService::calculateSettingsHash()
{
	String totalString = "";
	totalString += "80.000.0000000000000";
	for (int i = 0; i < 3; i++)
	{
		totalString += String(energyDataTriggers[i].inputID);
		totalString += String(energyDataTriggers[i].lowerThreshold);
		totalString += String(energyDataTriggers[i].upperThreshold);
		totalString += String(energyDataTriggers[i].onThresholdCount);
		totalString += String(energyDataTriggers[i].offThresholdCount);
		totalString += String(energyDataTriggers[i].multiplier);
		totalString += String(energyDataTriggers[i].powerfactor);
		totalString += String(energyDataTriggers[i].referencevoltage);
	}

	DebugService::Information("EnergyService::calculateSettingsHash", totalString);

	MD5Builder md5;
	md5.begin();
	md5.add(totalString);
	md5.calculate();
	ConfigurationService::vibrationSettingsHash = md5.toString();
	DebugService::Information("EnergyService::calculateSettingsHash", ConfigurationService::vibrationSettingsHash);

}
