#pragma once
// An array of known actual vs sensor readings. The order doesn't matter.
// But for more efficiency, it should be sorted by the reading.
struct CalibrationPoint {
	double actual;
	double reading;
};

static CalibrationPoint knownValues100Clamp[] = {
	{0, 0},
	{0, 1},
	{0.15, 1.15},
	{1.76, 1.8},
	{2.05, 2.11},
	{2.39, 2.55},
	{4.75, 4.75},
	{6.94, 7.00},
	{9.48, 9.48},
};

static CalibrationPoint knownValues300Clamp[] = {
	{0, 0},
	{0, 2},
	{0.15, 2.15},
	{1.76, 2.24},
	{2.05, 2.48},
	{2.39, 2.80},
	{4.75, 4.86},
	{6.94, 7.00},
	{9.48, 9.40},
};

static CalibrationPoint knownValues100Clamp4G[] = {
	{0, 1.2},
	{2.04, 2.29},
	{2.47, 2.66},
	{4.94, 5.03},
	{7.21, 7.28},
	{9.48, 9.61}
};

static CalibrationPoint knownValues300Clamp4G[] = {
	{0, 0},
	{0, 3.2},
	{2.04, 3.3},
	{2.47, 3.7},
	{4.94, 5.5},
	{7.21, 7.6},
	{9.48, 9.7}
};

static CalibrationPoint knownValues30Clamp[] = {
	{0, 0},
	{0, 0.25},
	{2.45, 2.75},
	{4.78, 5.25},
	{7.04, 7.86},
	{9.33, 10.4}
};

static CalibrationPoint knownValues30Clamp4G[] = {
	{0, 0},
	{0, 0.3},
	{2.45, 2.75},
	{4.78, 5.25},
	{7.04, 7.86},
	{9.33, 10.4}
};