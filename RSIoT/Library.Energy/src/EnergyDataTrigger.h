/*
 Name:		Library.h
 Created:	11/18/2020 8:18:25 AM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <stdlib.h>
#include <WString.h>
#include <Arduino.h>
#include <ArduinoJson.h>

class EnergyDataTrigger {

public:
	EnergyDataTrigger();
	EnergyDataTrigger(int inputID);
	EnergyDataTrigger(int inputID, double lowerThreshold, double upperThreshold, int onThresholdCount, int offThresholdCount, int multiplier, double powerfactor, double referencevoltage, bool savedLocalNotSynced);
	int inputID;
	double lastValue = 0;
	double lowerThreshold = 0;
	double upperThreshold = 0;
	int onThresholdCount = 0;
	int offThresholdCount = 0;
	int multiplier = 30;
	double powerfactor = 1;
	double referencevoltage = 230;
	bool savedLocalNotSynced = false;
	bool currentState = false;
	bool sensorConnected = false;
	bool addReading(double value);
	String serialize();
private:
	void initialize(int inputID, double lowerThreshold, double upperThreshold, int onThresholdCount, int offThresholdCount, int multiplier, double powerfactor, double referencevoltage, bool savedLocalNotSynced);
	int currentStateCount = 0;
};
