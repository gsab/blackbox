#pragma once
#include <CalibrationPoint.h>

struct CalibrationDataSet {
public:
	CalibrationPoint* data;
	size_t size;
};