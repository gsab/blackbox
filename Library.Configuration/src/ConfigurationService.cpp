/*
 Name:		Library.cpp
 Created:	11/30/2020 1:05:32 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "ConfigurationService.h"

bool ConfigurationService::ethernetActive = false;
bool ConfigurationService::wifiActive = true;
bool ConfigurationService::gsmActive = false;
String ConfigurationService::simPin = "";
String ConfigurationService::wifiSSID = "";
String ConfigurationService::wifiPass = "";
String ConfigurationService::APNUrl = "";
String ConfigurationService::APNUser = "";
String ConfigurationService::APNPassword = "";
String ConfigurationService::MQTTUrl = "";
int ConfigurationService::MQTTPort = 1883;
String ConfigurationService::MQTTUser = "";
String ConfigurationService::MQTTPassword = "";
bool ConfigurationService::Reset = false;

void ConfigurationService::LoadConfig(String data)
{
	StaticJsonDocument<512> doc;
	deserializeJson(doc, data.c_str(), data.length());
	if (doc.containsKey("ethernetActive"))
	{
		DebugService::Information("Configuration::LoadConfig", "Deserialized configuration");
		ConfigurationService::ethernetActive = doc["ethernetActive"].as<String>() == "True";
		ConfigurationService::wifiActive = doc["wifiActive"].as<String>() == "True";
		ConfigurationService::gsmActive = doc["gsmActive"].as<String>() == "True";
		ConfigurationService::simPin = doc["simPin"].as<String>();
		ConfigurationService::wifiSSID = doc["wifiSSID"].as<String>();
		ConfigurationService::wifiPass = doc["wifiPass"].as<String>();
		ConfigurationService::APNUrl = doc["APNUrl"].as<String>();
		ConfigurationService::APNUser = doc["APNUser"].as<String>();
		ConfigurationService::APNPassword = doc["APNPassword"].as<String>();
		ConfigurationService::MQTTUrl = doc["MQTTUrl"].as<String>();
		ConfigurationService::MQTTPort = (doc["MQTTPort"].as<String>()).toInt();
		ConfigurationService::MQTTPassword = doc["MQTTPassword"].as<String>();
		DebugService::Information("Configuration::LoadConfig", data);

	}
}
String ConfigurationService::SerializeConfig()
{
	StaticJsonDocument<512> doc;
	DebugService::Information("Configuration::LoadConfig", "Serialized configuration");
	doc["ethernetActive"] = ConfigurationService::ethernetActive ? "True" : "False";
	doc["wifiActive"] = ConfigurationService::wifiActive ? "True" : "False";
	doc["gsmActive"] = ConfigurationService::gsmActive ? "True" : "False";
	doc["simPin"] = ConfigurationService::simPin;
	doc["wifiSSID"] = ConfigurationService::wifiSSID;
	doc["wifiPass"] = ConfigurationService::wifiPass;
	doc["APNUrl"] = ConfigurationService::APNUrl;
	doc["APNUser"] = ConfigurationService::APNUser;
	doc["APNPassword"] = ConfigurationService::APNPassword;
	doc["MQTTUrl"] = ConfigurationService::MQTTUrl;
	doc["MQTTPort"] = ConfigurationService::MQTTPort;
	doc["MQTTUser"] = ConfigurationService::MQTTUser;
	doc["MQTTPassword"] = ConfigurationService::MQTTPassword;

	char buffer[512];
	serializeJson(doc, buffer, 512);
	DebugService::Information("Configuration::LoadConfig", String(buffer));
	return String(buffer);
}

