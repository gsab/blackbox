/*
 Name:		Library.h
 Created:	11/30/2020 1:05:32 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/
#pragma once
#include <WString.h>
#include <Arduino.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <DebugService.h>

class ConfigurationService {
public:
	static bool ethernetActive;
	static bool wifiActive;
	static bool gsmActive;
	static String simPin;
	static String wifiSSID;
	static String wifiPass;
	static String APNUrl;
	static String APNUser;
	static String APNPassword;
	static String MQTTUrl;
	static int MQTTPort;
	static String MQTTUser;
	static String MQTTPassword;
	static void LoadConfig(String data);
	static String SerializeConfig();
	static bool Reset;
};
