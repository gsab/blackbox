#include "pch.h"
#include "CppUnitTest.h"
#include <src\AccDataTrigger.h>

#include <src\DebugService.h>
#include <src\StringUtils.h>
#include <src\InputEvent.h>
#include <src\MemoryService.h>
#include <src\MQTTService.h>
#include <src\ConnectionService.h>
#include <src\VibrationService.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LibraryVibrationTest
{
	TEST_CLASS(LibraryVibrationTest)
	{
	public:
		
		TEST_METHOD(TestAccDataTrigger)
		{
			AccDataTrigger trigger(1, 0.2, 2, 2, 10, true, true, true, true, true);

			bool result1 = trigger.addReading(0.1, 0.0, 0.1);

			bool result2 = trigger.addReading(0.2, 0.2, 0.3);

			Assert::IsTrue(!result1 && result2);
		}
	};
}
