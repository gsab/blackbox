/*
 Name:		Library.cpp
 Created:	11/25/2020 1:20:38 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#include "WebServerService.h"

void WebServerService::initialize(ConnectionService* connectionService, MemoryService* memoryService)
{
	this->connectionService = connectionService;
	this->memoryService = memoryService;


	this->ethernetServer.begin();
	this->wifiServer.begin();
}

void WebServerService::loop()
{
	EthernetClient ethernetClientRequest = ethernetServer.available();

	if (ethernetClientRequest)
	{
		handleRequest(&ethernetClientRequest);
	}

	WiFiClient wifiClientRequest = wifiServer.available();

	if (wifiClientRequest)
	{

		DebugService::Information("WebServerService::loop", "--------------Client request!-------------");
		handleRequest(&wifiClientRequest);
	}

	if (WiFi.status() == WL_CONNECTED && wifiServer.status() != 1)
	{
		DebugService::Information("WebServerService::loop", "Wifiserver status " + String(wifiServer.status()));
		wifiServer.begin();
	}

}

void WebServerService::handleRequest(Client* client)
{
	Serial.println("new client");

	// an http request ends with a blank line

	bool currentLineIsBlank = true;

	String readString = "";

	while (client->connected()) {
		if (client->available()) {
			char c = client->read();

			Serial.write(c);

			readString += c;

			// if you've gotten to the end of the line (received a newline

			// character) and the line is blank, the http request has ended,

			// so you can send a reply

			if (c == '\n' && currentLineIsBlank) {

				// send a standard http response header

				if (readString.indexOf("/saveconfiguration") > 0)
				{
					Serial.print(F("POST data: "));

					String postData = "";
					while (client->available())
					{
						postData += (char)client->read();

					}
					if (postData != "")
					{
						setConfiguration(postData);

						memoryService->saveConfiguration();

						Serial.println(postData);
					}
				}
				else if (readString.indexOf("/reset") > 0)
				{
					ConfigurationService::Reset = true;
				}
				client->println("HTTP/1.1 200 OK");

				client->println("Content-Type: text/html");

				client->println("Connection: close");  // the connection will be closed after completion of the response

				client->println();

				client->println("<!DOCTYPE HTML>");

				client->println("<html>");

				client->println("<body>");
				client->println("<table style = \"height: 96px; width: 100%; border-collapse: collapse; background-color: black;\" border = \"0\">");
				client->println("<tbody>");
				client->println("<tr style = \"height: 78px;\">");
				client->println("<td style = \"width: 100%; text-align: center; height: 78px;\">");
				client->println("<p></p>");
				client->println("<h1><span style = \"color: #ffffff;\">RSBlackBox v2</span></h1>");
				client->println("</td>");
				client->println("</tr>");
				client->println("</tbody>");
				client->println("</table>");
				client->println("<table style = \"width: 100%; border-collapse: collapse; background-color: orange;\" border = \"0\">");
				client->println("<tbody>");
				client->println("<tr>");
				client->println("<td style = \"width: 25%;\"></td>");
				client->println("<td style = \"width: 50%;\">");
				client->println("<form action = \"/saveconfiguration\" method = \"post\" target = \"_self\">");
				client->println("<h2>Connection interfaces</h2>");
				client->println("<h3>Ethernet</h3>");
				client->println("<label for = \"ethenable\">Enabled : </label>");
				client->println("<input type = \"checkbox\" id=\"ethernetActive\" name=\"ethernetActive\" value=\"True\" " + String(ConfigurationService::ethernetActive ? "checked" : "") + "><br><br>");
				client->println("<h3>Wifi</h3>");
				client->println("<label for = \"wifienable\">Enabled : </label>");
				client->println("<input type = \"checkbox\" id=\"wifiActive\" name=\"wifiActive\" value=\"True\"" + String(ConfigurationService::wifiActive ? "checked" : "") + "><br><br>");
				client->println("<label for = \"wifiSSID\">SSID : </label>");
				client->println("<input type = \"text\" id = \"wifiSSID\" name = \"wifiSSID\" value=\"" + ConfigurationService::wifiSSID + "\"><br><br>");
				client->println("<label for = \"wifiPass\">Wifi key : </label>");
				client->println("<input type = \"password\" id = \"wifiPass\" name = \"wifiPass\" value=\"" + ConfigurationService::wifiPass + "\"><br><br>");
				client->println("<h3>GSM</h3>");
				client->println("<label for = \"gsmenable\">Enabled : </label>");
				client->println("<input type = \"checkbox\" id=\"gsmActive\" name=\"gsmActive\" value=\"True\"" + String(ConfigurationService::gsmActive ? "checked" : "") + "><br><br>");
				client->println("<label for = \"simpin\">SIM Pin : </label>");
				client->println("<input type = \"text\" id = \"simPin\" name = \"simPin\" value=\"" + ConfigurationService::simPin + "\"><br><br>");
				client->println("<label for = \"apnurl\">APN Url : </label>");
				client->println("<input type = \"text\" id = \"APNUrl\" name = \"APNUrl\" value=\"" + ConfigurationService::APNUrl + "\"><br><br>");
				client->println("<label for = \"apnuser\">APN User : </label>");
				client->println("<input type = \"text\" id = \"APNUser\" name = \"APNUser\" value=\"" + ConfigurationService::APNUser + "\"><br><br>");
				client->println("<label for = \"apnpass\">APN Password : </label>");
				client->println("<input type = \"text\" id = \"APNPassword\" name = \"APNPassword\" value=\"" + ConfigurationService::APNPassword + "\"><br><br>");

				client->println("<h2>MQTT Settings</h2>");
				client->println("<label for = \"mqtturl\">MQTTServer URL : </label>");
				client->println("<input type = \"text\" id = \"MQTTUrl\" name = \"MQTTUrl\" value=\"" + ConfigurationService::MQTTUrl + "\"><br><br>");
				client->println("<label for = \"mqttport\">MQTTServer Port : </label>");
				client->println("<input type = \"text\" id = \"MQTTPort\" name = \"MQTTPort\" value=\"" + String(ConfigurationService::MQTTPort) + "\"><br><br>");
				client->println("<label for = \"mqtturl\">MQTTServer User : </label>");
				client->println("<input type = \"text\" id = \"MQTTUser\" name = \"MQTTUser\" value=\"" + ConfigurationService::MQTTUser + "\"><br><br>");
				client->println("<label for = \"mqttport\">MQTTServer Password : </label>");
				client->println("<input type = \"text\" id = \"MQTTPassword\" name = \"MQTTPassword\" value=\"" + ConfigurationService::MQTTPassword + "\"><br><br>");

				client->println("<input type = \"submit\" value = \"Save configuration\">");
				client->println("</form><br>");
				client->println("<p></p>");

				client->println("<form action = \"/reset\" method = \"post\" target = \"_self\">");
				client->println("<input type = \"submit\" value = \"Reset device\">");
				client->println("</form>");
				client->println("<p></p>");
				client->println("<p></p>");
				client->println("<p></p>");
				client->println("</td>");
				client->println("<td style = \"width: 25%; height: 467px;\"></td>");
				client->println("</tr>");
				client->println("</tbody>");
				client->println("</table>");

				client->println("</body>");

				client->println("</html>");

				break;

			}

			if (c == '\n') {

				// you're starting a new line

				currentLineIsBlank = true;

			}
			else if (c != '\r') {

				// you've gotten a character on the current line

				currentLineIsBlank = false;

			}

		}

	}

	// give the web browser time to receive the data

	delay(1);

	// close the connection:

	client->stop();

	Serial.println("client disonnected");


}


void WebServerService::setConfiguration(String postData)
{
	String ethernetActive = parseData("ethernetActive=", postData);
	if (ethernetActive != "�NoData�")
	{
		ConfigurationService::ethernetActive = ethernetActive == "True";
	}
	String wifiActive = parseData("wifiActive=", postData);
	if (wifiActive != "�NoData�")
	{
		ConfigurationService::wifiActive = wifiActive == "True";
	}
	String gsmActive = parseData("gsmActive=", postData);
	if (gsmActive != "�NoData�")
	{
		ConfigurationService::gsmActive = gsmActive == "True";
	}
	String wifiSSID = parseData("wifiSSID=", postData);
	if (wifiSSID != "�NoData�")
	{
		ConfigurationService::wifiSSID = wifiSSID;
	}
	String wifiPass = parseData("wifiPass=", postData);
	if (wifiPass != "�NoData�")
	{
		ConfigurationService::wifiPass = wifiPass;
	}
	String APNUrl = parseData("APNUrl=", postData);
	if (APNUrl != "�NoData�")
	{
		ConfigurationService::APNUrl = APNUrl;
	}
	String APNUser = parseData("APNUser=", postData);
	if (APNUser != "�NoData�")
	{
		ConfigurationService::APNUser = APNUser;
	}
	String APNPassword = parseData("APNPassword=", postData);
	if (APNPassword != "�NoData�")
	{
		ConfigurationService::APNPassword = APNPassword;
	}
	String MQTTUrl = parseData("MQTTUrl=", postData);
	if (MQTTUrl != "�NoData�")
	{
		ConfigurationService::MQTTUrl = MQTTUrl;
	}
	String MQTTPort = parseData("MQTTPort=", postData);
	if (MQTTPort != "�NoData�")
	{
		ConfigurationService::MQTTPort = MQTTPort.toInt();
	}
	String MQTTUser = parseData("MQTTUser=", postData);
	if (MQTTUser != "�NoData�")
	{
		ConfigurationService::MQTTUser = MQTTUser;
	}
	String MQTTPassword = parseData("MQTTPassword=", postData);
	if (MQTTPassword != "�NoData�")
	{
		ConfigurationService::MQTTPassword = MQTTPassword;
	}
}

String WebServerService::parseData(String searchString, String postData)
{
	int index = postData.indexOf(searchString);

	if (index >= 0)
	{
		String data = "";

		int currentIndex = index + searchString.length();

		while (currentIndex < postData.length())
		{
			char nextChar = postData[currentIndex];

			if (nextChar == '&')
			{
				break;
			}

			data += nextChar;

			currentIndex++;
		}
		return data;
	}
	return "�NoData�";
}
