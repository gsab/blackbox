/*
 Name:		Library.h
 Created:	11/25/2020 1:20:38 PM
 Author:	david
 Editor:	http://www.visualmicro.com
*/

#pragma once
#include <WString.h>
#include <ConnectionService.h>
#include <MemoryService.h>
#include <EthernetENC.h>
#include <WiFiNINA.h>
#include <DebugService.h>
#include <ConfigurationService.h>

class WebServerService {
public:
	void initialize(ConnectionService* connectionService, MemoryService* memoryService);
	void loop();
private:
	EthernetServer ethernetServer = EthernetServer(80);
	WiFiServer wifiServer = WiFiServer(80);
	ConnectionService* connectionService;
	MemoryService* memoryService;
	void handleRequest(Client* client);
	String parseData(String searchString, String postData);
	void setConfiguration(String postData);
};

