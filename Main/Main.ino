#include <uEEPROMLib.h>

#include <ArduinoUniqueID.h>

#include <SPI.h>
#include <WiFiNINA.h>


const int SW1 = 1;
const int SW2 = 0;

bool SW1_LAST_STATUS = false;
bool SW2_LAST_STATUS = false;

const int PWR_LED = 13;
const int ERROR_LED = 10;
const int WIFI_LED = 11;
const int BLT_LED = 12;

const int D0_INPUT = 2;
const int D1_INPUT = 3;
const int D2_INPUT = 4;
const int D3_INPUT = 5;
const int D4_INPUT = 6;
const int D5_INPUT = 7;
const int D6_INPUT = 8;
const int D7_INPUT = 9;

const int LED_OFF = 0;
const int LED_ON = 1;
const int LED_SLOWBLINK = 2;
const int LED_FASTBLINK = 3;

int PWR_LED_STATUS = 0;
int ERROR_LED_STATUS = 0;
int WIFI_LED_STATUS = 0;
int BLT_LED_STATUS = 0;

uEEPROMLib eeprom(0x50);

///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = "Lyckelid";        // your network SSID (name)
char pass[] = "lyckelid6";    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key Index number (needed only for WEP)

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(2500);
  Serial.println("Delay OK");
  PWR_LED_STATUS = LED_SLOWBLINK;
  ERROR_LED_STATUS = LED_OFF;
  BLT_LED_STATUS = LED_OFF;
  WIFI_LED_STATUS = LED_OFF;
  pinMode(SW1, INPUT_PULLUP);
  pinMode(SW2, INPUT_PULLUP);
  
  pinMode(PWR_LED, OUTPUT);
  pinMode(ERROR_LED, OUTPUT);
  pinMode(WIFI_LED, OUTPUT);
  pinMode(BLT_LED, OUTPUT);

  pinMode(D0_INPUT, INPUT_PULLUP);
  pinMode(D1_INPUT, INPUT_PULLUP);
  pinMode(D2_INPUT, INPUT_PULLUP);
  pinMode(D3_INPUT, INPUT_PULLUP);
  pinMode(D4_INPUT, INPUT_PULLUP);
  pinMode(D5_INPUT, INPUT_PULLUP);
  pinMode(D6_INPUT, INPUT_PULLUP);
  pinMode(D7_INPUT, INPUT_PULLUP);
}

int cpuCycleCounter = 0;
bool outputSet = false;

bool configured = false;

bool noWifiMode = false;

bool wifiConnected = false;

void loop() 
{
  //Check if configured and WifiMode
  if(configured)
  {
    PWR_LED_STATUS = LED_ON;
    BLT_LED_STATUS = LED_OFF;
  }
  
  if(configured and not noWifiMode)
  {
    if(not wifiConnected)
    {
      //Connect to wifi network
      
      connectWifi();
    }
    else
    {
      //Verify wificonnection
      //Send input events and status to server
      if(cpuCycleCounter % 10000000 == 0)
      {
        sendCollectedInputEventsAndStatus();
      }
      
    }
  }
  if(not configured)
  {
    PWR_LED_STATUS = LED_SLOWBLINK;
    WIFI_LED_STATUS = LED_OFF;
    
    if(wifiConnected)
    {
      disconnectWifi();
    }
    //Activate bluetooth
    BLT_LED_STATUS = LED_FASTBLINK;
  }

  readSWInputs();
  endOfCpuCycle();
}

void readSWInputs()
{
  if(digitalRead(SW1) == LOW)
  {
    if(not SW1_LAST_STATUS)
    {
      configured = not configured;
      SW1_LAST_STATUS = true;
    }
  }
  else
  {
    SW1_LAST_STATUS = false;
  }
}

void disconnectWifi()
{
  WIFI_LED_STATUS = LED_OFF;
}

int wifiConnectCycle = 0;
bool wifiStartConnect = false;

void connectWifi()
{
  if(not wifiConnected and (not wifiStartConnect or abs(wifiConnectCycle - cpuCycleCounter) > 1000000))
  {
    WIFI_LED_STATUS = LED_FASTBLINK;
    WiFi.begin(ssid, pass);
    delay(10);
    wifiStartConnect = true;
  }
  else if(not wifiConnected and cpuCycleCounter % 100000 == 0)
  {
    if(WiFi.status() == WL_CONNECTED)
    {
      wifiConnected = true;
      wifiStartConnect = false;
      WIFI_LED_STATUS = LED_ON;
    }
  }
  else if(wifiConnected and cpuCycleCounter % 100000 == 0)
  {
    if(WiFi.status() != WL_CONNECTED)
    {
      wifiConnected = false;
    }
  }
  else if(wifiStartConnect)
  {
    delay(10);
  }
}

void sendCollectedInputEventsAndStatus()
{
  
}

void endOfCpuCycle()
{
  //Update and set LEDs
  setLEDOutputs();

  //Output data to usb-connected computer
  outputSerialData();

  //Update CpuCounter used to schedule functions
  updateCpuCounter();
}

void outputSerialData()
{
  if(cpuCycleCounter % 100000 == 0)
  {
    Serial.println(getUniqueID());
  }
}

void updateCpuCounter()
{
  cpuCycleCounter += 1;

  if(cpuCycleCounter > 100000000)
  {
    cpuCycleCounter = 0;
  }
}

void setLEDOutputs()
{
  setLEDOutput(PWR_LED, PWR_LED_STATUS);
  setLEDOutput(ERROR_LED, ERROR_LED_STATUS);
  setLEDOutput(WIFI_LED, WIFI_LED_STATUS);
  setLEDOutput(BLT_LED, BLT_LED_STATUS);
}

void setLEDOutput(int ledID, int ledStatus)
{
  if(ledStatus == LED_OFF and digitalRead(ledID) == HIGH)
  {
    digitalWrite(ledID, LOW);
  }
  else if(ledStatus == LED_ON and digitalRead(ledID) == LOW)
  {
    digitalWrite(ledID, HIGH);
  }
  else if(ledStatus == LED_SLOWBLINK and cpuCycleCounter % 50000 == 0)
  {
    if(digitalRead(ledID) == HIGH)
    {
      digitalWrite(ledID, LOW);
    }
    else
    {
      digitalWrite(ledID, HIGH);
    }
  }
  else if(ledStatus == LED_FASTBLINK and cpuCycleCounter % 20000 == 0)
  {
    if(digitalRead(ledID) == HIGH)
    {
      digitalWrite(ledID, LOW);
    }
    else
    {
      digitalWrite(ledID, HIGH);
    }
  }
}

String getUniqueID()
{
  String result = "ID";
  for (size_t i = 0; i < UniqueIDsize; i++)
  {                                        
      if (UniqueID[i] < 0x10)
      {
        result += "0";
      }
      result += String(UniqueID[i], HEX);
    }
  return result;
}
