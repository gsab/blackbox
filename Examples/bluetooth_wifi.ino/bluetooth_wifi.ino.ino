/*
  Web client

 This sketch connects to a website (http://www.google.com)
 using the WiFi module.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 Circuit:
 * Board with NINA module (Arduino MKR WiFi 1010, MKR VIDOR 4000 and UNO WiFi Rev.2)

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include <WiFiNINA.h>

///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = "Lyckelid";        // your network SSID (name)
char pass[] = "lyckelid6";    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key Index number (needed only for WEP)

int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)
char server[] = "test001.rsproduction.se";    // name address for Google (using DNS)

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);

  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);

  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    sleepBlink(5, 3);
  }
  digitalWrite(3,HIGH);
  Serial.println("Connected to wifi");
  printWifiStatus();
  
}
int lastValueD0 = 900;
int lastValueD1 = 900;
int lastValueD2 = 900;

void sleepBlink(int amount, int output)
{
  while(amount > 0)
  {
    if(digitalRead(output) == LOW)
    {
      digitalWrite(output, HIGH);
    }
    else
    {
      digitalWrite(output, LOW);
    }
    delay(1000);
    amount = amount - 1;
  }
}

int checkEvent(int lastValue, int sensorID)
{
  int sensorVal = digitalRead(sensorID);
  
  if (sensorVal != lastValue)
  {
    lastValue = sensorVal;
    if (sensorVal == HIGH) 
    {
      return 1;
    } 
    else 
    {
      return 2;
    }
  }
  return 0;
}

bool readInputStatus(int sensorID, int outputID)
{
  int sensorVal = digitalRead(sensorID);

  if(sensorVal == HIGH){
    digitalWrite(outputID, LOW);
    return false;
  }
  digitalWrite(outputID, HIGH);
  return true;
}

int loopCounter = 0;

void loop() {
  while (client.available()) {
    char c = client.read();
    Serial.write(c);
  }
  bool d0Status = readInputStatus(7, 2);
  bool d1Status = readInputStatus(10, 5);
  bool d2Status = readInputStatus(9, 4);

  int d0Flank = checkEvent(lastValueD0, 7);
  int d1Flank = checkEvent(lastValueD1, 10);
  int d2Flank = checkEvent(lastValueD2, 9);

  if(d0Flank == 1){
    lastValueD0 = HIGH;
    Serial.println("Downflank on D0");
    sendFlankMessage(0, false);
  }
  if(d0Flank == 2){
    lastValueD0 = LOW;
    Serial.println("Upflank on D0");
    sendFlankMessage(0, true);
  }
  if(d1Flank == 1){
    lastValueD1 = HIGH;
    Serial.println("Downflank on D1");
    sendFlankMessage(1, false);
  }
  if(d1Flank == 2){
    lastValueD1 = LOW;
    Serial.println("Upflank on D1");
    sendFlankMessage(1, true);
  }
  if(d2Flank == 1){
    lastValueD2 = HIGH;
    Serial.println("Downflank on D2");
    sendFlankMessage(2, false);
  }
  if(d2Flank == 2){
    lastValueD2 = LOW;
    Serial.println("Upflank on D2");
    sendFlankMessage(2, true);
  }
  delay(20);
  loopCounter = loopCounter + 1;
  if(loopCounter == 50)
  {
    sendStatusMessage(d0Status, d1Status, d2Status);
    loopCounter = 0;
  }
}

void sendStatusMessage(bool d0Status, bool d1Status, bool d2Status)
{
  int responce = client.connect(server, 80);
  Serial.println("\nSending status message to server...");
  if (responce == 1) {
    client.println("POST /200512/api/webhook/ HTTP/1.1");
    Serial.println("POST /200512/api/webhook/ HTTP/1.1");
    client.println("Authorization: Bearer a46b6c84-92d1-4ee8-b5e8-6dfd80407e95");
    Serial.println("Authorization: Bearer a46b6c84-92d1-4ee8-b5e8-6dfd80407e95");
    client.println("Host: test001.rsproduction.se");
    Serial.println("Host: test001.rsproduction.se");
    client.println("Content-Type: application/json");
    Serial.println("Content-Type: application/json");
    client.print("Content-Length: ");
    Serial.print("Content-Length: ");
    client.println("73");
    Serial.println("73");
    client.println();
    Serial.println();
    client.println("{\"webhookId\": \"4d40be7b-c925-4ecf-a3d7-bd4a6b278962\",");
    Serial.println("{\"webhookId\": \"4d40be7b-c925-4ecf-a3d7-bd4a6b278962\",");
    client.print("\"status\": \"");
    Serial.print("\"status\": \"");
    if(d0Status)
    {
      client.print("1,");
      Serial.print("1,");
    }
    else
    {
      client.print("0,");
      Serial.print("0,");
    }
    if(d1Status)
    {
      client.print("1,");
      Serial.print("1,");
    }
    else
    {
      client.print("0,");
      Serial.print("0,");
    }
    if(d2Status)
    {
      client.println("1\"}");
      Serial.println("1\"}");
    }
    else
    {
      client.println("0\"}");
      Serial.println("0\"}");
    }
  }
  else
  {
    Serial.println("Connection failed");
    Serial.println(responce);
  }
}

void sendFlankMessage(int inputID, bool upFlank)
{
  int responce = client.connect(server, 80);
  Serial.println("\nSending status message to server...");
  if (responce == 1) {
    client.println("POST /200512/api/webhook/ HTTP/1.1");
    Serial.println("POST /200512/api/webhook/ HTTP/1.1");
    client.println("Authorization: Bearer a46b6c84-92d1-4ee8-b5e8-6dfd80407e95");
    Serial.println("Authorization: Bearer a46b6c84-92d1-4ee8-b5e8-6dfd80407e95");
    client.println("Host: test001.rsproduction.se");
    Serial.println("Host: test001.rsproduction.se");
    client.println("Content-Type: application/json");
    Serial.println("Content-Type: application/json");
    client.print("Content-Length: ");
    Serial.print("Content-Length: ");
    client.println("84");
    Serial.println("84");
    client.println();
    Serial.println();
    client.println("{\"webhookId\": \"3baff1cf-dfae-40f5-a991-a4e9c687d03a\",");
    Serial.println("{\"webhookId\": \"3baff1cf-dfae-40f5-a991-a4e9c687d03a\",");
    client.print("\"flank\": \"");
    Serial.print("\"flank\": \"");
    if(upFlank)
    {
      client.println("1\",");
      Serial.println("1\",");
    }
    else
    {
      client.println("0\",");
      Serial.println("0\",");
    }
    client.print("\"inputID\":\"");
    Serial.print("\"inputID\":\"");
    client.print(inputID);
    Serial.print(inputID);
    client.println("\"}");
    Serial.println("\"}");
  }
  else
  {
    Serial.println("Connection failed");
    Serial.println(responce);
  }
}

void sendMessage(){
  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  int responce = client.connect(server, 80);
  
  if (responce == 1) {
    Serial.println("connected to server");
    // Make a HTTP request:
    client.println("POST /19000/api/webhook/ HTTP/1.1");
    Serial.println("POST /19000/api/webhook/ HTTP/1.1");
    client.println("Authorization: Bearer 05faf693-d35c-4ccf-bb24-d5671f72cee4");
    Serial.println("Authorization: Bearer 05faf693-d35c-4ccf-bb24-d5671f72cee4");
    client.println("Host: 19000.rsproduction.se");
    Serial.println("Host: 19000.rsproduction.se");
    client.println("Content-Type: application/json");
    Serial.println("Content-Type: application/json");
    client.println("Content-Length: 53");
    Serial.println("Content-Length: 53");
    client.println();
    Serial.println();
    client.println("{\"webhookId\": \"35d4a78f-9a42-434a-b801-cb1696e33c82\"}");
    Serial.println("{\"webhookId\": \"35d4a78f-9a42-434a-b801-cb1696e33c82\"}");
  }
  else
  {
    Serial.println("Connection failed");
    Serial.println(responce);
  }
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
